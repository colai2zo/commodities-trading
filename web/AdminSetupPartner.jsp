<%-- 
    Document   : AdminSetupPartner
    Created on : Aug 7, 2016, 2:06:58 PM
    Author     : Joey
--%>

<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.ChannelPartner" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|Setup Partner</title>
         <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header">
            <h1>Channel Partner Setup</h1>
            <h2>Enter the following information to setup a channel partner:</h2>
        </div>
        <div id="central" align="center">
            <%ChannelPartner partners = new ChannelPartner(); //Instanciate a ChannelPartner object to access the database of Channel Partners.
            %>
            <form name="partnerInfoForm" action="AdminSetupPartner.jsp" method="post">
                <table border="0" cellpadding="15">
                    <tbody>
                        <tr>
                            <td>Name: </td>
                            <td colspan="2">
                                <input id="name" type="text" autocomplete="off" name="nameInput" value="" size="50" style="padding:10px 0px 10px 0px"/>
                            </td>
                        </tr>
                        <tr>
                            <td>ID Code: </td>
                            <td colspan="2">
                                <input id="IDCode" type="text" autocomplete="off" name="IDInput" value="" size="50" style="padding:10px 0px 10px 0px"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Username: </td>
                            <td colspan="2"><input id="user" type="text" autocomplete="off" name="userNameInput" value="" size="50" style="padding:10px 0px 10px 0px"/></td>
                        </tr>
                        <tr>
                            <td>Password: </td>
                            <td><input id="password" type="text" autocomplete="off" name="passwordInput" value="<%=partners.createPassword()%>" size="50" style="padding:10px 0px 10px 0px" onkeyup="checkPassword()"/></td>
                            <td id="checkX" style="color: green">✓</td>
                        </tr>
                    </tbody>
                </table>
                <input id="button" type="submit" value="Create Channel Partner" name="submitChannelInfoButton" onclick="formSubmission()"/>
            <script>
                /**
                 * This function checks to make sure the enetered password has at least six chatacters and at least one upper case letter.
                 * It updates the Table Data field with ID "checkX" to contain a green ✓ if the password is adequate or a red X if it is not.
                 * The function is run each time a key is released in the input with ID "password".
                 * 
                 */
                function checkPassword(){
                    var pass = document.getElementById('password').value;
                    if(pass.length >= 6 && (pass.includes('A') || pass.includes('B') || pass.includes('C') || pass.includes('D') || pass.includes('E') || pass.includes('F') || 
                            pass.includes('G') || pass.includes('H') || pass.includes('I') || pass.includes('J') || pass.includes('K') || pass.includes('L') || pass.includes('M') || 
                            pass.includes('N') || pass.includes('O') || pass.includes('P') || pass.includes('Q') || pass.includes('R') || pass.includes('S') || pass.includes('T') || 
                            pass.includes('U') || pass.includes('V') || pass.includes('W') || pass.includes('X') || pass.includes('Y') || pass.includes('Z'))){
                        document.getElementById('checkX').innerHTML = '✓';
                        document.getElementById('checkX').style.color = 'green';
                        document.getElementById('button').disabled = false;
                    }
                    else{
                        document.getElementById('checkX').innerHTML = 'X';
                        document.getElementById('checkX').style.color = 'red';
                        document.getElementById('button').disabled = true;
                    }
                }
            </script>    
            </form>
            <%            
            //Submit info to database once the button is clicked.
            if(request.getParameter("submitChannelInfoButton") != null){
                Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
                partners.insertChannelPartners(request.getParameter("IDInput"), request.getParameter("nameInput"), request.getParameter("userNameInput"), request.getParameter("passwordInput"), currentTimestamp);
            }
        %>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminPartnerOptions.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  //Close connection to the database.
        partners.closeConnection(); %>
    <script>        
    /**
     * The purpose of this function is to alert the user of any fields that they still need to fill in in order to submit the form.
     * It also alerts the user of successful submissions.
     * It is run each time the submit button is cliecked and the form is submitted.
     */    
    function formSubmission(){
            if(document.getElementById("name").value === "" || document.getElementById("user").value === "" || document.getElementById("pass").value === "")
            {
                window.alert("Please fill in all text fields to create partner.");
                document.getElementById("button").type = "";
                document.location.href = "AdminSetupPartner.jsp";
            }
            else{
                window.alert("The channel partner has been added to the database.");
                document.getElementById("button").type = "submit";
                document.location.href = "AdminPartnerOptions.jsp";
            }
        }
    </script>
</html>