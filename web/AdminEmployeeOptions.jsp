<%-- 
    Document   : AdminEmployeeOptions
    Created on : Aug 20, 2016, 2:18:49 PM
    Author     : Lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|Employee Options</title>
         <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body onload="">
	<div id="header">
	    <h1>Employee Options</h1>
	    <h2>Please select from one of the following options:</h2>
	</div>
	<div id="central" align="center">
 	    <form name="addEmployeeForm" action="AdminSetupEmployee.jsp" method="POST">
                <input id="button" type="submit" value="Add an Employee" name="addEmployeeButton"/>
            </form>

 	    <form name="deleteAndViewEmployeeForm" action="AdminViewEmployees.jsp" method="POST">
                <input id="button" type="submit" value="View/Edit/Delete an Employee" name="deletetAndViewButton"/>
            </form>
	</div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminEmployeePartnerOptions.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>

</html>