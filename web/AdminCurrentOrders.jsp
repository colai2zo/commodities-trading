 <%-- 
    Document   : AdminCurrentOrders
    Created on : Aug 7, 2016, 1:04:04 PM
    Author     : Lucas
--%>

<%@page import="java.sql.Timestamp"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        //Instantiate DateFormat and Calendar Objects to display date in desired format in header.
        DateFormat dateFormat = new SimpleDateFormat("EEEE MMMM d, yyyy");
        Calendar cal = Calendar.getInstance();
        
        //Instantiate Orders object and call methods to get the pending orders and todays orders.
        Orders orders = new Orders();
        ResultSet pendingOrderData = orders.getPendingOrders();
        ResultSet todaysOrderData = orders.getTodaysOrders();
        
        //Declare counters and required booleans.
        int count = 0;
        boolean approve = false, decline = false;
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="30">
        <title>Commodities Trading | Current Orders</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <script src="validityCheck.js"></script>
    </head>
    <body onload="calcExecutedStrike(), overwriteValues()">
        <div id="header" align="center">
            <h1>Orders: <%= dateFormat.format(cal.getTime())%> </h1>
        </div>
        
        
        <div id="central" align="center" style="margin-bottom: 5%">
            <table id="pendingOrders" class="oddRowColor" border="1" style="width:90%" cellpadding="18%" id="theTable">
                <thead>
                    <tr>
                        <th colspan="22">Pending Orders</th>
                    </tr>
                    <tr>
                        <th>Channel Partner</th>
                        <th>Farm ID</th>
                        <th>Farm Physical Contract ID</th>
                        <th>Order #</th>
                        <th>Underlying Commodity Name</th>
                        <th>Underlying Futures Code</th>
                        <th>Client Month</th>
                        <th>Expiration Date</th>
                        <th>Instrument</th>
                        <th>Product</th>
                        <th>Frequency</th>
                        <th>Order Volume Units</th>
                        <th>Unit Type</th>
                        <th>Order Volume Contracts</th>
                        <th>Product Current Price / Unit</th>
                        <th>Execution Product Total Cost</th>
                        <th>Strike Shift</th>
                        <th>Current Underlying Futures Price</th>
                        <th>Executed Hedge</th>
                        <th>Executed Strike</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                   <form name="<%= "submitChoice" + count%>" action="AdminCurrentOrders.jsp" method="POST">
                    <%
                        while(pendingOrderData.next()){
                            if(pendingOrderData.getString("status").equals("Pending")){
                                count++;
                    %>
                        <tr>
                            <td><%= pendingOrderData.getString("channel_partner_id")%></td>
                            <td><%= pendingOrderData.getString("farmer_id")%></td>
                            <td><%= pendingOrderData.getString("farmer_physical_contract_id")%></td>
                            <td><%= pendingOrderData.getString("order_id")%><input type="hidden" name="<%="orderID" + count%>" value="<%= pendingOrderData.getString("order_id")%>" /></td>
                            <td><%= pendingOrderData.getString("underlying_commodity_name")%></td>
                            <td><%= pendingOrderData.getString("underlying_futures_code")%></td>
                            <td><%= pendingOrderData.getString("client_month")%></td>
                            <td><%= pendingOrderData.getDate("expiration_date")%></td>
                            <td><%= pendingOrderData.getString("instrument")%></td>
                            <td><%= pendingOrderData.getString("product")%></td>
                            <td><%= pendingOrderData.getString("frequency")%></td>
                            <td><%= (int)pendingOrderData.getDouble("order_volume_quantity")%></td>
                            <td><%= pendingOrderData.getString("unit_type")%></td>
                            <td><%= pendingOrderData.getDouble("order_volume_contracts")%></td>
                            <td><%= pendingOrderData.getDouble("product_current_price_per_unit")%></td>
                            <td><%= pendingOrderData.getDouble("execution_product_total_cost")%></td>
                            <td><%= pendingOrderData.getInt("strike_shift") %><input type="hidden" class="ss" value="<%= pendingOrderData.getInt("strike_shift") %>"</td>
                            <td><%= pendingOrderData.getDouble("current_underlying_futures_price")%></td>
                            <td><input class="hedge" type="text" required="true" name="<%= "hedgeInput" + count%>" value="<%= pendingOrderData.getDouble("current_underlying_futures_price")%>" size="20" onkeyup="calcExecutedStrike();checkNum(this);" /></td>
                            <td><input type="text" autocomplete="off" readonly class="execStrike readonly" name="<%= "executedStrike" + count%>" value=""</td>
                            <td>
                                <label><input type="checkbox" name="<%="Approve" + count%>" value="OFF" /> Approve </label>
                                <label><input type="checkbox" name="<%="Decline" + count%>" value="OFF" /> Decline </label>
                            </td>
                        <% }} %>
                            <td>
                                <input id="statusButton" class="statusButton" type="submit" value="Enter" name="<%= "finalizeOrder"%>" style="width:130px; height: 40px" onclick="buttonClicked(<%= count%>)"/>
                            </td>
                        </tr>
                    </form>
                </tbody>
            </table>
                <% 
                    //Set counter to 0.
                    count = 0;
                    pendingOrderData.beforeFirst(); //Moves ResultSet Cursor back to beginning.
                    //Runs inner code if "Enter" Button has been clicked.
                    if(request.getParameter("finalizeOrder") != null){
                        //Loop through each of the pending orders. 
                        while(pendingOrderData.next()){
                            count++; //increment counter
                            Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime()); //Create timestamp object.
                            String newStatus = ""; //Create a string to house the new status of the order.
                            //Assess which checkbox was checked. If approve box cheked, approve == true, else, If decline box checked , decline == true.
                            approve = request.getParameter("Approve" + count) != null;
                            decline = request.getParameter("Decline" + count) != null;
                            //Runs inner code if the approve box was checked.
                            if(approve){
                                newStatus = "Original";
                                orders.updateStatus(pendingOrderData.getString("order_id"),newStatus); //Updates order status to original.
                                orders.updateTradeDate(pendingOrderData.getString("order_id")); //Updates the trade date if necessary.
                                //Call values for executed hedge and executed strike using parameter requests, and set these values in the order.
                                Double executedHedge = Double.parseDouble(request.getParameter("hedgeInput" + count));
                                Double executedStrike = Double.parseDouble(request.getParameter("executedStrike" + count));
                                orders.setExecutedHedgeAndStrike(pendingOrderData.getString("order_id"), executedHedge, executedStrike);
                                //Make a copy of the order with the status "Active"
                                orders.copyToActive(pendingOrderData.getString("order_id"), currentTimestamp);
                                
                                /*** UPDATE BLACK SCHOLES VALUES ***/
                                //Generate a single row resultset object named "active" containing the active order.
                                ResultSet active = orders.getOrder(pendingOrderData.getString("order_id").substring(0, pendingOrderData.getString("order_id").length() -1 ) + "A");
                                active.first();
                                Interpolation i = new Interpolation(active.getString("underlying_futures_code"), active.getDouble("executed_strike")); // Create an Interpolation object.
                                double interpolated_volatility = i.getRequestedVolatilityStrikeValue(); //Use interpolation to find requested volatility.
                                double newDelta = 0, newGamma = 0, newVega = 0, newTheta = 0; //Declare values to house new Black Scholes values
                                //Create an instance of PulledData in order to get today's Bloomberg Data.
                                PulledData pd = new PulledData(); 
                                ResultSet pdData = pd.getTodaysDataByCode(active.getString("underlying_futures_code"));
                                pdData.first();
                                double interest_rate = BlackScholes.calcCurrentInterestRate(pdData.getDouble("interest_30_day"), pdData.getDouble("interest_90_day"),
                                    pdData.getDouble("interest_180_day"), pdData.getDouble("interest_360_day"), active.getInt("original_number_of_days")); //Calculate interpolated interest rate.
                                
                                /**Perform BULLET calculations for orders with Bullet frequency.**/
                                if(active.getString("frequency").toLowerCase().equals("bullet")){
                                    BlackScholes bs = new BlackScholes( //Declare new instance of black scholes class using parameters from active order.
                                    active.getString("instrument"), active.getDouble("current_underlying_futures_price"), active.getDouble("executed_strike"),
                                    interest_rate, 0, interpolated_volatility, active.getInt("original_number_of_days"));
                                    
                                    //Use Black Scholes Object to get Black Scholes values for active order, and convert to either contracts or units.
                                    newDelta = bs.getDelta() * active.getDouble("order_volume_contracts");
                                    newGamma = bs.getGamma() * active.getDouble("order_volume_contracts");
                                    newVega = bs.getVega()   * active.getDouble("order_volume_quantity");
                                    newTheta = bs.getTheta() * active.getDouble("order_volume_quantity");
                                    
                                    //Adjust signs of values due to the instrument of the order.
                                    if(active.getString("instrument").toLowerCase().equals("put")){
                                        newDelta = Math.abs(newDelta);
                                        newGamma = -1 * Math.abs(newGamma);
                                        newVega = -1 * Math.abs(newVega);
                                        newTheta = Math.abs(newTheta);
                                    }
                                    else if(active.getString("instrument").toLowerCase().equals("call")){
                                        newDelta = -1* Math.abs(newDelta);
                                        newGamma = -1 * Math.abs(newGamma);
                                        newVega = -1 * Math.abs(newVega);
                                        newTheta = Math.abs(newTheta);
                                    }
                                }
                                /**Perform DAILY calculations for orders with Daily frequency.**/
                                else if(active.getString("frequency").toLowerCase().equals("daily")){ 
                                    BlackScholesDailyAccumulator bsda = new BlackScholesDailyAccumulator(active.getString("instrument"), //Declare new instance of black scholes daily accumulator class using parameters from active order.
                                    active.getDouble("current_underlying_futures_price"), active.getDouble("executed_strike"), active.getString("underlying_futures_code"),
                                    0, interpolated_volatility, active.getDate("trade_date").toString(), active.getDate("expiration_date").toString(), active.getString("action"),
                                    active.getDouble("order_volume_quantity"), active.getInt("original_number_of_days"));
                                   
                                    //Use Black Scholes Daily Accumulator object to get Black Scholes values for active order.
                                    newDelta = bsda.getDeltaTotal();
                                    newGamma = bsda.getGammaTotal();
                                    newVega = bsda.getVegaTotal();
                                    newTheta = bsda.getThetaTotal();
                                    String commodity = active.getString("underlying_commodity_name").toLowerCase(); //Access name of commodity
                                    //Divide Delta and Gamma Values by number of units in a contract for the commodity speciied.
                                    if(commodity.equals("euronext rapeseed") || commodity.equals("euronext wheat")){ 
                                        newDelta /= 50.0;
                                        newGamma /= 50.0;
                                    }else{
                                        newDelta /= 100.0;
                                        newGamma /= 100.0;
                                    }
                                }
                                
                                //Update the black scholes values for both the original and active orders in the Database.
                                orders.updateBlackScholes(active.getString("order_id"), newDelta, newGamma, newTheta, newVega);
                                orders.updateBlackScholes(pendingOrderData.getString("order_id"), newDelta, newGamma, newTheta, newVega);
                                response.setHeader("Refresh", "0; URL=AdminCurrentOrders.jsp"); // Refresh the page.
                                //Update the order ID based upon the new status of the order.
                                orders.updateID(pendingOrderData.getString("order_id"), orders.newOrderId(pendingOrderData.getString("order_id"), newStatus));
                            }
                            //Runs inner code if the decline box was checked.
                            if(decline){
                                newStatus = "Decline";
                                orders.updateStatus(pendingOrderData.getString("order_id"),newStatus); //Update the status of the order to declined.
                                response.setHeader("Refresh", "0; URL=AdminCurrentOrders.jsp"); // Refresh the page.
                                //Update the order ID based upon the new status of the order.
                                orders.updateID(pendingOrderData.getString("order_id"), orders.newOrderId(pendingOrderData.getString("order_id"), newStatus));
                            }
                            response.setHeader("Refresh", "0; URL=AdminCurrentOrders.jsp"); // Refresh the page, even if o orders were approved or declined.
                        }
                    }         
                    %>
            <br><br>
            <table class="oddRowColor" id="acceptedOrders" border="1" style="width:90%" cellpadding="18%">
                <thead>
                    <tr>
                        <th colspan="22">Accepted Orders</th>
                    </tr>
                    <tr>
                        <th>Channel Partner</th>
                        <th>Hedge or Client Trade</th>
                        <th>Farm ID</th>
                        <th>Farm Physical Contract ID</th>
                        <th>Order #</th>
                        <th>Underlying Commodity Name</th>
                        <th>Underlying Futures Code</th>
                        <th>Client Month</th>
                        <th>Expiration Date</th>
                        <th>Instrument</th>
                        <th>Product</th>
                        <th>Frequency</th>
                        <th>Order Volume Units</th>
                        <th>Unit Type</th>
                        <th>Order Volume Contracts</th>
                        <th>Product Executed Price / Unit</th>
                        <th>Execution Product Total Cost</th>
                        <th>Strike Shift</th>
                        <th>Current Underlying Futures Price</th>
                        <th>Executed Hedge</th>
                        <th>Executed Strike</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <%  //Loop through original orders for Approved order table.
                        while(todaysOrderData.next()){
                            if(todaysOrderData.getString("status").equals("Original")){
                    %>
                    <tr>
                        <td><%= todaysOrderData.getString("channel_partner_id")%></td>
                        <td><%= todaysOrderData.getString("transaction_type")%></td>
                        <td><%= todaysOrderData.getString("farmer_id")%></td>
                        <td><%= todaysOrderData.getString("farmer_physical_contract_id")%></td>
                        <td><%= todaysOrderData.getString("order_id")%></td>
                        <td><%= todaysOrderData.getString("underlying_commodity_name")%></td>
                        <td><%= todaysOrderData.getString("underlying_futures_code")%></td>
                        <td><%= todaysOrderData.getString("client_month")%></td>
                        <td><%= todaysOrderData.getDate("expiration_date")%></td>
                        <td><%= todaysOrderData.getString("instrument")%></td>
                        <td><%= todaysOrderData.getString("product")%></td>
                        <td><%= todaysOrderData.getString("frequency")%></td>
                        <td><%= (int)todaysOrderData.getDouble("order_volume_quantity")%></td>
                        <td><%= todaysOrderData.getString("unit_type")%></td>
                        <td><%= todaysOrderData.getDouble("order_volume_contracts")%></td>
                        <td><%= todaysOrderData.getDouble("product_current_price_per_unit")%></td>
                        <td><%= todaysOrderData.getDouble("execution_product_total_cost")%></td>
                        <td><%= todaysOrderData.getInt("strike_shift") %></td>
                        <td><%= todaysOrderData.getDouble("current_underlying_futures_price")%></td>
                        <td><%= todaysOrderData.getDouble("executed_hedge")%></td>
                        <td><%= todaysOrderData.getDouble("executed_strike")%></td>
                        <td><%= todaysOrderData.getString("status")%></td>
                    </tr>
                    <% }} %>
                </tbody>
            </table>
            <br><br>
            <table id="declinedOrders" class="oddRowColor" border="1" style="width:90%" cellpadding="18%">
                <thead>
                    <tr>
                        <th colspan="21">Declined Orders</th>
                    </tr>
                    <tr>
                        <th>Channel Partner</th>
                        <th>Farm ID</th>
                        <th>Farm Physical Contract ID</th>
                        <th>Order #</th>
                        <th>Underlying Commodity Name</th>
                        <th>Underlying Futures Code</th>
                        <th>Client Month</th>
                        <th>Expiration Date</th>
                        <th>Instrument</th>
                        <th>Product</th>
                        <th>Frequency</th>
                        <th>Order Volume Units</th>
                        <th>Unit Type</th>
                        <th>Order Volume Contracts</th>
                        <th>Product Executed Price / Unit</th>
                        <th>Execution Product Total Cost</th>
                        <th>Strike Shift</th>
                        <th>Current Underlying Futures Price</th>
                        <th>Executed Strike</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <%  //Loop through declined orders for declined orders table.
                        todaysOrderData.beforeFirst();
                        while(todaysOrderData.next())
                            if(todaysOrderData.getString("status").equals("Decline")){
                    %>
                    <tr>
                        <td><%= todaysOrderData.getString("channel_partner_id")%></td>
                        <td><%= todaysOrderData.getString("farmer_id")%></td>
                        <td><%= todaysOrderData.getString("farmer_physical_contract_id")%></td>
                        <td><%= todaysOrderData.getString("order_id")%></td>
                        <td><%= todaysOrderData.getString("underlying_commodity_name")%></td>
                        <td><%= todaysOrderData.getString("underlying_futures_code")%></td>
                        <td><%= todaysOrderData.getString("client_month")%></td>
                        <td><%= todaysOrderData.getDate("expiration_date")%></td>
                        <td><%= todaysOrderData.getString("instrument")%></td>
                        <td><%= todaysOrderData.getString("product")%></td>
                        <td><%= todaysOrderData.getString("frequency")%></td>
                        <td><%= (int)todaysOrderData.getDouble("order_volume_quantity")%></td>
                        <td><%= todaysOrderData.getString("unit_type")%></td>
                        <td><%= todaysOrderData.getDouble("order_volume_contracts")%></td>
                        <td><%= todaysOrderData.getDouble("product_current_price_per_unit")%></td>
                        <td><%= todaysOrderData.getDouble("execution_product_total_cost")%></td>
                        <td><%= todaysOrderData.getInt("strike_shift") %></td>
                        <td><%= todaysOrderData.getDouble("current_underlying_futures_price")%></td>
                        <td><%= todaysOrderData.getDouble("executed_strike")%></td>
                        <td><%= todaysOrderData.getString("status")%></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>

        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <script>
            /**
             * RUN onchange of Executed Hedge input
             * CHANGES value of Executed Strike to that of the executed hedge plus the strike shift.
             */
            function calcExecutedStrike(){
                var strikeShifts = document.getElementsByClassName("ss");
                var executedHedges = document.getElementsByClassName("hedge");
                var executedStrikes = document.getElementsByClassName("execStrike");
                for(var i = 0; i < strikeShifts.length; i++){
                    executedStrikes[i].value = parseFloat(executedHedges[i].value) + parseFloat(strikeShifts[i].value);
                }
            }
            /**
             * Changes any undefined value to display "-"
             */
            function overwriteValues(){
                var table = document.getElementById("acceptedOrders");
                for (var i = 0; i<table.rows.length; i++)
                    for(var j = 0; j<table.rows[i].cells.length; j++){
                        var innerText = table.rows[i].cells[j].innerHTML;
                        var visibleText = innerText;
                        if( visibleText === "-999.0" 
                                || visibleText === "-999.00" 
                                || visibleText === "-999" 
                                || visibleText === "null" 
                                || visibleText === "N/A")
                            table.rows[i].cells[j].innerHTML = "-";
                    }
            }
        </script>
    </body>
    <% orders.closeConnection(); %>
</html>