<%-- 
    Document   : AdminManageExpirations
    Created on : Sep 5, 2016, 2:06:37 PM
    Author     : LucasCarey
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*"%>
<%@page import="java.text.NumberFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
    <script src="http://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <title>Commodities Trading | Manage Expirations</title>
    </head>
    <body onload="overwriteValues()">
        <div id="header" align="center">
            <h1>Manage Expirations</h1>
        </div>
        <div id ="central" align="center">
            <form name="selectRangeForm" action="AdminManageExpirations.jsp" method="POST">
            <select id="selectView" name="selectView" onchange="this.form.submit()">
                <option value="default">Select a Date Range</option>
                <option value="today">View Today's Expirations</option>
                <option value="next5Days">View Next 5 Days Expirations</option>
            </select>
            </form>
            <select id="selectPartner" class="changers" name="selectPartner">
                <option value="default">Select a Channel Partner</option>
                <%
                    NumberFormat formatter = NumberFormat.getInstance();
                    formatter.setMaximumFractionDigits(2);
                    formatter.setMinimumFractionDigits(2);
                    ChannelPartner cp = new ChannelPartner();
                    ResultSet partner = cp.getChannelPartners();
                    //adds all the partners to the dropdown menu
                    while(partner.next()){ 
                %>
                    <option value="<%=partner.getString("channel_partner_id") %>"><%=partner.getString("name") %></option>
                <% } %>
            </select>
            <select id="selectTransaction" class="changers" name="selectTransaction">
                <option value="default">Select a Trade Type</option>
                <option value="Hedge">Hedge</option>
                <option value="Trade">Client Trade</option>
            </select>
            <table border="1" id="expirationsTable" style="width: 90%" >
                <thead>
                    <tr>
                        <th>Channel Partner</th>
                        <th>Expiration Date</th>
                        <th>Order Volume Units</th>
                        <th>Executed Strike Price</th>
                        <th>Underlying Futures Code</th>
                        <th>Current Underlying Futures Price</th>
                        <th>Delta</th>
                        <th>Gamma</th>
                        <th>Vega</th>
                        <th>Theta</th>
                        <th>Order #</th>
                    </tr>
                </thead>
                <tbody>
                    <% 
                        boolean expiringTodayIsEmpty = false, expiringSoonIsEmpty = false;
                        int count = 0;
                        Orders orders = new Orders();
                        ResultSet manageExpirationsData = null;
                        //If selectView is null, get Today's expirations
                        if(request.getParameter("selectView") == null){
                            manageExpirationsData = orders.getExpiredOrders();
                            try{
                                manageExpirationsData.first();
                                manageExpirationsData.getString("order_id");
                                manageExpirationsData.beforeFirst();
                            }catch(java.sql.SQLException e){
                                //e.printStackTrace();
                                expiringTodayIsEmpty = true;
                            }catch(java.lang.NullPointerException f){
                                //f.printStackTrace();
                                expiringTodayIsEmpty = true;
                            }
                            //if 'today' was selected, get Today's expirations
                        }else if(request.getParameter("selectView").equals("today")){
                            manageExpirationsData = orders.getExpiredOrders();
                            try{
                                manageExpirationsData.first();
                                manageExpirationsData.getString("order_id");
                                manageExpirationsData.beforeFirst();
                            }catch(java.sql.SQLException e){
                                //e.printStackTrace();
                                expiringTodayIsEmpty = true;
                            }catch(java.lang.NullPointerException f){
                                //f.printStackTrace();
                                expiringTodayIsEmpty = true;
                            }
                            //if next5days was selected, get the expirations for the next five business days
                        }else if(request.getParameter("selectView").equals("next5Days")){
                            manageExpirationsData = orders.getOrdersExpiringSoon();
                            try{
                                manageExpirationsData.first();
                                manageExpirationsData.getString("order_id");
                                manageExpirationsData.beforeFirst();
                            }catch(java.sql.SQLException e){
                                //e.printStackTrace();
                                expiringSoonIsEmpty = true;
                            }catch(java.lang.NullPointerException f){
                                //f.printStackTrace();
                                expiringSoonIsEmpty = true;
                            }
                        }
                        if(expiringTodayIsEmpty || expiringSoonIsEmpty){
                    %>
                    <tr class="tableRow"><td style="text-align:center" colspan="20"><strong>No Orders</strong></td></tr>
                    <% }else{
                            while(manageExpirationsData.next()){
                            count++;
                    %>
                    <tr class="tableRow">
                        <td><%= manageExpirationsData.getString("channel_partner_id") %></td>
                        <td><%= manageExpirationsData.getDate("expiration_date") %></td>
                        <td><%= formatter.format(manageExpirationsData.getDouble("order_volume_contracts")) %></td>
                        <td><%= formatter.format(manageExpirationsData.getDouble("executed_strike")) %></td>
                        <td><%= manageExpirationsData.getString("underlying_futures_code") %></td>
                        <td><%= formatter.format(manageExpirationsData.getDouble("current_underlying_futures_price")) %></td>
                        <td><%= formatter.format(manageExpirationsData.getDouble("delta")) %></td>
                        <td><%= formatter.format(manageExpirationsData.getDouble("gamma")) %></td>
                        <td><%= formatter.format(manageExpirationsData.getDouble("vega")) %></td>
                        <td><%= formatter.format(manageExpirationsData.getDouble("theta")) %></td>
                        <td><%= manageExpirationsData.getString("order_id") %>
                        <input type="hidden" id="<%="channelPartnerId" + count%>" value="<%= manageExpirationsData.getString("channel_partner_id")%>"/> 
                        <input type="hidden" id="<%="expirationDate" + count%>" value="<%= manageExpirationsData.getString("expiration_date")%>"/> 
                        <input type="hidden" id="<%="transactionType" + count%>" value="<%= manageExpirationsData.getString("transaction_type")%>"/> 
                        <input type="hidden" id="<%="status" + count%>" value="<%= manageExpirationsData.getString("status")%>"/> 
                        </td>
                        
                    </tr>
                    <% }} %>
                </tbody>
            </table>
            
            
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminReports.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  cp.closeConnection();
        orders.closeConnection(); %>
    <script>
        $(document).ready(function() {
            //this changes the visibility of the rows as appropriate based on the selections made in the menus
            $('.changers').change(function() {
                var selectedPartner = $("#selectPartner").val();
                var selectedTransaction = $("#selectTransaction").val();
                var tableRows = $('.tableRow');
                if(selectedPartner === 'default' && selectedTransaction === 'default'){
                    for(var i = 0; i < tableRows.length; i++){
                        tableRows[i].style.display = 'table-row';
                    } 
                }else if(selectedPartner === 'default' && selectedTransaction !== 'default'){
                    for(var i = 0; i < tableRows.length; i++){
                        if($('#transactionType' + (i+1)).val().toString().includes(selectedTransaction)){
                            tableRows[i].style.display = 'table-row';
                        }else{
                           tableRows[i].style.display = 'none'; 
                        }  
                    }
                }else if(selectedPartner !== 'default' && selectedTransaction === 'default'){
                    for(var i = 0; i < tableRows.length; i++){
                         if($('#channelPartnerId' + (i+1)).val() === selectedPartner){
                             tableRows[i].style.display = 'table-row';
                         }else{
                            tableRows[i].style.display = 'none'; 
                         }  
                    } 
                }else{
                    for(var i = 0; i < tableRows.length; i++){
                         if($('#channelPartnerId' + (i+1)).val() === selectedPartner 
                                && $('#transactionType' + (i+1)).val().toString().includes(selectedTransaction)){
                             tableRows[i].style.display = 'table-row';
                         }else{
                            tableRows[i].style.display = 'none'; 
                         }  
                    } 
                }
            });
        });
        function overwriteValues(){
                var table = document.getElementById("expirationsTable");
                for (var i = 0; i<table.rows.length; i++)
                    for(var j = 0; j<table.rows[i].cells.length; j++){
                        var visibleText = table.rows[i].cells[j].innerHTML;
                        if( visibleText === "-999.0" 
                                || visibleText === "-999.00" 
                                || visibleText === "-999" 
                                || visibleText === "null" 
                                || visibleText === "N/A"){
                            table.rows[i].cells[j].innerHTML = "-";
                                }
                    }
            }
    </script>
</html>