<%-- 
    Document   : editDatabase
    Created on : Jan 31, 2017, 4:46:20 PM
    Author     : LucasCarey
--%>

<%@page import="java.sql.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Database</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h1>Edit Database</h1>
        <h5>Type the SQL code to directly modify the database in the Ubuntu server. WARNING: Executing SQL code this way is irreversible and should only be done if absolutely necessary.</h5>
        <div id="central">
            <form action="editDatabase.jsp" method="POST" align="center">
                <textarea name="sqlTextBox" rows="10" cols="150" align="center"></textarea>
                <input type="submit" value="Submit SQL Code" name="submitDataButton" />
            </form>
        </div>
    </body>
    <%
        if(request.getParameter("submitDataButton") != null){
            String sqlCode = request.getParameter("sqlTextBox");
            Orders orders = new Orders();
            orders.modifyDatabase(sqlCode);
        }
    %>
</html>
