<%-- 
    Document   : PartnerAccountsDue.jsp
    Created on : Aug 7, 2016, 4:15:42 PM
    Author     : Dutchess Developers
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page import="java.text.NumberFormat" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
        <title>Commodities Trading | Accounts Due</title>
    </head>
    <body onload="">
        <%
            boolean isEmpty = false, moveOn = false, nextFarm = false;
            int count = 0;
            NumberFormat formatter = NumberFormat.getInstance();
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);
            Farmers farm = new Farmers();
            Orders orders = new Orders();
            ChannelPartner cp = new ChannelPartner();
            ResultSet orderData = null;
            String username = "";
            String pID = "";
            //gets the username of the partner using the page
            try{
                username = session.getAttribute("username").toString();
                pID = cp.getIDByUsername(username);
            }catch(java.lang.NullPointerException e){
                e.printStackTrace();
                response.sendRedirect("login.jsp");
            }
            //calls the proper method based on the selection in the dropdown
            if(request.getParameter("farmSelect") != null && !request.getParameter("farmSelect").equals("all")){
                orderData = orders.getOrdersDuePartner(pID, request.getParameter("farmSelect"));
            }else{
                orderData = orders.getOrdersDuePartner(pID);
            }
            //checkes if the result set is empty
            try{
                orderData.first();
                orderData.getString("order_id");
                orderData.beforeFirst();
            }catch(java.sql.SQLException e){
                e.printStackTrace();
                isEmpty = true;
            }catch(java.lang.NullPointerException f){
                f.printStackTrace();
                isEmpty = true;
            }
        %>
        <div id="header" align="center" style="padding-bottom: 20px">
            <h1>Accounts Due</h1>
            <form name="farmChooser" action="PartnerAccountsDue.jsp" method="POST">
                <table>
                    <tr>
                        <td>
                            <select id="farmSelect" name="farmSelect" onchange="this.form.submit()" >
                                <option value="default">Select a Farm</option>
                                <option value="all">View All</option>
                                <% 
                                    ResultSet farmers = farm.getFarmersForPartner(pID);
                                    //adds all the farms to the drop down list
                                    while (farmers.next()){
                                %>
                                <option value="<%=farmers.getString("farmer_id")%>"><%=farmers.getString("name")%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        
        <%
            if(isEmpty){
        %> 
        <h3 style="text-align: center">No Accounts Due</h3>
        <%
            }else{
                String currentFarm = "", currentFarmName = "", currentFarmContractID = "";
                int currentRow = 0;
                double totalDueForOrder = 0, totalDueForFarmer = 0;
                ArrayList farms = new ArrayList();
                //adds all of the farms in the orderData to an ArrayList
                while(orderData.next()){
                    if(!orderData.getString("farmer_id").equals(currentFarm) && orderData.getBoolean("isReconciled") == false){
                        currentFarm = orderData.getString("farmer_id");
                        farms.add(currentFarm);
                    }
                }
                //creates a table for each farm in the ArrayList
                for(int i = 0; i < farms.size(); i++){
                    orderData.beforeFirst();
                    totalDueForFarmer = 0;
                    currentFarmName = farm.getNameById(farms.get(i).toString());
        %>
        <div>
            <table id="central" class="highlightRows" style="text-align: center; margin-bottom: 5%">
                <thead>
                    <tr>
                        <th><%= currentFarmName %></th>
                    </tr>
                    <tr style="background-color:rgba(50,200,200,.5)">
                        <th>Farm ID</th>
                        <th>Farm Physical Contract ID</th>
                        <th>Transaction Date</th>
                        <th>Order #</th>
                        <th>Order Volume Units</th>
                        <th>Underlying Futures Code</th>
                        <th>Client Month</th>
                        <th>Product Type</th>
                        <th>Expiration Date</th>
                        <th>Execution Product Total Cost</th>
                        <th>Unwind Values</th>
                        <th>Finalized Value</th>                       
                    </tr>
                </thead>
                <%
                    //adds the appropriate orders to the table for each farm
                    while(orderData.next()){
                        currentRow = orderData.getRow();
                        currentFarmContractID = orderData.getString("farmer_physical_contract_id");
                        if(orderData.getString("farmer_id").equals(farms.get(i).toString())){
                            count++;
                            totalDueForOrder += orderData.getDouble("execution_product_total_cost");
                %>
                <tbody>
                        <tr class="tableRow">
                            <td><input type="hidden" name="<%= ("farmer_id" + count)%>" value="<%= orderData.getString("farmer_id")%>" /> <%= orderData.getString("farmer_id") %></td>
                            <td><input type="hidden" name="<%= ("farmer_physical_contract_id" + count)%>" value="<%= orderData.getString("farmer_physical_contract_id")%>" /> <%= orderData.getString("farmer_physical_contract_id") %></td>
                            <td><input type="hidden" name="<%= ("trade_date" + count)%>" value="<%= orderData.getDate("trade_date")%>" /> <%= orderData.getDate("trade_date") %></td>
                            <td><input type="hidden" name="<%= ("order_id" + count)%>" value="<%= orderData.getString("order_id")%>" /> <%= orderData.getString("order_id")%></td>                                    
                            <td><input type="hidden" name="<%= ("order_volume_quantity" + count)%>" value="<%= orderData.getDouble("order_volume_quantity")%>" /> <%= orderData.getDouble("order_volume_quantity")%></td>
                            <td><input type="hidden" name="<%= ("underlying_futures_code" + count)%>" value="<%= orderData.getString("underlying_futures_code")%>" /> <%= orderData.getString("underlying_futures_code")%></td>
                            <td><input type="hidden" name="<%= ("client_month" + count)%>" value="<%= orderData.getString("client_month")%>" /> <%= orderData.getString("client_month")%></td>
                            <% if(orderData.getString("product").equals("European") && orderData.getString("frequency").equals("Daily")){ %>
                            <td><input type="hidden" name="<%= ("product" + count)%>" value="Enhanced Market Average" />Enhanced Market Average</td>
                            <% }else if(orderData.getString("product").equals("European") && orderData.getString("frequency").equals("Bullet")){ %>
                            <td><input type="hidden" name="<%= ("product" + count)%>" value="Freedom Reprice" />Freedom Reprice</td>
                            <% }else{ %>
                            <td>-</td>
                            <% } %>
                            <td><input type="hidden" name="<%= ("expiration_date" + count)%>" value="<%= orderData.getDate("expiration_date")%>" /> <%= orderData.getDate("expiration_date") %></td>
                            <%if(orderData.getString("status").equals("Original")){ %>
                            <td><input type="hidden" name="<%= ("execution_product_total_cost" + count)%>" value="<%= formatter.format(orderData.getDouble("execution_product_total_cost"))%>" /> <%= formatter.format((orderData.getDouble("execution_product_total_cost")))%></td>
                            <td>-</td>
                            <td>-</td>
                            <%}else if(orderData.getString("status").equals("Unwind")){ %>
                            <td>-</td>
                            <td><input type="hidden" name="<%= ("execution_product_total_cost" + count)%>" value="<%= formatter.format(orderData.getDouble("execution_product_total_cost"))%>" /> (<%= formatter.format((orderData.getDouble("execution_product_total_cost")))%>)</td>
                            <td>-</td>
                            <%}else if(orderData.getString("status").equals("Finalized")){ %>
                            <td>-</td>
                            <td>-</td>
                            <td><input type="hidden" name="<%= ("execution_product_total_cost" + count)%>" value="<%= formatter.format(orderData.getDouble("execution_product_total_cost"))%>" /> (<%= formatter.format((orderData.getDouble("execution_product_total_cost")))%>)</td>
                            <% } %>
                            <input type="hidden" id="<%="channelPartnerID" + count%>" value="<%=orderData.getString("channel_partner_id")%>"
                        </tr>
                        <%  
                            orderData.next();
                            //checks if the next order should be grouped with the current one
                            if(!orderData.isAfterLast() 
                                    && (!orderData.getString("farmer_physical_contract_id").equals(currentFarmContractID) 
                                    || !(orderData.getString("farmer_id").equals(farms.get(i).toString()))))
                                moveOn = true;
                            orderData.absolute(currentRow);
                            
                            //if it is time to move on to the next contract ID this will add the 'Total' row for that order
                            if((orderData.getString("farmer_id").equals(farms.get(i).toString()) && moveOn) || orderData.isLast()){
                                count++;
                                totalDueForFarmer += totalDueForOrder;
                        %>
                        <tr class="accountsDueTotalRow">
                            <td colspan="9" style="text-align:right"><strong>Client Owes For Order #<%=orderData.getString("order_id").substring(0, orderData.getString("order_id").length() - 2)%>:</strong></td>
                            <% if(totalDueForOrder >= 0){ %>
                            <td colspan="2"><%= formatter.format(Math.abs(totalDueForOrder))%></td>
                            <% }else if(totalDueForOrder < 0){ %>
                            <td colspan="2">(<%= formatter.format((totalDueForOrder))%>)</td>
                            <% }if (orderData.getString("order_id").indexOf("F") > 0 || orderData.getString("order_id").indexOf("E") > 0){ %>
                            <td>Finalized</td>
                            <% } %>
                        </tr>
                        <tr>
                            <td colspan="40"><br></td>
                        </tr>
                    <%
                        totalDueForOrder = 0;
                            moveOn = false;
                        }
                        orderData.next();
                        //checks if there is another farm to come after the current one
                        if(!orderData.isAfterLast() && !orderData.getString("farmer_id").equals(farms.get(i).toString())){ 
                            nextFarm = true;
                        }   
                        orderData.absolute(currentRow);
                        
                        //adds a 'Total' row for the entire farm
                        if((nextFarm) || orderData.isLast()){  
                    %>
                    <tr>
                        <td colspan="9" style="text-align:right"><strong><%=currentFarmName%> Owes:</strong></td>
                        <% if(totalDueForFarmer >= 0){ %>
                        <td colspan="3"><%= formatter.format(Math.abs(totalDueForFarmer))%></td>
                        <% }else if(totalDueForFarmer < 0){ %>
                        <td colspan="3">(<%= formatter.format(Math.abs(totalDueForFarmer))%>)</td>
                        <% } %>
                    </tr>
                    <%  nextFarm = false;
                        }}}}}
                    %>
                    <input type="hidden" name="count" value="<%= count%>" />
                </tbody>
            </table>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="PartnerReports.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  orders.closeConnection();
        farm.closeConnection();
        cp.closeConnection(); %>
    <script>
        function overwriteValues(){
                var table = document.getElementById("central");
                for (var i = 0; i<table.rows.length; i++)
                    for(var j = 0; j<table.rows[i].cells.length; j++){
                        var innerText = table.rows[i].cells[j].innerHTML;
                        var visibleText = innerText.substring(innerText.indexOf(">") + 2);
                        if( visibleText === "-999.0" 
                                || visibleText === "-999.00" 
                                || visibleText === "-999" 
                                || visibleText === "null" 
                                || visibleText === "N/A")
                            table.rows[i].cells[j].innerHTML = "-";
                    }
            }
    </script>
</html>