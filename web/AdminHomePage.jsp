<%-- 
    Document   : AdminHomePage
    Created on : Aug 7, 2016, 9:45:35 AM
    Author     : Joey Colaizzo
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
    <head>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|Admin</title>
    </head>
    <%  //Get Username of Logged in User from session. If session is inactive, redirect to login screen.
        String username = "";
        try{
            username = session.getAttribute("username").toString();
        }catch(java.lang.NullPointerException e){
            response.sendRedirect("login.jsp");
        }
        
        //Assess whether the user who is logged in has admin rights, book management rights
        Boolean admin = false, book = false, reporting = false, pricing = false;
        Employee employee = new Employee();
        ResultSet empData = employee.getEmployee();
        while(empData.next()){
            if(empData.getString("username").equals(username)){
                admin = empData.getBoolean("admin");
                book = empData.getBoolean("book_management");
                reporting = empData.getBoolean("reporting");
                pricing = empData.getBoolean("pricing");
            }
        }
    %>
    <body>
        <div style="float:right">
            <form name="logoutForm" id="logoutForm" action="AdminHomePage.jsp" method="POST">
                <input style="background-color:transparent" type="button" value="Logout" name="logout" id="logout" onclick="confirmLogout()" />
            </form>
        </div>
        <div id="header">
            <h1 style="text-align: center">Administrator Page For <%= employee.getNameFromUsername(username) %></h1>
        </div>
        <div id="central" align="center">
            <h2 style="text-align: center">Choose from the following
            administrator options:</h2>
            
            <% if(admin || pricing){ %>
            <form name="updateVolatility" action="AdminUpdateMarketData.jsp" method="POST">
                <input id ="button" type="submit" value="Update Market Data" name="updateMarketData" />
            </form>
            <% }if(admin || pricing){ %>
            <form name="updateDailyPricingForm" action="AdminUpdatePricing.jsp" method="POST">
                <input id="button" type="submit" value="Update Pricing" name="updatePricing" />
            </form>
            <% }if(admin || book){ %>
            <form name="viewOrders" action="AdminCurrentOrders.jsp" method="POST">
                <input id="button" type="submit" value="View Today's Orders" name="currentOrders" />
            </form>
            <% }if(admin || book){ %>
            <form name="tradeEntry" action="AdminTradeEntry.jsp" method="POST">
                <input id="button" type="submit" value="Enter Trade" name="enterTrade" />
            </form>
            <% }if(admin || book){ %>
            <form name="completeBook" action="AdminManageBook.jsp" method="POST">
                <input id="button" type="submit" value="Manage Book" name="manageBook" />
            </form>
            <% }if(admin || reporting){ %>
            <form name="reportingForm" action="AdminReports.jsp" method="POST">
                <input id='button' type="submit" value="Reports" name="reporting" />
            </form>
            <% }if(admin){ %>
            <form name="employee/CPForm" action="AdminEmployeePartnerOptions.jsp" method="POST">
                <input id='button' type="submit" value="Create Employee / Channel Partner" name="employeePartnerOptions" />
            </form>
            <% } %>
        </div>
        <script>
            function confirmLogout(){
                var c = confirm("Are you sure you want to logout?");
                if(c === true){
                    document.getElementById("logout").type = "hidden";
                    document.getElementById("logoutForm").submit();
                }
            }
        </script>
        <%
            if(request.getParameter("logout") != null){
                session.setAttribute("username", "");
                response.sendRedirect("login.jsp");
            }
            employee.closeConnection();
        %>
    </body>   
</html>