<%-- 
    Document   : AdminSubmitPricing
    Created on : Aug 31, 2016, 9:49:42 PM
    Author     : Joey
--%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.ArrayList"%>"
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <%  /** SUBMIT PRICING
        *   This page is run after the administrator hits submit on AdminUpdatePricing.jsp
        *   This page takes all of the submitted parameters and enters them into the pricing databse.
        */
        //Assignment of basic variables.
        Pricing pricing =  new Pricing(); //Declare instance of Pricing
        MarkupsVolatilities markupsVolatilities = new MarkupsVolatilities(); //Declare instance of markups volatilities class
        Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime()); //Create a timestamp object.
        String product = "European"; //Product is always European in this version of the platform.
        String frequency = ""; //Pricing frequency (bullet or daily)
        String commodity = ""; //Commodity being priced (Euronext Wheat, Euronext Rapeseed, Soybean Meal)
        
        //Assign Bullet or Daily to Frequency based upon selected frequency product.
        if(request.getParameter("chooseProduct").equals("Freedom Reprice")){
            frequency = "Bullet";
        }else{
            frequency = "Daily";
        }
        
        //Assignment of commodity (default is Euronext Rapeseed)
        if(!request.getParameter("ChooseCommodity").equals("null")){
            commodity = request.getParameter("ChooseCommodity");
        }else{
            commodity = "Euronext Rapeseed";
        }
        
        //Get the Count (total number of rows of pricing, and limit for for loop.)
        int count = Integer.parseInt(request.getParameter("Count"));
        /**
         * MAIN LOOP: Insert a row into the pricing database for every row in the Update Pricing page.
         */
        for(int i = 0; i< count; i++)
        {   
            int strikeShiftPositive = Integer.parseInt(request.getParameter("strike_shift_positive")); //Obtain positive strike shift.
            int strikeShiftNegative = Integer.parseInt(request.getParameter("strike_shift_negative")); //Obtain negative strike shift.
            //Determine which strike shift to use based upon where we are in the count (first table is 0, 2nd is positive, 3rd is negative.)
            int strike_shift;
            if(i < count/3){
                strike_shift = 0;
            }else if(i < count * 2/3){
                strike_shift = strikeShiftPositive;
            }else{
                strike_shift = -1 * strikeShiftNegative; //Value returned is alway positive
            }
            
            //Get expiration date and convert to date object. 
            String expiryDate = request.getParameter("expiration" + i);
            Date expiration = Date.valueOf(expiryDate);
            
            /** INSERT THE PARAMETERS INTO THE PRICING DATABASE**/
              pricing.insertPricing(
                request.getParameter("month" + i),
                request.getParameter("futuresCode" + i), 
                expiration,
                Double.parseDouble(request.getParameter("futuresPrice" + i)),
                Double.parseDouble(request.getParameter("strike" + i)),
                0, //current_implied_volatility
                Double.parseDouble(request.getParameter("dailyTraderVolatility" + i)), 
                Double.parseDouble(request.getParameter("vSpreadInput" + i)), 
                Double.parseDouble(request.getParameter("price" + i)),
                Double.parseDouble(request.getParameter("markupInput" + i)),
                Double.parseDouble(request.getParameter("finalPrice" + i)),
                product,
                frequency,
                commodity,
                strike_shift,
                currentTimestamp);
              
              /** INSERT THE MARKUP AND VOLATILITY SPREAD INTO THE MARKUP/VOLATILITY DATABASE **/
              markupsVolatilities.insertMarkupsAndVolatilities(
                      frequency,
                      commodity,
                      Double.parseDouble(request.getParameter("markupInput" + i)),
                      Double.parseDouble(request.getParameter("vSpreadInput" + i)),
                      i);
        }

        //Redirect back to AdminUpdatePricing.jsp page.
        response.sendRedirect("AdminUpdatePricing.jsp"); 

    %>
    </body>
    <%
        //Close connection with the database.
        pricing.closeConnection();
    %>
</html>
