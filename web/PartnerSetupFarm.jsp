<%-- 
    Document   : PartnerSetupFarm
    Created on : Aug 7, 2016, 2:06:58 PM
    Author     : Joey
--%>

<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|Add Farm</title>
         <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header">
            <h1>Farm Setup</h1>
            <h2>Enter the following information to add a farm to the database:</h2>
        </div>
        <%
            //Create instances of Farmer and ChannelPartner to access the database.
            Farmers farmers = new Farmers();
            ChannelPartner channelPartner = new ChannelPartner(); 
        %>
        <div id="central" align="center">
            <form name="partnerInfoForm" method="post" action="PartnerSetupFarm.jsp">
                <table border="0" cellpadding="15">
                    <tbody>
                        <tr>
                            <td>Farm Name: </td>
                            <td>
                                <input required id="farmName" type="text" autocomplete="off" name="farmNameInput" value="" size="50" style="padding:10px 0px 10px 0px"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input id="button" type="submit" value="Add Farm" name="submitFarmerInfoButton" />
            </form>
            <%
                //Code is run if the button is clicked.
                if(request.getParameter("submitFarmerInfoButton") != null){
                    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime()); //Generate timestamp
                    //Get username of currently logged in channel partner.
                    String username = "";
                    try{
                        username = session.getAttribute("username").toString();
                    }catch(java.lang.NullPointerException e){
                        response.sendRedirect("login.jsp");
                    }
                    //Convert username to ID using method.
                    String id = channelPartner.getIDByUsername(username);
                    //Insert farm into farmers database.
                    farmers.insertFarmers(request.getParameter("farmNameInput"), farmers.generateFarmerID(id), id, currentTimestamp);
                }
            %>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="PartnerFarmInfo.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  
        //Close connections with database
        farmers.closeConnection();
        channelPartner.closeConnection(); 
    %>
</html>
