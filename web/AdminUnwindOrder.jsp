<%-- 
    Document   : AdminUnwindOrder
    Created on : Sep 7, 2016, 7:03:51 PM
    Author     : LucasCarey
--%>

<%@page import="java.text.NumberFormat"%>
<%@page import="org.jquantlib.time.TimeUnit"%>
<%@page import="org.jquantlib.time.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page import="java.sql.*" %>
<%@page import="org.jquantlib.time.calendars.UnitedKingdom" %>
<%@page import="org.jquantlib.time.calendars.UnitedStates" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
        <script src="http://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
        <script src="validityCheck.js"></script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trade | Unwind Order</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body onload="calcCurrentPricePerUnit(); calcUnwindTotalPaid(); calcUnwindPaidPerUnit()">
        <%  if(new UnitedKingdom().isBusinessDay(new org.jquantlib.time.Date(new java.util.Date()))){
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(2);
            Orders orders = new Orders();
            PulledData pd = new PulledData();
            String orderNumber = request.getParameter("orderNumber");
            ResultSet orderData = orders.getOrder(orderNumber + "A");
            Calendar tradingCal = new Calendar();
            orderData.first();
            if(Double.parseDouble(request.getParameter("total_daily_option_volume_units")) == 0){
        %>
        <h3>This order has already been completely unwound.</h3>
        <%
            }else{
            ResultSet currentPrice = pd.getFuturesPrice(orderData.getString("underlying_futures_code"));
            currentPrice.first();
            //Determine Relevant Trading Calendar
                if(orderData.getString("instrument").equals("Call")){
                    tradingCal = new org.jquantlib.time.calendars.UnitedKingdom();
                }else if(orderData.getString("instrument").equals("Put")){
                    tradingCal = new org.jquantlib.time.calendars.UnitedStates();
                }
            int days = tradingCal.businessDaysBetween(new org.jquantlib.time.Date(new java.util.Date()), new org.jquantlib.time.Date(orderData.getDate("expiration_date")));
            
            /*Create an instance of Interpolation, and use that data for calculation in black scholes*/
            Interpolation i = new Interpolation(orderData.getString("underlying_futures_code").substring(0,4),orderData.getDouble("executed_strike"));
            double interpolated_volatility = i.getRequestedVolatilityStrikeValue();
            BlackScholes bs = new BlackScholes(orderData.getString("instrument"), currentPrice.getDouble("current_underlying_futures_price"), orderData.getDouble("executed_strike"), 
                    Double.parseDouble(request.getParameter("interest_rate")), 0, interpolated_volatility, days);
            
            double unitTypeValue = 50;
            if(orderData.getString("underlying_commodity_name").equals("Soybean Meal"))
                unitTypeValue = 100;
            else
                unitTypeValue = 50;
            
            ResultSet todaysData = pd.getTodaysData(orderData.getString("underlying_commodity_name"));
            todaysData.first();
                            
            double sumDelta = 0, sumGamma = 0, sumVega = 0, sumTheta = 0, fairValueSum = 0, fairValueAvg = 0;
            if(orderData.getString("frequency").equals("Daily")){
                    
                    BlackScholesDailyAccumulator bsda = new BlackScholesDailyAccumulator(
                    orderData.getString("instrument"),
                    orderData.getDouble("current_underlying_futures_price"),
                    orderData.getDouble("executed_strike"),
                    orderData.getString("underlying_futures_code").substring(0,4),
                    0.0, interpolated_volatility, new java.sql.Date(new java.util.Date().getTime()).toString(),
                    orderData.getDate("expiration_date").toString(), orderData.getString("action"), orderData.getDouble("order_volume_quantity"),
                    orderData.getInt("original_number_of_days"));
             
                    sumDelta = bsda.getDeltaTotal();
                    sumVega = bsda.getVegaTotal();
                    sumGamma = bsda.getGammaTotal();
                    sumTheta = bsda.getThetaTotal();
                    fairValueAvg = bsda.getFairValueAvg();
                }
            double units = orderData.getDouble("order_volume_quantity");
                nf.setGroupingUsed(false);
        %>
        <form name="unwindOrderForm" id="submitForm" action="AdminSubmitUnwind.jsp" method="POST">
        <input type="hidden" name="orderNumber" value="<%=orderNumber + "A"%>" />
        <a href="AdminUnwindOrder.jsp"></a>
        <input type="hidden" name="total_daily_option_volume_units" id="total_daily_option_volume_units" value="<%= Double.parseDouble(request.getParameter("total_daily_option_volume_units"))%>" />
        <input type="hidden" name="accrued_value" id="accrued_value" value="<%= orderData.getDouble("accrued_value")%>" />
        <input type="hidden" name="frequency" id="frequency" value="<%= orderData.getString("frequency")%>" />
        <input type="hidden" name="order_volume_units" id="order_volume_units" value="<%= orderData.getDouble("order_volume_quantity")%>" />
        <input type="hidden" name="mark_to_market" id="mark_to_market" value="<%= orderData.getDouble("mark_to_market")%>" />
        <input type="hidden" name="vega" id="vegaHidden" value="<%= request.getParameter("total_vega")%>" />
        <input type="hidden" id="days" value="<%= days%>" />
        <input type="hidden" id="trade_date" value="<%= orderData.getDate("trade_date")%>" />
        <input type="hidden" id="option" value="<%= orderData.getString("instrument")%>" />
        <input type="hidden" id="code" value="<%= orderData.getString("underlying_futures_code")%>" />
        <input type="hidden" id="action" value="<%= orderData.getString("action")%>" />
        <input type="hidden" id="conversionFactor" value="<%= unitTypeValue%>" />
        <h1>Unwind Order #<%=orderNumber%>A</h1>
        <input type="hidden" name="searchInput" value="<%=orderNumber + "A"%>" />
        <div id='central' align='center'>
            <h2>Client Trade Unwind Order</h2>
                <table border="1" style="width: 90%">
                <thead>
<!--                    <tr>
                        <th colspan="5" border="0"></th>
                    </tr>-->
                    <tr>
                        <th>Current Active Order Volume</th>
                        <th>Underlying Futures Code</th>
                        <th>Action</th>
                        <th>Instrument</th>
                        <th>Frequency</th>
                        <th>Unwind Accrued Value Paid</th>
                        <th>Unwind Option Value Paid</th>
                        <th>Unwind Vega</th>
                        <th>Override Option Value Paid</th>
                        <th>Unwind Total Value Paid</th>
                        <th>Unwind Value Paid / Unit</th>
                        <th>Delta (Contracts)</th>
                        <th>Gamma (Contracts)</th>
                        <th>Vega ($)</th>
                        <th>Theta ($)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" id="UnwindOrderVolumeUnits" name="UnwindOrderVolumeUnits" value="<%= orderData.getDouble("order_volume_quantity")%>" readonly class="readonly"/></td>
                        <td><%= orderData.getString("underlying_futures_code")%></td>
                        <td><%= orderData.getString("action")%></td>
                        <td><%= orderData.getString("instrument")%></td>
                        <td><%= orderData.getString("frequency")%></td>
                        <td><input type="text" name="unwind_accrued_value_paid" value="<%=nf.format(Double.parseDouble(request.getParameter("total_daily_accrual_value")))%>" readonly class="readonly"/></td>
                        <td><input type="text" name="unwind_option_value_paid" value="<%=nf.format(Double.parseDouble(request.getParameter("option_value_totals")))%>" readonly class="readonly"/></td>
                        <td><input type="text" name="unwind_vega" value="<%=nf.format(Math.abs(Double.parseDouble(request.getParameter("total_vega"))))%>" readonly class="readonly"/></td>
                        <td><input type="text" name="override_option_value_paid" value="<%= nf.format(Double.parseDouble(request.getParameter("option_value_totals")) - Double.parseDouble(request.getParameter("total_vega"))) %>" autocomplete="off" onchange="calcUnwindTotalPaid()" onkeyup="checkNum(this); calcUnwindTotalPaid()"/></td>
                        <td><input type="text" name="unwind_total_value_paid" value="0" onclick="calcUnwindTotalPaid()" readonly class="readonly"/></td>
                        <td><input type="text" name="unwind_value_paid_per_unit" value="0" onclick="calcUnwindPaidPerUnit()" readonly class="readonly"/></td>
                        <td><%= nf.format(orderData.getDouble("delta"))%></td>
                        <td><%= nf.format(orderData.getDouble("gamma"))%></td>
                        <td><%= nf.format(orderData.getDouble("vega"))%></td>
                        <td><%= nf.format(orderData.getDouble("theta"))%></td>
                    </tr>
                    </tbody>
                </table>
            <br><br>
            <h2>Client Trade Replacement Order</h2>
            <table border="1" style="width: 90%">
                <thead>
                    <tr>
                        <th>Order Volume Units</th>
                        <th>Unit Type</th>
                        <th>Order Volume Contracts</th>
                        <th>Expiration Date</th>
                        <th>Interest Rate</th>
                        <th>Current Underlying Futures Price</th>
                        <th>Override Underlying Futures Price</th>
                        <th>Daily Trader Volatility</th>
                        <th>Volatility Spread</th>
                        <th>Product Volatility</th>
                        <th>Executed Strike</th>
                        <th>Product Fair Value / Unit</th>
                        <th>Product Markup</th>
                        <th>Product Current Price / Unit</th>
                        <th>Execution Product Total Cost</th>
                        <th>Delta (Contracts)</th>
                        <th>Gamma (Contracts)</th>
                        <th>Vega ($)</th>
                        <th>Theta ($)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" id="orderVolumeUnits" class="bs" name="orderVolumeUnits" value="" onkeyup="checkNum(this); calcExecutionTotalCost(); calcContracts(); checkOrderVolumeUnits();" autocomplete="off"/></td>
                        <td><input type="text" name="unitType" value="<%=orderData.getString("unit_type") %>" readonly class="readonly"/></td>
                        <td><input type="text" name="orderVolumeContracts" id="contracts" value="" readonly class="readonly" onclick="calcContracts()"/></td>
                        <td><input type="text" name="expirationDate" id="expiration" value="<%=orderData.getDate("expiration_date") %>" readonly class="readonly" /></td>
                        <td><input type="text" name="interestRate" id="interest" value="<%=nf.format(Double.parseDouble(request.getParameter("interest_rate"))) %>" readonly class="readonly" onclick="calcContracts()"/></td>
                        <td><input type="text" name="currentUnderlyingFuturesPrice" id="price" value="<%=nf.format(currentPrice.getDouble("current_underlying_futures_price"))%>" readonly class="readonly" /></td>
                        <td><input type="text" name="overrideUnderlyingFuturesPrice" id="priceOverride" class="bs" value="<%=nf.format(currentPrice.getDouble("current_underlying_futures_price"))%>" autocomplete="off" onkeyup="checkNum(this)"/></td>
                        <td><input type="text" id="dailyTraderVolatility" name="dailyTraderVolatility" value="<%=nf.format(interpolated_volatility) %>" readonly class="readonly" /></td>
                        <td><input type="text" id="volSpread" class="bs" name="volatilitySpread" value="0" onkeyup="calcProductVolatility(); checkNum(this)" autocomplete="off"/></td>
                        <td><input type="text" id="prodVol" name="productVolatility" value="<%=nf.format(interpolated_volatility) %>" readonly class="readonly"/></td>
                        <td><input type="text" name="executedStrike" id="strike" value="<%=orderData.getDouble("executed_strike") %>" readonly class="readonly"/></td>
                        <% if(orderData.getString("frequency").equals("Bullet")){ %>
                        <td><input type="text" name="productFairValuePerUnit" value="<%=nf.format(bs.getMarketPricePerUnit()) %>" readonly class="readonly" /></td>
                        <% }else{ %>
                        <td><input type="text" id="fairValue" name="productFairValuePerUnit" value="<%= nf.format(fairValueAvg) %>" readonly class="readonly" /></td>
                        <% } %>
                        <td><input type="text" name="productMarkup" value="0" onchange="calcCurrentPricePerUnit(); calcExecutionTotalCost();" onkeyup="checkNumWithNegatives(this)" autocomplete="off"/></td>
                        <td><input type="text" name="currentPricePerUnit" value="" readonly class="readonly"/></td>
                        <td><input type="text" name="executionProductTotalCost" value="" readonly class="readonly"/></td>
                        <% if(orderData.getString("frequency").equals("Bullet")){ %>
                        <td><input type="text" name="replacementDelta" id="delta" value="<%=nf.format(bs.getDelta()) %>" readonly class="readonly"/></td>
                        <td><input type="text" name="replacementGamma" id="gamma" value="<%=nf.format(bs.getGamma()) %>" readonly class="readonly"/></td>
                        <td><input type="text" name="replacementVega" id="vega" value="<%=nf.format(bs.getVega() * units) %>" readonly class="readonly"/></td>
                        <td><input type="text" name="replacementTheta" id="theta" value="<%=nf.format(bs.getTheta() * units) %>" readonly class="readonly"/></td>
                        <% }else{ %>
                        <td><input type="text" name="replacementDelta" id="delta" value="<%=nf.format(sumDelta)%>" readonly class="readonly"/></td>
                        <td><input type="text" name="replacementGamma" id="gamma" value="<%=nf.format(sumGamma)%>" readonly class="readonly"/></td>
                        <td><input type="text" name="replacementVega" id="vega" value="<%=nf.format(sumVega)%>" readonly class="readonly"/></td>
                        <td><input type="text" name="replacementTheta" id="theta" value="<%=nf.format(sumTheta)%>" readonly class="readonly"/></td>
                        <% } %>
                    </tr>
                    <tr>
                        <td colspan="15" style="border: 0"></td>
                        <td><b>Net Delta (Contracts)</b></td>
                        <td><b>Net Gamma (Contracts)</b></td>
                        <td><b>Net Vega ($)</b></td>
                        <td><b>Net Theta ($)</b></td>
                    </tr>
                    <tr>
                        <td colspan="15" style="border: 0"></td>
                        <td><input type="text" class="readonly" readonly id="netDelta" value="0" /></td>
                        <td><input type="text" class="readonly" readonly id="netGamma" value="0" /></td>
                        <td><input type="text" class="readonly" readonly id="netVega" value="0" /></td>
                        <td><input type="text" class="readonly" readonly id="netTheta" value="0" /></td>
                    </tr>
                </tbody>
            </table>
                <div class="fixedCenter">
                    <input class='button' id="unwindButton" type="button" value="Unwind/Finalize Order" name="unwindFinalizeButton" onclick="confirmSubmit()" disabled/>
                </div>
            </form>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form id="homeForm" name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="button" value="Return to Home Screen" name="homeButton" onclick="confirmHome()"/>
                            </form>
                        </td>
                        <td>
                            <form id="backForm" name="backForm" action="AdminIndividualOrderSummary.jsp" method="POST">
                                <input type="hidden" name="searchInput" value="<%= orderNumber %>" />
                                <input id="backAndHome" type="button" value="Go Back" name="backButton" onclick="confirmBack()" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <script>
        function confirmHome(){
            var c = confirm("Are you sure you want to go home? All unsaved data will be lost.");
            if(c === true){
                document.getElementById("homeForm").submit();
            }
        }
        function confirmBack(){
            var c = confirm("Are you sure you want to go back? All unsaved data will be lost.");
            if(c === true){
                document.getElementById("backForm").submit();
            }
        }
        function confirmSubmit(){
            var c = confirm("Are you sure you want to submit?");
            if(c === true){
                document.getElementById("submitForm").submit();
            }
        }
        
        /**
         * This function checks to make sure the order volume units does not exceed the current
         * active order volume units
         */
        function checkOrderVolumeUnits(){
            var originalOrderVolume = parseFloat(document.getElementById("UnwindOrderVolumeUnits").value);
            var orderVolumeUnits= parseFloat(document.getElementById("orderVolumeUnits").value);  
            if(orderVolumeUnits > originalOrderVolume){
               window.alert("WARNING: Order volume units may not exceed " + originalOrderVolume);
               document.getElementById("orderVolumeUnits").value="";
            }
        }
        
        function calcUnwindTotalPaid(){
            var unwindTotalValuePaid = document.getElementsByName("unwind_total_value_paid")[0];
            var unwindAccruedValuePaid = document.getElementsByName("unwind_accrued_value_paid")[0].value;
            var overrideOptionValuePaid = document.getElementsByName("override_option_value_paid")[0].value;
            unwindTotalValuePaid.value = (parseFloat(unwindAccruedValuePaid) + parseFloat(overrideOptionValuePaid)).toFixed(2);
            var unwindVolumeUnits = document.getElementsByName("UnwindOrderVolumeUnits")[0].value;
            var unwindValuePaidPerUnit = document.getElementsByName("unwind_value_paid_per_unit")[0];
            unwindValuePaidPerUnit.value = (parseFloat(unwindTotalValuePaid.value) / parseFloat(unwindVolumeUnits)).toFixed(2);
        }
        
        function calcUnwindPaidPerUnit(){
            var unwindTotalValuePaid = document.getElementsByName("unwind_total_value_paid")[0].value;
            var unwindVolumeUnits = document.getElementsByName("UnwindOrderVolumeUnits")[0].value;
            var unwindValuePaidPerUnit = document.getElementsByName("unwind_value_paid_per_unit")[0];
            unwindValuePaidPerUnit.value = (parseFloat(unwindTotalValuePaid) / parseFloat(unwindVolumeUnits)).toFixed(2);
        }
        
        function calcCurrentPricePerUnit(){
            var currentPricePerUnit = document.getElementsByName("currentPricePerUnit")[0];
            var productFairValuePerUnit = document.getElementsByName("productFairValuePerUnit")[0].value;
            var productMarkup = document.getElementsByName("productMarkup")[0].value;
            currentPricePerUnit.value = (parseFloat(productFairValuePerUnit) + parseFloat(productMarkup)).toFixed(2);
            var orderVolumeUnits = document.getElementsByName("orderVolumeUnits")[0].value;
            var executionTotalCost = document.getElementsByName("executionProductTotalCost")[0];
            executionTotalCost.value = (parseFloat(orderVolumeUnits) * parseFloat(currentPricePerUnit.value)).toFixed(2);
        }
        
        function calcUnwindCurrentPricePerUnit(){
            var currentPricePerUnit = document.getElementsByName("UnwindCurrentPricePerUnit")[0];
            var productFairValuePerUnit = document.getElementsByName("UnwindProductFairValuePerUnit")[0].value;
            var productMarkup = document.getElementsByName("UnwindProductMarkup")[0].value;
            currentPricePerUnit.value = (parseFloat(productFairValuePerUnit) + parseFloat(productMarkup)).toFixed(2);
            var orderVolumeUnits = document.getElementsByName("UnwindOrderVolumeUnits")[0].value;
            var executionTotalCost = document.getElementsByName("UnwindExecutionProductTotalCost")[0];
            executionTotalCost.value = (parseFloat(orderVolumeUnits) * parseFloat(currentPricePerUnit.value)).toFixed(2);
        }
        
        function calcExecutionTotalCost(){
            var orderVolumeUnits = document.getElementsByName("orderVolumeUnits")[0].value;
            var executionTotalCost = document.getElementsByName("executionProductTotalCost")[0];
            var currentPricePerUnit = document.getElementsByName("currentPricePerUnit")[0].value;
            executionTotalCost.value = (parseFloat(orderVolumeUnits) * parseFloat(currentPricePerUnit)).toFixed(2);
        }
        
        function calcUnwindExecutionTotalCost(){
            var orderVolumeUnits = document.getElementsByName("UnwindOrderVolumeUnits")[0].value;
            var executionTotalCost = document.getElementsByName("UnwindExecutionProductTotalCost")[0];
            var currentPricePerUnit = document.getElementsByName("UnwindCurrentPricePerUnit")[0].value;
            executionTotalCost.value = (parseFloat(orderVolumeUnits) * parseFloat(currentPricePerUnit)).toFixed(2);
        }
        
        function calcContracts(){
            var orderVolumeUnits = document.getElementsByName("orderVolumeUnits")[0].value;
            var unitType = document.getElementsByName("unitType")[0].value;
            var orderVolumeContracts = document.getElementsByName("orderVolumeContracts")[0];
            if(unitType === "Metric Tons")
                orderVolumeContracts.value = (parseFloat(orderVolumeUnits) / 50).toFixed(2);
            else if(unitType === "Short Tons")
                orderVolumeContracts.value = (parseFloat(orderVolumeUnits) / 100).toFixed(2);
        }
        
        function calcProductVolatility(){
            var productVolatility = document.getElementsByName("productVolatility")[0];
            var dailyTraderVolatility = <%=interpolated_volatility %>;
            var vSpreadInput = document.getElementsByName("volatilitySpread")[0].value;
            productVolatility.value = (dailyTraderVolatility + parseFloat(vSpreadInput)).toFixed(2);
        }
        
        function calcUnwindProductVolatility(){
            var productVolatility = document.getElementsByName("UnwindProductVolatility")[0];
            var dailyTraderVolatility = <%=interpolated_volatility %>;
            var vSpreadInput = document.getElementsByName("UnwindVolatilitySpread")[0].value;
            productVolatility.value = (dailyTraderVolatility + parseFloat(vSpreadInput)).toFixed(2);
        }
        
        /**
         * This function is used to check that the information being entered into the text fields is
         * made up of digits or periods or negative signs.
         */
        function checkNumWithNegatives(e){
            var val = e.value;
            switch(val.substring(val.length - 1, val.length)){
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '0':
                    break;
                case '.':
                    if(val.substring(0, val.length - 1).includes('.')){
                        e.value = val.substring(0, val.length - 1);
                    }
                    break;
                case '-':
                    if(val.substring(0, val.length - 1).includes('-')){
                        e.value = val.substring(0, val.length - 1);
                    }
                    break;
                default:
                    e.value = val.substring(0, val.length - 1);
                    break;

            }
        }


        //These four values are pulled from the accrual table on the AdminIndividualOrderSummary page
        var activeDelta = <%= request.getParameter("total_delta_contracts")%>;
        var activeGamma = <%= request.getParameter("total_gamma_contracts")%>;
        var activeVega = <%= request.getParameter("total_vega")%>;
        var activeTheta = <%= request.getParameter("total_theta")%>;
        
        /**
         * These five AJAX calls are used anytime one of the text fields with the class "bs" has a keyup.
         * They calculate the Black Scholes Values for the replacement order table.
         */
        $(document).ready(function() {
            $('.bs').keyup(function() {
                $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            code : $('#code').val(),
                            item: 'fairValue',
                            option : $('#option').val(),
                            futures_price : $('#priceOverride').val(), //g
                            executed_strike : $('#strike').val(), //g
                            interest_rate : $('#interest').val(), //g
                            dividend_yield : '0', //g
                            daily_trader_volatility : $('#prodVol').val(), //g
                            expiry : $('#expiration').val(),
                            frequency : $('#frequency').val(),
                            action : $('#action').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            $('#fairValue').val(parseFloat(responseText).toFixed(2));
                            calcCurrentPricePerUnit();
                    }
                });
            });
        });
        $(document).ready(function() {
            $('.bs').keyup(function() {
                $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            code : $('#code').val(),
                            item: 'theta',
                            option : $('#option').val(),
                            futures_price : $('#priceOverride').val(), //g
                            executed_strike : $('#strike').val(), //g
                            interest_rate : $('#interest').val(), //g
                            dividend_yield : '0', //g
                            daily_trader_volatility : $('#prodVol').val(), //g
                            expiry : $('#expiration').val(),
                            frequency : $('#frequency').val(),
                            action : $('#action').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var conversion = 1;
                            if($('#frequency').val() === 'Bullet'){
                                conversion = $('#orderVolumeUnits').val();
                            }
                            $('#theta').val((parseFloat(responseText) * parseFloat(conversion)).toFixed(2));
                            $('#netTheta').val((activeTheta - $('#theta').val()).toFixed(2));
                    }
                });
            });
        });
        $(document).ready(function() {
            $('.bs').keyup(function() {
                $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            code : $('#code').val(),
                            item: 'gamma',
                            option : $('#option').val(),
                            futures_price : $('#priceOverride').val(), //g
                            executed_strike : $('#strike').val(), //g
                            interest_rate : $('#interest').val(), //g
                            dividend_yield : '0', //g
                            daily_trader_volatility : $('#prodVol').val(), //g
                            expiry : $('#expiration').val(),
                            frequency : $('#frequency').val(),
                            action : $('#action').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var units = 1;
                            if($('#frequency').val() === "Bullet"){
                                units = parseFloat($('#orderVolumeUnits').val());
                            }
                            $('#gamma').val((parseFloat(responseText) * units / parseFloat($('#conversionFactor').val())).toFixed(2));
                            $('#netGamma').val((activeGamma - $('#gamma').val()).toFixed(2));
                    }
                });
            });
        });
        $(document).ready(function() {
            $('.bs').keyup(function() {
                $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            code : $('#code').val(),
                            item: 'vega',
                            option : $('#option').val(),
                            futures_price : $('#priceOverride').val(), //g
                            executed_strike : $('#strike').val(), //g
                            interest_rate : $('#interest').val(), //g
                            dividend_yield : '0', //g
                            daily_trader_volatility : $('#prodVol').val(), //g
                            expiry : $('#expiration').val(),
                            frequency : $('#frequency').val(),
                            action : $('#action').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var conversion = 1;
                            if($('#frequency').val() === 'Bullet'){
                                conversion = $('#orderVolumeUnits').val();
                            }
                            $('#vega').val((parseFloat(responseText) * parseFloat(conversion)).toFixed(2));
                            $('#netVega').val((activeVega - $('#vega').val()).toFixed(2));
                    }
                });
            });
        });
        $(document).ready(function() {
            $('.bs').keyup(function() {
                $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            code : $('#code').val(),
                            item: 'delta',
                            option : $('#option').val(),
                            futures_price : $('#priceOverride').val(), //g
                            executed_strike : $('#strike').val(), //g
                            interest_rate : $('#interest').val(), //g
                            dividend_yield : '0', //g
                            daily_trader_volatility : $('#prodVol').val(), //g
                            expiry : $('#expiration').val(),
                            frequency : $('#frequency').val(),
                            action : $('#action').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var units = 1;
                            if($('#frequency').val() === "Bullet"){
                                units = parseFloat($('#orderVolumeUnits').val());
                            }
                            $('#delta').val((parseFloat(responseText) * units / parseFloat($('#conversionFactor').val())).toFixed(2));
                            $('#netDelta').val((activeDelta - $('#delta').val()).toFixed(2));
                    }
                });
            });
        });
        $(document).ready(function() {
                    $("input:text").change(function() {
                        allFilled = true;
                        var inputs = $("input:text");
                        for(var i = 0; i < inputs.length; i++){
                            if(inputs[i].value === ""){
                                allFilled = false;
                            }
                        }
                        if(allFilled){
                            document.getElementById("unwindButton").disabled = false;
                        } else{
                            document.getElementById("unwindButton").disabled = true;
                        }
                    });
                });
                
        $(document).keypress(
            function(event){
             if (event.which == '13') {
                event.preventDefault();
            }});
    </script>
    <% }}else{ %>
    <h1>Orders may only be unwound on business days. Please check back on the next available business day.</h1>
    <% } %>
</html>
