
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%-- 
    Document   : hedgeTrades
    Created on : Aug 7, 2016, 2:29:41 PM
    Author     : Lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page import="org.jquantlib.time.calendars.UnitedKingdom" %>
<%@page import="java.text.NumberFormat" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
    <script src="http://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Enter Trade</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <script src="validityCheck.js"></script>
    </head>
    <body onload = 'disableAll()'>
        <h1>Trade Entry</h1>
        <div id='central' align='center'>
            <%  //Counter -- to count number of rows (if a future update allows multiple trades at once)
                int count = 1;
                
                /**
                 * ONLY RUN THE WEBPAGE IF TODAY IS A BUSINESS DAY***
                 */
                if(new UnitedKingdom().isBusinessDay(new org.jquantlib.time.Date(new java.util.Date()))){
                
                    //Access the orders, farm, channel partner, and brokers databases.
                    Orders order = new Orders();
                    Farmers farm = new Farmers();
                    ChannelPartner channelPartner = new ChannelPartner();
                    Brokers broker = new Brokers();
                    ResultSet brokerData = null;
                    ResultSet cpData = channelPartner.getChannelPartners();
                    ResultSet farmData = farm.getFarmers();  
            %>
                
            <form id="tradeForm" name="submitTrade" action="AdminSubmitTrade.jsp" method="POST">
                <table border="1" id='tradeTable'>
                    <thead>
                        <tr>
                            <th>Transaction Type</th>
                            <th>Channel Partner</th>
                            <th>Action</th>
                            <th>Underlying Futures Code</th>
                            <th>Underlying Commodity Name</th>
                            <th>Client Month</th>
                            <th>Expiration Date</th>
                            <th>Instrument</th>
                            <th>Product</th>
                            <th>Frequency</th>
                            <th>Order Volume Units</th>
                            <th>Unit Type</th>
                            <th>Order Volume Contracts</th>
                            <th>Current Underlying Futures Price</th>
                            <th>Executed Strike</th>
                            <th>Daily Trader Volatility</th>
                            <th>Product Fair Value / Unit</th>
                            <th>Executed Product Cost / Unit
                            <th>Executed Product Total Cost</th>
                            <th>Delta (contracts)</th>
                            <th>Gamma (contracts)</th>
                            <th>Vega ($)</th>
                            <th>Theta ($)</th>
                            <th>Farm ID</th>
                            <th>Farm Physical Contract ID</th>
                            <th>Broker</th>
                            <th>Order Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="col1">
                                <select id="transactionType" class="dropdown" name="<%= "transactionType" + count %>" onchange=" disableAll(); setAction(); setProductType();">
                                    <option value="default">Select a Transaction Type</option>
                                    <option>Hedge</option>
                                    <option>Client Trade</option>
                                </select>
                                <input type="hidden" name="transactionSet" value ='document.getElementById("transactionType").value.toString()' />
                            </td>
                            <td id="col2">
                                <select class="selectPartner dropdown" name="<%="selectPartner" + count%>" id="<%="selectPartner" + count%>" onchange="selectFarmers();selectBrokers();">
                                    <option value="default">Select Channel Partner</option>
                                    <% while(cpData.next()){ %>
                                    <option value="<%= cpData.getString("channel_partner_id") %>"><%= cpData.getString("channel_partner_id") %></option>    
                                    <% } %>
                                </select>
                            </td>
                            <td id="col3">
                                <select id="actionSelect" class="dropdown" name="<%= "actionSelect" + count%>" onchange="setSigns()" >
                                    <option value="default">Select an Action</option>
                                    <option value="Buy">Buy</option>
                                    <option value="Sell">Sell</option>
                                </select>
                            </td>
                            <td id="col4">
                                <input class="int editable" id="futuresCodeInput" type="text" name="<%="underlyingFuturesCodeInput" + count%>" placeholder="Underlying Futures Code" onchange="setProductNameAndUnitType();setMonthChooser();"  />
                            </td>
                            <td id="col5">
                                <input type="text" autocomplete="off" name="<%="productName" + count%>" id="productName" value="" readonly class="readonly"/>
                            </td>  
                            <td id="col6">
                                <select class="int dropdown" id="clientMonth" name="<%="months" + count%>">
                                </select>
                            </td>
                            <td id="col7">
                                <input class="int editable bs" id="expirationDate" type="text" autocomplete="off" name="<%="expirationDate" + count%>" value="" placeholder="yyyy-mm-dd" onchange="setSigns()"/>   
                            </td>
                            <input id="interestRate" type="hidden" name="<%="interestRate" + count%>" value="" />   
                            <td id="col9">
                                <select class="int bs dropdown" id="instrument" name="<%="instrumentSelect" + count%>" onchange="setSigns(); setInstrument();" >
                                    <option value="default">Select an Instrument</option>
                                    <option value="Call">Call</option>
                                    <option value="Put">Put</option>
                                    <option value="Futures">Futures</option>
                                </select>
                            </td>
                            <td id="col10">
                                <select id="productInput" class="dropdown" name="<%="product" + count%>" onchange="setFrequency()">
                                    <option value="default">Select A Product</option>
                                    <option>Enhanced Market Average</option>
                                    <option>Freedom Reprice</option>
                                </select>
                            </td>
                            <td id="col11">
                                <select id="frequencyInput" class="dropdown" type="text" name="<%= "frequency" + count%>" >
                                    <option value="Bullet">Bullet</option>
                                    <option value="Daily">Daily</option>
                                </select>                              
                            </td>
                            <td id="col12">
                                <input id="orderVolumeUnits" class="bs editable" type="text" name="<%="orderVolumeQuantity" + count%>" value="" onchange="calculateContracts(); calcExecutionTotalCost(); setSigns();" onkeyup="checkNum(this)"/>
                            </td>
                            <td id="col13">
                                <input id="unitType" type="text" autocomplete="off" name="<%="unitType" + count%>" value="" readonly class="readonly"/>
                            </td>
                            <td id="col14">
                                <input id="orderVolumeContracts" type="text" autocomplete="off" name="<%="orderVolumeContracts" + count%>" value="" readonly class="readonly"/>
                            </td>
                            <td id="col15">
                                <input id="underlyingFuturesPrice" type="text" autocomplete="off" name="<%="underlyingFuturesPrice" + count%>" value="" readonly class="readonly"/>
                            </td>                       
                            <td id="col19">
                                <input id="executedStrike" type="text" name="<%="executedStrike" + count%>" value="" class="bs editable" onkeyup="checkNum(this)" onchange="setSigns()"/>
                            </td>
                            <td>
                                <input id="dailyTraderVolatility" type="text" name="<%="dailyTraderVolatility" + count%>" value="" readonly class="readonly"/>
                            </td>
                            <td id="col20">
                                <input id="productFairValue" type="text" name="<%="productFairValue" + count%>" value="" onkeyup="checkNum(this)" readonly class="readonly"/>
                            </td>
                            <td id="col22">
                                <input id="executionCostPerUnit" type="text" autocomplete="off" name="<%="executionCostPerUnit" + count%>" value="" onkeyup="calcExecutionTotalCost()" class="bs editable"/>
                            </td>
                            <td id="col23">
                                <input id="executionTotalCost" type="text" autocomplete="off" name="<%="executionTotalCost" + count%>" value="" class="readonly" onclick="calcExecutionTotalCost()" readonly/>
                            </td>
                            <td id="col24">
                                <input id="deltaContracts" type="text" autocomplete="off" name="<%="deltaContracts" + count%>" value="" readonly class="readonly" />
                            </td>
                            <td id="col25">
                                <input id="gammaContracts" type="text" autocomplete="off" name="<%="gammaContracts" + count%>" value="" readonly class="readonly" />
                            </td>
                            <td id="col26">
                                <input id="vegaUnits" type="text" autocomplete="off" name="<%="vegaUnits" + count%>" value="" readonly class="readonly" />
                            </td>
                            <td id="col27">
                                <input id="thetaUnits" type="text" autocomplete="off" name="<%="thetaUnits" + count%>" value="" readonly class="readonly" />
                            </td>
                            <td id="col31">
                                <select id="farmSelect" class="dropdown" name="<%="farmSelect" + count%>" id="<%="farmSelect" + count%>" disabled>
                                    <option value="default">Select a Farm</option>
                                    <option value="N/A">No Farm</option>
                                    <% while(farmData.next()){ 
                                    %>
                                    <option><%= farmData.getString("farmer_id")%></option>
                                    <%      }  %>
                                </select>
                            </td>
                            
                            <td id="col32">
                                <input id="farmContractId" class="editable" type="text" autocomplete="off" name="<%="farmerPhysicalContractId" + count%>" value="" />
                            </td>
                            <% brokerData = broker.getBroker(); %>
                            <td id="col33">
                                <select name="<%="brokerSelect" + count%>" class="dropdown" id="brokerSelect">
                                    <option value="default">Select A Broker</option>
                                    <option value="N/A">No Broker</option>
                                    <% while(brokerData.next()){ %>
                                    <option><%= brokerData.getString("company_name") + "(" + channelPartner.getIdByName(brokerData.getString("channel_partner")) + ")" %></option>
                                    <%}%>
                                </select>
                            </td>
                            <td id="col34">
                                <input type="text" name="<%="orderId" + count%>" value="<%=order.generateTradeID()%>" readonly class="readonly" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
                <div class="fixedCenter" align='center'>
                    <input type="hidden" name="count" value="<%=count%>" />
                    <input id='button' type="button" value="Finalize Trade" name="finalizeTradeButton" onclick="makeReadonly()" disabled/>
                    <input id='button' type="submit" value="Submit Trade" name="submitTradeButton" style="display:none" onclick="makeEditable()"/>
                    <input id='button' type="button" value="Return to Edit" name="returnButton" style="display:none" onclick="makeEditable()"/>
                </div>
            </form>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form id="homeForm" name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <button id="backAndHome" type="button" value="Return to Home Screen" name="homeButton" onclick="confirmHome() ">Return to Home Screen</button>
                            </form>
                        </td>
                        <td>
                            <form id="backForm" name="backForm" action="AdminHomePage.jsp" method="POST">
                                <button id="backAndHome" type="button" value="Go Back" name="backButton" onclick="confirmBack()">Go Back</button>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <% 
            //Increment the counter if the finalize trade button is clicked.
            if(request.getParameter("finalizeTrade" + count) != null){
                count++;
            } 
        %>             
    </body>
    <%
        //Close several connections to the database.
        order.closeConnection();
        farm.closeConnection();
        channelPartner.closeConnection();
        broker.closeConnection();
        } 

        /** IF IT IS NOT A BUSINESS DAY, THIS PAGE IS DISABLED AND AN ERROR MESSAGE IS DISPLAYED**/
        else { 
    %>
            <h1>Orders may only be placed on business days. Please check back on the next available business day.</h1>
       <% } %>
            
</html>
<script>
    /*** 
     * Run on the change of the transaction type selector.
     * Assigns the product of the order based upon the transaction type.
     */
    function setProductType(){
        if(document.getElementById("transactionType").value.toString() === "Client Trade"){
            //document.getElementById("col9").innerHTML = "";
            document.getElementById("productInput").value = "European";
        }
        else if(document.getElementById("transactionType").value.toString() === "Hedge"){
            document.getElementById("expirationDate").placeholder = "yyyy-mm-dd";
            document.getElementById("productInput").value = "N/A";
        }
    }
    
    /**
     * Run on the change of the transaction type selector
     * Modifies many fields, including action, client month, product, frequency, farm ID, and farm physical contract ID.
     * Each of these columns it either disabled or enabled and given a default value based upon the transaction type selected.
     */
    function setAction(){
        var type = document.getElementById("transactionType").value.toString();
        document.getElementById("tradeForm").reset();
        document.getElementById("transactionType").value = type;
        if( type === "Client Trade"){
            document.getElementById("col3").innerHTML = '<input type="text" id="actionSelect" name="<%= "action" + count%>" value="Sell" readonly class="readonly"/>';
            document.getElementById("clientMonth").disabled = false;
            document.getElementById("productInput").disabled = false;
            document.getElementById("frequencyInput").value = "";
            document.getElementById("farmSelect").disabled = false;
            document.getElementById("farmContractId").value = "";
            document.getElementById("farmContractId").disabled = false;
        }else if(type === "Hedge"){
            document.getElementById("col3").innerHTML = '<select id="actionSelect" class="dropdown" name="<%= "actionSelect" + count%>"><option value="default">Select an Action</option><option value="Buy">Buy</option><option value="Sell">Sell</option></select>';
            document.getElementById("clientMonth").disabled = true;
            document.getElementById("productInput").disabled = true;
            document.getElementById("frequencyInput").value = "Bullet";
            document.getElementById("farmSelect").disabled = true;
            document.getElementById("farmContractId").value = "N/A";
            document.getElementById("farmContractId").disabled = true;
        }

    }
    
    /**
    * This function is run when an instrument is selected
    * If the selected instrument is futures, many fields are disabled.
    * If the selected instrument is call or put, these fields remain enabled.
    */
    function setInstrument(){
        var instrument = document.getElementById("instrument").value;
            if(instrument === "Futures"){
                document.getElementById("productFairValue").value = "N/A";
                document.getElementById("executionCostPerUnit").value = "N/A";
                document.getElementById("executionCostPerUnit").readonly = "readonly";
                document.getElementById("executionCostPerUnit").class = "readonly";
                document.getElementById("executionTotalCost").value = "N/A";
                document.getElementById("farmSelect").value = "N/A";
                document.getElementById("farmSelect").disabled = true;
                document.getElementById("farmContractId").value = "N/A";
            }else{
                document.getElementById("productFairValue").value = "";
                document.getElementById("executionCostPerUnit").value = "";
                document.getElementById("executionTotalCost").value = "";
                document.getElementById("farmSelect").value = "";
                document.getElementById("farmSelect").disabled = false;
                document.getElementById("farmContractId").value = "";
            }
    }
    
    /**
    * Called on the load of the page, as well as when the transaction type is changed. 
    * Assesses whether the transaction type selector is in its default state. 
    * If it is in the default state, all of the other selects and editable text fields in the table are disabled and/or made read only. 
    * If it is not in the default state, all of the other selects and editable text fields in the table are enabled and/or made editable.
    */
    function disableAll(){
        var selects = document.getElementsByTagName('select');
        var boxes = document.getElementsByTagName('input');
        var transaction = document.getElementById('transactionType');
        if(transaction.value === 'default'){
            for(var i = 0; i < selects.length; i++){
                selects[i].disabled = true;
            } 
            for(var i = 0; i < boxes.length; i++){
                boxes[i].disabled = true;
            }
            transaction.disabled = false;
        }else{
             for(var i = 0; i < selects.length; i++){
                selects[i].disabled = false;
            } 
            for(var i = 0; i < boxes.length; i++){
                boxes[i].disabled = false;
            } 
        }
    }
    
    /**
    * Called when the commodity selector changes in value. 
    * Assigns the correct value for unit type based upon the commodity selected.
    */
    function setUnitType(){
        var commodityCode = document.getElementById("commodityChooser").value;
        if(commodityCode === "IJ" || commodityCode === "CA"){
            document.getElementById("unitType").value = "Metric Tons";
        }else if(commodityCode === "SM"){
            document.getElementById("unitType").value = "Short Tons";
        }
    }
    
    /**
    * Called when the values of delta, gamma, vega, and theta are calculated and changed.
    * Adjusts the signs of these values based upon the action and the instrument.
    */
    function setSigns(){
        var action = document.getElementById("actionSelect").value;
        var instrument = document.getElementById("instrument").value;
        var delta = document.getElementById("deltaContracts");
        var gamma = document.getElementById("gammaContracts");
        var vega = document.getElementById("vegaUnits");
        var theta = document.getElementById("thetaUnits");
        if(action === "Buy"){
            vega.value = Math.abs(parseFloat(vega.value));
            theta.value = -1 * Math.abs(parseFloat(theta.value));
            gamma.value = Math.abs(parseFloat(gamma.value));
            if(instrument === "Call"){
                delta.value = Math.abs(parseFloat(delta.value));
            }
            if(instrument === "Put"){
                delta.value = -1 * Math.abs(parseFloat(delta.value));
            }
        }
        else if(action === "Sell"){
            vega.value = -1 * Math.abs(parseFloat(vega.value));
            theta.value = Math.abs(parseFloat(theta.value));
            gamma.value = -1 * Math.abs(parseFloat(gamma.value));
            if(instrument === "Call"){
                delta.value = -1 * Math.abs(parseFloat(delta.value));
            }
            if(instrument === "Put"){
                delta.value = Math.abs(parseFloat(delta.value));
            }
        }
    }
    
    /**
    * This function is run when a value for the underlying futures code is entered. 
    * It modifies the options available in the client month select based upon the futures code.
    */
    function setMonthChooser(){
        var chooser = document.getElementById('clientMonth');
        var code = $('#futuresCodeInput').val().substring(0,3);
        switch(code){
            case 'CAH':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="11">November</option><option value="12">December</option><option value="1">January</option>';
                break;
            case 'CAK':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="2">February</option><option value="3">March</option>';
                break;
            case 'CAU':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option>';
                break;
            case 'CAZ':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="8">August</option><option value="9">September</option><option value="10">October</option>';
                break;
            case 'IJH':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="12">December</option><option value="1">January</option>';
                break;
            case 'IJM':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="2">February</option><option value="3">March</option><option value="4">April</option>';
                break;
            case 'IJQ':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="5">May</option><option value="6">June</option>';
                break;
            case 'IJX':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="7">July</option><option value="8">August</option><option value="9">September</option>';
                break;
            case 'IJF':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="10">October</option><option value="11">November</option>';
                break;
            case 'SMH':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="1">January</option><option value="2">February</option>';
                break;
            case 'SMK':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="3">March</option><option value="4">April</option>';
                break;
            case 'SMN':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="5">May</option><option value="6">June</option>';
                break;
            case 'SMQ':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="7">July</option>';
                break;
            case 'SMU':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="8">August</option>';
                break;
            case 'SMV':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="9">September</option>';
                break;
            case 'SMZ':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="10">October</option><option value="11">November</option>';
                break;
            case 'SMF':
                chooser.innerHTML = '<option value="default">Select a Month</option><option value="12">December</option>';
                break;
            default:
                chooser.innerHTML = '<option value="default">Select a Month</option>';
                break;
        }
    }
    
    /**
    * This function is run when the user selects a product.
    * It modifies the value for frequency based upon the product inputted.
    */
    function setFrequency(){
        var product = document.getElementById("productInput").value;
        var frequency = document.getElementById("frequencyInput");
        if(product === "Enhanced Market Average"){
            frequency.value = "Daily";
            frequency.disabled = true;
        }
        else if(product === "Freedom Reprice"){
            frequency.value = "Bullet";
            frequency.disabled = true;
        }
    }
    
    /**
    * This function calculates the number of order volume contracts based upon the number of order volume units.
    * It divides the order volume units by 100 for soybean meal trades and by 50 for all other trades. 
    */
    function calculateContracts(){
        var commodity = document.getElementById("productName").value;
        var contracts = document.getElementById("orderVolumeContracts");
        var orderVolume = document.getElementById("orderVolumeUnits").value;
            if(commodity === "Euronext Wheat" || commodity === "Euronext Rapeseed"){
                contracts.value = orderVolume / 50;
            }else if(commodity === "Soybean Meal"){
                contracts.value = orderVolume / 100;
            }
    }
    
    /**
    * This function is run when the number of order volume units or the execution product cost per unit changes.
    * It multiplies these two values to get the new execution product total cost.
    */
    function calcExecutionTotalCost(){
        var orderVolume = document.getElementById("orderVolumeUnits").value;
        var executionCost = document.getElementById("executionCostPerUnit").value;
        document.getElementById("executionTotalCost").value = parseFloat(orderVolume) * parseFloat(executionCost);
    }

    /**
    * This function is run when you select a channel partner
    * It removes any brokers from the broker select that do not pertain to the selected channel partner.
    */
    function selectBrokers(){
        var partnerSelector = document.getElementsByClassName("selectPartner")[0];
        var options = partnerSelector.options;
        var index = partnerSelector.selectedIndex;
        var selection = options[index].value;
        var brokerSelector = document.getElementById("brokerSelect");
        brokerSelector.disabled = false; 
        brokerSelector.value = "defaut";
        var brokerOptions = brokerSelector.options;

        for(var i = 0; i < brokerOptions.length; i++){
            if(!brokerOptions[i].value.includes(selection) && brokerOptions[i].value !== "default" && brokerOptions[i].value !== "N/A"){
                brokerOptions[i].disabled = true;
            }else{
                brokerOptions[i].disabled = false;
            }
        }
    }
    
    /**
    * This function is run when you select a channel partner
    * It removes any farmers from the farmer select that do not belong to the selected channel partner.
    */
    function selectFarmers(){
        var partnerSelector = document.getElementsByClassName("selectPartner")[0];
        var options = partnerSelector.options;
        var index = partnerSelector.selectedIndex;
        var selection = options[index].value;
        var farmSelector = document.getElementById("farmSelect");
        farmSelector.disabled = false; 
        farmSelector.value = "defaut";
        var farmOptions = farmSelector.options;
        for(var i = 0; i < farmOptions.length; i++){
            if(!farmOptions[i].value.includes(selection) && farmOptions[i].value !== "default" && farmOptions[i].value !== "N/A"){
                farmOptions[i].disabled = true;
            }else{
                farmOptions[i].disabled = false;
            }
        }
    }
    
    /**
    * This function is run when the user clicks the "Return To Home Screen Button"
    * It confirms that they actually want to go home, given that they will lose unsaved changes. 
    */
    function confirmHome(){
        var c = confirm("Are you sure you want to go home? All unsaved data will be lost.");
        if(c === true){
            document.getElementById("homeForm").submit();
        }
    }
    
    /**
    * This function is run when the user clicks the "Return To Previous Screen Button"
    * It confirms that they actually want to go back, given that they will lose unsaved changes. 
    */
    function confirmBack(){
        var c = confirm("Are you sure you want to go back? All unsaved data will be lost.");
        if(c === true){
            document.getElementById("backForm").submit();
        }
    }
    
    /**
    * This function is run whenever the user enters an underlying futures code
    * It sets the underlying commodity name, unit type, and instrument accordingly. 
    */
    function setProductNameAndUnitType(){
        var futuresCode = document.getElementById("futuresCodeInput").value.toString();
        if(futuresCode.substring(0,2) === ("IJ")){
            document.getElementById("productName").value = "Euronext Rapeseed";
            document.getElementById("unitType").value = "Metric Tons";
            if(document.getElementById("transactionType").value === "Client Trade")
                document.getElementById("instrument").value = "Call";
        }else if(futuresCode.substring(0,2) === ("CA")){
            document.getElementById("productName").value = "Euronext Wheat";
            document.getElementById("unitType").value = "Metric Tons";
            if(document.getElementById("transactionType").value === "Client Trade")
                document.getElementById("instrument").value = "Call";
        }else if(futuresCode.substring(0,2) === ("SM")){
            document.getElementById("productName").value = "Soybean Meal";
            document.getElementById("unitType").value = "Short Tons";
            if(document.getElementById("transactionType").value === "Client Trade")
                document.getElementById("instrument").value = "Put";
        }
    }
    
    /**
    * This function is run when the "Finalize Trade" button is clicked.
    * It disables all select elements and makes all editable text fields readonly.
    * Additionally, the finalize trade button disappears, and the submit trade and return to edit buttons appear. 
    */
    function makeReadonly(){
        var editables = document.getElementsByClassName("editable");
        var dropdowns = document.getElementsByClassName("dropdown");
        for(var i = 0; i < editables.length; i++){
            editables[i].readOnly = true;
            editables[i].style.backgroundColor = "transparent";
            editables[i].style.border = "0px";
        }
        for(var i = 0; i < dropdowns.length; i++){
            dropdowns[i].disabled = true;
        }
        document.getElementsByName("finalizeTradeButton")[0].style.display = "none";
        document.getElementsByName("submitTradeButton")[0].style.display = "block";
        document.getElementsByName("returnButton")[0].style.display = "block";
    }
    
    /**
    * This function is run when the "Return to Edit" button is clicked.
    * It enables all select elements and makes all editable text fields editable.
    * Additionally, the finalize trade button appears, and the submit trade and return to edit buttons disappear. 
    */
    function makeEditable(){
        var editables = document.getElementsByClassName("editable");
        var dropdowns = document.getElementsByClassName("dropdown");
        for(var i = 0; i < editables.length; i++){
            editables[i].readOnly = false;
            editables[i].style.backgroundColor = "white";
            editables[i].style.border = "1px";
        }
        for(var i = 0; i < dropdowns.length; i++){
            dropdowns[i].disabled = false;
        }
        document.getElementsByName("finalizeTradeButton")[0].style.display = "block";
        document.getElementsByName("submitTradeButton")[0].style.display = "none";
        document.getElementsByName("returnButton")[0].style.display = "none";
    }
    
    /**
    * This function is run when the "Submit Trade" button is clicked.
    * It submits the details of the trade to the database.
    */
    function submitTrade(){
        var editables = document.getElementsByClassName("editable");
        var dropdowns = document.getElementsByClassName("dropdown");
        for(var i = 0; i < editables.length; i++){
            editables[i].readOnly = false;
            editables[i].style.backgroundColor = "white";
            editables[i].style.border = "1px";
        }
        for(var i = 0; i < dropdowns.length; i++){
            dropdowns[i].disabled = false;
        }
        document.getElementById("tradeForm").submit();
    }
    
    /**
    * Enforces Caps Lock for Futures Code Input
    */
    $(document).ready(function() {
        $('#futuresCodeInput').keyup(function() {
            this.value = this.value.toLocaleUpperCase();
        });
    });
    
    /**
    * Makes sure the futures code entered is valid AND
    * uses an AJAX call to the Futures Price Servlet to obtain the current underlying futures price for the futures code entered. 
    */
    $(document).ready(function() {
        $('#futuresCodeInput').change(function() {
            var isValid = false;
            while(!isValid){
                var root = this.value.substring(0,3);
                if( (root !== 'CAH' &&
                    root !== 'CAK' &&
                    root !== 'CAU' &&
                    root !== 'CAZ' &&
                    root !== 'IJH' &&
                    root !== 'IJM' &&
                    root !== 'IJQ' &&
                    root !== 'IJX' &&
                    root !== 'IJF' &&
                    root !== 'SMH' &&
                    root !== 'SMK' &&
                    root !== 'SMN' &&
                    root !== 'SMQ' &&
                    root !== 'SMU' &&
                    root !== 'SMV' &&
                    root !== 'SMZ' &&
                    root !== 'SMF') ||
                    this.value.length !== 4
                ){
                    var newCode = prompt(this.value + ' is not a valid underlying futures code. Please enter a valid underlying futures code below. (example: SMV7)');
                    this.value = newCode.toUpperCase();
                }
                else{
                    isValid = true;
                    $.ajax({ url : 'FuturesPriceServlet', data : {
                                        futuresCode : $('#futuresCodeInput').val()},
                              success : function(responseText) {
                                        $('#underlyingFuturesPrice').val(responseText);}});
                    $.ajax({ url : 'FuturesPriceServlet', data : {
                                        futuresCode : $('#futuresCodeInput').val()},
                                success : function(responseText) {
                                        $('#overrideFuturesPrice').val(responseText);}});
                    setProductNameAndUnitType();
                }
            }
        });
    });
    
    /**
    * Update the expiration date based upon the client month and the underlying commodity name.
    * Update the interest rate based upon the interest, expiration date, and underlying futures code. 
    */
    $(document).ready(function() {
        $('#clientMonth').change(function() {
            var month = $('#clientMonth').val();
            var code = $('#futuresCodeInput').val().substring(0,2);
            var commodity ='';
            if(code === 'SM'){commodity = 'SoybeanMeal';}
            else if(code === 'IJ'){commodity = 'EuronextRapeseed';}
            else if(code === 'CA'){commodity = 'EuronextWheat';}
            $.ajax({
                url : 'ExpirationDateServlet',
                data : {
                        clientMonth : month,
                        underlyingCommodity : commodity
                },
                success : function(responseText) {
                    $('#expirationDate').val(responseText);
                    $.ajax({
                        url : 'InterestRateServlet',
                        data : {
                                option : $('#instrument').val(),
                                futuresCode : $('#futuresCodeInput').val(),
                                expiry : $('#expirationDate').val()
                        },
                        success : function(responseText) {
                                $('#interestRate').val(responseText);
                        }
                    });
                }
            });
        });
    });
    
    /**
    * AJAX CALLS FOR BLACK SCHOLES
    * RUN EVERY TIME A BLACK SCHOLE PARAMETER IS CHANGED
    * UPDATES DELTA, GAMMA, VEGA, THETA, PRODUCT FAIR VALUE / UNIT, and DAILY TRADER VOLATILITY automatically
    */
    $(document).ready(function() {
        $('.bs').keyup(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'delta',
                            option : $('#instrument').val(),
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var delta = parseFloat(responseText);
                            var commodity = $('#productName').val();
                            var conversionFactor = 0;
                            var units = 1;
                            if($('#frequencyInput').val() === "Bullet"){
                                units = parseFloat($('#orderVolumeUnits').val());
                            }
                            if(commodity === "Euronext Wheat" || commodity === "Euronext Rapeseed"){
                                conversionFactor = 50;
                            }
                            else{
                                conversionFactor = 100;
                            }
                            var response = (delta * units / conversionFactor).toFixed(2);
                            $('#deltaContracts').val(response);
                            setSigns();
                    }
            });
        });
    });
    
    $(document).ready(function() {
        $('.bs').keyup(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                url : 'BlackScholesServlet',
                data : {
                        frequency : $('#frequencyInput').val(),
                        code : $('#futuresCodeInput').val(),
                        item: 'gamma',
                        option : $('#instrument').val(),
                        futures_price : price, //g
                        executed_strike : $('#executedStrike').val(), //g
                        interest_rate : $('#interestRate').val(), //g
                        dividend_yield : '0', //g
                        expiry : $('#expirationDate').val(),
                        action : $('#actionSelect').val(),
                        order_volume_quantity : $('#orderVolumeUnits').val()
                },
                success : function(responseText) {
                        var gamma = parseFloat(responseText);
                        var commodity = $('#productName').val();
                        var conversionFactor = 0;
                        var units = 1;
                        if($('#frequencyInput').val() === "Bullet"){
                            units = parseFloat($('#orderVolumeUnits').val());
                        }
                        if(commodity === "Euronext Wheat" || commodity === "Euronext Rapeseed"){
                            conversionFactor = 50;
                        }
                        else{
                            conversionFactor = 100;
                        }
                        var response = (gamma * units / conversionFactor).toFixed(2);
                        $('#gammaContracts').val(response);
                        setSigns();
                }
            });
        });
    });
    
    $(document).ready(function() {
        $('.bs').keyup(function() { 
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'vega',
                            option : $('#instrument').val(),
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var vega = parseFloat(responseText);
                            var response = 0;
                            if($('#frequencyInput').val() === "Bullet"){
                                var units = parseFloat($('#orderVolumeUnits').val());
                                response = (vega * units).toFixed(2);
                            }else{
                                response = vega.toFixed(2);
                            }
                            $('#vegaUnits').val(response);
                            setSigns();
                    }
            });
        });
    });
    
    $(document).ready(function() {
        $('.bs').keyup(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'theta',
                            option : $('#instrument').val(),
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var theta = parseFloat(responseText);
                            var response = 0;
                            if($('#frequencyInput').val() === "Bullet"){
                                var units = parseFloat($('#orderVolumeUnits').val());
                                response = (theta * units).toFixed(2);
                            }else{
                                response = theta.toFixed(2);
                            }
                            $('#thetaUnits').val(response);
                            setSigns();
                    }
            });
        });
    });
                    
    $(document).ready(function() {
        $('.bs').change(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'delta',
                            option : $('#instrument').val(),
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var delta = parseFloat(responseText);
                             var commodity = $('#productName').val();
                            var conversionFactor = 0;
                            var units = 1;
                            if($('#frequencyInput').val() === "Bullet"){
                                units = parseFloat($('#orderVolumeUnits').val());
                            }
                            if(commodity === "Euronext Wheat" || commodity === "Euronext Rapeseed"){
                                conversionFactor = 50;
                            }
                            else{
                                conversionFactor = 100;
                            }
                            var response = (delta * units / conversionFactor).toFixed(2);
                            $('#deltaContracts').val(response);
                            setSigns();
                    }
            });
        });
    });
    
    $(document).ready(function() {
        $('.bs').change(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'gamma',
                            option : $('#instrument').val(),
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var gamma = parseFloat(responseText);
                             var commodity = $('#productName').val();
                            var conversionFactor = 0;
                            var units = 1;
                            if($('#frequencyInput').val() === "Bullet"){
                                units = parseFloat($('#orderVolumeUnits').val());
                            }
                            if(commodity === "Euronext Wheat" || commodity === "Euronext Rapeseed"){
                                conversionFactor = 50;
                            }
                            else{
                                conversionFactor = 100;
                            }
                            var response = (gamma * units / conversionFactor).toFixed(2);
                            $('#gammaContracts').val(response);
                            setSigns();
                    }
            });
        });
    });
    
    $(document).ready(function() {
        $('.bs').change(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'vega',
                            option : $('#instrument').val(),
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var vega = parseFloat(responseText);
                            var response = 0;
                            if($('#frequencyInput').val() === "Bullet"){
                                var units = parseFloat($('#orderVolumeUnits').val());
                                response = (vega * units).toFixed(2);
                            }else{
                                response = vega.toFixed(2);
                            }
                            $('#vegaUnits').val(response);
                            setSigns();
                    }
            });
        });
    });
                    
    $(document).ready(function() {
        $('.bs').change(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'theta',
                            option : $('#instrument').val(),
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            var theta = parseFloat(responseText);
                            var response = 0;
                            if($('#frequencyInput').val() === "Bullet"){
                                var units = parseFloat($('#orderVolumeUnits').val());
                                response = (theta * units).toFixed(2);
                            }else{
                                response = theta.toFixed(2);
                            }
                            $('#thetaUnits').val(response);
                            setSigns();
                    }
            });
        });
    });
                    
    //EXECUTION COST
    $(document).ready(function() {
        $('.bs').change(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'fairValue',
                            option : $('#instrument').val(), //g
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            $('#productFairValue').val(parseFloat(responseText).toFixed(2));
                            calcExecutionTotalCost();
                    }
            });
        });
    });
    
    $(document).ready(function() {
        $('.bs').keyup(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'fairValue',
                            option : $('#instrument').val(), //g
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            $('#productFairValue').val(parseFloat(responseText).toFixed(2));
                            calcExecutionTotalCost();
                    }
            });
        });
    });
    
    $(document).ready(function() {
        $('.bs').keyup(function() { 
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'volatility',
                            option : $('#instrument').val(), //g
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            $('#dailyTraderVolatility').val(parseFloat(responseText).toFixed(2));
                    }
            });
        });
    });
    
    $(document).ready(function() {
        $('.bs').change(function() {
            var price = $('#underlyingFuturesPrice').val();
            $.ajax({
                    url : 'BlackScholesServlet',
                    data : {
                            frequency : $('#frequencyInput').val(),
                            code : $('#futuresCodeInput').val(),
                            item: 'volatility',
                            option : $('#instrument').val(), //g
                            futures_price : price, //g
                            executed_strike : $('#executedStrike').val(), //g
                            interest_rate : $('#interestRate').val(), //g
                            dividend_yield : '0', //g
                            expiry : $('#expirationDate').val(),
                            action : $('#actionSelect').val(),
                            order_volume_quantity : $('#orderVolumeUnits').val()
                    },
                    success : function(responseText) {
                            $('#dailyTraderVolatility').val(parseFloat(responseText).toFixed(2));
                    }
            });
        });
    });   
    /** END OF CALLS TO BLACK SCHOLES SERVLET */
                  
    /**
    * Make a call to the interest rate servlet every time an interest rate parameter is changed.
    */
    $(document).ready(function() {
        $('.int').change(function() {
            $.ajax({
                    url : 'InterestRateServlet',
                    data : {
                            option : $('#instrument').val(),
                            futuresCode : $('#futuresCodeInput').val(),
                            expiry : $('#expirationDate').val()
                    },
                    success : function(responseText) {
                            $('#interestRate').val(responseText);
                    }
            });
        });
    });
    
    /**
    * Make a call to the futures price servlet every time the futures code parameter is changed.
    */
    $(document).ready(function() {
        $('#futuresCodeInput').keyup(function() {
            $.ajax({
                    url : 'FuturesPriceServlet',
                    data : {
                            futuresCode : $('#futuresCodeInput').val()
                    },
                    success : function(responseText) {
                            $('#underlyingFuturesPrice').val(responseText);
                            $('#overrideFuturesPrice').val(responseText);
                    }
            });
        });
    });              
                
    /** FUNCTIONS FOR VALIDITY CHECKS 
    *   Checks to make sure all of the selects have been changed and the text inputs have been filled before allowing the user to finalize the trade.
    */
        $(document).ready(function() {
            var allSelected = true;
            var allFilled = true;
            $("select").change(function() {
                allSelected = true;
                allFilled = true;
                var selects = $("select");
                for(var i = 0; i < selects.length; i++){
                    if(selects[i].disabled === false && selects[i].value === "default"){
                        allSelected = false;
                    }
                }
                var inputs = $("input:text");
                for(var i = 0; i < inputs.length; i++){
                    if(inputs[i].value === "" && inputs[i].disabled === false){
                        allFilled = false;
                    }
                }
                if(allSelected && allFilled){
                    document.getElementById("button").disabled = false;
                } else{
                    document.getElementById("button").disabled = true;
                }
            });
            $("input:text").change(function() {
                allSelected = true;
                allFilled = true;
                var inputs = $("input:text");
                for(var i = 0; i < inputs.length; i++){
                    if(inputs[i].value === "" && inputs[i].disabled === false){
                        allFilled = false;
                    }
                }
                var selects = $("select");
                for(var i = 0; i < selects.length; i++){
                    if(selects[i].disabled === false && selects[i].value === "default"){
                        allSelected = false;
                    }
                } 
                if(allSelected && allFilled){
                    document.getElementById("button").disabled = false;
                } else{
                    document.getElementById("button").disabled = true;
                }
            });
        });
        
        /**
        * Disables enter key from submitting the page. 
        */
        $(document).keypress(
            function(event){
             if (event.which == '13') {
                event.preventDefault();
            }});
</script>