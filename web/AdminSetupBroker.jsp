<%-- 
    Document   : AdminSetupBroker
    Created on : Aug 10, 2016, 3:06:56 PM
    Author     : Dino Martinez
--%>
<%@page import="java.sql.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.Brokers" %>
<%@page import="com.dutchessdevelopers.commoditieswebsite.ChannelPartner" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|Add Broker</title>
         <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body onload="">
        <%
            //Instanciate a ChannelPartner object to access the database of Channel Partners.
            ChannelPartner channelPartner = new ChannelPartner();
            ResultSet cpData = channelPartner.getChannelPartners(); // Create a resultSet Object containing each of the channel partners.
        %>
	<div id="header">
	    <h1>Broker Setup</h1>
            <h2>Enter the following information to setup a broker:</h2>
	</div>
	<div id="central" align="center">
        
        <%Brokers brokers = new Brokers();%>
	    <form name="brokerInfoForm" action="AdminSetupBroker.jsp" method="post">
                <select id="partnerChooser" name="partnerChooser" align="left">
                    <option value='default'>Select A Channel Partner</option>
                    <% while(cpData.next()){ %>
                        <option value="<%= cpData.getString("name") %>"><%= cpData.getString("name") %></option>    
                    <% } %>
                </select>
                <table border="0" cellpadding="15">
                    <tbody>
                        <tr>
                            <td>Company Name: </td>
                            <td><input required type="text" autocomplete="off" name="companyName" id="companyName" value="" size="50" style="padding:10px 0px 10px 0px" /></td>
                        </tr>
                        <tr>
                            <td>Contact Name: </td>
                            <td><input required type="text" autocomplete="off" name="contactName" id="contactName" value="" size="50" style="padding:10px 0px 10px 0px" /></td>
                        </tr>
                    </tbody>
                </table>
                <input id="button" type="submit" value="Submit" name="submitButton" onclick="formSubmission()"/>
            </form>
            
            <%
            //Submit the inputted information IF the button is clicked and the validity is verified through the formSubmission() JS Function.
            if(request.getParameter("submitButton") != null){
                brokers.insertBroker(request.getParameter("companyName"), request.getParameter("contactName"), request.getParameter("partnerChooser"));
            }
            
        %>
	</div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminBrokerOptions.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  brokers.closeConnection();
        channelPartner.closeConnection();
    %>
    <script> 
        function formSubmission(){
            if(document.getElementById("companyName").value === "" 
            || document.getElementById("contactName").value === ""
            || document.getElementById("partnerChooser").value === "default")
            {
                window.alert("Please select a channel partner and fill in all text fields to submit.");
                document.getElementById("button").type = "";
                document.location.href = "AdminSetupBroker.jsp";
            }
            else{
                window.alert("Your broker has been added to the database.");
                document.getElementById("button").type = "submit";
                document.location.href= "AdminBrokerOptions.jsp";
            }
        }
    </script>
</html>