<%-- 
    Document   : AdminViewPartners
    Created on : Aug 10, 2016, 2:43:14 PM
    Author     : Dino martinez
--%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|Channel Partner Page</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
	<div id="header">
	<h1>View, Edit, and Delete Channel Partners</h1>
	</div>
        <div id="central" align="center">
            <%  //Access the database of channel partners and create a resultset containing all Channel Partner info.
                ChannelPartner channelPartner = new ChannelPartner();
                ResultSet cpData = channelPartner.getChannelPartners();
                int cpCount = 0;
                
                //Get the password of the current employee to validate deletions.
                Employee employee = new Employee();
                ResultSet employeeData = employee.getEmployee();
                //Get Username from Session Attribute
                String username = "";
                try{
                    username = session.getAttribute("username").toString();
                }catch(java.lang.NullPointerException e){
                    response.sendRedirect("login.jsp");
                }
                //Get password that corresponds with username.
                String password = "";
                while(employeeData.next()){
                    if(employeeData.getString("username").equals(username)){
                        password = employeeData.getString("password");
                    }
                }
                %>
            <form name="submitForm" action="AdminViewPartners.jsp" method="POST">
                <input type="hidden" id="password" value="<%= password%>" />
            <table id="channelPartnerTable" border="1" cellpadding="15%" style="width:90%">
                <thead>
                    <th>Channel Partner Code</th>
                    <th>Channel Partner Name</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Submit</th>
                    <th>Delete</th>
                </thead>
                <tbody>
                <%  //Create a row in the table for each of the channel partners currently in the database.
                    while(cpData.next()) {
                        cpCount = cpData.getRow(); //Adjust count to current row (used to identify which row the elements are from)
                %>
                <tr>
                    <td><input required type="text" name="<%="code" + cpCount%>" value="<%= cpData.getString("channel_partner_id")%>" size="20px" readonly class="readonly"></td>
                    <td><input required type="text" name="<%= ("name" + cpCount)%>" id="<%= ("name" + cpCount)%>" value="<%= cpData.getString("name")%>" size="20px" /></td>
                    <td><input required type="text" name="<%= ("username" + cpCount)%>" value="<%= cpData.getString("username")%>" size="20px" /></td>
                    <td><input required type="text" name="<%= ("password" + cpCount)%>" value="<%= cpData.getString("password")%>" size="20px" /></td>
                    <td>     
                            <input type="submit" value="Update" name="<%= ("submitButton" + cpCount)%>" />
                    </td>
                    <td>
                            <input type="submit" value="Delete" name="<%= ("deleteButton" + cpCount)%>" id="<%= ("deleteButton" + cpCount)%>" onclick="enterPassword(this)"/>
                            <input type="submit" value="Delete" name="<%= ("trueDeleteButton" + cpCount)%>" id="<%= ("trueDeleteButton" + cpCount)%>" style="display: none"/>
                    </td>
                    
                </tr>
                <% } %>
	    </tbody>
            </table>
            </form>
            <script>
                /**
                * This method is for the purpose of validating the password when a user attempts to delete a broker.
                * It uses prompts to ask a user to enter their password and then confirm that they would like to delete the broker. 
                * @param {element} e the element which triggers the call of the method.
                */
                function enterPassword(e){
                    //USE PROMPT to allow user to enter password.
                    var password = prompt("Please enter your administrator password: ");
                    
                    /**PASSWORD CORRECT**/
                    if(password === document.getElementById("password").value){
                        var deletes = prompt("Are you sure you would like to delete " + document.getElementById('name' + i).value +  " from the database? Please type 'YES' to confirm.");
                        if(deletes === 'YES'){
                            var i = e.id.substring(e.id.length - 1, e.id.length);
                            document.getElementById("deleteButton" + i).style.display = "none";
                            document.getElementById("trueDeleteButton" + i).type = "hidden";
                        }
                    }
                    
                    /**PASSWORD INCORRECT**/
                    else{
                        alert('Password Incorrect.');
                    }
                }
            </script>
            <%
            int count=0;
            cpData.beforeFirst();
            while (cpData.next()){
                count++;
                
                /**
                * UPDATE ANY CHANNEL PARTNER in Database for which the update button was clicked.
                */
                if(request.getParameter("submitButton" + count) != null){
                    channelPartner.updateChannelPartners(
                            cpData.getString("channel_partner_id"),
                            request.getParameter("name" + count),
                            request.getParameter("username" + count),
                            request.getParameter("password" + count),
                            new Timestamp(Calendar.getInstance().getTime().getTime()));
                    response.setHeader("Refresh", "0; URL=AdminViewPartners.jsp");
                }
            }
            count = 0;
            cpData.beforeFirst();
            while (cpData.next()){
                count++;
                
                /**
                * DELETE ANY CHANNEL PARTNER in Database for which the update button was clicked.
                */
                if (request.getParameter("trueDeleteButton" + count) != null){
                    channelPartner.deleteChannelPartner(request.getParameter("code" + count));
                    response.setHeader("Refresh", "0; URL=AdminViewPartners.jsp");
                }
            }
        %>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminPartnerOptions.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  //Close connection with database.
        channelPartner.closeConnection();
    %>
</html>