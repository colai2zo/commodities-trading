<%-- 
    Document   : AdminPositionReport
    Created on : Aug 7, 2016, 2:49:07 PM
    Author     : Lucas
--%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page import="java.text.NumberFormat"%>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Channel Partner Position</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header" align="center">
            <table>
                <tr>
                    <td>
                        <h1>
                            Channel Partner Positions
                        </h1>
                    </td>
                </tr>
            </table>
            
        </div>
        <div id="central" align="center">
            <%
                //Declare instance of number formatter to truncate decimals to two places.
                NumberFormat formatter = NumberFormat.getInstance();
                formatter.setMaximumFractionDigits(2);
                formatter.setMinimumFractionDigits(2);
                //Declare instance of channel partner and create a ResultSet object containing all of the channel partners.
                ChannelPartner channelPartner = new ChannelPartner();
                ResultSet channelPartnerData = channelPartner.getChannelPartners();
                //Declare an instance of Orders and create a ResultSet object containing all of the active orders.
                Orders orders = new Orders();
                ResultSet orderData = orders.getActiveOrders();
                //Declare an instance of Pulled Data and create a ResultSet object containing all of today's data for each of the futures codes.
                PulledData pulledData = new PulledData();
                ResultSet codes = pulledData.getTodaysData();
                %>
                
            <%  /*LAYER ONE: LOOP THROUGH EACH CHANNEL PARTNER 
                  Creates a position table for each Channel partner in the database.
                */
                while(channelPartnerData.next()){
            %>
                <table  border="1" cellpadding="10px" width="70%">
                    <thead>
                        <tr>
                            <th colspan="6" style="font-size: 25px">Position For <%= channelPartnerData.getString("name")%> </th>
                        </tr>
                        <tr>
                            <th>Underlying Futures Code</th>
                            <th>Current Underlying Price</th>
                            <th>Delta</th>
                            <th>Gamma</th>
                            <th>Vega</th>
                            <th>Theta</th>
                        </tr>
                    </thead>
                    <tbody>
            <%      /*LAYER 2: LOOP THROUGH EACH FUTURES CODE
                      Creates a row in the table for each futures code for which the current channel partner currently has active orders.
                    */
                    codes.beforeFirst(); //Brings futures code data iterator back to beginning.
                    while(codes.next()){ //Performs loop for each futures code in today's pulled data.
                        orderData.beforeFirst(); //Brings order data iterator back to beginning.
                        double sumDelta = 0, sumGamma = 0, sumVega = 0, sumTheta = 0; //Declare variables to store sums of Black Scholes values.
                        
                        /*LAYER THREE: LOOP THROUGH EACH ACTIVE ORDER with the current CHANNEL PARTNER and FUTURES CODE
                          Accrue sums of Black Scholes values for every iteration of loop.
                        */
                        while(orderData.next()){ 
                            if(orderData.getString("channel_partner_id").equals(channelPartnerData.getString("channel_partner_id"))
                               && orderData.getString("underlying_futures_code").equals(codes.getString("underlying_futures_code"))){
                                sumDelta += orderData.getDouble("delta");
                                sumGamma += orderData.getDouble("gamma");
                                sumTheta += orderData.getDouble("theta");
                                sumVega += orderData.getDouble("vega");
                            }  
                        }
                        /** DISPLAY All formatted Data for the futures code (AS LONG AS there is at leastt 1 value > 0.) **/
                        if(sumDelta > 0 || sumGamma > 0 || sumVega > 0 || sumTheta > 0){
            %>
                            <tr>
                                <td><%= codes.getString("underlying_futures_code")%></td>
                                <td><%= formatter.format(codes.getDouble("current_underlying_futures_price"))%></td>
                                <td><%= formatter.format(sumDelta)%></td>
                                <td><%= formatter.format(sumGamma)%></td>
                                <td><%= formatter.format(sumVega)%></td>
                                <td><%= formatter.format(sumTheta)%></td>
                            </tr>
            <%
                        }
                    }
            %>
                    </tbody>
                </table>
                <br><br>
            <%
                }
            %>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminReports.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        
    </body>
    <%  /** CLOSE DATABASE CONNECTIONS **/
        channelPartner.closeConnection();
        orders.closeConnection();
        pulledData.closeConnection(); %>
</html>