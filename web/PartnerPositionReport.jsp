<%-- 
    Document   : PartnerPositionReport
    Created on : Aug 7, 2016, 2:49:07 PM
    Modified on : Jan 7, 2017, 6:40:00 AM
    Author     : Lucas
--%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page import="java.text.NumberFormat"%>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Channel Partner Position</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header" align="center">
            <table>
                <tr>
                    <td>
                        <h1>
                            Channel Partner Positions
                        </h1>
                    </td>
                </tr>
            </table>
            
        </div>
        <div id="central" align="center">
            <%
                //Declare instance of number formatter to truncate decimals to two places.
                NumberFormat formatter = NumberFormat.getInstance();
                formatter.setMaximumFractionDigits(2);
                formatter.setMinimumFractionDigits(2);
                
                //Declare instance of channel partner, orders, and pulled data and use them to generate result sets.
                ChannelPartner channelPartner = new ChannelPartner();
                Orders orders = new Orders();
                PulledData pd = new PulledData();
                ResultSet channelPartnerData = channelPartner.getChannelPartners();
                ResultSet orderData = orders.getActiveOrders();
                ResultSet codes = pd.getTodaysData();
                
                //Get the ID of the channel partner by retrieving the session attribute 'username' and converting it to the ID.
                String username = "";
                try{
                    username = session.getAttribute("username").toString();
                }catch(java.lang.NullPointerException e){
                    response.sendRedirect("login.jsp");
                }
                String channelPartnerID = channelPartner.getIDByUsername(username);
                
                //Move Channel Partner result set iterator to the row containing the channel partner that is logged in
                while(channelPartnerData.next()){
                   if(channelPartnerData.getString("channel_partner_id").equals(channelPartnerID)){
                       break;
                   } 
                }
            %>
                <table border="1" cellpadding="10px">
                    <thead>
                        <tr>
                            <th colspan="6" style="font-size: 25px"><%= channelPartnerData.getString("name")%>'s Position</th>
                        </tr>
                        <tr>
                            <th>Underlying Futures Code</th>
                            <th>Current Underlying Price</th>
                            <th>Delta</th>
                            <th>Gamma</th>
                            <th>Vega</th>
                            <th>Theta</th>
                        </tr>
                    </thead>
                    <tbody>
            <%      //LOOP THROUGH EACH FUTURES CODE to generate the rows of the table
                    codes.beforeFirst(); //Move futures code iterator to beginning.
                    while(codes.next()){
                        orderData.beforeFirst();
                        /** CALCULATE THE BLACK SCHOLES SUM FOR ALL ORDERS PLACED WITH THIS CODE**/
                        double sumDelta = 0, sumGamma = 0, sumVega = 0, sumTheta = 0;
                        while(orderData.next()){
                            if(orderData.getString("channel_partner_id").equals(channelPartnerData.getString("channel_partner_id"))
                               && orderData.getString("underlying_futures_code").equals(codes.getString("underlying_futures_code"))){
                                sumDelta += orderData.getDouble("delta");
                                sumGamma += orderData.getDouble("gamma");
                                sumTheta += orderData.getDouble("theta");
                                sumVega += orderData.getDouble("vega");
                            }  
                        }
                        //Display row in table if at least one sum is greater than 0 (there is at least 1 order with that futures code.)
                        if(sumDelta > 0 || sumGamma > 0 || sumVega > 0 || sumTheta > 0){
            %>
                            <tr>
                                <td><%= codes.getString("underlying_futures_code")%></td>
                                <td><%= formatter.format(codes.getDouble("current_underlying_futures_price"))%></td>
                                <td><%= formatter.format(sumDelta)%></td>
                                <td><%= formatter.format(sumGamma)%></td>
                                <td><%= formatter.format(sumVega)%></td>
                                <td><%= formatter.format(sumTheta)%></td>
                            </tr>
            <%
                        }
                    }
            %>
                    </tbody>
                </table>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="PartnerReports.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        
    </body>
    <%  //Close connections to database.
        pd.closeConnection();
        orders.closeConnection();
        channelPartner.closeConnection();
    %>
</html>