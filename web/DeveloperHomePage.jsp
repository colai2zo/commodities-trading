<%-- 
    Document   : DeveloperHomePage
    Created on : Jul 24, 2017, 2:40:05 PM
    Author     : Joey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Developer Page</title>
    </head>
    <body>
        <div style="float:right">
            <form name="logoutForm" id="logoutForm" action="AdminHomePage.jsp" method="POST">
                <input style="background-color:transparent" type="button" value="Logout" name="logout" id="logout" onclick="document.location.href='login.jsp'" />
            </form>
        </div>
        <div id="header">
            <h1 style="text-align: center">Saranac Management Developer's Portal</h1>
        </div>
        <div id="central" align="center">
            <h2 style="text-align: center">Choose from the following developer options:</h2>
            <button id='button' onclick='document.location.href = "DeveloperGuide.jsp"'>Developer's Guide and Notes PDF</button>
            <button id='button' onclick='window.open("http://commoditieswebsite.bitballoon.com/")'>JavaDocs</button>
            <button id='button' onclick='document.location.href = "editDatabase.jsp"'>Direct Database Access</button>
        </div>
    </body>
</html>
