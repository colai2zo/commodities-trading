<%-- 
    Document   : AdminPartnerOptions
    Created on : Aug 10, 2016, 2:27:47 PM
    Author     : Dino martinez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|Channel Partner Page</title>
         <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
	<div id="header">
	    <h1>Channel Partner Options</h1>
	    <h2>Choose from the following options:</h1>
	</div>
	<div id="central" align="center">
 	    <form name="addCPForm" action="AdminSetupPartner.jsp" method="POST">
                <input id="button" type="submit" value="Channel Partner Setup" name="setupPartner"/>
            </form>

            <form name="editAndViewCPForm" action="AdminViewPartners.jsp" method="POST">
                <input id="button" type="submit" value="Edit/View Channel Partners" name="editAndViewPartner" />
            </form>
            
            <form name="brokerOptionsForm" action="AdminBrokerOptions.jsp" method="POST">
                <input id="button" type="submit" value="Broker Options" name="brokerOptions" />
            </form>
	</div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminEmployeePartnerOptions.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>