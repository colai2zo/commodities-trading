<%-- 
    Document   : index
    Created on : Aug 7, 2016, 9:13:17 AM
    Author     : Lucas Carey
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Login</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
 
    </head>
    
    <body>  
        <form id="submit" name="submit" action="login.jsp" method="POST">
            <div align="center"> 
                <h1 style="padding: 0% 0% 15% 0%">Commodities Trading Login</h1>
            </div>
            
            <div align="center">
                <table border="0">
                    <tbody>
                        <tr style="padding-bottom: 20px">
                            <td>Username :</td>
                            <td><input id="username" type="text" autocomplete="off" name="username" value="" size="50" autofocus="autofocus"/></td>
                        </tr>
                        <tr>
                            <td>Password :</td>
                            <td><input id="password" type="password" autocomplete="off" name="password" value="" size="50" /></td>
                        </tr>
                    </tbody>
                </table>
               
             <input id="hidden" type="hidden" name="hidden" value="hidden" />
             <input  type="reset" value="Clear" name="clear" />
             <input  type="submit" value="Submit" name="submit" />
            </div>
        </form>
        <%
            /**
             * Run the following code if the submit button is clicked.
             */
            if(request.getParameter("submit")!=null){
                /**
                *ACCESS THE DATABASES 
                */
                ChannelPartner channelPartner = new ChannelPartner();
                ResultSet cpData = channelPartner.getChannelPartners();
                Employee employee = new Employee();
                ResultSet emData = employee.getEmployee();
                
                //Evaluate if the developer username and password are entered.
                if(request.getParameter("username").equals("developer") && request.getParameter("password").equals("developer")){
                    response.sendRedirect("DeveloperHomePage.jsp");
                }
                
                //Evaluate if the username and password entered are a match with any of those of the channel partners in the database.
                while(cpData.next()){
                    if(cpData.getString("username").equals(request.getParameter("username")) &&
                        cpData.getString("password").equals(request.getParameter("password"))){
                        session.setAttribute("username", request.getParameter("username"));
                        response.sendRedirect("PartnerHomePage.jsp"); //REDIRECT TO PARTNER HOME PAGE
                        session.setMaxInactiveInterval(6000);
                    }}
                
                //Evaluate if the username and password entered are a match with any of those of the employees in the database.
                while(emData.next()){
                    if( emData.getString("username").equals(request.getParameter("username")) &&
                        emData.getString("password").equals(request.getParameter("password"))){
                        session.setAttribute("username", request.getParameter("username"));
                        response.sendRedirect("AdminHomePage.jsp"); //REDIRECT TO ADMIN HOME PAGE
                        session.setMaxInactiveInterval(6000);
                        
                        //Set Full Book Column Session attributes to TRUE
                        session.setAttribute("channelPartner", "true");
                        session.setAttribute("farmID", "true");
                        session.setAttribute("orderNumber", "true");
                        session.setAttribute("status", "true");
                        session.setAttribute("tradeDate", "true");
                        session.setAttribute("transactionType", "true");
                        session.setAttribute("commodityName", "true");
                        session.setAttribute("futuresCode", "true");
                        session.setAttribute("clientMonth", "true");
                        session.setAttribute("expirationDate", "true");
                        session.setAttribute("instrument", "true");
                        session.setAttribute("product", "true");
                        session.setAttribute("frequency", "true");
                        session.setAttribute("orderVolumeUnits", "true");
                        session.setAttribute("unitType", "true");
                        session.setAttribute("orderVolumeContracts", "true");
                        session.setAttribute("executedStrike", "true");
                        session.setAttribute("executedPricePerUnit", "true");
                        session.setAttribute("executedTotalCost", "true");
                        session.setAttribute("futuresPrice", "true");
                        session.setAttribute("volatility", "true");
                        session.setAttribute("delta", "true");
                        session.setAttribute("gamma", "true");
                        session.setAttribute("vega", "true");
                        session.setAttribute("theta", "true");
                        session.setAttribute("broker", "true");
                        session.setAttribute("contractID", "true");
                        session.setAttribute("action", "true");
                    }
                } 
                //Close connections with database.
                employee.closeConnection();
                channelPartner.closeConnection();
                
                //IF SUBMIT BUTTON IS CLICKED AND NOT MATCH WAS FOUND, AN ERROR MESSAGE IS DISPLAYED.
        %>
                <script>
                    window.alert("The username and password combination entered is incorrect.");
                </script>
        <%
            }    
        %>
    </body>
</html>