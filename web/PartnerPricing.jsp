<%-- 
    Document   : PartnerPricing.jsp
    Created on : Aug 7, 2016, 11:59:11 AM
    Author     : Dino Martinez
--%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.jquantlib.time.Date"%>
<%@page import="org.jquantlib.time.calendars.UnitedKingdom"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page import="java.text.NumberFormat"%> 
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <style>
    tr:nth-child(even) {
    background-color: rgba(50,200,200,.5);
    }
    </style>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
        <title>Commodities Trading | today's specs</title>
        <script src="validityCheck.js"></script>
        <script src="http://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
    </head>
    <body onload="setChoosers();checkChoosers();">
        <%
            /**
             * CHECK TO MAKE SURE THAT IT IS, INDEED, A BUSINESS DAY
             */
            if(new UnitedKingdom().isBusinessDay(new org.jquantlib.time.Date(new java.util.Date()))){
                
            /**
             * DECLATION OF NECESSARY OBJECTS, VARIABLES, ETC.
             */
            //Create the number formatter to keep decimal places to two.
            NumberFormat formatter = NumberFormat.getInstance();
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);
            Calendar cal = Calendar.getInstance(); //Create calendar.
            
            //Access the Farmers and Pricing Database.
            Farmers farmers = new Farmers();
            Pricing pricing = new Pricing();
            ResultSet farmData = farmers.getFarmers();

            //Assess the commodity selected if it has been (default = rapeseed)
            String commodity = "Euronext Rapeseed";
            if(request.getParameter("ChooseCommodity") != null)
                commodity = request.getParameter("ChooseCommodity");
            
            //Assess who is the channel partner tat is currently logged in.
            String username = "";
            String pID = "";
            try{
                username = session.getAttribute("username").toString();
                pID = new ChannelPartner().getIDByUsername(username);
            }catch(java.lang.NullPointerException e){
                response.sendRedirect("login.jsp");
            }
            
            //Assess the farm selected if it has been (default = "default)
            String farmValue = "default";
            if(request.getParameter("ChooseFarmer") != null){
                farmValue = request.getParameter("ChooseFarmer");
            }
        %>
        
        <div id="header" align="center" style="padding-bottom: 20px">
            <h1>Channel Partner Offers</h1>
            <h1>Today's Pricing</h1>
            <form id="commodityForm" name="CommodityForm" action="PartnerPricing.jsp" method="POST" style="padding-top: 20px;">
                <select id="farmerChooser" name="ChooseFarmer" align="left">
                    <option value="default">Select A Farm</option>
                <%
                    //Loop through the farms that belong to the logged in channel partner, and make each an option in the select.
                    while(farmData.next()){
                        if(farmData.getString("channel_partner_id").equals(pID)){
                %>
                            <option value="<%= farmData.getString("farmer_id")%>"><%= farmData.getString("name") + " (" + farmData.getString("farmer_id") + ")"%></option>
                <%        
                        }
                    }
                %>
                </select>

                <select id="commodityChooser" name="ChooseCommodity" align="right" value="" onChange="commoditySelected()" disabled="true">
                   <option value="Euronext Rapeseed">Euronext Rapeseed (IJ)</option>
                   <option value="Euronext Wheat">Euronext Wheat (CA)</option>
                   <option value="Soybean Meal">Soybean Meal (SM)</option>
                </select>
            </form>
        </div>
        <form name="SubmitForm" action="PartnerSubmitOrder.jsp" method="POST" style="padding-top: 20px;">
            <input type="hidden" name="farmerID" id="farmerID" value="" />
            <input type="hidden" name="commodityName" id="commodityName" value="Euronext Rapeseed" />
            <input type="hidden" class="unit" name="unit_type" value="" />
        <div id="central" align="center" >
            <h4>Enhanced Market Average</h4>
            <table class="pricing" id="theTable1" border="1" cellpadding="15%" style="width:90%" >
            <thead>
                <tr>
                    <th colspan="12">Current Market</th>
                </tr>
                <tr>
                    <th>Client Month</th>
                    <th>Expiration Date</th>
                    <th>Underlying Futures Code</th>
                    <th>Underlying Commodity Name</th>
                    <th>Current Underlying Futures Price</th>
                    <th>Indicative Product Strike Price</th>
                    <th>Product Current Price / Unit</th>
                    <th>Order Volume Units</th>
                    <th>Unit Type</th>
                    <th>Order Volume Contracts</th>
                    <th>Execution Product Total Cost</th>
                    <th>Farm Physical Contract ID</th>
                </tr>
            </thead>
            <tbody>
                <%  int strikeShiftPositive = 0;
                    int count = 0;
                    //Create an 8 row table to display the Current Market, Enhanced Market Average products.
                    for(int i = 0; i < 8; i++){
                        int month = cal.get(Calendar.MONTH) + 1 + i; //Get the current month in the table.
                        String code = Orders.getFuturesCode(month, commodity); //Get the futures code in the current month.
                        ResultSet pricingDataEMA = pricing.getCurrentPricingByCode(code, "daily", month); //Get the correct pricing.
                        //Only use one whose strike shift is 0.
                        while(pricingDataEMA.next()){
                            if(pricingDataEMA.getInt("strike_shift") == 0){
                            count++;
                %>
                            <tr>
                                <td><%= pricingDataEMA.getString("client_month")%><input type="hidden" name="<%= "clientMonth" + count%>" value="<%= pricingDataEMA.getString("client_month")%>" /></td>
                                <td><%= pricingDataEMA.getDate("expiration_date")%><input type="hidden" name="<%= "expiration" + count%>" value="<%= pricingDataEMA.getDate("expiration_date")%>" /></td>
                                <td><%= pricingDataEMA.getString("underlying_futures_code")%><input type="hidden" name="<%= "futuresCode" + count%>" value="<%= pricingDataEMA.getString("underlying_futures_code")%>" /></td>
                                <td><%= pricingDataEMA.getString("underlying_commodity_name")%><input type="hidden" name="<%= "commodity" + count%>" value="<%= pricingDataEMA.getString("underlying_commodity_name")%>" /></td>
                                <td><%= formatter.format(pricingDataEMA.getDouble("current_underlying_futures_price"))%><input type="hidden" name="<%= "futures" + count%>" value="<%= pricingDataEMA.getDouble("current_underlying_futures_price")%>" /></td>
                                <td><%= formatter.format(pricingDataEMA.getDouble("indicative_product_strike_price"))%><input type="hidden" name="<%= "strike" + count%>" value="<%= pricingDataEMA.getDouble("indicative_product_strike_price")%>" /></td>
                                <td><%= pricingDataEMA.getDouble("product_current_price_per_unit")%><input type="hidden" name="<%= "product_current_price_per_unit" + count%>" value="<%= pricingDataEMA.getDouble("product_current_price_per_unit")%>" /></td>
                                <td>
                                    <input class="editable" style="width:100px; padding-top: 10px; padding-bottom: 10px;" type="text" autocomplete="off" name="<%= "orderVolumeQuantity" + count%>" value="0" onchange="calcContractsAndTotal(<%=count%>)" onkeyup="checkNum(this)"/>
                                </td>
                                <td><input class="unit readonly" value="" type="text" autocomplete="off" readonly/></td>
                                 <td><input id="<%= "contracts" + count%>" name="<%= "contracts" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input id="<%= "finalprice" + count%>" name="<%= "finalprice" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input name="<%= "contractid" + count%>" class="editable" name="<%="farmerPhysicalContractId" + count%>" type="text" autocomplete="off" onkeyup="maxChars(this,45)"/></td>
                            </tr>
                            <input type="hidden" name="<%= "frequency" + count%>" value="<%= pricingDataEMA.getString("frequency")%>" />
                            <input type="hidden" name="<%= "current_implied_volatility" + count%>" value="<%= pricingDataEMA.getDouble("current_implied_volatility")%>" />
                            <input type="hidden" name="<%= "daily_trader_volatility" + count%>" value="<%= pricingDataEMA.getDouble("daily_trader_volatility")%>" />
                            <input type="hidden" name="<%= "strike_shift" + count%>" value="<%= pricingDataEMA.getInt("strike_shift")%>" />
                <%          } 
                            //Find the positive strike shift value.
                            if(pricingDataEMA.getString("frequency").equals("Daily") && pricingDataEMA.getInt("strike_shift") > 0){
                                strikeShiftPositive = pricingDataEMA.getInt("strike_shift");
                            }
                        } 
                    }
                %>
            </tbody>
        </table>
            <br><br>
            <table class="pricing" id="theTable2" border="1" cellpadding="15%" style="width:90%">
            <thead>
                <tr>
                    <th colspan="12">Strike Shift: + <%= strikeShiftPositive%></th>
                </tr>
                <tr>
                    <th>Client Month</th>
                    <th>Expiration Date</th>
                    <th>Underlying Futures Code</th>
                    <th>Underlying Commodity Name</th>
                    <th>Current Underlying Futures Price</th>
                    <th>Indicative Product Strike Price</th>
                    <th>Product Current Price / Unit</th>
                    <th>Order Volume Units</th>
                    <th>Unit Type</th>
                    <th>Order Volume Contracts</th>
                    <th>Execution Product Total Cost</th>
                    <th>Farm Physical Contract ID</th>
                </tr>
            </thead>
            <tbody>
                <%  int strikeShiftNegative = 0;
                    //Create an 8 row table to display the Positive Strike shift, Enhanced Market Average products.
                    for(int i = 0; i < 8; i++){
                        int month = cal.get(Calendar.MONTH) + 1 + i;
                        String code = Orders.getFuturesCode(month, commodity);
                        ResultSet pricingDataEMA = pricing.getCurrentPricingByCode(code, "daily", month);
                        while(pricingDataEMA.next()){
                            if(pricingDataEMA.getInt("strike_shift") > 0){
                            count++;
                %>
                             <tr>
                                <td><%= pricingDataEMA.getString("client_month")%><input type="hidden" name="<%= "clientMonth" + count%>" value="<%= pricingDataEMA.getString("client_month")%>" /></td>
                                <td><%= pricingDataEMA.getDate("expiration_date")%><input type="hidden" name="<%= "expiration" + count%>" value="<%= pricingDataEMA.getDate("expiration_date")%>" /></td>
                                <td><%= pricingDataEMA.getString("underlying_futures_code")%><input type="hidden" name="<%= "futuresCode" + count%>" value="<%= pricingDataEMA.getString("underlying_futures_code")%>" /></td>
                                <td><%= pricingDataEMA.getString("underlying_commodity_name")%><input type="hidden" name="<%= "commodity" + count%>" value="<%= pricingDataEMA.getString("underlying_commodity_name")%>" /></td>
                                <td><%= formatter.format(pricingDataEMA.getDouble("current_underlying_futures_price"))%><input type="hidden" name="<%= "futures" + count%>" value="<%= pricingDataEMA.getDouble("current_underlying_futures_price")%>" /></td>
                                <td><%= formatter.format(pricingDataEMA.getDouble("indicative_product_strike_price"))%><input type="hidden" name="<%= "strike" + count%>" value="<%= pricingDataEMA.getDouble("indicative_product_strike_price")%>" /></td>
                                <td><%= pricingDataEMA.getDouble("product_current_price_per_unit")%><input type="hidden" name="<%= "product_current_price_per_unit" + count%>" value="<%= pricingDataEMA.getDouble("product_current_price_per_unit")%>" /></td>
                                <td>
                                    <input class="editable" style="width:100px; padding-top: 10px; padding-bottom: 10px;" type="text" autocomplete="off" name="<%= "orderVolumeQuantity" + count%>" value="0" onchange="calcContractsAndTotal(<%=count%>)" onkeyup="checkNum(this)"/>
                                </td>
                                <td><input class="unit readonly" value="" type="text" autocomplete="off" readonly class="readonly" /></td>
                                 <td><input id="<%= "contracts" + count%>" name="<%= "contracts" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input id="<%= "finalprice" + count%>" name="<%= "finalprice" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input name="<%= "contractid" + count%>" class="editable" type="text" autocomplete="off" onkeyup="maxChars(this,45)"/></td>
                            </tr>
                            <input type="hidden" name="<%= "frequency" + count%>" value="<%= pricingDataEMA.getString("frequency")%>" />
                            <input type="hidden" name="<%= "current_implied_volatility" + count%>" value="<%= pricingDataEMA.getDouble("current_implied_volatility")%>" />
                            <input type="hidden" name="<%= "daily_trader_volatility" + count%>" value="<%= pricingDataEMA.getDouble("daily_trader_volatility")%>" />
                            <input type="hidden" name="<%= "strike_shift" + count%>" value="<%= pricingDataEMA.getInt("strike_shift")%>" />
                <%          }
                            if(pricingDataEMA.getString("frequency").equals("Daily") && pricingDataEMA.getInt("strike_shift") < 0){
                                strikeShiftNegative = pricingDataEMA.getInt("strike_shift");
                            }
                        }
                    }
                %>
            </tbody>
        </table>
            <br><br>
            <table class="pricing" id="theTable3" border="1" cellpadding="15%" style="width:90%">
            <thead>
                <tr>
                    <th colspan="12">Strike Shift: <%= strikeShiftNegative%></th>
                </tr>
                <tr>
                   <th>Client Month</th>
                    <th>Expiration Date</th>
                    <th>Underlying Futures Code</th>
                    <th>Underlying Commodity Name</th>
                    <th>Current Underlying Futures Price</th>
                    <th>Indicative Product Strike Price</th>
                    <th>Product Current Price / Unit</th>
                    <th>Order Volume Units</th>
                    <th>Unit Type</th>
                    <th>Order Volume Contracts</th>
                    <th>Execution Product Total Cost</th>
                    <th>Farm Physical Contract ID</th>
                </tr>
            </thead>
            <tbody>
                <%  //Create an 8 row table to display the Negative strike shift, Enhanced Market Average products.
                    for(int i = 0; i < 8; i++){
                        int month = cal.get(Calendar.MONTH) + 1 + i;
                        String code = Orders.getFuturesCode(month, commodity);
                        ResultSet pricingDataEMA = pricing.getCurrentPricingByCode(code, "daily", month);
                        while(pricingDataEMA.next()){
                            if(pricingDataEMA.getInt("strike_shift") < 0){
                            count++;
                %>
                             <tr>
                                <td><%= pricingDataEMA.getString("client_month")%><input type="hidden" name="<%= "clientMonth" + count%>" value="<%= pricingDataEMA.getString("client_month")%>" /></td>
                                <td><%= pricingDataEMA.getDate("expiration_date")%><input type="hidden" name="<%= "expiration" + count%>" value="<%= pricingDataEMA.getDate("expiration_date")%>" /></td>
                                <td><%= pricingDataEMA.getString("underlying_futures_code")%><input type="hidden" name="<%= "futuresCode" + count%>" value="<%= pricingDataEMA.getString("underlying_futures_code")%>" /></td>
                                <td><%= pricingDataEMA.getString("underlying_commodity_name")%><input type="hidden" name="<%= "commodity" + count%>" value="<%= pricingDataEMA.getString("underlying_commodity_name")%>" /></td>
                                <td><%= pricingDataEMA.getDouble("current_underlying_futures_price")%><input type="hidden" name="<%= "futures" + count%>" value="<%= pricingDataEMA.getDouble("current_underlying_futures_price")%>" /></td>
                                <td><%= formatter.format(pricingDataEMA.getDouble("indicative_product_strike_price"))%><input type="hidden" name="<%= "strike" + count%>" value="<%= pricingDataEMA.getDouble("indicative_product_strike_price")%>" /></td>
                                <td><%= formatter.format(pricingDataEMA.getDouble("product_current_price_per_unit"))%><input type="hidden" name="<%= "product_current_price_per_unit" + count%>" value="<%= pricingDataEMA.getDouble("product_current_price_per_unit")%>" /></td>
                                <td>
                                    <input class="editable" style="width:100px; padding-top: 10px; padding-bottom: 10px;" type="text" autocomplete="off" name="<%= "orderVolumeQuantity" + count%>" value="0" onchange="calcContractsAndTotal(<%=count%>)" onkeyup="checkNum(this)"/>
                                </td>
                                <td><input class="unit readonly" value="" type="text" readonly class="readonly"/></td>
                                 <td><input id="<%= "contracts" + count%>" name="<%= "contracts" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input id="<%= "finalprice" + count%>" name="<%= "finalprice" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input name="<%= "contractid" + count%>" class="editable" type="text" autocomplete="off" onkeyup="maxChars(this,45)"/></td>
                            </tr>
                            <input type="hidden" name="<%= "frequency" + count%>" value="<%= pricingDataEMA.getString("frequency")%>" />
                            <input type="hidden" name="<%= "current_implied_volatility" + count%>" value="<%= pricingDataEMA.getDouble("current_implied_volatility")%>" />
                            <input type="hidden" name="<%= "daily_trader_volatility" + count%>" value="<%= pricingDataEMA.getDouble("daily_trader_volatility")%>" />
                            <input type="hidden" name="<%= "strike_shift" + count%>" value="<%= pricingDataEMA.getInt("strike_shift")%>" />
                <%          }
                        }
                    }
                %>
            </tbody>
            </table><br><br>
        
            </div>
            
            
            <div id="central" align="center" >
                <h4>Freedom Reprice</h4>
            <table class="pricing" id="theTable1" border="1" cellpadding="15%" style="width:90%" >
            <thead>
                <tr>
                    <th colspan="12">Current Market</th>
                </tr>
                <tr>
                   <th>Client Month</th>
                    <th>Expiration Date</th>
                    <th>Underlying Futures Code</th>
                    <th>Underlying Commodity Name</th>
                    <th>Current Underlying Futures Price</th>
                    <th>Indicative Product Strike Price</th>
                    <th>Product Current Price / Unit</th>
                    <th>Order Volume Units</th>
                    <th>Unit Type</th>
                    <th>Order Volume Contracts</th>
                    <th>Execution Product Total Cost</th>
                    <th>Farm Physical Contract ID</th>
                </tr>
            </thead>
            <tbody>
                <%  
                    //Create an 8 row table to display the current market, freedom reprice products.
                    strikeShiftPositive = 0;
                    for(int i = 0; i < 8; i++){
                        int month = cal.get(Calendar.MONTH) + 1 + i;
                        String code = Orders.getFuturesCode(month, commodity);
                        ResultSet pricingDataFR = pricing.getCurrentPricingByCode(code, "bullet", month);
                        while(pricingDataFR.next()){
                            if(pricingDataFR.getInt("strike_shift") == 0){
                            count++;
                %>
                            <tr>
                                <td><%= pricingDataFR.getString("client_month")%><input type="hidden" name="<%= "clientMonth" + count%>" value="<%= pricingDataFR.getString("client_month")%>" /></td>
                                <td><%= pricingDataFR.getDate("expiration_date")%><input type="hidden" name="<%= "expiration" + count%>" value="<%= pricingDataFR.getDate("expiration_date")%>" /></td>
                                <td><%= pricingDataFR.getString("underlying_futures_code")%><input type="hidden" name="<%= "futuresCode" + count%>" value="<%= pricingDataFR.getString("underlying_futures_code")%>" /></td>
                                <td><%= pricingDataFR.getString("underlying_commodity_name")%><input type="hidden" name="<%= "commodity" + count%>" value="<%= pricingDataFR.getString("underlying_commodity_name")%>" /></td>
                                <td><%= formatter.format(pricingDataFR.getDouble("current_underlying_futures_price"))%><input type="hidden" name="<%= "futures" + count%>" value="<%= pricingDataFR.getDouble("current_underlying_futures_price")%>" /></td>
                                <td><%= formatter.format(pricingDataFR.getDouble("indicative_product_strike_price"))%><input type="hidden" name="<%= "strike" + count%>" value="<%= pricingDataFR.getDouble("indicative_product_strike_price")%>" /></td>
                                <td><%= pricingDataFR.getDouble("product_current_price_per_unit")%><input type="hidden" name="<%= "product_current_price_per_unit" + count%>" value="<%= pricingDataFR.getDouble("product_current_price_per_unit")%>" /></td>
                                <td>
                                    <input class="editable" style="width:100px; padding-top: 10px; padding-bottom: 10px;" type="text" autocomplete="off" name="<%= "orderVolumeQuantity" + count%>" value="0" onchange="calcContractsAndTotal(<%=count%>)" onkeyup="checkNum(this)"/>
                                </td>
                                <td><input class="unit readonly" value="" type="text" autocomplete="off" readonly class="readonly"/></td>
                                 <td><input id="<%= "contracts" + count%>" name="<%= "contracts" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input id="<%= "finalprice" + count%>" name="<%= "finalprice" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input name="<%= "contractid" + count%>" class="editable" type="text" autocomplete="off" onkeyup="maxChars(this,45)"/></td>
                            </tr>
                            <input type="hidden" name="<%= "frequency" + count%>" value="<%= pricingDataFR.getString("frequency")%>" />
                            <input type="hidden" name="<%= "current_implied_volatility" + count%>" value="<%= pricingDataFR.getDouble("current_implied_volatility")%>" />
                            <input type="hidden" name="<%= "daily_trader_volatility" + count%>" value="<%= pricingDataFR.getDouble("daily_trader_volatility")%>" />
                            <input type="hidden" name="<%= "strike_shift" + count%>" value="<%= pricingDataFR.getInt("strike_shift")%>" />
                <%          }
                            if(pricingDataFR.getString("frequency").equals("Bullet") && pricingDataFR.getInt("strike_shift") > 0){
                            strikeShiftPositive = pricingDataFR.getInt("strike_shift");
                            }
                        }   
                    }
                %>
            </tbody>
        </table>
            <br><br>
            <table class="pricing" id="theTable2" border="1" cellpadding="15%" style="width:90%">
            <thead>
                <tr>
                    <th colspan="12">Strike Shift: + <%= strikeShiftPositive%></th>
                </tr>
                <tr>
                    <th>Client Month</th>
                    <th>Expiration Date</th>
                    <th>Underlying Futures Code</th>
                    <th>Underlying Commodity Name</th>
                    <th>Current Underlying Futures Price</th>
                    <th>Indicative Product Strike Price</th>
                    <th>Product Current Price / Unit</th>
                    <th>Order Volume Units</th>
                    <th>Unit Type</th>
                    <th>Order Volume Contracts</th>
                    <th>Execution Product Total Cost</th>
                    <th>Farm Physical Contract ID</th>
                </tr>
            </thead>
            <tbody>
                <%  //Create an 8 row table to display the positive strike shift, freedom reprice products.
                    strikeShiftNegative = 0; //negative strike shift.
                    for(int i = 0; i < 8; i++){
                        int month = cal.get(Calendar.MONTH) + 1 + i;
                        String code = Orders.getFuturesCode(month, commodity);
                        ResultSet pricingDataFR = pricing.getCurrentPricingByCode(code, "bullet", month);
                        while(pricingDataFR.next()){
                            if(pricingDataFR.getInt("strike_shift") > 0){
                            count++;
                %>
                             <tr>
                                <td><%= pricingDataFR.getString("client_month")%><input type="hidden" name="<%= "clientMonth" + count%>" value="<%= pricingDataFR.getString("client_month")%>" /></td>
                                <td><%= pricingDataFR.getDate("expiration_date")%><input type="hidden" name="<%= "expiration" + count%>" value="<%= pricingDataFR.getDate("expiration_date")%>" /></td>
                                <td><%= pricingDataFR.getString("underlying_futures_code")%><input type="hidden" name="<%= "futuresCode" + count%>" value="<%= pricingDataFR.getString("underlying_futures_code")%>" /></td>
                                <td><%= pricingDataFR.getString("underlying_commodity_name")%><input type="hidden" name="<%= "commodity" + count%>" value="<%= pricingDataFR.getString("underlying_commodity_name")%>" /></td>
                                <td><%= formatter.format(pricingDataFR.getDouble("current_underlying_futures_price"))%><input type="hidden" name="<%= "futures" + count%>" value="<%= pricingDataFR.getDouble("current_underlying_futures_price")%>" /></td>
                                <td><%= formatter.format(pricingDataFR.getDouble("indicative_product_strike_price"))%><input type="hidden" name="<%= "strike" + count%>" value="<%= pricingDataFR.getDouble("indicative_product_strike_price")%>" /></td>
                                <td><%= pricingDataFR.getDouble("product_current_price_per_unit")%><input type="hidden" name="<%= "product_current_price_per_unit" + count%>" value="<%= pricingDataFR.getDouble("product_current_price_per_unit")%>" /></td>
                                <td>
                                    <input class="editable" style="width:100px; padding-top: 10px; padding-bottom: 10px;" type="text" autocomplete="off" name="<%= "orderVolumeQuantity" + count%>" value="0" onchange="calcContractsAndTotal(<%=count%>)" onkeyup="checkNum(this)"/>
                                </td>
                                <td><input class="unit readonly" value="" type="text" autocomplete="off" readonly class="readonly"/></td>
                                 <td><input id="<%= "contracts" + count%>" name="<%= "contracts" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input id="<%= "finalprice" + count%>" name="<%= "finalprice" + count%>" value="0" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input name="<%= "contractid" + count%>" class="editable" type="text" autocomplete="off" onkeyup="maxChars(this,45)"/></td>
                            </tr>
                            <input type="hidden" name="<%= "frequency" + count%>" value="<%= pricingDataFR.getString("frequency")%>" />
                            <input type="hidden" name="<%= "current_implied_volatility" + count%>" value="<%= pricingDataFR.getDouble("current_implied_volatility")%>" />
                            <input type="hidden" name="<%= "daily_trader_volatility" + count%>" value="<%= pricingDataFR.getDouble("daily_trader_volatility")%>" />
                            <input type="hidden" name="<%= "strike_shift" + count%>" value="<%= pricingDataFR.getInt("strike_shift")%>" />
                <%          }
                            if(pricingDataFR.getString("frequency").equals("Bullet") && pricingDataFR.getInt("strike_shift") < 0){
                            strikeShiftNegative = pricingDataFR.getInt("strike_shift");
                            }
                        }
                    }    
                %>
            </tbody>
        </table>
            <br><br>
            <table class="pricing" id="theTable3" border="1" cellpadding="15%" style="width:90%">
            <thead>
                <tr>
                    <th colspan="12">Strike Shift: <%= strikeShiftNegative%></th>
                </tr>
                <tr>
                   <th>Client Month</th>
                    <th>Expiration Date</th>
                    <th>Underlying Futures Code</th>
                    <th>Underlying Commodity Name</th>
                    <th>Current Underlying Futures Price</th>
                    <th>Indicative Product Strike Price</th>
                    <th>Product Current Price / Unit</th>
                    <th>Order Volume Units</th>
                    <th>Unit Type</th>
                    <th>Order Volume Contracts</th>
                    <th>Execution Product Total Cost</th>
                    <th>Farm Physical Contract ID</th>
                </tr>
            </thead>
            <tbody>
                <%
                    //Create an 8 row table to display the positive strike shift, freedom reprice products.
                    for(int i = 0; i < 8; i++){
                        int month = cal.get(Calendar.MONTH) + 1 + i;
                        String code = Orders.getFuturesCode(month, commodity);
                        ResultSet pricingDataFR = pricing.getCurrentPricingByCode(code, "bullet", month);
                        while(pricingDataFR.next()){
                            if(pricingDataFR.getInt("strike_shift") < 0){
                            count++;
                %>
                            <tr>
                                <td><%= pricingDataFR.getString("client_month")%><input type="hidden" name="<%= "clientMonth" + count%>" value="<%= pricingDataFR.getString("client_month")%>" /></td>
                                <td><%= pricingDataFR.getDate("expiration_date")%><input type="hidden" name="<%= "expiration" + count%>" value="<%= pricingDataFR.getDate("expiration_date")%>" /></td>
                                <td><%= pricingDataFR.getString("underlying_futures_code")%><input type="hidden" name="<%= "futuresCode" + count%>" value="<%= pricingDataFR.getString("underlying_futures_code")%>" /></td>
                                <td><%= pricingDataFR.getString("underlying_commodity_name")%><input type="hidden" name="<%= "commodity" + count%>" value="<%= pricingDataFR.getString("underlying_commodity_name")%>" /></td>
                                <td><%= formatter.format(pricingDataFR.getDouble("current_underlying_futures_price"))%><input type="hidden" name="<%= "futures" + count%>" value="<%= pricingDataFR.getDouble("current_underlying_futures_price")%>" /></td>
                                <td><%= formatter.format(pricingDataFR.getDouble("indicative_product_strike_price"))%><input type="hidden" name="<%= "strike" + count%>" value="<%= pricingDataFR.getDouble("indicative_product_strike_price")%>" /></td>
                                <td><%= pricingDataFR.getDouble("product_current_price_per_unit")%><input type="hidden" name="<%= "product_current_price_per_unit" + count%>" value="<%= pricingDataFR.getDouble("product_current_price_per_unit")%>" /></td>
                                <td>
                                    <input class="editable" style="width:100px; padding-top: 10px; padding-bottom: 10px;" type="text" autocomplete="off" name="<%= "orderVolumeQuantity" + count%>" value="0" onchange="calcContractsAndTotal(<%=count%>)" onkeyup="checkNum(this)"/>
                                </td>
                                <td><input class="unit readonly" value="" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input id="<%= "contracts" + count%>" name="<%= "contracts" + count%>" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input id="<%= "finalprice" + count%>" name="<%= "finalprice" + count%>" type="text" autocomplete="off" readonly class="readonly"/></td>
                                <td><input name="<%= "contractid" + count%>" class="editable" type="text" autocomplete="off" onkeyup="maxChars(this,45)"/></td>
                            </tr>
                            <input type="hidden" name="<%= "frequency" + count%>" value="<%= pricingDataFR.getString("frequency")%>" />
                            <input type="hidden" name="<%= "current_implied_volatility" + count%>" value="<%= pricingDataFR.getDouble("current_implied_volatility")%>" />
                            <input type="hidden" name="<%= "daily_trader_volatility" + count%>" value="<%= pricingDataFR.getDouble("daily_trader_volatility")%>" />
                            <input type="hidden" name="<%= "strike_shift" + count%>" value="<%= pricingDataFR.getInt("strike_shift")%>" />
                <%          }
                        }
                    }
                %>
            </tbody>
            </table><br><br>
            <input type="hidden" name="count" value="<%= count%>" />
            
            
            </div>
        
            <div class="fixedCenter" align="center">
                <input disabled class="button" id="previewButton" type="button" value="Finalize Offer" name="finalizeButton" onclick="makeReadonly()"/>
                <input id="button" type="button" value="Return to Edit" name="returnButton" style="display:none" onclick="makeEditable()"/>
                <input id="button" type="submit" value="Submit Offer" name="submitButton" style="display:none"/>
            </div>
        </form>    
            <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
            <script>
                /**
                * Changes the style of all of the editable text fields to make them readonly
                * and to appear as normal text.
                * Additionally removes the "preview changes" button, and makes the submission and return to edit buttons appear.
                */
                function makeReadonly(){
                    var editables = document.getElementsByClassName("editable");
                    for(var i = 0; i < editables.length; i++){
                        editables[i].readOnly = true;
                        editables[i].style.backgroundColor = "transparent";
                        editables[i].style.border = "0px";
                    }
                    document.getElementsByName("finalizeButton")[0].style.display = "none";
                    document.getElementsByName("submitButton")[0].style.display = "block";
                    document.getElementsByName("returnButton")[0].style.display = "block";
                }
                
                /**
                * Changes the style of all of the readonly editable text fields to make them editable
                * and to appear as text boxes.
                * Additionally removes the submission and return to edit buttons, and makes the "preview changes" button appear.
                */
                function makeEditable(){
                    var editables = document.getElementsByClassName("editable");
                    for(var i = 0; i < editables.length; i++){
                        editables[i].readOnly = false;
                        editables[i].style.backgroundColor = "white";
                        editables[i].style.border = "1px";
                    }
                    document.getElementsByName("finalizeButton")[0].style.display = "block";
                    document.getElementsByName("submitButton")[0].style.display = "none";
                    document.getElementsByName("returnButton")[0].style.display = "none";
                }
                
                /**
                 * Function run on load of the page.
                 * Assigns the value of the commodity and farm choosers based upon what was selected before the page submission.
                 * Defaults: Euronext Rapeseed and Select a Farm.
                 * Also assigns the unit type based upon the commodity.
                 */
                function setChoosers(){
                    var commodityChooser = document.getElementById("commodityChooser");
                    var selection = "";
                    for (var i = 0; i < commodityChooser.options.length; i++) {
                        if (commodityChooser.options[i].value === "<%= commodity%>"){
                            commodityChooser.options[i].selected = true;
                            selection = commodityChooser.options[i].value;
                        }
                    }
                    var farmChooser = document.getElementById("farmerChooser");
                    for (var i = 0; i < farmChooser.options.length; i++) {
                        if (farmChooser.options[i].value === "<%= farmValue%>")
                            farmChooser.options[i].selected = true;
                    }
                    
                    var units = document.getElementsByClassName("unit");
                    for(var i = 0; i < units.length; i++){
                        if(selection === "Euronext Rapeseed" || selection === "Euronext Wheat"){
                            units[i].value = "Metric Tons";
                        }else if(selection === "Soybean Meal"){
                            units[i].value = "Short Tons";
                        }
                    }  
                }
                
                /**
                * This function is run when a commodity is selected, and reloads the page via a form submission. 
                */
                function commoditySelected(){
                    document.getElementById("commodityForm").submit();
                }
                
                /*
                * This function is run whenever an order volume units textbox is edited.
                * It is in charge of calculating and updating the order volume contracts 
                * and executed product total cost for the row in which the quantity was entered. 
                * @param id the number row in the loop that needs calculations performed.
                */
                function calcContractsAndTotal(id){
                    //Get the unit type and order volume quantity measured.
                    var quantity = document.getElementsByName("orderVolumeQuantity" + id)[0].value;
                    var unit = document.getElementsByClassName("unit")[0].value;
                    
                    //Declare necessary variables
                    var factor = 0;
                    var contracts = 0.0;
                    
                    //Determine conversion factor based upon unit type.
                    if(unit === "Short Tons"){
                        factor = 100;
                    }else if(unit === "Metric Tons"){
                        factor = 50;
                    }
                    //Find number of contracts by division and update.
                    contracts = quantity / factor;
                    document.getElementById("contracts" + id).value = contracts;
                    
                    //Find new executed product total cost and update in table.
                    var price = document.getElementsByName("product_current_price_per_unit" + id)[0].value;
                    var totalPrice = price * quantity;
                    document.getElementById("finalprice" + id).value = totalPrice.toFixed(2);
                    document.getElementById("finalprice" + id).outerHTML.value = totalPrice;
                }
                
                /** FUNCTIONS FOR VALIDITY CHECKS **/
                
                /**
                * The check choosers function is run on the load of the page.
                * It is responsible for disabling the commodity chooser and preview button until a farm is chosen, at which point they are enabled. 
                */
                function checkChoosers(){
                        document.getElementById('farmerID').value = document.getElementById('farmerChooser').value;
                        document.getElementById('commodityName').value = document.getElementById('commodityChooser').value;
                        if(document.getElementById('farmerChooser').value === 'default'){
                            document.getElementById('commodityChooser').disabled = true;
                            document.getElementById('previewButton').disabled = true;
                        }else{
                            document.getElementById('commodityChooser').disabled = false;
                            document.getElementById('previewButton').disabled = false;
                        }
                }
                
                //
                $(document).ready(function() {
                    $("#farmerChooser").change(function() {
                        $('#farmerID').val($(this).val());
                        if($(this).val() === 'default'){
                            $('#commodityChooser').prop('disabled',true);
                            $('#previewButton').prop('disabled',true);
                        }else{
                            $('#commodityChooser').prop('disabled',false);
                            $('#previewButton').prop('disabled',false);
                        }
                    });
                    $("#commodityChooser").change(function() {
                        $('#commodityName').val($(this).val());
                    });
                });
                
                $(document).keypress(
                    function(event){
                     if (event.which == '13') {
                        event.preventDefault();
                    }});
            </script>
           
            <% 
               //Close connection with the database.
               farmers.closeConnection();
               pricing.closeConnection();  }
               /**
                * DISPLAY ERROR MESSAGE if tried to access page on non business day.
                */
                else {
            %>
                <h1>Orders may only be placed on business days. Please check back on the next available business day.</h1>
            <% }
            %>
    </body>
    <%   %>
</html>