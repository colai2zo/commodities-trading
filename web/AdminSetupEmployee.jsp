<%-- 
    Document   : AdminSetupEmployee
    Created on : Aug 7, 2016, 12:18:14 PM
    Author     : Joey
--%>

<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.Employee" %>

<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|Setup Employee</title>
         <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header">
            <h1>Employee Setup</h1>
            <h2>Enter the following information to setup an employee:</h2>
        </div>
        <%Employee employees = new Employee(); //Create an instance of Employee to access the database.%>
        <div id="central" align="center">
            <form name="employeeInfoForm" action="AdminSetupEmployee.jsp" method="post">
                <table border="0" cellpadding="5">
                    <tbody>
                        <tr>
                            <td>First Name: </td>
                            <td>
                                <input required type="text" autocomplete="off" name="firstNameInput" value="" size="50" style="padding:10px 0px 10px 0px"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Last Name: </td>
                            <td>
                                <input required type="text" autocomplete="off" name="lastNameInput" value="" size="50" style="padding:10px 0px 10px 0px"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Username: </td>
                            <td><input required type="text" autocomplete="off" name="userNameInput" value="" size="50" style="padding:10px 0px 10px 0px"/></td>
                        </tr>
                        <tr>
                            <td>Password: </td>
                            <td><input required type="text" autocomplete="off" name="passwordInput" value="<%= employees.createPassword()%>" size="50" style="padding:10px 0px 10px 0px" /></td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>Rights:</b> </td>
                        </tr>
                        <tr>
                            <td>All</td>
                            <td><input type="checkbox" class="box" name="adminCheckBox" value="ON" onchange="checkAll(this)" />
                        </tr>
                        <tr>
                            <td>Book Management</td>
                            <td><input type="checkbox" class="box" name="bookManagementBox" value="ON" />
                        </tr>
                        <tr>
                            <td>Pricing</td>
                            <td><input type="checkbox" class="box" name="pricingCheckBox" value="ON" />
                        </tr>
                        <tr>
                            <td>Reporting</td>
                            <td><input type="checkbox" class="box" name="reportingCheckBox" value="ON" />
                        </tr>
                    </tbody>
                </table>
                <input id="button" type="submit" value="Create Employee" name="submitEmployeeInfo" />
            </form>
            
            <%
            if(request.getParameter("submitEmployeeInfoButton") != null){
                Boolean adminChecked = false;
                Boolean bookManagementChecked = false;
                Boolean reportingChecked = false;
                Boolean pricingChecked = false;
                Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
                
                if(request.getParameter("adminCheckBox") != null)
                    adminChecked = true;
                if(request.getParameter("bookManagementBox") != null)
                    bookManagementChecked = true;
                if(request.getParameter("reportingCheckBox") != null)
                    reportingChecked = true;
                if(request.getParameter("pricingCheckBox") != null)
                    pricingChecked = true;
                
                employees.insertEmployee(request.getParameter("firstNameInput"), request.getParameter("lastNameInput"), request.getParameter("userNameInput"), request.getParameter("passwordInput"), adminChecked, bookManagementChecked, reportingChecked, pricingChecked, currentTimestamp);
            }
            %>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminEmployeeOptions.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <script>        
        function checkAll(ele) {
            var checkboxes = document.getElementsByClassName('box');
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = ele.checked;
            }
        }
    </script>
    
    </body>
    <%
        employees.closeConnection();
        %>
</html>