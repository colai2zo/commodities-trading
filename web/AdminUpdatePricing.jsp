<%-- 
    Document   : AdminUpdatePricing
    Created on : Aug 7, 2016, 2:37:58 PM
    Author     : Joey
--%>
<%@page import="org.jquantlib.time.TimeUnit"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*"%>
<%@page import="org.jquantlib.time.calendars.UnitedKingdom"%>
<%@page import="org.jquantlib.time.calendars.UnitedStates"%>
<%@page import="org.jquantlib.time.Date"%>
<%@page import="org.jquantlib.time.Period"%>
<%@page import="java.text.NumberFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
    <script src="http://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
    <style>
        tr:nth-child(even){
            background-color: rgba(50,200,200,.5);
        }
        #noEdit{
            background-color: transparent; text-align: center; border: 0px
        }
    </style>
    <%
        /*********************************************************************
         * SECTION 1: Declaration, instantiation of variables, objects, etc. *
         *********************************************************************/
        
        //Access the Pulled Data database and use to create result sets with each commodity's bloomberg data.
        PulledData data = null;
        data = new PulledData();
        ResultSet todaysDataRapeseed = data.getTodaysData("Euronext Rapeseed");
        ResultSet todaysDataWheat = data.getTodaysData("Euronext Wheat");
        ResultSet todaysDataSoybeanMeal = data.getTodaysData("Soybean Meal");
        ResultSet todaysData = todaysDataRapeseed; //set data to rapeseed.
        
        //Instantiate Trading Calendar objects 
        UnitedStates usCal = new UnitedStates();
        UnitedKingdom ukCal = new UnitedKingdom();
        org.jquantlib.time.Calendar tradingCal = ukCal;
        
        //Create a calendar object and a means of formatting the date for the header.
        DateFormat dateFormat = new SimpleDateFormat("EEEE MMMM d, yyyy");
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        
        //Get the value of the commodity selected, if it was changed (default = rapeseed).
        String commodityValue = "Euronext Rapeseed";
        if(request.getParameter("ChooseCommodity") != null){
            commodityValue = request.getParameter("ChooseCommodity");
        }
        
        //Get the value of the product selected, if it was changed (default = enhanced market average).
        String productValue = "Enhanced Market Average";
        if(request.getParameter("chooseProduct") != null){
            productValue = request.getParameter("chooseProduct");
        }
     %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Update Pricing</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <script src="validityCheck.js"></script>
    </head>
    <body onload="strikeShift(); updateFinalPrice(); productPlacement(); calcProductVolatility();">
            <div id="header" align="center" style="padding-bottom: 20px">
                <h1>Update Pricing: <%= dateFormat.format(calendar.getTime())%></h1>
                
                <table border="0">
                    <tbody>
                        <tr>
                            
                            
                        <form name="SubmitForm" action="AdminUpdatePricing.jsp" method="POST">
                            <td>
                                <select name="chooseProduct" id="chooseProduct" onchange="this.form.submit()" value="Enhanced Market Average">
                                    <option value="Enhanced Market Average">Enhanced Market Average</option>
                                    <option value="Freedom Reprice">Freedom Reprice</option>
                                </select>
                            </td>
                            <td>
                                <select id="ChooseCommodity" name="ChooseCommodity" onchange="this.form.submit()" value="Euronext Rapeseed">
                                    <option value="Euronext Rapeseed">Euronext Rapeseed (IJ)</option>
                                    <option value="Euronext Wheat">Euronext Wheat (CA)</option>
                                    <option value="Soybean Meal">Soybean Meal (SM)</option>
                                </select>
                            </td>
                        </form>                          
                        </tr>
                    </tbody>
                </table>
            </div>
        <%  
                //Retrieve the commodity and product selected.
                String commodity = commodityValue;
                String product = productValue;
                
                //Use product to assign frequency.
                String frequency = "";
                if(product.equals("Enhanced Market Average")){
                    frequency = "daily";
                }else if(product.equals("Freedom Reprice")){
                    frequency = "bullet";
                }
                
                //Use commodity selection to assign trading calendar and result set objects.
                 if(commodity.equals("Euronext Rapeseed")){
                    todaysData = todaysDataRapeseed;
                    tradingCal = ukCal;
                }if(commodity.equals("Euronext Wheat")){
                    todaysData = todaysDataWheat;
                    tradingCal = ukCal;
                }if(commodity.equals("Soybean Meal")){
                    todaysData = todaysDataSoybeanMeal;
                    tradingCal = usCal;
                } 
        %>
        <form name="submitForm" id="submitForm" action="AdminSubmitPricing.jsp" autocomplete="off">
        <input type="hidden" name="ChooseCommodity" value="<%= request.getParameter("ChooseCommodity")%>" />
        <input type="hidden" name="chooseProduct" value="<%= request.getParameter("chooseProduct")%>" />
        <input type="hidden" id="commodity" value="<%= commodityValue%>" />
        <input type="hidden" id="product" value="<%= productValue%>" />
        <div id="central" align="center" >
         
            <script>
                /**
                 * Function run on load of the page.
                 * Assigns the value of the commodity and product choosers based upon what was selected before the page submission.
                 * Defaults: Euronext Rapeseed and Enhanced market average.
                 */
                function productPlacement(){
                    document.getElementById("chooseProduct").value = '<%= productValue%>';
                    document.getElementById("ChooseCommodity").value = '<%= commodityValue%>';
                }
            </script>
            
            <table id="theTable1" border="1" cellpadding="15" style="width:90%">
                <thead>
                    <tr>
                        <th colspan="12">Current Market</th>
                    </tr>
                    <tr>
                        <th>Client Month</th>
                        <th>Expiration Date</th>
                        <th>Underlying Futures Code</th>
                        <th>Current Underlying Futures Price</th>
                        <th>Indicative Product Strike Price</th>
                        <th>Daily Trader Volatility</th>
                        <th>Volatility Spread</th>
                        <th>Product Volatility</th>
                        <th>Product Fair Value / Unit</th>
                        <th>Product Markup</th>
                        <th>Product Current Price / Unit</th>
                        <th>Interest Rate (Dollar)</th>
                <tbody>
                    <%  
                        //Create an instance of the NumberFormatting class to truncate decimals to two places.
                        NumberFormat formatter = NumberFormat.getInstance();
                        formatter.setMaximumFractionDigits(2);
                        formatter.setMinimumFractionDigits(2);
                        formatter.setGroupingUsed(false);
                        
                        //Declare a counter object and count how many 4 character futures codes are available in the resultset.
                        int Count = 0;
                        while(todaysData.next()){
                            if(todaysData.getString("underlying_futures_code").length() == 4)
                                Count++;
                        }
                        
                        /*************************************
                         * SECTION 2: FILLING OF DATE ARRAYS *
                         *************************************/
                        
                        //Create an array of month names.
                        String[] monthName = {"January", "February",
                        "March", "April", "May", "June", "July",
                        "August", "September", "October", "November",
                        "December"};
 
                        //Array of months to be displayed. First stored is current month.
                        String[] months = new String[Count]; 
                        //This array stores the months, in integer format rather than strings. (e.g. January = 1)
                        int[] monthInts = new int[Count];
                        //Array of expiration dates to go along with each of the months
                        java.sql.Date[] expirations = new java.sql.Date[Count];
                        
                        /**FILL MONTHS ARRAY
                         **Uses the calendar class to fill the months array.
                         **Starts with the current month and increments every time it goes through loop.
                         **/
                        for(int i = 0; i < Count; i++){
                            if(commodity.equals("Soybean Meal") && (calendar.get(Calendar.MONTH) == 1 || calendar.get(Calendar.MONTH) == 3 || calendar.get(Calendar.MONTH) == 5 || calendar.get(Calendar.MONTH) == 6 || calendar.get(Calendar.MONTH) == 7 || calendar.get(Calendar.MONTH) == 8 || calendar.get(Calendar.MONTH) == 10 || calendar.get(Calendar.MONTH) == 11)) {
                                if( calendar.get(Calendar.MONTH) + i < 12)
                                    months[i] = monthName[calendar.get(Calendar.MONTH) + i ];
                                else if(calendar.get(Calendar.MONTH) + i  >= 12 && calendar.get(Calendar.MONTH) + i  < 24)
                                    months[i] = monthName[calendar.get(Calendar.MONTH) + i - 12];
                                else if(calendar.get(Calendar.MONTH) + i  >= 24)
                                    months[i] = monthName[calendar.get(Calendar.MONTH) + i - 24];
                            }
                            else{
                                if( calendar.get(Calendar.MONTH) + i < 12)
                                    months[i] = monthName[calendar.get(Calendar.MONTH) + i];
                                else if(calendar.get(Calendar.MONTH) + i >= 12 && calendar.get(Calendar.MONTH) + i < 24)
                                    months[i] = monthName[calendar.get(Calendar.MONTH) + i - 12];
                                else if(calendar.get(Calendar.MONTH) + i >= 24)
                                    months[i] = monthName[calendar.get(Calendar.MONTH) + i - 24];
                            }
                        }
                        
                        /**Fill Expirations Array
                         * Fills the expirations array with the expiration dates that correspond to the months in the months array.
                         */
                        for(int i = 0; i < Count; i++){
                            int year = calendar.get(Calendar.YEAR);
                            int month = -999;
                                //Convert the months in the array to integers and fill MonthInts array
                                if(months[i].equals("January"))
                                    month=1;
                                if(months[i].equals("February"))
                                    month=2;
                                if(months[i].equals("March"))
                                    month=3;
                                if(months[i].equals("April"))
                                    month=4;
                                if(months[i].equals("May"))
                                    month=5;
                                if(months[i].equals("June"))
                                    month=6;
                                if(months[i].equals("July"))
                                    month=7;
                                if(months[i].equals("August"))
                                    month=8;
                                if(months[i].equals("September"))
                                    month=9;
                                if(months[i].equals("October"))
                                    month=10;
                                if(months[i].equals("November"))
                                    month=11;
                                if(months[i].equals("December"))
                                    month=12;
                                
                                monthInts[i] = month; //Fill array.
                            
                            if(i>0 && month == 1){
                                year++; //Adds a year when it hits January
                            }

                            //Adds expiration date to array as last business day of month for Wheat and Rapeseed
                            if(commodity.equals("Euronext Rapeseed") || commodity.equals("Euronext Wheat")){
                                int day = 10; //Start date for tester (will never be the actual end).
                                java.sql.Date dayInMonth = java.sql.Date.valueOf(year + "-" + month + "-" + day); //Create test date to find the end of the month of this date.
                                java.util.Date expiration = tradingCal.endOfMonth(new org.jquantlib.time.Date(dayInMonth)).inc().shortDate(); //Use test date to get last business day of that month
                                java.sql.Date exp = new java.sql.Date(expiration.getTime()); //Convert from util date to SQL date
                                expirations[i] = exp; /**Add expiration to array**/
                            }
                            //Different conditional expirations for Soybean Meal.
                            else if(commodity.equals("Soybean Meal")){
                                //For ceratin months, it is simply the end of the month
                                if(month == 1 || month == 3 || month == 5 || month == 10){
                                    Date date = new Date(25,month,year); //Create test date.
                                    //Loops until it hits the end of the month
                                    while(!tradingCal.isEndOfMonth(date)){
                                        date = tradingCal.advance(date, Period.ONE_DAY_FORWARD); //Increment date.
                                    }
                                    expirations[i] = new java.sql.Date(date.shortDate().getTime() + 24*60*60*1000); /**Add expiration to array**/
                                }
                                //For other months it is the last friday of the month.
                                else{
                                    Date date = new Date(1,month,year); //Create test date.
                                    int friCount = 0; //Keep track of number of fridays passed.
                                    //Loop until the third friday of the month is hit.
                                    while(friCount < 3){
                                        //Increment friCount everytime a friday is passed.
                                        if(date.weekday().toString().toLowerCase().equals("friday")){
                                            friCount++;
                                        }
                                        date = tradingCal.advance(date, Period.ONE_DAY_FORWARD); //Increment date.
                                    }
                                    date = tradingCal.advance(date, Period.ONE_DAY_BACKWARD); //Move backward one day.
                                    expirations[i] = new java.sql.Date(date.shortDate().getTime() + 24*60*60*1000); /**Add expiration to array**/
                                }
                            }
                            
                        } /**END OF EXPIRATION DATES FOR LOOP **/
                        
                        /*******************************************************************************
                         * SECTION 3: FILLING OF INTEREST RATE AND FAIR VALUE ARRAYS                   *
                         * LOOPS THROUGH EACH ROW IN THE TABLE AND PERFORMS BLACK SCHOLES CALCULATIONS *
                         * BASED UPON THE EXPIRATION DATE, MONTH, AND FUTURES CODE                     *
                         *******************************************************************************/
                        
                        //DECLARATION of necessary variables and arrays.
                        int monthCount = 0; //Counter for number of months passed in the loop.
                        double[] interestRates = new double[Count]; //Create an array to store all of the interest rates that correspond with the months
                        double[] fairValue = new double[Count * 3]; // Declare array of product fair values per unit that correspond with the months
                        double strikeShift; //Declare a variable to store the strike shift of a given table.
                        int counter = 0;
                        double[] markups = new double[24];
                        double[] volatilitySpreads = new double[24];
                        
                        /**
                         * MAIN LOOP
                         * Goes through thrice for each of the three tables
                         * Each table is representative of a strike shift (0, positive, and negative).
                         */
                        for(int i = 1; i <= 3; i++){
                            
                            //Assign strike shift default values based upon time going through the loop.
                            if(i==1){
                                strikeShift=0;
                            }
                            else if(i==2){
                                strikeShift=2;
                            }
                            else{
                                strikeShift=-2;
                            } 
                            
                            //Move the cursor of the pulled data result set to the beginning of the set.
                            todaysData.first(); 
                            String option;
                            /**While Loop for each of the three tables
                             * Creates 8 rows in each table.
                             */
                            int j = 0; // counter
                            while(j < 8){
                                
                                //Get the last entered product markup and volatility spread for this row
                                MarkupsVolatilities mv = new MarkupsVolatilities();
                                ResultSet mvData = mv.getMarkupsAndVolatilites(frequency, commodity, counter);
                                mvData.first();
                                markups[counter] = mvData.getDouble("markup");
                                volatilitySpreads[counter] = mvData.getDouble("volatility");
                                
                                //Get the resultset of data for the futures code of the current month in the array, based on loop counter j.
                                todaysData = data.getTodaysDataByCode(Orders.getFuturesCode(monthInts[j], commodity));
                                todaysData.first();
                                
                                //Assign the option (instrument) to either call or put depending on the commodity.
                                if(todaysData.getString("underlying_commodity_name").equals("Euronext Rapeseed") ||
                                   todaysData.getString("underlying_commodity_name").equals("Euronext Wheat")){
                                    option = "call";
                                }
                                else{
                                    option = "put";
                                }
                                /**CREATE A HIDDEN INPUT CONTAINING THE OPTION (INSTRUMENT)**/
                        %>
                                <input type="hidden" name="option" id="<%= "option" + (j + 8 * (i-1))%>" value="<%= option%>" />
                        <%
                                //Find the number of dats between today and this row's expiration date
                                int days = tradingCal.businessDaysBetween(new org.jquantlib.time.Date(new java.util.Date()), new org.jquantlib.time.Date(expirations[j]), true, true);
                                //Calculate the interest rate for current row using the row's parameters
                                double current_interest_rate = BlackScholes.calcCurrentInterestRate(
                                        todaysData.getDouble("interest_30_day"),
                                        todaysData.getDouble("interest_90_day"),
                                        todaysData.getDouble("interest_180_day"), 
                                        todaysData.getDouble("interest_360_day"),
                                        days);
                                interestRates[j] = current_interest_rate; //Add this interest rate to the array of interest rates.
                                
                                /** CREATE A HIDDEN INPUT CONTAINING THE NUMBER OF DAYS BETWEEN TODAY AND THIS ROW'S EXPIRATION DATE **/
                        %>
                                <input type="hidden" id="<%= "days" + (j + 8 * (i-1))%>" value="<%= days%>" />
                        <%
                                
                                /**
                                 * BULLET CALCULATION
                                 * Calculate the fair value using Black Scholes for Bullet pricing.
                                 */
                                if(frequency.equals("bullet")){
                                    
                                    //Use black scholes models to calculate fair value per unit
                                    BlackScholes bs = new BlackScholes(
                                    option,
                                    todaysData.getDouble("current_underlying_futures_price"),
                                    todaysData.getDouble("current_underlying_futures_price") + strikeShift,
                                    current_interest_rate, //INTEREST RATE
                                    0, //Dividend Yield Defaults to 0.0
                                    todaysData.getDouble("current_implied_volatility") + mvData.getDouble("volatility"),
                                    days
                                    ); 
                                    
                                    //Fill the fair value array according to the loop count with the Black Scholes value.
                                    if(i==1){
                                         fairValue[j] = bs.getMarketPricePerUnit(); //fill fair value per unit array
                                    }if(i==2){
                                         fairValue[j + 8] = bs.getMarketPricePerUnit(); //fill fair value per unit array
                                    }if(i==3){
                                         fairValue[j + 16] = bs.getMarketPricePerUnit(); //fill fair value per unit array
                                    }
                                }
                                
                                /**
                                 * DAILY CALCULATION
                                 * Calculate the fair value using the Black Scholes daily accumulator for Daily pricing.
                                **/
                                else if(frequency.equals("daily")){
                                    
                                    ////Use black scholes daily accumulator models to calculate fair value per unit.
                                    BlackScholesDailyAccumulator bsda = new BlackScholesDailyAccumulator(
                                    option,
                                    todaysData.getDouble("current_underlying_futures_price"),
                                    todaysData.getDouble("current_underlying_futures_price") + strikeShift,
                                    todaysData.getString("underlying_futures_code"),
                                    0, todaysData.getDouble("daily_trader_volatility") + mvData.getDouble("volatility"), new java.sql.Date(new java.util.Date().getTime()).toString(), expirations[j].toString(), "sell",
                                    1,1);
                                    
                                    double fairValAvg = bsda.getFairValueAvg();
                                    //fill the fair value array according to the loop count
                                    if(i==1){
                                         fairValue[j] = fairValAvg; //fill fair value per unit array
                                    }if(i==2){
                                         fairValue[j + 8] = fairValAvg; //fill fair value per unit array
                                    }if(i==3){
                                         fairValue[j + 16] = fairValAvg; //fill fair value per unit array
                                    }
                                    
                                }
                                //Increment row while loop counter.
                                j++;
                                counter++;
                            }
                        }

                        //Reset counter variables to prepare for display of data.
                        Count = 0;
                        todaysData.first();
                        monthCount = 0;

                        /**************************************************
                         * SECTION 4: GENERATE TABLES                     *
                         * GENERATE Current Market table using while loop *
                         **************************************************/

                        while(Count < 8){
                            todaysData = data.getTodaysDataByCode(Orders.getFuturesCode(monthInts[monthCount], commodity));
                            todaysData.first();

                        %>
                    <tr>
                        <td><input class="month" id="<%= ("month" + Count)%>" type="text" autocomplete="off" name="<%= ("month" + Count)%>" value="<%=months[monthCount]%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="exp" id="<%= ("expiration" + Count)%>" type="text" autocomplete="off" name="<%= ("expiration" + Count)%>" value="<%=expirations[monthCount]%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="code" id="<%= ("futuresCode" + Count)%>" type="text" autocomplete="off" name="<%= ("futuresCode" + Count)%>" value="<%= todaysData.getString("underlying_futures_code")%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="futures" id="<%= ("futuresPrice" + Count)%>" type="text" autocomplete="off" name="<%= ("futuresPrice" + Count)%>" value="<%= formatter.format(todaysData.getDouble("current_underlying_futures_price"))%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="strike" id="<%= ("strike" + Count)%>" type="text" autocomplete="off" name="<%= ("strike" + Count)%>" value="<%= formatter.format(todaysData.getDouble("current_underlying_futures_price"))%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="volatility" id="<%= ("dailyTraderVolatility" + Count)%>" type="text" autocomplete="off" name="<%= ("dailyTraderVolatility" + Count)%>" value=<%=formatter.format(todaysData.getDouble("daily_trader_volatility"))%> readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="spread editable" onkeyup="calcProductVolatility(); checkNum(this);" id="<%= ("vSpreadInput" + Count)%>" type="text" autocomplete="off" name="<%= ("vSpreadInput" + Count)%>" value="<%= formatter.format(volatilitySpreads[Count]) %>" onchange="autoFillVolatilities(this.id, this.value)" /></td>
                        <td><input class="prodVol" id="<%= ("productVolatility" + Count)%>" type="text" autocomplete="off" name="<%= ("productVolatility" + Count)%>" value=<%=formatter.format(todaysData.getDouble("daily_trader_volatility"))%> readonly style="background-color: transparent; text-align: center; border: 0px" /></td>
                        <td><input id="<%= ("price" + Count)%>" type="text" autocomplete="off" name="<%= ("price" + Count)%>" value="<%= formatter.format(fairValue[Count])%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="markup editable" id="<%= ("markupInput" + Count)%>" type="text" autocomplete="off" name="<%= ("markupInput" + Count)%>" value="<%= formatter.format(markups[Count]) %>" onkeyup="checkNum(this); updateFinalPrice()" onchange="autoFillMarkups(this.id, this.value)"/></td>
                        <td><input id="<%= ("finalPrice" + Count)%>" type="text" autocomplete="off" name="<%= ("finalPrice" + Count)%>" value="" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input id="<%= ("interestRate" + Count)%>" type="text" autocomplete="off" name="<%= ("interestRate" + Count)%>" value="<%= formatter.format(interestRates[monthCount])%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <input type="hidden" name="<%= ("strike_shift" + Count)%>" value="0" />
                    </tr>
                    <% 
                            Count++;
                            monthCount++;
                        }
                        monthCount = 0;
                    %>
                </tbody>
            </table> <br><br>

            <table id="theTable2" border="1" cellpadding="15" style="width:90%">
                <thead>
                    <tr>
                        <th colspan="12">Strike Shift: + <input style="width:15px; text-align:center" type="text" autocomplete="off" class="strikeShift" name="strike_shift_positive" id="strike_shift_positive" value="2" size="10" onkeyup="strikeShift()"/></th>
                    </tr>
                    <tr>
                        <th>Client Month</th>
                        <th>Expiration Date</th>
                        <th>Underlying Futures Code</th>
                        <th>Current Underlying Futures Price</th>
                        <th>Indicative Product Strike Price</th>
                        <th>Daily Trader Volatility</th>
                        <th>Volatility Spread</th>
                        <th>Product Volatility</th>
                        <th>Product Fair Value / Unit</th>
                        <th>Product Markup</th>
                        <th>Product Current Price / Unit</th>
                        <th>Interest Rate (Dollar)</th>
                <tbody>
                    <%  /**GENERATE Positive Strike Shift table using while loop**/
                        while(Count < 16){
                            todaysData = data.getTodaysDataByCode(Orders.getFuturesCode(monthInts[monthCount], commodity));
                            todaysData.first();
                        %>
                    <tr>
                        <td><input class="month" id="<%= ("month" + Count)%>" type="text" autocomplete="off" name="<%= ("month" + Count)%>" value="<%=months[monthCount]%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="exp" id="<%= ("expiration" + Count)%>" type="text" autocomplete="off" name="<%= ("expiration" + Count)%>" value="<%=expirations[monthCount]%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="code" id="<%= ("futuresCode" + Count)%>" type="text" autocomplete="off" name="<%= ("futuresCode" + Count)%>" value="<%= todaysData.getString("underlying_futures_code")%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="futures" id="<%= ("futuresPrice" + Count)%>" type="text" autocomplete="off" name="<%= ("futuresPrice" + Count)%>" value="<%= formatter.format(todaysData.getDouble("current_underlying_futures_price"))%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="strike" id="<%= ("strike" + Count)%>" type="text" autocomplete="off" name="<%= ("strike" + Count)%>" value="<%= formatter.format(todaysData.getDouble("current_underlying_futures_price") + 2.0)%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="volatility" id="<%= ("dailyTraderVolatility" + Count)%>" type="text" autocomplete="off" name="<%= ("dailyTraderVolatility" + Count)%>" value=<%=formatter.format(todaysData.getDouble("daily_trader_volatility"))%> readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="spread editable" onkeyup="calcProductVolatility(); checkNum(this);" id="<%= ("vSpreadInput" + Count)%>" type="text" autocomplete="off" name="<%= ("vSpreadInput" + Count)%>" value="<%= formatter.format(volatilitySpreads[Count]) %>" /></td>
                        <td><input class="prodVol" id="<%= ("productVolatility" + Count)%>" type="text" autocomplete="off" name="<%= ("productVolatility" + Count)%>" value=<%= formatter.format(todaysData.getDouble("daily_trader_volatility"))%> readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input id="<%= ("price" + Count)%>" type="text" autocomplete="off" name="<%= ("price" + Count)%>" value="<%= formatter.format(fairValue[Count])%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="markup editable" id="<%= ("markupInput" + Count)%>" type="text" autocomplete="off" name="<%= ("markupInput" + Count)%>" value="<%= formatter.format(markups[Count]) %>" onkeyup="checkNum(this); updateFinalPrice()"/></td>
                        <td><input id="<%= ("finalPrice" + Count)%>" type="text" autocomplete="off" name="<%= ("finalPrice" + Count)%>" value="" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input id="<%= ("interestRate" + Count)%>" type="text" autocomplete="off" name="<%= ("interestRate" + Count)%>" value="<%= formatter.format(interestRates[monthCount])%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <input type="hidden" class="strikeShiftPos" name="<%= ("strike_shift" + Count)%>" value="0" />
                    </tr> 
                    <% 
                        Count++;
                        monthCount++;
                        }
                        monthCount=0;
                    %>
                </tbody>
            </table> <br><br>

            <table id="theTable3" border="1" cellpadding="15" style="width:90%">
                <thead>
                    <tr>
                        <th colspan="12">Strike Shift: - <input style="width:15px; text-align:center" type="text" class="strikeShift" name="strike_shift_negative" id="strike_shift_negative" value="2" size="10" onkeyup="strikeShift()"/></th>
                    </tr>
                    <tr>
                        <th>Client Month</th>
                        <th>Expiration Date</th>
                        <th>Underlying  Code</th>
                        <th>Current Underlying Futures Price</th>
                        <th>Indicative Product Strike Price</th>
                        <th>Daily Trader Volatility</th>
                        <th>Volatility Spread</th>
                        <th>Product Volatility</th>
                        <th>Product Fair Value / Unit</th>
                        <th>Product Markup</th>
                        <th>Product Current Price / Unit</th>
                        <th>Interest Rate (Dollar)</th>
                <tbody>
                    <%  /**GENERATE negative strike shift table using while loop**/
                        while(Count < 24){
                            todaysData = data.getTodaysDataByCode(Orders.getFuturesCode(monthInts[monthCount], commodity));
                            todaysData.first();
                    %>
                    <tr>
                        <td><input class="month" id="<%= ("month" + Count)%>" type="text" autocomplete="off" name="<%= ("month" + Count)%>" value="<%=months[monthCount]%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="exp" id="<%= ("expiration" + Count)%>" type="text" autocomplete="off" name="<%= ("expiration" + Count)%>" value="<%=expirations[monthCount]%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="code" id="<%= ("futuresCode" + Count)%>" type="text" autocomplete="off" name="<%= ("futuresCode" + Count)%>" value="<%= todaysData.getString("underlying_futures_code")%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="futures" id="<%= ("futuresPrice" + Count)%>" type="text" autocomplete="off" name="<%= ("futuresPrice" + Count)%>" value="<%= formatter.format(todaysData.getDouble("current_underlying_futures_price"))%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="strike" id="<%= ("strike" + Count)%>" type="text" autocomplete="off" name="<%= ("strike" + Count)%>" value="<%= formatter.format(todaysData.getDouble("current_underlying_futures_price") - 2)%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="volatility" id="<%= ("dailyTraderVolatility" + Count)%>" type="text" autocomplete="off" name="<%= ("dailyTraderVolatility" + Count)%>" value=<%=formatter.format(todaysData.getDouble("daily_trader_volatility"))%> readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="spread editable" onkeyup="calcProductVolatility(); checkNum(this);" id="<%= ("vSpreadInput" + Count)%>" type="text" autocomplete="off" name="<%= ("vSpreadInput" + Count)%>" value="<%= formatter.format(volatilitySpreads[Count]) %>" /></td>
                        <td><input class="prodVol" id="<%= ("productVolatility" + Count)%>" type="text" autocomplete="off" name="<%= ("productVolatility" + Count)%>" value=<%=formatter.format(todaysData.getDouble("daily_trader_volatility"))%> readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input id="<%= ("price" + Count)%>" type="text" autocomplete="off" name="<%= ("price" + Count)%>" value="<%= formatter.format(fairValue[Count])%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <td><input class="markup editable" id="<%= ("markupInput" + Count)%>" type="text" autocomplete="off" name="<%= ("markupInput" + Count)%>" value="<%= formatter.format(markups[Count]) %>" onkeyup="checkNum(this); updateFinalPrice()"/></td>
                        <td><input id="<%= ("finalPrice" + Count)%>" type="text" autocomplete="off" name="<%= ("finalPrice" + Count)%>" value="" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                        <input type="hidden" class="strikeShiftNeg" name="<%= ("strike_shift" + Count)%>" value="0" />
                        <td><input id="<%= ("interestRate" + Count)%>" type="text" autocomplete="off" name="<%= ("interestRate" + Count)%>" value="<%= formatter.format(interestRates[monthCount])%>" readonly style="background-color: transparent; text-align: center; border: 0px"/></td>
                    </tr>
                    <% 
                            Count++;
                            monthCount++;
                        }                
                    %>
                </tbody>
            </table> <br><br>
        </div>
        <div class="updatePricingFixedCenter" align="center" >
            <input class="button" id="previewButton" type="button" value="Preview Changes" name="proceedButton" onclick="makeReadonly()" />
            <input id="button" type="button" value="Return to Edit" name="returnButton" style="display:none" onclick="makeEditable()"/>
            <input id="button" type="submit" value="Submit Changes" name="submitButton" style="display:none"/>
            <input id="Count" type="hidden" name="Count" value="<%= Count%>" />
        </div>    
        </form> 
        <div class="fixed">
            <table border="0">
                <tbody>
                     <tr>
                         <td>
                             <form id="homeForm" name="homeForm" action="AdminHomePage.jsp" method="POST">
                                 <input id="backAndHome" type="button" value="Return to Home Screen" name="homeButton" onclick="confirmHome()" />
                             </form>
                         </td>
                         <td>
                             <form id="backForm" name="backForm" action="AdminHomePage.jsp" method="POST">
                                 <input id="backAndHome" type="button" value="Go Back" name="backButton" onclick="confirmBack()" />
                             </form>
                         </td>
                     </tr>
                 </tbody>
             </table>
         </div>
    <script>
            /**
             * This function is run when the very first product markup is entered.
             * The purpose of this function is to update all of the product markups for a given table when the first one is entered.
             * Subsequently, a new product current price per unit is calculated.
             * @param {float} markup a floating point number representing the product markup that the user would like to autofill
             * @param {string} id the id of the element that triggered the function call.
             */
            function autoFillMarkups(id, markup){
                //Run the contents of the function only if the very first input triggered it.
                if(id === "markupInput0"){
                    for(i=1;i<24;i++){
                        document.getElementById("markupInput" + i).value = markup;
                    }
                    updateFinalPrice();
                }
            }
            
            /**
             * This function is run when the very first volatility spread is entered.
             * The purpose of this function is to update all of the volatilities for a given table when the first one is entered.
             * Subsequently, a new product volatility and product vair value per unit is calculated for each
             * @param {float} spread a floating point number representing the volatility spread that the user would like to autofill
             * @param {string} id the id of the element that triggered the function call.
             */
            function autoFillVolatilities(id, spread){
                //Run the contents of the function only if the very first input triggered it.
                if(id === "vSpreadInput0"){
                    for(i=1;i<24;i++){
                        document.getElementById("vSpreadInput" + i).value = spread;
                        calcProductVolatility();
                        updatePrices(i);
                    }
                }
            }
            
            
            /**
             * ConfirmHome function confirms that the user would like to return to the home screen
             * AND reminds then that all unsaved data will be lost if they choose to do so.
             */
            function confirmHome(){
                var c = confirm("Are you sure you want to go home? All unsaved data will be lost.");
                if(c === true){
                    document.getElementById("homeForm").submit();
                }
            }
            
            /**
             * ConfirmBack function confirms that the user would like to return to the previous screen
             * AND reminds then that all unsaved data will be lost if they choose to do so.
             */
            function confirmBack(){
                var c = confirm("Are you sure you want to go back? All unsaved data will be lost.");
                if(c === true){
                    document.getElementById("backForm").submit();
                }
            }
            
            /**
             * This function is run when a product markup is entered.
             * It recalculates the prodcut current price / unit and displays this value in the respective cell.
             */
            function updateFinalPrice(){
                var count = document.getElementById("Count").value;
                for(i=0;i<count;i++){
                   document.getElementById("finalPrice" + i).value = (parseFloat(document.getElementById("price" + i).value) + parseFloat(document.getElementById("markupInput" + i).value)).toFixed(2);
                }
            }
            
            /**
             * Performs all changes that are invoked when a strike shift changes.
             * Fills hidden strike shift elements in each of the rows (required for submission of each row to pricing database)
             * Updates the indicative product strike prices.
             */
            function strikeShift(){
                //Get Positive and Negative Strike shift values.
                var positive = document.getElementById("strike_shift_positive");
                var negative = document.getElementById("strike_shift_negative");
                
                //Retrieve hidden input elements with that hold strike shift values.
                var positiveHidden = document.getElementsByClassName("strikeShiftPos");
                var negativeHidden = document.getElementsByClassName("strikeShiftNeg");
                
                //Fill hidden positive strike shift inputs with positive strike shift value.
                for(var i = 0; i < positiveHidden.length; i++){
                    positiveHidden[i].value = positive.value;
                }for(var j = 0; j < negativeHidden.length; j++){
                    negativeHidden[j].value = -1 * negative.value;
                }
                
                //Update indicative product strike prices.
                var strikes = document.getElementsByClassName("strike");
                var futures = document.getElementsByClassName("futures");
                for(var i = 0; i < strikes.length; i++){
                    strikes[i].value = (parseFloat(futures[i].value) + parseFloat(document.getElementsByName("strike_shift" + i)[0].value)).toFixed(2);
                }
            }
            
            /**
             * Recalculates the product volatility whenever a new volatility spread is inputted.
             */
            function calcProductVolatility(){
                var spreads = document.getElementsByClassName("spread");
                var vols = document.getElementsByClassName("volatility");
                var prodVols = document.getElementsByClassName("prodVol");
                for(var i = 0; i < spreads.length; i++){
                    prodVols[i].value = (parseFloat(vols[i].value)  + parseFloat(spreads[i].value)).toFixed(2);
                }
            }
            
            /**
             * Changes the style of all of the editable text fields to make them readonly
             * and to appear as normal text.
             * Additionally removes the "preview changes" button, and makes the submission and return to edit buttons appear.
             */
            function makeReadonly(){
                var editables = document.getElementsByClassName("editable");
                for(var i = 0; i < editables.length; i++){
                    editables[i].readOnly = true;
                    editables[i].style.backgroundColor = "transparent";
                    editables[i].style.border = "0px";
                    editables[i].style.textAlign = "center";
                }
                document.getElementsByName("proceedButton")[0].style.display = "none";
                document.getElementsByName("submitButton")[0].style.display = "block";
                document.getElementsByName("returnButton")[0].style.display = "block";
            }
            
            /**
             * Changes the style of all of the readonly editable text fields to make them editable
             * and to appear as text boxes.
             * Additionally removes the submission and return to edit buttons, and makes the "preview changes" button appear.
             */
            function makeEditable(){
                var editables = document.getElementsByClassName("editable");
                for(var i = 0; i < editables.length; i++){
                    editables[i].readOnly = false;
                    editables[i].style.backgroundColor = "white";
                    editables[i].style.border = "1px";
                    editables[i].style.textAlign = "left";
                }
                document.getElementsByName("proceedButton")[0].style.display = "block";
                document.getElementsByName("submitButton")[0].style.display = "none";
                document.getElementsByName("returnButton")[0].style.display = "none";
            }
            
            /**
             * AJAX FUNCTION
             * Run every time a volatility spread is eneterd or the volatility changes.
             * Sends updated volatility and other parameters from row to CalcFairValueServlet to retrieve the black scholes value for the Product Fair Value / Unit
             * Displays this value in the product fair value / unit column.
             */
            $(document).ready(function() {
                    $('.spread').keyup(function() {
                        
                            //Figure out which row the change was from.
                            var id = $(this).attr('id'); //Get ID of one that 
                            var i;
                            if(id.substring(id.length - 2, id.length - 1) === 't'){
                                i = id.substr(id.length - 1);
                            }else{
                                i = id.substr(id.length - 2);
                            }
                           
                            
                            //Create a string representing today's date.
                            var today = new Date();
                            var yyyy = today.getFullYear();
                            var mm = today.getMonth() + 1;
                            if(mm < 10){
                                mm = '0' + mm;
                            }
                            var dd = today.getDate();
                            if(dd < 10){
                                dd = '0' + dd;
                            }
                            var todayEscape = yyyy + '-' + mm + '-' + dd;
                            
                            //Evaluate the frequency of the pricing.
                            var product = $('#chooseProduct').val();
                            var frequency = "";
                            if(product === "Enhanced Market Average"){
                                frequency = "daily";
                            } else{
                                frequency = "bullet";
                            }
                            
                            //Make AJAX Call
                            $.ajax({
                                    url : 'CalcFairValueServlet',
                                    data : {
                                            option : $('#option' + i).val(),
                                            futures_price : $('#futuresPrice' + i).val(), //g
                                            executed_strike : $('#strike' + i).val(), //g
                                            interest_rate : $('#interestRate' + i).val(), //g
                                            dividend_yield : '0', //g
                                            daily_trader_volatility : $('#productVolatility' + i).val(), //g
                                            days : $('#days' + i).val(),
                                            frequency : frequency,
                                            underlying_futures_code : $('#futuresCode' + i).val(),
                                            trade_date : todayEscape,
                                            expiration_date : $("#expiration" + i).val(),
                                            action : "sell",
                                            order_volume_quantity : '1.0'
                                    },
                                    success : function(responseText) {
                                            $('#price' + i).val(responseText);
                                    }
                            });
                        });
                    });
                    
            var updatePrices = null;
            $(document).ready(function() {
                    function update(i){
  
                            console.log(i);
                            //Create a string representing today's date.
                            var today = new Date();
                            var yyyy = today.getFullYear();
                            var mm = today.getMonth() + 1;
                            if(mm < 10){
                                mm = '0' + mm;
                            }
                            var dd = today.getDate();
                            if(dd < 10){
                                dd = '0' + dd;
                            }
                            var todayEscape = yyyy + '-' + mm + '-' + dd;
                            
                            //Evaluate the frequency of the pricing.
                            var product = $('#chooseProduct').val();
                            var frequency = "";
                            if(product === "Enhanced Market Average"){
                                frequency = "daily";
                            } else{
                                frequency = "bullet";
                            }
                                //Run AJAX call
                                $.ajax({
                                    
                                        url : 'CalcFairValueServlet',
                                        data : {
                                            option : $('#option' + i).val(),
                                            futures_price : $('#futuresPrice' + i).val(), //g
                                            executed_strike : $('#strike' + i).val(), //g
                                            interest_rate : $('#interestRate' + i).val(), //g
                                            dividend_yield : '0', //g
                                            daily_trader_volatility : $('#productVolatility' + i).val(), //g
                                            days : $('#days' + i).val(),
                                            frequency : frequency,
                                            underlying_futures_code : $('#futuresCode' + i).val(),
                                            trade_date : todayEscape,
                                            expiration_date : $("#expiration" + i).val(),
                                            action : "sell",
                                            order_volume_quantity : '1.0'
                                        },
                                        success : function(responseText) {
                                            $('#price' + i).val(responseText);
                                        }
                                });  
                        }
                        updatePrices = update;
                    });
            
            
                /** FUNCTIONS FOR VALIDITY CHECKS
                 *  Run every time a select or a text input changes its value.
                 *  Checks to make sure all of the required fields are filled.
                 *  If they are not filled, the preview button is disabled.
                 *  If they are all filled, the preview button is enabeld.
                 **/
                $(document).ready(function() {
                    //Declare booleans to see if all selects are selected and all text inputs are filled.
                    var allSelected = true;
                    var allFilled = true; 
                   $("select").change(function() {
                        allSelected = true;
                        allFilled = true;
                        var selects = $("select");
                        //Checks to see if selects have been selected.
                        for(var i = 0; i < selects.length; i++){
                            if(selects[i].disabled === false && selects[i].value === "default"){
                                allSelected = false;
                            }
                        }
                        //Checkt ot see if text inputs have been filled.
                        var inputs = $("input:text");
                        for(var i = 0; i < inputs.length; i++){
                            if(inputs[i].value === ""){
                                allFilled = false;
                            }
                        }
                        
                        //Enable or disbable button accordingly.
                        if(allSelected && allFilled){
                            document.getElementById("previewButton").disabled = false;
                        } else{
                            document.getElementById("previewButton").disabled = true;
                        }
                    });
                    $("input:text").change(function() {
                        allSelected = true;
                        allFilled = true;
                        //Checkt ot see if text inputs have been filled.
                        var inputs = $("input:text");
                        for(var i = 0; i < inputs.length; i++){
                            if(inputs[i].value === ""){
                                allFilled = false;
                            }
                        }
                        //Checks to see if selects have been selected.
                        var selects = $("select");
                        for(var i = 0; i < selects.length; i++){
                            if(selects[i].disabled === false && selects[i].value === "default"){
                                allSelected = false;
                            }
                        } 
                        //Enable or disbable button accordingly.
                        if(allSelected && allFilled){
                            document.getElementById("previewButton").disabled = false;
                        } else{
                            document.getElementById("previewButton").disabled = true;
                        }
                    });
                });

            
    </script>
    </body>
    <%  
        //Close connection with the database.
        data.closeConnection();
    %>
</html>