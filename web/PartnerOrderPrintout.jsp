<%-- 
    Document   : PartnerOrderPrintout
    Created on : Oct 29, 2016, 10:19:54 PM
    Author     : Joey
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.Orders"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Print Order</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            window.onload = function() { window.print(); };
        </script>
    </head>
    <style>
        body{
            background-color: white;
        }
    </style>
    <body>
        <%
            int i = 0;
            int count = Integer.parseInt(request.getParameter("count"));
            int chosenRow = 1;
            //find the print button that sumbitted the form from the previous page
            while(i<=count){
                if(request.getParameter("print" + i) != null){
                    chosenRow = i;
                }
                i++;
            }
        %>
        <h1>Order Printout</h1>
        <h2>Farm Name: <%= request.getParameter("farmer_name" + chosenRow) %></h2>
        <table border="1" cellpadding="10%">
            <tbody>
                <tr>
                    <td id="column1">Farm ID: </td>
                    <td><%= request.getParameter("farmer_id" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Order ID: </td>
                    <td><%= request.getParameter("order_id" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Trade Date: </td>
                    <td><%= request.getParameter("trade_date" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Underlying Commodity Name: </td>
                    <td><%= request.getParameter("underlying_commodity_name" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Client Month: </td>
                    <td><%= request.getParameter("client_month" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Underlying Futures Code: </td>
                    <td><%= request.getParameter("underlying_futures_code" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Order Volume Quantity: </td>
                    <td><%= request.getParameter("order_volume_quantity" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Unit Type: </td>
                    <td><%= request.getParameter("unit_type" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Order Volume Contracts: </td>
                    <td><%= request.getParameter("order_volume_contracts" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Executed Strike: </td>
                    <td><%= request.getParameter("executed_strike" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Product Current Price Per Unit: </td>
                    <td><%= request.getParameter("product_current_price_per_unit" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Execution Product Total Cost: </td>
                    <td><%= request.getParameter("execution_product_total_cost" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Status: </td>
                    <td><%= request.getParameter("status" + chosenRow) %></td>
                </tr>
                <tr>
                    <td id="column1">Farm Physical Contract ID</td>
                    <td><%= request.getParameter("farm_physical_contract_id" + chosenRow) %></td>
                </tr>
            </tbody>
        </table>
                
    </body>
</html>