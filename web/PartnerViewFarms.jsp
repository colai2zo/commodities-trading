<%-- 
    Document   : PartnerViewFarms
    Created on : Aug 10, 2016, 3:35:12 PM
    Author     : Joey
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <title>Commodities Trading|View Farmers</title>
    </head>
    <body>
        <div id="header" align="center">
            <h1 style="align:center">View Current Farmers</h1>
        </div>
        <div id="central" align="center">
            <%
                //Create an instance of Farmers and Channel Partners to access database, create resultset with all farmers.
                Farmers farms = new Farmers();
                ResultSet farmData = farms.getFarmers();
                ChannelPartner channelPartner = new ChannelPartner();
            %>
            <table id="farmTable" border="1" cellpadding="15%" style="width:90%">
            <thead>
                <tr>
                    <th>Farm ID</th>
                    <th>Farm Name</th>
                    <th>Date and Time Added</th>
                </tr>
            </thead>
            <tbody>
                
                <%  try{
                        //Loop through all farmers in the database to fill the table.
                        while(farmData.next()){
                            int farmCount = farmData.getRow();
                            
                            //Only display those farmers that pertain to the particular channel partner that is logged in.
                            if(farmData.getString("channel_partner_id").equals(channelPartner.getIDByUsername(session.getAttribute("username").toString()))){
                                String formattedDate = new SimpleDateFormat("dd-MM-yyyy hh:mm").format(farmData.getTimestamp("timestamp"));
                %>
                            <tr>
                                <td name=<%=("farmerID" + farmCount)%>><%=farmData.getString("farmer_id")%></td>
                                <td name=<%=("firstName" + farmCount)%>><%=farmData.getString("name")%></td>
                                <td name=<%=("dateAdded" + farmCount)%>><%=formattedDate%></td>
                            </tr>
                <%
                            }
                        }
                    }catch(java.lang.NullPointerException e){
                        response.sendRedirect("login.jsp");
                    }
                %>
            </tbody>
        </table>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="PartnerFarmInfo.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%
        //Close the connections to the database.
        farmers.closeConnection();
        channelPartner.closeConnection();
    %>
</html>