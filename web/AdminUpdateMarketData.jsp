<%-- 
    Document   : AdminUpdateMarketData
    Created on : Nov 6, 2016, 11:48:01 AM
    Author     : Joey
--%>
<%@page import="java.sql.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Market Data</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h1>Update Market Data</h1>
        <h5>Please paste the code from your Bloomberg Request Jar application here, then click submit to update pricing.</h5>
        <div id="central">
            <form action="AdminSubmitMarketData.jsp" method="POST" align="center">
                <textarea name="sqlTextBox" rows="50" cols="150" placeholder="Drop text here." align="center"></textarea>
                <input type="submit" value="Submit Market Data" name="submitDataButton" />
            </form>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
