<%-- 
    Document   : PartnerFarmInfo
    Created on : Aug 10, 2016, 2:14:45 PM
    Author     : Joey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <div id="header" align="center" >
        <h1 text-align="center">Farmer Information</h1>
        </div>
        
        <div id="central" align="center">
        
            <h2>Please select from the following:</h2>
            <form name="AddFarmerForm" action="PartnerSetupFarm.jsp" method="POST">
                <input id="button" type="submit" value="Add Farm" name="addFarmButton" />
            </form>

            <form name="viewFarmersForm" action="PartnerViewFarms.jsp" method="POST">
                <input id="button" type="submit" value="View Existing Farms" name="viewFarmsButton" />
            </form>
            
        </div>
    </body>
    <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
</html>