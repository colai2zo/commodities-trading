<%-- 
    Document   : AdminSubmitUnwind
    Created on : Nov 10, 2016, 5:01:16 PM
    Author     : Development
--%>

<%@page import="java.sql.Timestamp"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.Orders"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%  /**
            * SUBMIT UNWIND
            * This page takes parameters from the AdminUnwindOrder.jsp page and uses them to submit an unwind to the database.
            */
            
            String orderNumber = request.getParameter("orderNumber"); //Retrieve the order number of the order to be unwound.
            //Create Orders instance and use to create ResultSet of all active orders.
            Orders orders = new Orders();
            ResultSet activeOrderData = orders.getOrder(orderNumber);
            activeOrderData.first();

            /**
             * Submit the unwind on the order using the parameters from the active order and from the unwind order page.
             */    
            orders.unwind(orderNumber,
                /**Unwind Order Data**/
                activeOrderData.getDouble("order_volume_quantity"),
                activeOrderData.getDouble("daily_trader_volatility"),
                Double.parseDouble(request.getParameter("unwind_value_paid_per_unit")),
                Double.parseDouble(request.getParameter("unwind_accrued_value_paid")), //unwind_accrued_value_paid,
                Double.parseDouble(request.getParameter("unwind_option_value_paid")), //unwind_option_value_paid,
                Double.parseDouble(request.getParameter("override_option_value_paid")), //override_option_value_paid,
                -(Math.abs(Double.parseDouble(request.getParameter("unwind_total_value_paid")))),
                /**Replacement Order Data**/
                Double.parseDouble(request.getParameter("orderVolumeUnits")),
                Double.parseDouble(request.getParameter("orderVolumeContracts")),
                Double.parseDouble(request.getParameter("productVolatility")),
                Double.parseDouble(request.getParameter("productFairValuePerUnit")),
                Double.parseDouble(request.getParameter("currentPricePerUnit")),
                Double.parseDouble(request.getParameter("overrideUnderlyingFuturesPrice")),
                Double.parseDouble(request.getParameter("executionProductTotalCost")),
                Double.parseDouble(request.getParameter("productVolatility")),
                Double.parseDouble(request.getParameter("replacementDelta")),
                Double.parseDouble(request.getParameter("replacementGamma")),
                Double.parseDouble(request.getParameter("replacementVega")),
                Double.parseDouble(request.getParameter("replacementTheta")),
                new Timestamp(new java.util.Date().getTime()) //Current Timestamp
            ); 
            
            //Change Status and Order Number Symbol to "Finalized" if the order is being completely unwound. 
            if(Double.parseDouble(request.getParameter("orderVolumeUnits")) == 0){
                orders.updateStatus(orderNumber + "A", "Finalized");
                orders.updateID(orderNumber + "A", orders.newOrderId(orderNumber + "A", "Finalized"));
            }
            
            //Redirect back to the Book Management page.
            response.sendRedirect("AdminManageBook.jsp");    
            %>
    </body>
    <%  //Close connection with database.
        orders.closeConnection();
        %>
</html>
