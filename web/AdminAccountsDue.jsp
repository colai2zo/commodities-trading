<%-- 
    Document   : AdminAccountsDue
    Created on : Feb 4, 2017, 7:38:25 PM
    Author     : Development
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page import="java.text.NumberFormat" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="http://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
        <title>Commodities Trading | Accounts Due</title>
    </head>
    <body onload="overwriteValues()">
        <%
            //Basic instantiations of the variables used on this page
            boolean isEmpty = false, moveOn = false, nextFarm = false;
            int count = 0;
            NumberFormat formatter = NumberFormat.getInstance();
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);
            Farmers farm = new Farmers();
            Orders orders = new Orders();
            ChannelPartner cp = new ChannelPartner();
            ResultSet orderData = null;
            
            //This variable will be null until the page is reloaded by the change of the channelPartnerSelect
            orderData = orders.getOrdersDue(request.getParameter("ChannelPartnerSelect"), "Original");
            
            //This will check if there is any data in the orderData ResultSet
            try{
                orderData.first();
                orderData.getString("order_id");
                orderData.beforeFirst();
            }catch(java.sql.SQLException e){
                //e.printStackTrace();
                isEmpty = true;
            }catch(java.lang.NullPointerException f){
                //f.printStackTrace();
                isEmpty = true;
            }
        %>
        <div id="header" align="center" style="padding-bottom: 20px">
            <h1>Accounts Due</h1>
            <form name="PartnerChooser" action="AdminAccountsDue.jsp" method="POST">
                <table>
                    <tr>
                        <td>
                            <select id="channelPartnerSelect" name="ChannelPartnerSelect" onchange="this.form.submit()">
                                <option value="default">Select Channel Partner</option>
                                <% 
                                    ResultSet partners = cp.getChannelPartners();
                                    //adds all the channel partners to the drop down
                                    while (partners.next()){
                                %>
                                <option value="<%=partners.getString("channel_partner_id")%>"><%=partners.getString("name")%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        
        <%
            if(isEmpty){
        %> 
        <h3 style="text-align: center">No accounts due. Select a Channel Partner</h3>
        <%  }else{  %>
        <h2 style="text-align: center"><%=cp.getNameByID(request.getParameter("ChannelPartnerSelect")) %></h2>
        <%
            String currentFarm = "", currentFarmName = "", currentFarmContractID = "";
            int currentRow = 0;
            double totalDueForOrder = 0, totalDueForFarmer = 0;
            ArrayList farms = new ArrayList();
            //goes through the orders resultSet and adds each new farmer to an ArrayList
            while(orderData.next()){
                if(!orderData.getString("farmer_id").equals(currentFarm) && orderData.getBoolean("isReconciled") == false){
                    currentFarm = orderData.getString("farmer_id");
                    farms.add(currentFarm);
                }
            }
            //goes through each farmer and creates a table for each of them
            for(int i = 0; i < farms.size(); i++){
                orderData.beforeFirst();
                totalDueForFarmer = 0;
                currentFarmName = farm.getNameById(farms.get(i).toString());
        %>
        <div>
            <table id="central" name="table" class="highlightRows" style="text-align: center; margin-bottom: 5%">
                <thead>
                    <tr>
                        <th><%= currentFarmName %></th>
                    </tr>
                    <tr style="background-color:rgba(50,200,200,.5)">
                        <th>Farm ID</th>
                        <th>Farm Physical Contract ID</th>
                        <th>Transaction Date</th>
                        <th>Order #</th>
                        <th>Order Volume Units</th>
                        <th>Underlying Futures Code</th>
                        <th>Client Month</th>
                        <th>Product Type</th>
                        <th>Expiration Date</th>
                        <th>Execution Product Total Cost</th>
                        <th>Unwind Values</th>
                        <th>Finalized Value</th>                       
                    </tr>
                </thead>
                <%
                    //within each farm's table, this adds the appropriate orders
                    while(orderData.next()){
                        currentRow = orderData.getRow();
                        currentFarmContractID = orderData.getString("farmer_physical_contract_id");
                        if(orderData.getString("farmer_id").equals(farms.get(i).toString())){
                            count++;
                            totalDueForOrder += orderData.getDouble("execution_product_total_cost");
                %>
                <tbody>
                        <tr>
                            <td><input type="hidden" name="<%= ("farmer_id" + count)%>" value="<%= orderData.getString("farmer_id")%>" /> <%= orderData.getString("farmer_id") %></td>
                            <td><input type="hidden" name="<%= ("farmer_physical_contract_id" + count)%>" value="<%= orderData.getString("farmer_physical_contract_id")%>" /> <%= orderData.getString("farmer_physical_contract_id") %></td>
                            <td><input type="hidden" name="<%= ("trade_date" + count)%>" value="<%= orderData.getDate("trade_date")%>" /> <%= orderData.getDate("trade_date") %></td>
                            <td><input type="hidden" name="<%= ("order_id" + count)%>" value="<%= orderData.getString("order_id")%>" /> <%= orderData.getString("order_id")%></td>                                    
                            <td><input type="hidden" name="<%= ("order_volume_quantity" + count)%>" value="<%= orderData.getDouble("order_volume_quantity")%>" /> <%= orderData.getDouble("order_volume_quantity")%></td>
                            <td><input type="hidden" name="<%= ("underlying_futures_code" + count)%>" value="<%= orderData.getString("underlying_futures_code")%>" /> <%= orderData.getString("underlying_futures_code")%></td>
                            <td><input type="hidden" name="<%= ("client_month" + count)%>" value="<%= orderData.getString("client_month")%>" /> <%= orderData.getString("client_month")%></td>
                            <% if(orderData.getString("product").equals("European") && orderData.getString("frequency").equals("Daily")){ %>
                            <td><input type="hidden" name="<%= ("product" + count)%>" value="Enhanced Market Average" />Enhanced Market Average</td>
                            <% }else if(orderData.getString("product").equals("European") && orderData.getString("frequency").equals("Bullet")){ %>
                            <td><input type="hidden" name="<%= ("product" + count)%>" value="Freedom Reprice" />Freedom Reprice</td>
                            <% }else{ %>
                            <td>-</td>
                            <% } %>
                            <td><input type="hidden" name="<%= ("expiration_date" + count)%>" value="<%= orderData.getDate("expiration_date")%>" /> <%= orderData.getDate("expiration_date") %></td>
                            <%if(orderData.getString("status").equals("Original")){ %>
                            <td><input type="hidden" name="<%= ("execution_product_total_cost" + count)%>" value="<%= formatter.format(orderData.getDouble("execution_product_total_cost"))%>" /> <%= formatter.format(orderData.getDouble("execution_product_total_cost"))%></td>
                            <td>-</td>
                            <td>-</td>
                            <%}else if(orderData.getString("status").equals("Unwind")){ %>
                            <td>-</td>
                            <td><input type="hidden" name="<%= ("execution_product_total_cost" + count)%>" value="<%= formatter.format(orderData.getDouble("execution_product_total_cost"))%>" /> (<%= formatter.format(Math.abs(orderData.getDouble("execution_product_total_cost")))%>)</td>
                            <td>-</td>
                            <% }else if(orderData.getString("status").equals("Finalized") || orderData.getString("status").equals("Invoiced")){ %>
                            <td>-</td>
                            <td>-</td>
                            <td><input type="hidden" name="<%= ("execution_product_total_cost" + count)%>" value="<%= formatter.format(orderData.getDouble("execution_product_total_cost"))%>" /> (<%= formatter.format(Math.abs(orderData.getDouble("execution_product_total_cost")))%>)</td>
                            <% } %>
                            <input type="hidden" name="<%="channelPartnerID" + count%>" value="<%=orderData.getString("channel_partner_id")%>"
                        </tr>
                        <%  
                            //checks if the next order is one that has the same contract ID
                            orderData.next();
                            if(!orderData.isAfterLast() 
                                    && (!orderData.getString("farmer_physical_contract_id").equals(currentFarmContractID) 
                                    || !(orderData.getString("farmer_id").equals(farms.get(i).toString()))))
                                moveOn = true;
                            //moves the pointer back to the current row
                            orderData.absolute(currentRow);
                            
                            //if it is time to move on to the next contract ID this will add the 'Total' row for that order
                            if((orderData.getString("farmer_id").equals(farms.get(i).toString()) && moveOn) || orderData.isLast()){
                                count++;
                                totalDueForFarmer += totalDueForOrder;
                        %>
                        <tr class="accountsDueTotalRow">
                            <td colspan="9" style="text-align:right"><strong>Client Owes For Order #<%=orderData.getString("order_id").substring(0, orderData.getString("order_id").length() - 2)%>:</strong></td>
                            <% if(totalDueForOrder >= 0){ %>
                            <td colspan="2"><%= formatter.format(Math.abs(totalDueForOrder))%></td>
                            <% }else if(totalDueForOrder < 0){ %>
                            <td colspan="2">(<%= formatter.format(Math.abs(totalDueForOrder))%>)</td>
                            <% }if (orderData.getString("order_id").indexOf("F") > 0 || orderData.getString("order_id").indexOf("E") > 0){ %>
                            <td>Finalized</td>
                            <% } %>
                        </tr>
                        <tr>
                            <td colspan="40"><br></td>
                        </tr>
                        <%
                                totalDueForOrder = 0;
                                moveOn = false;
                            }
                            orderData.next();
                            //checks if there is another farm to come after the current one
                            if(!orderData.isAfterLast() && !orderData.getString("farmer_id").equals(farms.get(i).toString())){ 
                                nextFarm = true;
                            }   
                            orderData.absolute(currentRow);

                            //adds a 'Total' row for the entire farm
                            if((nextFarm) || orderData.isLast()){  
                        %>
                        <tr class="accountsDueTotalRow">
                            <td colspan="9" style="text-align:right"><strong><%=currentFarmName%> Owes:</strong></td>
                            <% if(totalDueForFarmer >= 0){ %>
                            <td colspan="3"><%= formatter.format(Math.abs(totalDueForFarmer))%></td>
                            <% }else if(totalDueForFarmer < 0){ %>
                            <td colspan="3">(<%= formatter.format(Math.abs(totalDueForFarmer))%>)</td>
                            <% } %>
                        </tr>
                        <%  nextFarm = false;
                            }
                            }
                                }
                            }
                        }
                        %>
                    <input type="hidden" name="count" value="<%= count%>" />
                </tbody>
            </table>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminReports.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  orders.closeConnection();
        farm.closeConnection();
        cp.closeConnection(); %>
    <script>
    /**
     * Used to turn unwanted values into nice little dash marks
     * @returns nothing, when called it changes the page
     */    
    function overwriteValues(){
        var table = document.getElementsByName("table");
        for(var t = 0; t<table.length; t++)
            for (var i = 0; i<table[t].rows.length; i++)
                for(var j = 0; j<table[t].rows[i].cells.length; j++){
                    var innerText = table[t].rows[i].cells[j].innerHTML;
                    var visibleText = innerText.substring(innerText.indexOf(">") + 2);
                    if( visibleText === "-999.0" 
                            || visibleText === "-999.00" 
                            || visibleText === "-999"
                            || visibleText === "null" 
                            || visibleText === "N/A")
                        table[t].rows[i].cells[j].innerHTML = "-";
                }
    }
    </script>
</html>