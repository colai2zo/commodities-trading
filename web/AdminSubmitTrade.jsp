<%-- 
    Document   : AdminSubmitTrade
    Created on : Nov 20, 2016, 2:02:58 PM
    Author     : LucasCarey
--%>

<%@page import="com.dutchessdevelopers.commoditieswebsite.Orders"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Submit Trade</title>
    </head>
    <body>
        <%
            /**
             * SUBMIT TRADE
             * This page is run after the user clicks submit on the AdminTradeEntry.jsp page.
             * It takes the parameters entered in the trade, and inserts them into as a row of the Orders Database.
             */
           
            //DECLARATION of variables, objects, arrays.
            Orders order = new Orders(); //Instantate Orders object
            String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}; //List of months (used to convert month integer to month string).
            int count = Integer.parseInt(request.getParameter("count")); //Count of number of rows of trades (**CURRENTLY, ALWAYS 1**)
            
            /** THIS CODE IS RUN IF SUBMIT TRADE BUTTON PRESSED**/
            if(request.getParameter("submitTradeButton") != null){
                
                Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime()); //Create timestamp.
                
                //Run through code for as many rowas as there are in the trade entry page. (CURRENTLY 1 ROW).
                for(int i=1; i<=count; i++){
                    
                    if(request.getParameter("transactionType" + i).equals("Hedge")){
                        
                        /***************************
                         * INSERT TRADE FOR HEDGES *
                         ***************************/
                        
                        order.insertTrade(request.getParameter("transactionType"+i), //transaction type
                                request.getParameter("selectPartner"+i), //channel partner
                                request.getParameter("farmSelect"+i), //farmer
                                "-999", //farm contract id
                                request.getParameter("actionSelect"+i), //action
                                request.getParameter("underlyingFuturesCodeInput"+i), //futures code
                                request.getParameter("productName"+i), //commodity name
                                "-999", //client month
                                java.sql.Date.valueOf(request.getParameter("expirationDate"+i)), //expiration date
                                Double.parseDouble(request.getParameter("orderVolumeQuantity"+i)), //order volume in units
                                request.getParameter("unitType"+i), //unit type
                                Double.parseDouble(request.getParameter("orderVolumeContracts"+i)), //order volume in contracts
                                request.getParameter("instrumentSelect"+i), //instrument
                                "-999", //product
                                request.getParameter("frequency"+i), //frequency
                                Double.parseDouble(request.getParameter("executedStrike"+i)), //executed strike
                                Double.parseDouble(request.getParameter("executionCostPerUnit"+i)), //executiion cost per unit
                                Double.parseDouble(request.getParameter("executionTotalCost"+i)), //execution total cost
                                -999.0,//Double.parseDouble(request.getParameter("barrierLevel"+i)), //barrier level
                                -999.0,//Double.parseDouble(request.getParameter("digitalPayoutPerUnit"+i)), //digital payout per unit
                                -999.0,//Double.parseDouble(request.getParameter("digitalPayoutTotal"+i)), //digital total payout
                                new Orders().generateTradeID(), //order id number
                                request.getParameter("brokerSelect"+1), //broker
                                new java.sql.Date(new java.util.Date().getTime()), //set trade date as today
                                Double.parseDouble(request.getParameter("underlyingFuturesPrice" + i)), //underlying futures price
                                Double.parseDouble(request.getParameter("dailyTraderVolatility" + i)), // daily trader volatility
                                Double.parseDouble(request.getParameter("deltaContracts" + i)), //delta
                                Double.parseDouble(request.getParameter("gammaContracts" + i)), //gamma
                                Double.parseDouble(request.getParameter("vegaUnits" + i)), //vega
                                Double.parseDouble(request.getParameter("thetaUnits" + i)), //theta
                                currentTimestamp); //timestamp
                    }
                    
                    else if(request.getParameter("transactionType" + i).equals("Client Trade")){
                        
                        /***************************
                         * INSERT TRADE FOR CLIENT TRADES *
                         ***************************/
 
                        order.insertTrade(request.getParameter("transactionType"+i), //transaction type
                                request.getParameter("selectPartner"+i), //channel partner
                                request.getParameter("farmSelect"+i), //farmer
                                request.getParameter("farmerPhysicalContractId"+i), //farm contract id
                                request.getParameter("action"+i), //action
                                request.getParameter("underlyingFuturesCodeInput"+i), //futures code
                                request.getParameter("productName"+i), //commodity name
                                monthNames[Integer.parseInt(request.getParameter("months"+i)) - 1], //client month converted from integer to string
                                java.sql.Date.valueOf(request.getParameter("expirationDate"+i)), //expiration date
                                Double.parseDouble(request.getParameter("orderVolumeQuantity"+i)), //order volume in units
                                request.getParameter("unitType"+i), //unit type
                                Double.parseDouble(request.getParameter("orderVolumeContracts"+i)), //order volume in contracts
                                request.getParameter("instrumentSelect"+i), //instrument
                                "European", //product (always European for now)
                                request.getParameter("frequency"+i), //frequency
                                Double.parseDouble(request.getParameter("executedStrike"+i)), //executed strike
                                Double.parseDouble(request.getParameter("executionCostPerUnit"+i)), //executiion cost per unit
                                Double.parseDouble(request.getParameter("executionTotalCost"+i)), //execution total cost
                                -999.0,//Double.parseDouble(request.getParameter("barrierLevel"+i)), //barrier level
                                -999.0,//Double.parseDouble(request.getParameter("digitalPayoutPerUnit"+i)), //digital payout per unit
                                -999.0,//Double.parseDouble(request.getParameter("digitalPayoutTotal"+i)), //digital total payout
                                new Orders().generateTradeID(), //order number
                                request.getParameter("brokerSelect"+1), //broker
                                new java.sql.Date(new java.util.Date().getTime()), //set trade date as today
                                Double.parseDouble(request.getParameter("underlyingFuturesPrice" + i)), //underlying futures price
                                Double.parseDouble(request.getParameter("dailyTraderVolatility" + i)), // daily trader volatility
                                Double.parseDouble(request.getParameter("deltaContracts" + i)), //delta
                                Double.parseDouble(request.getParameter("gammaContracts" + i)), //gamma
                                Double.parseDouble(request.getParameter("vegaUnits" + i)), //vega
                                Double.parseDouble(request.getParameter("thetaUnits" + i)), //theta
                                currentTimestamp); //timestamp
                    }
                
                }
            }
            response.sendRedirect("AdminTradeEntry.jsp");
        %>
    </body>
    <%
        order.closeConnection();
        %>
</html>
