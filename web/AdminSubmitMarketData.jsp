<%-- 
    Document   : AdminSubmitMarketData
    Created on : Nov 12, 2016, 10:03:44 AM
    Author     : LucasCarey
--%>
<%@page import="java.sql.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.jquantlib.time.calendars.UnitedKingdom" %>
<%@page import="org.jquantlib.time.calendars.UnitedStates" %>
<%@page import="org.jquantlib.time.*" %>
<%@page import="java.util.Calendar" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    </body>
    <%  /**
        * SUBMIT MARKET DATA PAGE
        * This page performs 3 main functions:
        * 1. To parse the data entered in raw SQL format submitted from the textarea in the AdminUpdateMarketData.jsp page
        *    and to update the Pulled Data database with this parsed data.
        * 2. To loop through all active orders in the database, and, to unwind orders that have expired but not yet been completely unwound.
        *    When these orders are completely unwound, they become finalized orders.
        * 3. To loop through all active orders in the database that have not expired, and to update the black scholes data for these orders 
        *    with the updated market data and date.
        * FOR READING CONVENIENCE, The code is separated by these 3 SECTIONS.
        */
        
        try{
        
        /**********************************************************/
        /* SECTION 1: SQL Data Parsing and Entering into Database */
        /**********************************************************/
        
            //Declare neccesary components in order to establish a direct connection to the MySQL Database.
            String URL = "jdbc:mysql://localhost:3306/commodities_trading?autoReconnect=true&useSSL=false";
            String USERNAME = "root";
            String PASSWORD = "1234";
            Connection connection=null;
            
            //Declare Pulled Data and Orders objects (used in later sections, must be declared here due to scope).
            PulledData pd = new PulledData();
            Orders orders = new Orders();
            
            //Establish a connection to the database (if establishement fails, print error message to stack trace).
            try{
                connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            } catch (SQLException e){
                e.printStackTrace();
            }
            
            /*IF the data submission button was pressed,
              THEN the entered data will be parsed and entered into the database.*/
            if(request.getParameter("submitDataButton") != null){
                /** DELETE all previous entried in the Pulled Data database. */
                PreparedStatement truncate = connection.prepareStatement("truncate pulled_data;");
                truncate.executeUpdate();

                /**PARSE  SQL MARKET DATA and ENTER INTO DATABASE**/
                String fullSql = request.getParameter("sqlTextBox"); //The full string entered in UpdateMarketData.jsp.
                String subSql; //String used to store each individual SQL command.
                int count = 0; //Counter.
                //Continue loop while Full SQL string still exists.
                while (fullSql.length() > 0){
                    int index = fullSql.indexOf(";"); //Semicolons mark the separation of SQL commands.
                    //Extract SQL Command from full text.
                    if(count == 0){
                        subSql = fullSql.substring(0, index + 1);
                    }
                    else{
                        subSql = fullSql.substring(1, index + 1);
                    }
                    //Execute command to Database.
                    PreparedStatement update = connection.prepareStatement(subSql);
                    update.executeUpdate();
                    //Truncate  executed command off of full text.
                    fullSql = fullSql.substring(index + 1);
                }
        
        /*****************************************************************/
        /* DECLARATION AND INITIALIZATION OF NECESSARY VARIABLES/OBJECTS */
        /*****************************************************************/
        
                //Declaration of Date object containing today'd date and timestamp.
                org.jquantlib.time.Date today = new org.jquantlib.time.Date(new java.util.Date());
                Timestamp timestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
                
                //Create a ResultSet which contains all of the ACTIVE orders in the orders Database.
                orders = new Orders();
                ResultSet orderData = orders.getActiveOrders();
                
                pd = new PulledData(); //Initialize PulledData object.
                org.jquantlib.time.Calendar tradingCal = new org.jquantlib.time.Calendar(); //Initialize a trading calendar objects.
                
                //Loop through all active orders.
                while(orderData.next()){
                    pd = new PulledData();
                    ResultSet todaysData = pd.getTodaysDataByCode(orderData.getString("underlying_futures_code")); //Create one row result Set with all of Today's data for the current order's futures code.
                    todaysData.first(); //Move cursor to only row of ResultSet.
                    //Determine Relevant Trading Calendar
                    if(orderData.getString("instrument").equals("Call")){
                        tradingCal = new org.jquantlib.time.calendars.UnitedKingdom();
                    }else if(orderData.getString("instrument").equals("Put")){
                        tradingCal = new org.jquantlib.time.calendars.UnitedStates();
                    }
                    //Check if order is expired.
                    if(new org.jquantlib.time.Date(orderData.getDate("expiration_date")).compareTo(today) < 0){
                        
                        /**********************************************************/
                        /* SECTION 2: Unwinding and finaliztion of expired orders */
                        /**********************************************************/
                        
                        if(orderData.getString("frequency").toLowerCase().equals("bullet")){
                            
                            /*****************************
                             * SECTION 2A: BULLET ORDERS *
                             *****************************/
                            
                            int days = tradingCal.businessDaysBetween(new org.jquantlib.time.Date(new java.util.Date()), new org.jquantlib.time.Date(orderData.getDate("expiration_date")), true, true); //Determine days until expiration date.
                            Interpolation i = new Interpolation(orderData.getString("underlying_futures_code"), orderData.getDouble("executed_strike")); //Instantiate Interpolation object with required parameters from current order.
                            double interpolated_volatility = i.getRequestedVolatilityStrikeValue(); //Use i to get the requested volatility strike value.
                            //Calculate the interpolated interest rate for current row using black scholes model
                            double current_interest_rate = BlackScholes.calcCurrentInterestRate(
                                    todaysData.getDouble("interest_30_day"),
                                    todaysData.getDouble("interest_90_day"),
                                    todaysData.getDouble("interest_180_day"), 
                                    todaysData.getDouble("interest_360_day"),
                                    days);
                            
                            //Declare new instance of Black Scholes class with current values for current order.
                            BlackScholes bs = new BlackScholes(
                                    orderData.getString("instrument"),
                                    todaysData.getDouble("current_underlying_futures_price"),
                                    orderData.getDouble("executed_strike"),
                                    current_interest_rate, //INTEREST RATE
                                    0, //Dividend Yield Defaults to 0.0
                                    interpolated_volatility,
                                    0
                                    );
                            double unwind_option_value_paid = bs.getMarketPricePerUnit() * (orderData.getDouble("order_volume_quantity")); //Calculate unwind option value paid.

                            /**** PERFROM UNWIND USING UNWIND METHOD AND REQUIRED PARAMETERS FOR CURRENT ORDER****/
                            orders.unwind(
                                orderData.getString("order_id"),
                                orderData.getDouble("order_volume_quantity"),
                                orderData.getDouble("daily_trader_volatility"),
                                bs.getMarketPricePerUnit(),
                                0.0, //unwind accrued value paid <-- zero for bullets
                                unwind_option_value_paid,
                                unwind_option_value_paid,
                                unwind_option_value_paid - (bs.getVega() * orderData.getDouble("order_volume_quantity")), //unwind total value paid = unwind option value paid minus unwind vega
                                //replacement order
                                0.0, //no units are being put back on
                                0.0, //no units are being put back on
                                interpolated_volatility,
                                bs.getMarketPricePerUnit(),
                                bs.getMarketPricePerUnit(), //This is the current price per unit, it is the same because there is no product markup for these unwinds
                                todaysData.getDouble("current_underlying_futures_price"),
                                0.0, //price per unit * units
                                interpolated_volatility,
                                0.0, //the greek values are all zero
                                0.0,
                                0.0,
                                0.0,
                                timestamp
                            );
                        } 
                        
                        else if(orderData.getString("frequency").toLowerCase().equals("daily")){
                            
                            /*****************************
                             * SECTION 2B: DAILY ORDERS *
                             *****************************/
                            
                            //Declaration of Counters and dates necessary for Black Scholes Daily Accumulator.
                            String expiration = orderData.getDate("expiration_date").toString(); //Expiration date (string format)
                            String tradeDate = (new java.sql.Date(new java.util.Date().getTime())).toString(); //Trade Date (string format)
                            Interpolation i = new Interpolation(orderData.getString("underlying_futures_code"), orderData.getDouble("executed_strike")); //Interpolation object using current order's parameters.
                            double interpolated_volatility = i.getRequestedVolatilityStrikeValue(); //Use i to get requested volatility strike value.
                            
                            //Declare new instance of Black Scholes Daily Accumulator class with current values for current order.
                            BlackScholesDailyAccumulator bsda = new BlackScholesDailyAccumulator(orderData.getString("instrument"),
                                                                                                 todaysData.getDouble("current_underlying_futures_price"),
                                                                                                 orderData.getDouble("executed_strike"),
                                                                                                 orderData.getString("underlying_futures_code"),
                                                                                                 0,
                                                                                                 interpolated_volatility,
                                                                                                 tradeDate,
                                                                                                 expiration,
                                                                                                 orderData.getString("action"),
                                                                                                 orderData.getDouble("order_volume_quantity"),
                                                                                                 orderData.getInt("original_number_of_days"));

                            double sumVega = bsda.getDeltaTotal(); //Get the value for the Vega Accrued Sum.
                            double fairVal = bsda.getFairValueAvg(); //Get the value for the Product Fair Value / Unit average.
                            double unwind_option_value_paid = bsda.getOptionValue(); //Get the value for the unwind option value paid.
                            double unwind_accrued_value_paid = bsda.getAccuredValue(); //Get the value for the unwind accrued value paid.
                            
                            /**** PERFROM UNWIND USING UNWIND METHOD AND REQUIRED PARAMETERS FOR CURRENT ORDER****/
                            orders.unwind( 
                                orderData.getString("order_id"),
                                orderData.getDouble("order_volume_quantity"),
                                orderData.getDouble("daily_trader_volatility"),
                                (unwind_option_value_paid + sumVega) / orderData.getDouble("order_volume_quantity"),
                                unwind_accrued_value_paid,
                                unwind_option_value_paid,
                                unwind_option_value_paid + sumVega,
                                unwind_option_value_paid + sumVega,
                                //replacement order
                                0.0,
                                0.0,
                                interpolated_volatility,
                                fairVal,
                                fairVal,
                                todaysData.getDouble("current_underlying_futures_price"),
                                0.0,
                                interpolated_volatility,
                                0.0,
                                0.0,
                                0.0,
                                0.0,
                                timestamp
                            );
                        }
                    /**END SECTION 2**/    
                    
                    }
                    else{
                        
                        /*******************************************************************/
                        /* SECTION 3: Updating the non-expired orders Black Scholes Values */
                        /*******************************************************************/

                    //Create "Action Adjust" which will adjust the sign appropriately based upon the action of the order.
                    String action = orderData.getString("action").toLowerCase();
                    int actionAdjust = 1;
                    if(action.equals("sell")){
                        actionAdjust = -1;
                    }
                    
                    if(orderData.getString("frequency").equals("Bullet")){
                        
                        /*****************************
                        * SECTION 3A: BULLET ORDERS *
                        *****************************/
                        
                        double order_volume_units = orderData.getDouble("order_volume_quantity"); //Get the order volume units of the order.
                        double order_volume_contracts = orderData.getDouble("order_volume_contracts"); //Get the order volume contracts of the order.
                        int days = tradingCal.businessDaysBetween(new org.jquantlib.time.Date(new java.util.Date()), new org.jquantlib.time.Date(orderData.getDate("expiration_date")), true, true); //Determine business days between today and expiration date.
                        
                        //Calculate the interpolated interest rate for current order using black scholes model
                        double current_interest_rate = BlackScholes.calcCurrentInterestRate(
                                todaysData.getDouble("interest_30_day"),
                                todaysData.getDouble("interest_90_day"),
                                todaysData.getDouble("interest_180_day"), 
                                todaysData.getDouble("interest_360_day"),
                                days);
                        
                        //Declare new instance of Black Scholes class with current values for current order
                        BlackScholes bs = new BlackScholes(
                                orderData.getString("instrument"),
                                todaysData.getDouble("current_underlying_futures_price"),
                                orderData.getDouble("executed_strike"),
                                current_interest_rate, //INTEREST RATE
                                0, //Dividend Yield Defaults to 0.0
                                todaysData.getDouble("current_implied_volatility"),
                                days
                                );
                        
                        //Update black scholes values in the Orders database for this order.
                        orders.updateBlackScholes(orderData.getString("order_id"),actionAdjust * bs.getDelta() * order_volume_contracts,actionAdjust * bs.getGamma() * order_volume_contracts,actionAdjust * bs.getTheta() * order_volume_units,actionAdjust * bs.getVega()* order_volume_units);
                    }
                    
                    else if(orderData.getString("frequency").equals("Daily")){
                        
                        /***************************
                        * SECTION 3B: DAILY ORDERS *
                        ****************************/
                        
                        String expiration = orderData.getDate("expiration_date").toString(); //Expiration date of order in string format.
                        String tradeDate = (new java.sql.Date(new java.util.Date().getTime())).toString(); //Trade date of order in string format.
                        Interpolation i = new Interpolation(orderData.getString("underlying_futures_code"), orderData.getDouble("executed_strike")); //Interpolation object using parameters from current order.
                        double interpolated_volatility = i.getRequestedVolatilityStrikeValue(); //Use i to get requested volatility strike value.
                        
                        //Declare new instance of Black Scholes Daily Accumulator class with current values for current order
                        BlackScholesDailyAccumulator bsda = new BlackScholesDailyAccumulator(orderData.getString("instrument"),
                                                                                             todaysData.getDouble("current_underlying_futures_price"),
                                                                                             orderData.getDouble("executed_strike"),
                                                                                             orderData.getString("underlying_futures_code"),
                                                                                             0,
                                                                                             interpolated_volatility,
                                                                                             tradeDate,
                                                                                             expiration,
                                                                                             orderData.getString("action"),
                                                                                             orderData.getDouble("order_volume_quantity"),
                                                                                             orderData.getInt("original_number_of_days"));
                       
                       //Use BSDA to generate Average Values for gamma, vega, delta, and theta totals.
                       double delta = bsda.getDeltaTotal();
                       double gamma = bsda.getGammaTotal();
                       double vega = bsda.getVegaTotal();
                       double theta = bsda.getThetaTotal();
                       
                       //Update black scholes values in the Orders database for this order.
                       orders.updateBlackScholes(orderData.getString("order_id"), actionAdjust * delta, actionAdjust * gamma, actionAdjust * theta, actionAdjust * vega);
                    }
                }
                }
            }
            //Close connections to database.
            try{ 
                connection.close();
            } catch (SQLException E){
                E.printStackTrace();
            }
            pd.closeConnection();
            orders.closeConnection();
            //Redirect to Market Data viewing page.
            response.sendRedirect("AdminViewMarketData.jsp");
        } catch(Exception e){
            e.printStackTrace();
            //Redirect backward if an exception is caught.
            response.sendRedirect("AdminUpdateMarketData.jsp");
        }
    %>
</html>
 