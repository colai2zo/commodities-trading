'<%-- 
    Document   : AdminViewEmployees
    Created on : Aug 20, 2016, 2:23:23 PM
    Author     : Lucas
--%>

<%@page import="java.util.Calendar"%>
<%@page import="java.sql.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.Employee" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|View, Edit or Delete an Employee</title>
         <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
	<div id="header">
	    <h1>View, Edit or Delete an Employee</h1>
	</div>
	<div id="central" align="center">
        <%  //Get the password of the current employee to validate deletions.
            Employee employee = new Employee();
            ResultSet employeeData = employee.getEmployee(); 
            int employeeCount = 0;
            //Get Username from Session Attribute
            String username = "";
            try{
                username = session.getAttribute("username").toString();
            }catch(java.lang.NullPointerException e){
                response.sendRedirect("login.jsp");
            }
            //Get password that corresponds with username.
            String password = "";
            while(employeeData.next()){
                if(employeeData.getString("username").equals(username)){
                    password = employeeData.getString("password");
                }
            }
        %>
        <form name="submitForm" action="AdminViewEmployees.jsp" method="POST">
        <input type="hidden" id="password" value="<%= password%>" />
	<table id="employeeTable" border="1" cellpadding="15%">
	    <thead>
		<tr>
		    <td>First Name</td>
		    <td>Last Name</td>
                    <td>Username</td>
                    <td>Password</td>
                    <td>Administration Rights</td>
                    <td>Book Management Rights</td>
                    <td>Reporting Rights</td>
                    <td>Pricing Rights</td>
                    <td>Submit</td>
		    <td>Delete</td>
		</tr>
	    </thead>
	    <tbody>
                <% 
                    //Move result set iterator to beginning.
                    employeeData.beforeFirst();
                    //Loop through each of the employees and display the data in tabular format.
                    while(employeeData.next()){
                        employeeCount = employeeData.getRow(); //Update count to identify which row elements belong to.
                        //Retrieve the rights of the employee in the current row.
                        boolean admin = employeeData.getBoolean("admin");
                        boolean book = employeeData.getBoolean("book_management");
                        boolean reporting = employeeData.getBoolean("reporting");
                        boolean pricing = employeeData.getBoolean("pricing");
                        String checkboxValue = "unchecked"; //Stores whether box should be checked or not.
                %>
                <tr>
                    <td><%= employeeData.getString("first_name")%></td>
                    <td><%= employeeData.getString("last_name")%></td>
                    <td><input type="hidden" name="<%= ("username" + employeeCount)%>" value="<%= employeeData.getString("username")%>"/><%= employeeData.getString("username")%></td>
                    <td><%= employeeData.getString("password")%></td>
                    <%
                        //Assign value of checkbox based on whether this employee has admin rights.
                        if(admin){
                            checkboxValue = "checked";
                        }
                        else if(admin == false){
                            checkboxValue = "unchecked";
                        }
                    %>
                    <td style="text-align: center"><input type="checkbox" name="<%= ("adminRights" + employeeCount)%>" value="ON" <%= checkboxValue%>/></td>
                    <%  //Assign value of checkbox based on whether this employee has book management rights.
                        if(book){
                            checkboxValue = "checked";
                        }
                        else if(book == false){
                            checkboxValue = "unchecked";
                        }
                    %>
                    <td style="text-align: center"><input type="checkbox" name="<%= ("bookRights" + employeeCount)%>" value="ON" <%= checkboxValue%>/></td>
                    <%
                         //Assign value of checkbox based on whether this employee has reporting rights.
                        if(reporting){
                            checkboxValue = "checked";
                        }
                        else if(reporting == false){
                            checkboxValue = "unchecked";
                        }
                    %>
                    <td style="text-align: center"><input type="checkbox" name="<%= ("reportingRights" + employeeCount)%>" value="ON" <%= checkboxValue%>/></td>
                    <%
                        //Assign value of checkbox based on whether this employee has pricing rights.
                        if(pricing){
                            checkboxValue = "checked";
                        }
                        else if(pricing == false){
                            checkboxValue = "unchecked";
                        }
                    %>
                    <td style="text-align: center"><input type="checkbox" name="<%= ("pricingRights" + employeeCount)%>" value="ON" <%= checkboxValue%>/></td>
                    <td>
                        <input type="submit" value="Submit" name="<%= ("submitButton" + employeeCount)%>" />
                    </td>
                    <td>
                        <input type="submit" value="Delete" id="<%= ("deleteButton" + employeeCount)%>" name="<%= ("deleteButton" + employeeCount)%>" onclick="enterPassword(this)"/>
                        <input type="submit" value="Delete" id="<%= ("trueDeleteButton" + employeeCount)%>" name="<%= ("trueDeleteButton" + employeeCount)%>" style="display: none" />
                    </td>
                    
                </tr>
                <% } %>
	    </tbody>
	</table>
        </form>
            <script>
                /**
                * This method is for the purpose of validating the password when a user attempts to delete a broker.
                * It uses prompts to ask a user to enter their password and then confirm that they would like to delete the broker. 
                * @param {element} e the element which triggers the call of the method.
                */
                function enterPassword(e){
                    //USE PROMPT to allow user to enter password.
                    var password = prompt("Please enter your administrator password: ");
                    
                    /**PASSWORD CORRECT**/
                    if(password === document.getElementById("password").value){
                        
                        //Final Confirmation
                        var deletes = prompt("Are you sure? Please type 'YES' to confirm.");
                        if(deletes === 'YES'){
                            var i = e.id.substring(e.id.length - 1, e.id.length); //Row of the element.
                            document.getElementById("deleteButton" + i).style.display = "none";
                            document.getElementById("trueDeleteButton" + i).type = "hidden";
                        }
                    }
                    
                    /**PASSWORD CORRECT**/
                    else{
                        alert('Password Incorrect.');
                    }
                }
            </script>
        <%  /**
            * PERFORM UPDATES AND DELETIONS WHEN BUTTONS ARE CLICKED.
            */
            int count=0;
            employeeData.beforeFirst();
            //Loop through each of the employee rows to evaluate if the button has been clicked.
            while (employeeData.next()){
                count++;
                
                /**
                * FIRE ANY EMPLOYEE in Database for which the delete button was clicked and deletion confirmed.
                */
                if (request.getParameter("trueDeleteButton" + count) != null){
                    employee.fireEmployee(request.getParameter("username" + count));
                    response.setHeader("Refresh", "0; URL=AdminViewEmployees.jsp");
                }
                
                /**
                * UPDATE ANY EMPLOYEE in Database for which the update button was clicked.
                */
                if (request.getParameter("submitButton" + count) != null){
                    Boolean adminChecked = false;
                    Boolean bookManagementChecked = false;
                    Boolean reportingChecked = false;
                    Boolean pricingChecked = false;
                    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
                    
                    //Evaluate which checkboxes are checked and assign variables accordingly.
                    if(request.getParameter("adminRights" + count) != null)
                        adminChecked = true;
                    if(request.getParameter("bookRights" + count) != null)
                        bookManagementChecked = true;
                    if(request.getParameter("reportingRights" + count) != null)
                        reportingChecked = true;
                    if(request.getParameter("pricingRights" + count) != null)
                        pricingChecked = true;
                
                    //Use Employee method Update Employeee to update the employee with the new rights.
                    employee.updateEmployee(employeeData.getString("first_name"), employeeData.getString("last_name"), employeeData.getString("username"), employeeData.getString("password"), adminChecked, bookManagementChecked, reportingChecked, pricingChecked, currentTimestamp);
                    response.setHeader("Refresh", "0; URL=AdminViewEmployees.jsp"); //Redirect back to this page.
                }
            }
        %>
	</div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminEmployeeOptions.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  //Close connection with database.
        employee.closeConnection();
    %>
</html>