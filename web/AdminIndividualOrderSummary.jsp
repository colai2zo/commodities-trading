<%-- 
    Document   : AdminIndividualOrderSummary
    Created on : Sep 6, 2016, 12:24:48 PM
    Author     : LucasCarey
--%>
<%@page import="java.math.BigDecimal"%>
<%@page import="org.jquantlib.time.calendars.UnitedStates"%>
<%@page import="org.jquantlib.time.calendars.UnitedKingdom"%>
<%@page import="java.sql.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page import="org.jquantlib.time.*" %>
<%@page import="java.text.NumberFormat" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <style>
        .table-fixed{
        table-layout: fixed;
        width: 200%;    }
    .table-col-fixed-width{width:5%!important;border:1px solid black;}
        </style>
    <script src="http://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
        <script src="http://cdn.jsdelivr.net/jquery.scrollto/2.1.0/jquery.scrollTo.min.js"></script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trade | Single Order</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body onload="checkActive(), overwriteValues()">
        <div align='center'>
            <h1>Individual Order Summary</h1>
            
            <form name="unwindForm" action="AdminUnwindOrder.jsp" id="toUnwindPage" method="POST">
                <input id='button' type="button" value="Unwind/Finalize Additional Product" name="unwindButton" onclick="document.getElementById('submitToUnwind').submit()" />
            </form> 
            
            <h3>Daily Tracking of Accrued Cash</h3>
           
        </div>
        <%      
            /******************************************************
             * SECTION 1: DECLARATION OF VARIABLES, OBJECTS, ETC. *
             ******************************************************/

            //Create instance of NumberFormat in order to truncate decimals to 2 places.
            NumberFormat formatter = NumberFormat.getInstance();
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);

            //Create an instance of the orders and pulled data Database.
            Orders orders = new Orders();
            ResultSet orderData = null;
            //Assign order data of order searched to ResultSet object. (Redirect to AdminManageBook if the order number is not in the correct format.)
            try{
                orderData = orders.getAllRelatedOrders(request.getParameter("searchInput").toString());
            }catch(java.lang.NullPointerException e){
                response.sendRedirect("AdminManageBook.jsp");
            }
            PulledData pd = new PulledData();
            double  underlyingFuturesPrice = 0; //Declare variable for underlying futures price.
            
            /**MAIN TRY/CATCH LOOP TO CATCH ANY SQL ERRORS**/    
            try{
                
                //Set order data result set to its first row.
                orderData.first();
                
                //Create pulled data result, set cursor to its only row, and use it to get underlying future's price.
                ResultSet pdData = pd.getTodaysDataByCode(orderData.getString("underlying_futures_code"));
                pdData.first();
                
                underlyingFuturesPrice = pdData.getDouble("current_underlying_futures_price"); //Retrieve underlying futures price from pulled data.
                BlackScholes bs = null; //Declare an instance of BlackScholes
                
                //Loop through orders result set to set cursor on active order.
                orderData.beforeFirst();
                while(orderData.next()){
                    if(orderData.getString("status").equals("Active")){
                        break;
                    }

                } 
                
                /***************************
                 * SECTION 2: DAILY ORDERS *
                 ***************************/
                
                if(! orderData.isAfterLast() && orderData.getString("frequency").toLowerCase().equals("daily")){
                
                    //Instantiate trading calendars.
                    UnitedStates tradingCalUS = new UnitedStates();
                    UnitedKingdom tradingCalUK = new UnitedKingdom();
        %>
                    
            <div id="singleOrderScrollDiv" style="margin-bottom: 5%;overflow-x:scroll;table-layout: fixed!important;margin-top:100px;width:100%;border-right: 1px solid black">
            <table class="oddRowColor table-fixed">
                <tbody>
                    <%
                        //Get resultset cursor back to the active order.
                        orderData.beforeFirst();
                        while(orderData.next()){
                            if(orderData.getString("status").equals("Active")){
                                break;
                            }
                        }
                        
                        //Assign correct trading calendar based upon commodity traded in the order.
                        org.jquantlib.time.Calendar tradingCal = null;
                        if(orderData.getString("underlying_commodity_name").equals("Euronext Wheat") || orderData.getString("underlying_commodity_name").equals("Euronext Rapeseed")){
                            tradingCal = tradingCalUK;
                        }else if(orderData.getString("underlying_commodity_name").equals("Soybean Meal")){
                            tradingCal = tradingCalUS;
                        }

                        //Accessing of SQL dates from Database.
                        java.sql.Date tradeDatePull = orderData.getDate("trade_date"); //Date on which order was traded
                        java.sql.Date expirationDatePull = orderData.getDate("expiration_date"); //Expiry Date
                        
                        //Conversion of trade date / expiration date from SQL date to many different formats necessary
                        org.jquantlib.time.Date tableDate = new org.jquantlib.time.Date(tradeDatePull);
                        java.sql.Date sqlTableDate = new java.sql.Date(tableDate.shortDate().getTime() + 24*60*60*1000);
                        org.jquantlib.time.Date expirationDate = new org.jquantlib.time.Date(expirationDatePull);
                        
                        //Declare a date object representing the current time.
                        org.jquantlib.time.Date today = new org.jquantlib.time.Date(new java.util.Date());
                        
                        double interestRate = 0; //Declare variable for interest rate
                        int count = 0; //Declare counter variable.
                        
                        //DECLARE VARIABLES TO STORE ACCRUED VALUE TOTALS
                        double deltaUnits = 0;
                        double gammaUnits = 0;
                        double vegaUnits = 0;
                        double thetaUnits = 0;
                        double dailyVolumeExpiring = 0;
                        double total_daily_accrual = 0;
                        double option_value_totals = 0;
                        double total_gamma = 0;
                        double total_theta = 0;
                        double total_vega = 0;
                        double total_delta = 0;
                        double total_daily_accrued_volume_units = 0;
                        double total_daily_option_volume_units = 0;
                        double total_market_price_per_unit = 0;
                        
                        //Create a new instance of the interpolation class with the order's given parameters.
                        Interpolation i = new Interpolation(orderData.getString("underlying_futures_code").substring(0,4),orderData.getDouble("executed_strike"));
                        double interpolated_volatility = i.getRequestedVolatilityStrikeValue(); // Use i to retrieve requested volatility strike value.
                        
                        /************************************************************* 
                         *  SECTION 2A:                                              *
                         *  RUN THE ACCRUAL TABLE CALCULATIONS                       *
                         *  USE WHILE LOOP TO RUN THROUGH EACH DATE IN ACCRUAL TABLE *
                         *  CONTINUES UNTIL TABLE DATE REACHES EXPIRATION DATE       * 
                         *  INCREMENT THE TABLE DATE EACH TIME THROUGH               *   
                         *************************************************************/
                        
                        while (tableDate.shortDate().before(expirationDate.shortDate()) || tableDate.equals(expirationDate)){
                            
                            count ++; //Increment counter.
                            
                            //CRITICAL CALCULATION OF REMAINING CALENDAR DAYS BASED ON CURRENT ROW IN CALENDAR AND EXPIRY
                            int remaining_days = tradingCal.businessDaysBetween(today, tableDate, false, true) + 1; 
                            
                            //Time to expiration Percent 252
                            BigDecimal time_to_exp_pct = new BigDecimal(remaining_days / 252.0);
                            
                            //Use black scholes class and pulled data values to calculate the current interest rate.
                            interestRate = BlackScholes.calcCurrentInterestRate(pdData.getDouble("interest_30_day"), pdData.getDouble("interest_90_day"),
                                    pdData.getDouble("interest_180_day"), pdData.getDouble("interest_360_day"), remaining_days);
                            
                            /* Instantiate new Black Shcoles with given parameter values. */
                            bs = new BlackScholes(orderData.getString("instrument").toLowerCase(), orderData.getDouble("current_underlying_futures_price"), orderData.getDouble("executed_strike"),
                                    interestRate, 0, interpolated_volatility , remaining_days);
                            
                            //Declare necessary variables for accrual table.
                            ResultSet closeData = null;
                            boolean closedDataExists=false;
                            double euro_payout = 0;
                            double settlementPrice = 0;
                            
                            /*
                                IF the current date in the table row is BEFORE TODAY
                                THEN We get the market close data for that day and calculate the Euro Payout
                            */
                            if(tableDate.shortDate().before(new java.util.Date())){
                                 
                                closeData = pd.getMarketCloseData(sqlTableDate, pdData.getString("underlying_futures_code")); //Fill Closed Data Database.
                                 
                                //Assess whether or not closed data exists for that date and futures code. If it does, get the settlement price.
                                 if(closeData.first()){
                                    settlementPrice = closeData.getDouble("settlement_price");
                                    closedDataExists = true;
                                 }
                                 //If the closed data did not exist, set the settlement price to 0.
                                 else{
                                    settlementPrice= 0;
                                 } 
                                 
                                
                                
                                //Euro Payment Calculation made based upon instrument being call or put.
                                euro_payout = 0.0; 
                                if(orderData.getString("instrument").equals("call")){ 
                                    euro_payout = settlementPrice - orderData.getDouble("executed_strike");
                                    if(euro_payout < 0){ //Euro Payout can be no less than 0.
                                        euro_payout = 0;
                                    } 
                                }else if(orderData.getString("instrument").equals("put")){
                                    euro_payout = orderData.getDouble("executed_strike") - settlementPrice;
                                    if(euro_payout < 0){ //Euro Payout can be no less than 0.
                                        euro_payout = 0;
                                    }
                                }
                            }
                            
                            //Calculations for individual rows of accrual table.
                            dailyVolumeExpiring = (orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days")); //Calculation of Daily Volume Expiring Units
                            deltaUnits = bs.getDelta() * dailyVolumeExpiring; //Delta for current row in table.
                            gammaUnits = bs.getGamma() * dailyVolumeExpiring; //Gamma for current row in table.
                            vegaUnits = bs.getVega() * dailyVolumeExpiring;   //Vega for current row in table. 
                            thetaUnits = bs.getTheta() * dailyVolumeExpiring; //Theta for current row in table.
                            
                            
                            /**
                             * ADJUSTMENT of Black Scholes Value signs based upon the value of the action and of the instrument.
                             */
                            String action = orderData.getString("action"); //Action of the order.
                            String instrument = orderData.getString("instrument"); //Instrument used in the order.
                            if(action.toLowerCase().equals("buy")){
                                vegaUnits = Math.abs(vegaUnits);
                                thetaUnits = -1 * Math.abs(thetaUnits);
                                gammaUnits = Math.abs(gammaUnits);
                                if(instrument.toLowerCase().equals("call")){
                                    deltaUnits = Math.abs(deltaUnits);
                                }
                                else if(instrument.toLowerCase().equals("put")){
                                    deltaUnits = -1 * Math.abs(deltaUnits);
                                }
                            }
                            else if(action.toLowerCase().equals("sell")){
                                vegaUnits = -1 * Math.abs(vegaUnits);
                                thetaUnits = Math.abs(thetaUnits);
                                gammaUnits = -1 * Math.abs(gammaUnits);
                                if(instrument.toLowerCase().equals("call")){
                                    deltaUnits = -1 * Math.abs(deltaUnits);
                                }
                                else if(instrument.toLowerCase().equals("put")){
                                    deltaUnits = Math.abs(deltaUnits);
                                }
                            }
                            
                            //INCREMENTATIONS: Add each individual table row black scholes value to attain total.
                            if(today.compareTo(tableDate) <= 0){
                            total_delta += deltaUnits;
                            total_gamma += gammaUnits;
                            total_vega += vegaUnits;
                            total_theta += thetaUnits;
                            option_value_totals += bs.getMarketPricePerUnit() * orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days");
                            total_market_price_per_unit += bs.getMarketPricePerUnit();
                            }
                        
                            /** DISPLAY ACCRUAL TABLE VALUES: **/
                    %>
                    <tr>
                        <td class="table-col-fixed-width"><%=tableDate%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days")) %></td>
                        <% if(today.compareTo(tableDate) <= 0 && !time_to_exp_pct.equals(new BigDecimal(0))){ %>
                        <td class="table-col-fixed-width"><%= time_to_exp_pct.setScale(6, BigDecimal.ROUND_HALF_UP) %>%</td>
                        <% }else{ %>
                        <td class="table-col-fixed-width">-</td>
                        <% } %>
                        <td class="table-col-fixed-width"><%= formatter.format(interpolated_volatility)%></td>
                        <td class="table-col-fixed-width"><%=formatter.format(interestRate)%></td>
                        <td class="table-col-fixed-width"><%= orderData.getString("instrument")%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(orderData.getDouble("executed_strike"))%></td>
                        <% if(closedDataExists){ %>
                        <td class="table-col-fixed-width"><%= settlementPrice%></td>
                        <% }else{ %>
                        <td class="table-col-fixed-width">-</td>
                        <% }
                            if(tableDate.shortDate().before(today.shortDate())){
                                total_daily_accrued_volume_units += orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days");
                        %>
                        <td class="table-col-fixed-width"><%= euro_payout%></td>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width"><%= formatter.format(orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days")) %></td>
                        <td class="table-col-fixed-width"><%= formatter.format(orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days") * euro_payout)%></td>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width">-</td>
                        <%
                            }else{
                                total_daily_option_volume_units += orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days");   
                        %>
                        <td class="table-col-fixed-width">0</td>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width"><%= formatter.format(orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days")) %></td>
                        <td class="table-col-fixed-width"><%= formatter.format(underlyingFuturesPrice)%></td>
                        <%
                            }
                            if(today.compareTo(tableDate) <= 0){
                        %>
                        <td class="table-col-fixed-width"><%= formatter.format(bs.getMarketPricePerUnit())%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(bs.getMarketPricePerUnit() * orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days")) %></td>
                        <td class="table-col-fixed-width"><%= formatter.format(deltaUnits) %></td>
                        <td class="table-col-fixed-width"><%= formatter.format(gammaUnits) %></td>
                        <td class="table-col-fixed-width"><%= formatter.format(vegaUnits) %></td>
                        <td class="table-col-fixed-width"><%= formatter.format(thetaUnits) %></td>
                            <% }else{ %>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width">-</td>
                        <td class="table-col-fixed-width">-</td>
                        <% } %>
                    </tr>
                    <%
                        //After each row is generated, increment the date by one business day.
                        tableDate = tradingCal.advance(tableDate, 1, TimeUnit.Days);
                        } //End of table-generating while loop
                        
                        total_market_price_per_unit /= (double) count;
                        /** ACCRUED VALUE TOTALS **/
                    %>
                    <tr>
                        <td colspan="10"><strong>Totals:</strong></td>
                        <td class="table-col-fixed-width"><%= formatter.format(total_daily_accrued_volume_units)%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(total_daily_accrual)%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(total_daily_option_volume_units)%></td>
                        <td class="table-col-fixed-width"></td>
                        <td class="table-col-fixed-width"><%= formatter.format(total_market_price_per_unit)%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(option_value_totals)%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(total_delta)%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(total_gamma)%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(total_vega)%></td>
                        <td class="table-col-fixed-width"><%= formatter.format(total_theta)%></td>
                    </tr>
                    <tr>
                        <td colspan="10"></td>
                        <td class="table-col-fixed-width">Total Daily Accrued Volume Units</td>
                        <td class="table-col-fixed-width">Total Daily Accrual</td>
                        <td class="table-col-fixed-width">Daily Option Volume Units Total</td>
                        <td class="table-col-fixed-width"></td>
                        <td class="table-col-fixed-width">Total Market Price / Unit</td>
                        <td class="table-col-fixed-width">Option Value Totals</td>
                        <td class="table-col-fixed-width">Total Delta</td>
                        <td class="table-col-fixed-width">Total Gamma</td>
                        <td class="table-col-fixed-width">Total Vega</td>
                        <td class="table-col-fixed-width">Total Theta</td>
                    </tr>
                </tbody>
                
            <%-- FLOATING HEADERS: MUST BE ITS OWN TABLE BELOW THE TABLE ROWS--%>
            </table>
                    <table class="table-fixed" style="width:200%;text-align: center;color:#ddd;background-color:#333;position:fixed;left:auto;top:260px;height:50px;border:1px solid black;border-collapse:collapse" id="header">
                    <tr>
                    <td class="table-col-fixed-width">Daily Expiration Dates</td>
                    <td class="table-col-fixed-width">Daily Volume Expiring Units</td>
                    <td class="table-col-fixed-width">Time to Expiration Percent 252</td>
                    <td class="table-col-fixed-width">Daily Trader Volatility</td>
                    <td class="table-col-fixed-width">Interest Rate</td>
                    <td class="table-col-fixed-width">Instrument</td>
                    <td class="table-col-fixed-width">Executed Strike</td>
                    <td class="table-col-fixed-width">Market Close for Day</td>
                    <td class="table-col-fixed-width">Euro Payout</td>
                    <td class="table-col-fixed-width">Digital Payout</td>
                    <td class="table-col-fixed-width">Daily Accrued Volume Units</td>
                    <td class="table-col-fixed-width">Total Daily Accrual Value</td>
                    <td class="table-col-fixed-width">Daily Option Volume Units</td>
                    <td class="table-col-fixed-width">Current Underlying Futures Price</td>
                    <td class="table-col-fixed-width">Market Price / Unit</td>
                    <td class="table-col-fixed-width">Option Value Total</td>
                    <td class="table-col-fixed-width">Delta (Units)</td>
                    <td class="table-col-fixed-width">Gamma (Units)</td>
                    <td class="table-col-fixed-width">Vega ($)</td>
                    <td class="table-col-fixed-width">Theta ($)</td>
                    <td style="width:29px!important" > </td>
                    </tr>
            </table>
        </div>
          <div style="width:100%; overflow: scroll;">
            <% 
                /*********************************************************** 
                * SECTION 2B: INFORMATION FROM FULL BOOK FOR DAILY ORDERS *
                ***********************************************************/ 
            %>
            <table id="tbl" border="1" class="oddRowColor" style='width: 90%' cellpadding='2%'>
                <thead>
                    <tr>
                            <th class="main">Channel Partner</th>
                            <th class="main">Farm ID</th>
                            <th class="main">Order #</th>
                            <th class="main">Status</th>
                            <th class="main">Trade Date</th>
                            <th class="main">Transaction Type</th>
                            <th class="main">Underlying Commodity Name</th>
                            <th class="main">Underlying Futures Code</th>
                            <th class="main">Client Month</th>
                            <th class="main">Instrument</th>
                            <th class="main">Product</th>
                            <th class="main">Frequency</th>
                            <th class="main">Order Volume Units</th>
                            <th class="main">Unit Type</th>
                            <th class="main">Order Volume Contracts</th>
                            <th class="main">Executed Strike</th>
                            <th class="main">Expiration Date</th>
                            <th class="main">Product Executed Price / Unit</th>
                            <th class="main">Execution Product Total Cost</th>
                            <th class="main">Barrier Level</th>
                            <th class="main">Digital Payout / Unit</th>
                            <th class="main">Digital Total Payout</th>
                            <th class="main">Current Underlying Futures Price</th>
                            <th class="main">Daily Trader Volatility</th>
                            <th class="main">Accrued Volume Units Total</th>
                            <th class="main">Accrued Value Total</th>
                            <th class="main">Daily Option Volume Units Total</th>
                            <th class="main">Option Value Total</th>
                            <th class="main">Marked to Market Total</th>
                            <th class="main">Marked to Market / Unit</th>
                            <th class="main">Delta (Contracts)</th>
                            <th class="main">Gamma (Contracts)</th>
                            <th class="main">Vega ($)</th>
                            <th class="main">Theta ($)</th>
                            <th class="main">Unwind Accrued Value Paid</th>
                            <th class="main">Override Option Value Paid</th>
                            <th class="main">Unwind Total Value Paid</th>
                            <th class="main">Unwind Value Paid / Unit</th>
                            <th class="main">Total Trade Days</th>
                            <th class="main">Days Accrued</th>
                            <th class="main">Days Remaining</th>
                            <th class="main">Interest Rate</th>
                            <th class="main">Broker</th>
                            <th class="main">Farm Physical Contract ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%  
                            boolean found = false;   //Declare boolean which sees if the order is found.
                            double unitTypeValue = 50; // Declare conversion value to be changed based upon commodity.
                            
                            //Search for requested order in the database and assign the unit type value based upon commodity (if found)
                            orderData.beforeFirst();
                            while(orderData.next()){
                                if(orderData.getString("order_id").substring(0,orderData.getString("order_id").length()-1).equals(request.getParameter("searchInput")) ||
                                   orderData.getString("order_id").substring(0,orderData.getString("order_id").length()-2).equals(request.getParameter("searchInput"))){
                                    found = true; //order was found if this code is run.
                                    if(orderData.getString("underlying_commodity_name").equals("Soybean Meal")){
                                        unitTypeValue = 100;
                                    }else{
                                        unitTypeValue = 50;
                                    }
                        %>          
                        <tr>
                            <td><%=orderData.getString("channel_partner_id")%></td>
                            <td><%=orderData.getString("farmer_id")%></td>
                            <td><%=orderData.getString("order_id")%></td>
                            <td><%=orderData.getString("status")%><input type="hidden" name="status" class="status" value="<%=orderData.getString("status")%>" /></td>
                            <td><%=orderData.getDate("trade_date")%></td>
                            <td><%=orderData.getString("transaction_type")%></td>
                            <td><%=orderData.getString("underlying_commodity_name")%></td>
                            <td><%=orderData.getString("underlying_futures_code")%></td>
                            <td><%=orderData.getString("client_month")%></td>
                            <td><%=orderData.getString("instrument")%></td>
                            <td><%=orderData.getString("product")%></td>
                            <td><%=orderData.getString("frequency")%></td>
                            <td><%=(int)orderData.getDouble("order_volume_quantity")%></td>
                            <td><%=orderData.getString("unit_type")%></td>
                            <td><%=orderData.getDouble("order_volume_contracts")%></td>
                            <td><%=orderData.getDouble("executed_strike")%></td>
                            <td><%=orderData.getDate("expiration_date")%></td>
                            <td><%= formatter.format(orderData.getDouble("product_current_price_per_unit"))%></td>
                            <td><%=(int)(orderData.getDouble("execution_product_total_cost"))%></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <% if(orderData.getString("status").equals("Active")){ %>
                            <td><%=formatter.format(underlyingFuturesPrice)%></td>
                            <td><%=formatter.format(interpolated_volatility)%></td>
                            <td><%=formatter.format(total_daily_accrued_volume_units)%></td>
                            <td><%=formatter.format(total_daily_accrual)%></td>
                            <td><%=formatter.format(total_daily_option_volume_units)%></td>
                            <td><%= formatter.format(option_value_totals)%></td>
                            <td><%= formatter.format(total_daily_accrual + option_value_totals) %></td>
                            <td><%= formatter.format((total_daily_accrual + option_value_totals) / orderData.getDouble("order_volume_quantity")) %></td>
                            <td><%=formatter.format(total_delta / unitTypeValue)%></td>
                            <td><%=formatter.format(total_gamma / unitTypeValue)%></td>
                            <td><%=formatter.format(total_vega)%></td>
                            <td><%=formatter.format(total_theta)%></td>
                            <% }else{ %>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <% } %>
                            <td><%=formatter.format(orderData.getDouble("unwind_accrued_value_paid"))%></td>
                            <td><%=formatter.format(orderData.getDouble("override_option_value_paid"))%></td>
                            <td><%=formatter.format(orderData.getDouble("unwind_total_value_paid"))%></td>
                            <td><%=formatter.format(orderData.getDouble("unwind_value_paid_per_unit")) %></td>
                            <td><%=orderData.getInt("original_number_of_days")%></td>
                            <td><%=orderData.getInt("original_number_of_days") - (tradingCal.businessDaysBetween(today, new org.jquantlib.time.Date(orderData.getDate("expiration_date")), true, true))%></td>
                            <td><%=tradingCal.businessDaysBetween(today, new org.jquantlib.time.Date(orderData.getDate("expiration_date")), true, true)%></td>
                            <td><%=formatter.format(interestRate) %></td>
                            <td><%=orderData.getString("broker_name")%></td>
                            <td><%=orderData.getString("farmer_physical_contract_id")%></td>
                        </tr>
                        <%      }
                            }

                            /**PRINT AN ERROR MESSAGE IF THE ORDER COULD NOT BE FOUND**/
                            if(!found){
                        %>
                        <tr>
                            <td colspan='33' style='text-align: center'><strong>Order Not Found</strong></td>
                        </tr>
                        <%  
                            }
                        %>
                    </tbody>
            </table>
                    <br><br>
        </div>
                    <%
                        /********************************************************
                         * SECTION 2C: Create hidden text boxes for daily orders*                        
                         * (necessary for Unwind Order parameters)              *                        
                         ********************************************************/
                        double total_delta_contracts = total_delta / unitTypeValue;
                        double total_gamma_contracts = total_gamma / unitTypeValue;
                    %>
                    <form name ="submitToUnwind" id="submitToUnwind" action="AdminUnwindOrder.jsp" method="POST">
                        <input type="hidden" name="orderNumber" value="<%=request.getParameter("searchInput")%>" />
                        <input type="hidden" name="interest_rate" value="<%=interestRate%>"/>
                        <input type="hidden" name="total_daily_accrual_value" value="<%=total_daily_accrual%>" />
                        <input type="hidden" name="option_value_totals" value="<%=option_value_totals%>" />
                        <input type="hidden" name="total_vega" value="<%=total_vega%>" />
                        <input type="hidden" name="total_delta_contracts" value="<%=total_delta_contracts%>" />
                        <input type="hidden" name="total_gamma_contracts" value="<%=total_gamma_contracts%>" />
                        <input type="hidden" name="total_theta" value="<%=total_theta%>" />
                        <input type="hidden" name="total_daily_accrued_volume_units" value="<%=total_daily_accrued_volume_units%>" />
                        <input type="hidden" name="total_daily_option_volume_units" value="<%=total_daily_option_volume_units%>" />
                    </form>
                
                <%  
                    /**********************************************
                     * SECTION 3 :BULLET ORDERS                   *  
                     * SECTION 3A:BULLET VERSION OF ACCRUAL TABLE *
                     **********************************************/ 
                    }
                    else{
                        //Declaration of multi-region trading calendars.
                        UnitedStates tradingCalUS = new UnitedStates();
                        UnitedKingdom tradingCalUK = new UnitedKingdom();
                %>
                    <div>
            <table id="central" class="oddRowColor">
                <thead>
                    <tr>
                        <th colspan="20">Daily Tracking of Accrued Cash</th>
                    </tr>
                    <tr>
                        <th>Trade Date</th>
                        <th>Order Volume Quantity</th>
                        <th>Time to Expiration Percent 252</th>
                        <th>Daily Trader Volatility</th>
                        <th>Interest Rate</th>
                        <th>Instrument</th>
                        <th>Executed Strike</th>
                        <th>Market Close for Day</th>
                        <th>Euro Payout</th>
                        <th>Digital Payout</th>
                        <th>Daily Accrued Volume Units</th>
                        <th>Total Daily Accrual Value</th>
                        <th>Daily Option Volume Units</th>
                        <th>Current Underlying Futures Price</th>
                        <th>Market Price / Unit</th>
                        <th>Option Value Total</th>
                        <th>Delta (Units)</th>
                        <th>Gamma (Units)</th>
                        <th>Vega ($)</th>
                        <th>Theta ($)</th>
                    </tr>
                </thead>
                <tbody>
                <%      
                        /**
                        * BULLET accrual table calculations
                        */
                        
                        //Move order data cursor back to the active order.
                        orderData.beforeFirst();
                        while(orderData.next()){
                            if(orderData.getString("status").equals("Active")){
                                break;
                            }
                        }
                        
                        //Assign appropriate trading calendar based upon the commodity being traded.
                        org.jquantlib.time.Calendar tradingCal = null;
                        if(orderData.getString("underlying_commodity_name").equals("Euronext Wheat") || orderData.getString("underlying_commodity_name").equals("Euronext Rapeseed")){
                            tradingCal = tradingCalUK;
                        }else if(orderData.getString("underlying_commodity_name").equals("Soybean Meal")){
                            tradingCal = tradingCalUS;
                        }
                        
                        //Get necessary date objects in necessary formats.
                        java.sql.Date expirationDatePull = orderData.getDate("expiration_date");
                        org.jquantlib.time.Date expirationDate = new org.jquantlib.time.Date(expirationDatePull);
                        org.jquantlib.time.Date tradeDate = new org.jquantlib.time.Date(new java.util.Date());
                        
                        /** DECLARATION OF VARIABLES TO STORE TOTALS**/
                        double deltaUnits = 0;
                        double gammaUnits = 0;
                        double vegaUnits = 0;
                        double thetaUnits = 0;
                        double total_daily_accrual = 0;
                        double option_value_totals = 0;
                        double total_gamma = 0;
                        double total_theta = 0;
                        double total_vega = 0; 
                        double total_delta = 0;
                        double total_daily_accrued_volume_units = 0;
                        double total_daily_option_volume_units = 0;
                        
                        //Calculation of remaining days, interest rate, and interpolated volatility.
                        int remaining_days = tradingCal.businessDaysBetween(tradeDate, expirationDate, true, true); //Days between trade date and expiration date.
                        double interestRate = BlackScholes.calcCurrentInterestRate(pdData.getDouble("interest_30_day"), pdData.getDouble("interest_90_day"), pdData.getDouble("interest_180_day"), pdData.getDouble("interest_360_day"), remaining_days);
                        Interpolation i = new Interpolation(orderData.getString("underlying_futures_code").substring(0,4),orderData.getDouble("executed_strike")); //Create an instance of the interpolation class
                        double interpolated_volatility = i.getRequestedVolatilityStrikeValue(); //Use i to get the requested volatility strike value.
                        
                        /** CREATE AN INSTANCE OF BLACK SCHOLES to find all black scholes related values. **/
                        bs = new BlackScholes(orderData.getString("instrument").toLowerCase(), orderData.getDouble("current_underlying_futures_price"), orderData.getDouble("executed_strike"),
                                    interestRate, 0, interpolated_volatility, remaining_days);
                        
                        double euro_payout = 0; //Euro Payout 0 for bullet orders.
                        total_daily_option_volume_units = orderData.getDouble("order_volume_quantity"); //Daily option volume units = order volume units for bullet orders.
                        
                        //Use black scholes object to get black scholes values.
                        deltaUnits = bs.getDelta() * orderData.getDouble("order_volume_quantity"); 
                        gammaUnits = bs.getGamma() * orderData.getDouble("order_volume_quantity"); 
                        vegaUnits = bs.getVega() * orderData.getDouble("order_volume_quantity"); 
                        thetaUnits = bs.getTheta() * orderData.getDouble("order_volume_quantity"); 
                        option_value_totals = bs.getMarketPricePerUnit() * orderData.getDouble("order_volume_quantity"); 
                        
                        //Convert sign of black scholes values based upon the order's action and instrument.
                        String action = orderData.getString("action");
                        String instrument = orderData.getString("instrument");
                        if(action.toLowerCase().equals("buy")){
                            vegaUnits = Math.abs(vegaUnits);
                            thetaUnits = -1 * Math.abs(thetaUnits);
                            gammaUnits = Math.abs(gammaUnits);
                            if(instrument.toLowerCase().equals("call")){
                                deltaUnits = Math.abs(deltaUnits);
                            }
                            else if(instrument.toLowerCase().equals("put")){
                                deltaUnits = -1 * Math.abs(deltaUnits);
                            }
                        }
                        else if(action.toLowerCase().equals("sell")){
                            vegaUnits = -1 * Math.abs(vegaUnits);
                            thetaUnits = Math.abs(thetaUnits);
                            gammaUnits = -1 * Math.abs(gammaUnits);
                            if(instrument.toLowerCase().equals("call")){
                                deltaUnits = -1 * Math.abs(deltaUnits);
                            }
                            else if(instrument.toLowerCase().equals("put")){
                                deltaUnits = Math.abs(deltaUnits);
                            }
                        }
                        
                        //Since there is only one row in the table, the total values are equivelant to the values in that row.
                        total_theta = thetaUnits;
                        total_vega = vegaUnits;
                        total_gamma = gammaUnits;
                        total_delta = deltaUnits;
                        
                        /** DISPLAY OF ACCRUAL TABLE DATA: **/
                %>    
                    <tr>
                        <td><%=tradeDate%></td>
                        <td><%= formatter.format(orderData.getDouble("order_volume_quantity")) %></td>
                        <td>-</td>
                        <td><%= formatter.format(interpolated_volatility)%></td>
                        <td><%=formatter.format(interestRate)%></td>
                        <td><%= orderData.getString("instrument")%></td>
                        <td><%= formatter.format(orderData.getDouble("executed_strike"))%></td>
                        <td>-</td>
                        <td><%= euro_payout%></td>
                        <td>-</td>
                        <td>-</td>
                        <td><%= formatter.format(orderData.getDouble("order_volume_quantity") / (double)orderData.getInt("original_number_of_days") * euro_payout)%></td>
                        <td>-</td>
                        <td><%= formatter.format(underlyingFuturesPrice)%></td>
                        <td><%= formatter.format(bs.getMarketPricePerUnit())%></td>
                        <td><%= formatter.format(option_value_totals)%></td>
                        <td><%= formatter.format(deltaUnits) %></td>
                        <td><%= formatter.format(gammaUnits) %></td>
                        <td><%= formatter.format(vegaUnits) %></td>
                        <td><%= formatter.format(thetaUnits) %></td>
                    </tr>
                    </tbody>
                </table>
            </div>
              <div style="width:100%; overflow: scroll;">
                <%
                    /*********************************************************** 
                    * SECTION 3B: INFORMATION FROM FULL BOOK FOR BULLET ORDERS *
                    ***********************************************************/ 
                %>
                  <table id="tbl" border="1" class="oddRowColor" style='width: 90%' cellpadding='2%'>
                    <thead>
                        <tr>
                            <th class="main">Channel Partner</th>
                            <th class="main">Farm ID</th>
                            <th class="main">Order #</th>
                            <th class="main">Status</th>
                            <th class="main">Trade Date</th>
                            <th class="main">Transaction Type</th>
                            <th class="main">Underlying Commodity Name</th>
                            <th class="main">Underlying Futures Code</th>
                            <th class="main">Client Month</th>
                            <th class="main">Instrument</th>
                            <th class="main">Action</th>
                            <th class="main">Product</th>
                            <th class="main">Frequency</th>
                            <th class="main">Order Volume Units</th>
                            <th class="main">Unit Type</th>
                            <th class="main">Order Volume Contracts</th>
                            <th class="main">Executed Strike</th>
                            <th class="main">Expiration Date</th>
                            <th class="main">Product Executed Price / Unit</th>
                            <th class="main">Execution Product Total Cost</th>
                            <th class="main">Barrier Level</th>
                            <th class="main">Digital Payout / Unit</th>
                            <th class="main">Digital Total Payout</th>
                            <th class="main">Current Underlying Futures Price</th>
                            <th class="main">Daily Trader Volatility</th>
                            <th class="main">Accrued Volume Units Total</th>
                            <th class="main">Accrued Value Total</th>
                            <th class="main">Daily Option Volume Units Total</th>
                            <th class="main">Option Value Total</th>
                            <th class="main">Marked to Market Total</th>
                            <th class="main">Marked to Market / Unit</th>
                            <th class="main">Delta (Contracts)</th>
                            <th class="main">Gamma (Contracts)</th>
                            <th class="main">Vega ($)</th>
                            <th class="main">Theta ($)</th>
                            <th class="main">Unwind Accrued Value Paid</th>
                            <th class="main">Override Option Value Paid</th>
                            <th class="main">Unwind Total Value Paid</th>
                            <th class="main">Unwind Value Paid / Unit</th>
                            <th class="main">Total Trade Days</th>
                            <th class="main">Days Accrued</th>
                            <th class="main">Days Remaining</th>
                            <th class="main">Interest Rate</th>
                            <th class="main">Broker</th>
                            <th class="main">Farm Physical Contract ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%  
                            //Declare boolean to see if the order searched is found and the unit type conversion factor.
                            boolean found = false;
                            double unitTypeValue = 100;
                            
                            //Search for order searched for in the database. If found, assign the unit type value based upon the commodity.
                            orderData.beforeFirst();
                            while(orderData.next()){
                                if(orderData.getString("order_id").substring(0,orderData.getString("order_id").length()-1).equals(request.getParameter("searchInput")) ||
                                   orderData.getString("order_id").substring(0,orderData.getString("order_id").length()-2).equals(request.getParameter("searchInput"))){
                                    if(orderData.getString("underlying_commodity_name").equals("Soybean Meal")){
                                        unitTypeValue = 100;
                                    }else{
                                        unitTypeValue = 50;
                                    }
                                    found = true;
                        %>          
                        <tr>
                            <td><%=orderData.getString("channel_partner_id")%></td>
                            <td><%=orderData.getString("farmer_id")%></td>
                            <td><%=orderData.getString("order_id")%></td>
                            <td><%=orderData.getString("status")%><input type="hidden" name="status" class="status" value="<%=orderData.getString("status")%>" /></td>
                            <td><%=orderData.getDate("trade_date")%></td>
                            <td><%=orderData.getString("transaction_type")%></td>
                            <td><%=orderData.getString("underlying_commodity_name")%></td>
                            <td><%=orderData.getString("underlying_futures_code")%></td>
                            <td><%=orderData.getString("client_month")%></td>
                            <td><%=orderData.getString("instrument")%></td>
                            <td><%=orderData.getString("action")%></td>
                            <td><%=orderData.getString("product")%></td>
                            <td><%=orderData.getString("frequency")%></td>
                            <td><%=(int)orderData.getDouble("order_volume_quantity")%></td>
                            <td><%=orderData.getString("unit_type")%></td>
                            <td><%=orderData.getDouble("order_volume_contracts")%></td>
                            <td><%=orderData.getDouble("executed_strike")%></td>
                            <td><%=orderData.getDate("expiration_date")%></td>
                            <td><%= formatter.format(orderData.getDouble("product_current_price_per_unit"))%></td>
                            <td><%=(int)(orderData.getDouble("execution_product_total_cost"))%></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <% if(orderData.getString("status").equals("Active")){ %>
                            <td><%=formatter.format(underlyingFuturesPrice)%></td>
                            <td><%=formatter.format(interpolated_volatility)%></td>
                            <td><%=formatter.format(total_daily_accrued_volume_units)%></td>
                            <td><%=formatter.format(total_daily_accrual)%></td>
                            <td><%=formatter.format(total_daily_option_volume_units)%></td>
                            <td><%= formatter.format(option_value_totals)%></td>
                            <td><%= formatter.format(option_value_totals) %></td>
                            <td><%= formatter.format((option_value_totals) / orderData.getDouble("order_volume_quantity")) %></td>
                            <td><%=formatter.format(total_delta / unitTypeValue)%></td>
                            <td><%=formatter.format(total_gamma / unitTypeValue)%></td>
                            <td><%=formatter.format(total_vega)%></td>
                            <td><%=formatter.format(total_theta)%></td>
                            <% }else{ %>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <% } %>
                            <td><%=formatter.format(orderData.getDouble("unwind_accrued_value_paid"))%></td>
                            <td><%=formatter.format(orderData.getDouble("override_option_value_paid"))%></td>
                            <td><%=formatter.format(orderData.getDouble("unwind_total_value_paid"))%></td>
                            <td><%=formatter.format(orderData.getDouble("unwind_value_paid_per_unit")) %></td>
                            <td><%=orderData.getInt("original_number_of_days")%></td>
                            <td><%=orderData.getInt("original_number_of_days") - (tradingCal.businessDaysBetween(tradeDate, new org.jquantlib.time.Date(orderData.getDate("expiration_date")), true, true))%></td>
                            <td><%=tradingCal.businessDaysBetween(tradeDate, new org.jquantlib.time.Date(orderData.getDate("expiration_date")), true, true)%></td>
                            <td><%=formatter.format(interestRate) %></td>
                            <td><%=orderData.getString("broker_name")%></td>
                            <td><%=orderData.getString("farmer_physical_contract_id")%></td>
                        </tr>
                        <%      }
                            }
                            if(!found){
                        %>
                        <tr>
                            <td colspan='33' style='text-align: center'><strong>Order Not Found</strong></td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                    <br><br>
                    <%
                        /********************************************************
                         * SECTION 3C: Create hidden text boxes for bullet orders*                        
                         * (necessary for Unwind Order parameters)              *                        
                         ********************************************************/
                        double total_delta_contracts = total_delta / unitTypeValue;
                        double total_gamma_contracts = total_gamma / unitTypeValue;
                    %>
                    <form name ="submitToUnwind" id="submitToUnwind" action="AdminUnwindOrder.jsp" method="POST">
                        <input type="hidden" name="orderNumber" value="<%=request.getParameter("searchInput")%>" />
                        <input type="hidden" name="interest_rate" value="<%=interestRate%>"/>
                        <input type="hidden" name="total_daily_accrual_value" value="<%=total_daily_accrual%>" />
                        <input type="hidden" name="option_value_totals" value="<%=option_value_totals%>" />
                        <input type="hidden" name="total_vega" value="<%=total_vega%>" />
                        <input type="hidden" name="total_delta_contracts" value="<%=total_delta_contracts%>" />
                        <input type="hidden" name="total_gamma_contracts" value="<%=total_gamma_contracts%>" />
                        <input type="hidden" name="total_theta" value="<%=total_theta%>" />
                        <input type="hidden" name="total_daily_accrued_volume_units" value="<%=total_daily_accrued_volume_units%>" />
                        <input type="hidden" name="total_daily_option_volume_units" value="<%=total_daily_option_volume_units%>" />
                    </form>
                
                <%  
                        /**END OF BULLET VERSION**/ 
                        }

                    } 
                    catch(java.sql.SQLException e){
                        e.printStackTrace();
                        /**GENERATE AN ERROR MESSAGE IF AN EXCEPTION IS CAUGHT**/
                %>
                <h3>Unable to generate accrual table: No active order found. Please try with an active order to run accrual table.</h3>
                <%
                    }
                %>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminManageBook.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  
        //Close connections with database.
        orders.closeConnection();
        pd.closeConnection();
    %>
    <script>
        /**
         * This function checks to see if the order searched for is an active order. 
         * If the order is not active, the unwind button is disabled.
         */
        function checkActive(){
            var status = document.getElementsByClassName("status");
            var isActive = false;
            for(var i = 0; i < status.length; i++){
                if(status[i].value === "Active"){
                    isActive = true;   
                }
            }
            if(!isActive){
                document.getElementsByName("unwindButton")[0].disabled = true;
            }
        }
        
        /**
         * This function checks to see if any values are invalid.
         * It converts any such values to dashes.
         */
        function overwriteValues(){
            var table = document.getElementById("tbl");
            for (var i = 0; i<table.rows.length; i++)
                for(var j = 0; j<table.rows[i].cells.length; j++){
                    if(table.rows[i].cells[j].innerHTML === "-999.0" 
                            || table.rows[i].cells[j].innerHTML === "-999.00" 
                            || table.rows[i].cells[j].innerHTML === "-999" 
                            || table.rows[i].cells[j].innerHTML === "null" 
                            || table.rows[i].cells[j].innerHTML === "N/A")
                        table.rows[i].cells[j].innerHTML = "-";
                }
        }
        
        $(document).ready(function(){
                $("#singleOrderScrollDiv").scroll(function(){
                   $("#header").css("margin-left", "-"+($('#singleOrderScrollDiv').scrollLeft())+"px"); 
                });
                $(document).scroll(function(){
                    if($(document).scrollTop()>=260){
                        $("#header").css("position", "fixed");
                        $("#header").css("top","0");
                        $("#header").css("margin-left", "-" + ($("#singleOrderScrollDiv").scrollLeft() ) + "px");
                    } else {
                        $("#header").css("position", "absolute");
                        $("#header").css("top","260px");
                    }
                });
            });
         
    </script>
</html>