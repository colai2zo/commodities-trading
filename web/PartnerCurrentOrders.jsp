<%-- 
    Document   : PartnerCurrentOrders.jsp
    Created on : Aug 7, 2016, 3:34:23 PM
    Author     : Peter.Colaizzo
--%>
<%@page import="java.sql.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <style>
    tr:nth-child(even) {
    background-color: rgba(50,200,200,.5);
    }
    </style>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="30">
        <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
        <title>Commodities Trading | current orders</title>
    </head>
    <body>
         <div id="header" align="center" style="padding-bottom: 20px">
         <%
            //Open up connections to necessary databases.
            Farmers farmer = new Farmers();
            Orders orders = new Orders();
            ChannelPartner channelPartner = new ChannelPartner();
            
            /**Get username and Partner ID of Channel Partner who is using the system. **************** 
             * Used for purposes of only getting the correct data for this specific channel partner. **/
            String username = "";
            String pID = "";
            try{
                username = session.getAttribute("username").toString();
                pID = channelPartner.getIDByUsername(username);
            }catch(java.lang.NullPointerException e){
                response.sendRedirect("login.jsp");
            }
            
            //Uses Orders methods to get Pending and Recent orders for the channel partner who is logged in.
            ResultSet pendingOrderData = orders.getPendingOrdersForPartner(pID);
            ResultSet todaysOrderData = orders.getTodaysOrdersForPartner(pID);
            
            //Create calendar and date formatter to display the date.
            DateFormat dateFormat = new SimpleDateFormat("EEEE MMMM d, yyyy");
            Calendar cal = Calendar.getInstance();
         %>
        <h1>Current Orders: <%= dateFormat.format(cal.getTime())%></h1>
        </div>
        
        <div id="central" align="center">
            <table border="1" cellpadding="10%" style="width:90%">
                <thead>
                    <tr>
                        <th colspan="13">Pending Orders</th>
                    </tr>
                    <tr>
                        <th>Farm Name</th>
                        <th>Status</th>
                        <th>Farm ID</th>
                        <th>Order #</th>
                        <th>Underlying Commodity Name</th>
                        <th>Calendar Month</th>
                        <th>Underlying Futures Code</th>
                        <th>Order Volume Units</th>
                        <th>Unit Type</th>
                        <th>Order Volume Contracts</th>
                        <th>Indicative Product Strike Price</th>
                        <th>Product Current Price / Unit</th>
                        <th>Execution Product Total Cost</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        /**
                         * GENERATE PENDING ORDER TABLE
                         */
                        while(pendingOrderData.next()){
                    %>
                                <tr>
                                    <td><%= farmer.getNameById(pendingOrderData.getString("farmer_id"))%></td>
                                    <td><%= pendingOrderData.getString("status")%></td>
                                    <td><%= pendingOrderData.getString("farmer_id")%></td>
                                    <td><%= pendingOrderData.getString("order_id")%></td>
                                    <td><%= pendingOrderData.getString("underlying_commodity_name")%></td>
                                    <td><%= pendingOrderData.getString("client_month")%></td>
                                    <td><%= pendingOrderData.getString("underlying_futures_code")%></td>
                                    <td><%= (int)pendingOrderData.getDouble("order_volume_quantity")%></td>
                                    <td><%= pendingOrderData.getString("unit_type")%></td>
                                    <td><%= pendingOrderData.getDouble("order_volume_contracts")%></td>
                                    <td><%= pendingOrderData.getInt("strike_shift") + pendingOrderData.getDouble("current_underlying_futures_price") %></td>
                                    <td><%= pendingOrderData.getDouble("product_current_price_per_unit")%></td>
                                    <td><%= pendingOrderData.getDouble("execution_product_total_cost")%></td>
                                </tr>
                    <%
                        }
                    %>
                    
                </tbody>
            </table>
                <br><br>
            <table border="1" cellpadding="10%" style="width:90%">
                <thead>
                    <tr>
                        <th colspan="13">Executed Orders</th>
                    </tr>
                    <tr>
                        <th>Farm Name</th>
                        <th>Status</th>
                        <th>Farm ID</th>
                        <th>Order #</th>
                        <th>Underlying Commodity Name</th>
                        <th>Calendar Month</th>
                        <th>Underlying Futures Code</th>
                        <th>Order Volume Units</th>
                        <th>Unit Type</th>
                        <th>Order Volume Contracts</th>
                        <th>Executed Strike</th>
                        <th>Product Current Price / Unit</th>
                        <th>Execution Product Total Cost</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        /**
                         * GENERATE EXECUTED ORDER TABLE (original orders)
                         */
                        todaysOrderData.beforeFirst();
                        while(todaysOrderData.next())
                            if(todaysOrderData.getString("channel_partner_id").indexOf(pID) >= 0 && todaysOrderData.getString("status").equals("Original") && !todaysOrderData.getString("transaction_type").equals("Hedge")){
                    %>
                    <tr>
                        <td><%= farmer.getNameById(todaysOrderData.getString("farmer_id"))%></td>
                        <td><%= todaysOrderData.getString("status")%></td>
                        <td><%= todaysOrderData.getString("farmer_id")%></td>
                        <td><%= todaysOrderData.getString("order_id")%></td>
                        <td><%= todaysOrderData.getString("underlying_commodity_name")%></td>
                        <td><%= todaysOrderData.getString("client_month")%></td>
                        <td><%= todaysOrderData.getString("underlying_futures_code")%></td>
                        <td><%= (int)todaysOrderData.getDouble("order_volume_quantity")%></td>
                        <td><%= todaysOrderData.getString("unit_type")%></td>
                        <td><%= todaysOrderData.getDouble("order_volume_contracts")%></td>
                        <td><%= todaysOrderData.getDouble("executed_strike")%></td>
                        <td><%= todaysOrderData.getDouble("product_current_price_per_unit")%></td>
                        <td><%= todaysOrderData.getDouble("execution_product_total_cost")%></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
                <br><br>
                    
            <table border="1" cellpadding="10%" style="width:90%">
                <thead>
                    <tr>
                        <th colspan="13">Declined Orders</th>
                    </tr>
                    <tr>
                        <th>Farm Name</th>
                        <th>Status</th>
                        <th>Farm ID</th>
                        <th>Order #</th>
                        <th>Underlying Commodity Name</th>
                        <th>Calendar Month</th>
                        <th>Underlying Futures Code</th>
                        <th>Order Volume Units</th>
                        <th>Unit Type</th>
                        <th>Order Volume Contracts</th>
                        <th>Indicative Product Strike Price</th>
                        <th>Product Current Price / Unit</th>
                        <th>Execution Product Total Cost</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        /**
                         * GENERATE DECLINED ORDER TABLE
                         */
                        todaysOrderData.beforeFirst();
                        while(todaysOrderData.next())
                            if(todaysOrderData.getString("channel_partner_id").indexOf(pID) >= 0 && todaysOrderData.getString("status").equals("Decline")){
                    %>
                    <tr>
                        <td><%= farmer.getNameById(todaysOrderData.getString("farmer_id"))%></td>
                        <td><%= todaysOrderData.getString("status")%></td>
                        <td><%= todaysOrderData.getString("farmer_id")%></td>
                        <td><%= todaysOrderData.getString("order_id")%></td>
                        <td><%= todaysOrderData.getString("underlying_commodity_name")%></td>
                        <td><%= todaysOrderData.getString("client_month")%></td>
                        <td><%= todaysOrderData.getString("underlying_futures_code")%></td>
                        <td><%= (int)todaysOrderData.getDouble("order_volume_quantity")%></td>
                        <td><%= todaysOrderData.getString("unit_type")%></td>
                        <td><%= todaysOrderData.getDouble("order_volume_contracts")%></td>
                        <td><%= todaysOrderData.getDouble("strike_shift")%></td>
                        <td><%= todaysOrderData.getDouble("product_current_price_per_unit")%></td>
                        <td><%= todaysOrderData.getDouble("execution_product_total_cost")%></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>

        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  //Close connections to database.
        farmer.closeConnection();
        orders.closeConnection();
        channelPartner.closeConnection(); %>
</html>