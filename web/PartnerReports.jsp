<%-- 
    Document   : PartnerReports
    Created on : Sep 7, 2016, 9:25:17 PM
    Author     : Joey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Report Views</title>
    </head>
    <body>
        <h1>Report Views</h1>
        
        <div id="central" align="center">
            <form name="partnerPosition" action="PartnerPositionReport.jsp" method="POST">
                <input id="button" type="submit" value="View My Position" name="partnerPosition" />
            </form>
            <form name="orderHistory" action="PartnerOrderHistory.jsp" method="POST">
                <input id="button" type="submit" value="Order History" name="orderHistory" />
            </form>
            <form name="accountsDue" action="PartnerAccountsDue.jsp" method="POST">
                <input id="button" type="submit" value="Accounts Due" name="accountsDue" />
            </form>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>