<%-- 
    Document   : AdminEmployeePartnerOptions
    Created on : Sep 5, 2016, 12:01:44 PM
    Author     : LucasCarey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Setup Options</title>
    </head>
    <body>
        <div id="header" align="center">
            <h1>Channel Partner and Employee Options</h1>
        </div>
        <div id="central" align ="center">
            <form name="CPSetupForm" action="AdminPartnerOptions.jsp" method="POST">
                <input id="button" type="submit" value="Channel Partner Options" name="partnerOptionsButton"/>
            </form>

            <form name="employeeOptionsForm" action="AdminEmployeeOptions.jsp" method="POST">
                <input id="button" type="submit" value="Employee Options" name="employeeOptionsButton" />
            </form>
        </div>
        
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
