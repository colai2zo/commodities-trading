/* 
 * This JS file contains many necessary functions for validity checks.
 */

function checkNum(e){
    var val = e.value;
    switch(val.substring(val.length - 1, val.length)){
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '0':
            break;
        case '.':
            if(val.substring(0, val.length - 1).includes('.')){
                e.value = val.substring(0, val.length - 1);
            }
            break;
        default:
            e.value = val.substring(0, val.length - 1);
            break;

    }
}

function checkNumNoDecimals(e){
    var val = e.value;
    switch(val.substring(val.length - 1, val.length)){
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '0':
            break;
        default:
            e.value = val.substring(0, val.length - 1);
            break;

    }
}

function maxChars(element,num){
    var text = element.value;
    if(text.length > num){
        element.value = text.substring(0,text.length-1);
    }
}

