<%-- 
    Document   : AdminManageBook
    Created on : Aug 7, 2016, 2:49:07 PM
    Author     : Lucas Carey
--%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.sql.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <style>
        body{
            overflow-x:hidden;
        }
        th{
        padding: 5px 25px 5px 25px;
        }
        .table-fixed{
            table-layout: fixed;
            width: 200%;    }
        .table-col-fixed-width{width:3.7%!important;border:1px solid black;}
    </style>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Manage Book</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <script src="validityCheck.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.js"></script>

    </head>
    <body onload="changeColumns(), overwriteValues()">
        
        <div>
            <input id="uncheckAll" style="background-color: transparent" type="button" value="Uncheck All" name="uncheckAll" onclick="uncheckAll(), changeColumns()" />
            <input id="checkAll" style="background-color: transparent" type="button" value="Check All" name="checkAll" onclick="checkAll(), changeColumns()" />
        </div>        
    <div id="fullBookHeader" align="center">
            <h1>Complete Book</h1>
        <div id="checkboxes" style="width: 100%; height: 80%; float: left; overflow: scroll" align="center">
            <table border="0" cellpadding="3%">
                <tbody>
                    <tr>
                        <% 
                            if(session.getAttribute("channelPartner").toString().equals("true")){ %>
                            <td>
                                <label><input type="checkbox" id="channelPartner" class="checkbox" name="channelPartner" value="ON" onchange="changeColumns()" checked/>  Channel Partner</label>
                            </td>
                        <% }else{ %>
                            <td>
                                <label><input type="checkbox" id="channelPartner" class="checkbox" name="channelPartner" value="ON" onchange="changeColumns()"/>  Channel Partner</label>
                            </td>
                        <% } %>
                        <td>
                            <% if(session.getAttribute("farmID").toString().equals("true")){ %>
                                <label><input type="checkbox" id="farmID" class="checkbox" name="farmNumber" value="ON" onchange="changeColumns()" checked/>  Farm ID</label>
                            <% }else{ %>
                                <label><input type="checkbox" id="farmID" class="checkbox" name="farmNumber" value="ON" onchange="changeColumns()"/>  Farm ID</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("orderNumber").toString().equals("true")){ %>
                                <label><input type="checkbox" id="orderNumber" class="checkbox" name="orderNumber" value="ON" onchange="changeColumns()" checked/>  Order #</label>
                            <% }else{ %>
                                <label><input type="checkbox" id="orderNumber" class="checkbox" name="orderNumber" value="ON" onchange="changeColumns()"/>  Order #</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("orderNumber").toString().equals("true")){ %>
                            <label><input type="checkbox" id="status" class="checkbox" name="status" value="ON" onchange="changeColumns()" checked/>  Status</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="status" class="checkbox" name="status" value="ON" onchange="changeColumns()"/>  Status</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("tradeDate").toString().equals("true")){ %>
                            <label><input type="checkbox" id="tradeDate" class="checkbox" name="tradeDate" value="ON" onchange="changeColumns()" checked/>  Trade Date</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="tradeDate" class="checkbox" name="tradeDate" value="ON" onchange="changeColumns()"/>  Trade Date</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("transactionType").toString().equals("true")){ %>
                            <label><input type="checkbox" id="transactionType" class="checkbox" name="transactionType" value="ON" onchange="changeColumns()" checked/>  Transaction Type</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="transactionType" class="checkbox" name="transactionType" value="ON" onchange="changeColumns()"/>  Transaction Type</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("commodityName").toString().equals("true")){ %>
                            <label><input type="checkbox" id="commodityName" class="checkbox" name="underlyingCommodityName" value="ON" onchange="changeColumns()" checked/>  Underlying Commodity Name</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="commodityName" class="checkbox" name="underlyingCommodityName" value="ON" onchange="changeColumns()"/>  Underlying Commodity Name</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("futuresCode").toString().equals("true")){ %>
                            <label><input type="checkbox" id="futuresCode" class="checkbox" name="futuresCode" value="ON" onchange="changeColumns()" checked/>  Underlying Futures Code</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="futuresCode" class="checkbox" name="futuresCode" value="ON" onchange="changeColumns()"/>  Underlying Futures Code</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("clientMonth").toString().equals("true")){ %>
                            <label><input type="checkbox" id="clientMonth" class="checkbox" name="clientMonth" value="ON" onchange="changeColumns()" checked/>  Client Month</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="clientMonth" class="checkbox" name="clientMonth" value="ON" onchange="changeColumns()"/>  Client Month</label>
                            <% } %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <% if(session.getAttribute("expirationDate").toString().equals("true")){ %>
                            <label><input type="checkbox" id="expirationDate" class="checkbox" name="expirationDate" value="ON" onchange="changeColumns()" checked/>  Expiration Date</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="expirationDate" class="checkbox" name="expirationDate" value="ON" onchange="changeColumns()"/>  Expiration Date</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("action").toString().equals("true")){ %>
                            <label><input type="checkbox" id="action" class="checkbox" name="action" value="ON" onchange="changeColumns()" checked/>  Action</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="action" class="checkbox" name="action" value="ON" onchange="changeColumns()"/>  Action</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("instrument").toString().equals("true")){ %>
                            <label><input type="checkbox" id="instrument" class="checkbox" name="instrument" value="ON" onchange="changeColumns()" checked/>  Instrument</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="instrument" class="checkbox" name="instrument" value="ON" onchange="changeColumns()"/>  Instrument</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("product").toString().equals("true")){ %>
                            <label><input type="checkbox" id="product" class="checkbox" name="product" value="ON" onchange="changeColumns()" checked/>  Product</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="product" class="checkbox" name="product" value="ON" onchange="changeColumns()"/>  Product</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("frequency").toString().equals("true")){ %>
                            <label><input type="checkbox" id="frequency" class="checkbox" name="frequency" value="ON" onchange="changeColumns()" checked/>  Frequency</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="frequency" class="checkbox" name="frequency" value="ON" onchange="changeColumns()" />  Frequency</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("orderVolumeUnits").toString().equals("true")){ %>
                            <label><input type="checkbox" id="orderVolumeUnits" class="checkbox" name="orderVolumeQuantity" value="ON" onchange="changeColumns()" checked/>  Order Volume Units</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="orderVolumeUnits" class="checkbox" name="orderVolumeQuantity" value="ON" onchange="changeColumns()"/>  Order Volume Units</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("unitType").toString().equals("true")){ %>
                            <label><input type="checkbox" id="unitType" class="checkbox" name="unitType" value="ON" onchange="changeColumns()" checked/>  Unit Type</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="unitType" class="checkbox" name="unitType" value="ON" onchange="changeColumns()"/>  Unit Type</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("orderVolumeContracts").toString().equals("true")){ %>
                            <label><input type="checkbox" id="orderVolumeContracts" class="checkbox" name="orderVolumeContracts" value="ON" onchange="changeColumns()" checked/>  Order Volume Contracts</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="orderVolumeContracts" class="checkbox" name="orderVolumeContracts" value="ON" onchange="changeColumns()"/>  Order Volume Contracts</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("executedStrike").toString().equals("true")){ %>
                            <label><input type="checkbox" id="executedStrike" class="checkbox" name="executedStrike" value="ON" onchange="changeColumns()" checked/>  Executed Strike</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="executedStrike" class="checkbox" name="executedStrike" value="ON" onchange="changeColumns()"/>  Executed Strike</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("executedPricePerUnit").toString().equals("true")){ %>
                            <label><input type="checkbox" id="executedPricePerUnit" class="checkbox" name="executionProductCostPerUnit" value="ON" onchange="changeColumns()" checked/>  Executed Product Price / Unit</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="executedPricePerUnit" class="checkbox" name="executionProductCostPerUnit" value="ON" onchange="changeColumns()"/>  Executed Product Price / Unit</label>
                            <% } %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <% if(session.getAttribute("executedTotalCost").toString().equals("true")){ %>
                            <label><input type="checkbox" id="executedTotalCost" class="checkbox" name="executionProductTotalCost" value="ON" onchange="changeColumns()" checked/>  Executed Product Total Cost</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="executedTotalCost" class="checkbox" name="executionProductTotalCost" value="ON" onchange="changeColumns()"/>  Executed Product Total Cost</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("futuresPrice").toString().equals("true")){ %>
                            <label><input type="checkbox" id="futuresPrice" class="checkbox" name="currentUnderlyingFuturesPrice" value="ON" onchange="changeColumns()" checked/>  Current Underlying Futures Price</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="futuresPrice" class="checkbox" name="currentUnderlyingFuturesPrice" value="ON" onchange="changeColumns()"/>  Current Underlying Futures Price</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("volatility").toString().equals("true")){ %>
                            <label><input type="checkbox" id="volatility" class="checkbox" name="currentImpliedVolatility" value="ON" onchange="changeColumns()" checked/>  Daily Trader Volatility</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="volatility" class="checkbox" name="currentImpliedVolatility" value="ON" onchange="changeColumns()"/>  Daily Trader Volatility</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("delta").toString().equals("true")){ %>
                            <label><input type="checkbox" id="delta" class="checkbox" name="delta" value="ON" onchange="changeColumns()" checked/>  Delta (Contracts)</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="delta" class="checkbox" name="delta" value="ON" onchange="changeColumns()"/>  Delta (Contracts)</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("gamma").toString().equals("true")){ %>
                            <label><input type="checkbox" id="gamma" class="checkbox" name="gamma" value="ON" onchange="changeColumns()" checked/>  Gamma (Contracts)</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="gamma" class="checkbox" name="gamma" value="ON" onchange="changeColumns()"/>  Gamma (Contracts)</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("vega").toString().equals("true")){ %>
                            <label><input type="checkbox" id="vega" class="checkbox" name="vega" value="ON" onchange="changeColumns()" checked/>  Vega ($)</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="vega" class="checkbox" name="vega" value="ON" onchange="changeColumns()"/>  Vega ($)</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("theta").toString().equals("true")){ %>
                            <label><input type="checkbox" id="theta" class="checkbox" name="theta" value="ON" onchange="changeColumns()" checked/>  Theta ($)</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="theta" class="checkbox" name="theta" value="ON" onchange="changeColumns()"/>  Theta ($)</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("broker").toString().equals("true")){ %>
                            <label><input type="checkbox" id="broker" class="checkbox" name="broker" value="ON" onchange="changeColumns()" checked/>  Broker</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="broker" class="checkbox" name="broker" value="ON" onchange="changeColumns()"/>  Broker</label>
                            <% } %>
                        </td>
                        <td>
                            <% if(session.getAttribute("contractID").toString().equals("true")){ %>
                            <label><input type="checkbox" id="contractID" class="checkbox" name="farmerPhysicalContractID" value="ON" onchange="changeColumns()" checked/>  Farm Physical Contract ID</label>
                            <% }else{ %>
                            <label><input type="checkbox" id="contractID" class="checkbox" name="farmerPhysicalContractID" value="ON" onchange="changeColumns()"/>  Farm Physical Contract ID</label>
                            <% } %>
                        </td>
                    </tr>
                </tbody>
            </table> <br> <br> <br>
        </div>
        <script>
            /**
             * AJAX FUNCTION
             * Called every time a checkbox is checked or unchecked.
             * Makes a call to the checkbox servlet, sending over the ID of the box that was changed
             * AND the new value of the checkbox (checked or unchecked)
             * This updates the session dattribute so that the state remains, even if the page is refreshed.
             */
            $(document).ready(function() {
                //Run exeverytime a checkbox state changes.
                $('.checkbox').change(function() {
                    //Get the ID and the nw value of the chekcbox
                    var id = $(this).prop("id");
                    var val = $(this).prop("checked");
                    //Make the AJAX call.
                    $.ajax({
                        url: 'CheckBoxServlet',
                        data: {
                            item: id,
                            value: val
                        },
                        success: function(responseText){
                            console.log(responseText);
                        }
                    });
                });
            });
            
            /**
             * AJAX FUNCTION
             * Called when the Uncheck All button is clicked.
             * Makes a call to the uncheck all servlet, which updates the Session Attribute of ALL checkboxes to unchecked.
             */
            $(document).ready(function() {
                //Run when the uncheck all button is clicked.
                $('#uncheckAll').click(function() {
                    //Makes the AJAX call.
                    $.ajax({
                        url: 'UncheckAllServlet',
                        data: {
                            item: $(this).prop("id")
                        },
                        success: function(responseText){
                            console.log(responseText);
                        }
                    });
                });
            });
            
            /**
             * AJAX FUNCTION
             * Called when the Uncheck All button is clicked.
             * Makes a call to the uncheck all servlet, which updates the Session Attribute of ALL checkboxes to unchecked.
             */
            $(document).ready(function() {
                //Run when the check all button is clicked.
                $('#checkAll').click(function() {
                    //Makes the AJAX call.
                    $.ajax({
                        url: 'CheckAllServlet',
                        data: {
                            item: $(this).prop("id")
                        },
                        success: function(responseText){
                            console.log(responseText);
                        }
                    });
                });
            });
                
        </script>
        <div style="width: 60%; padding-bottom: 2%; " align="center">
            <div style="width: 80%; float: left" >
                <form name="sortForm" id="sortForm" action="AdminManageBook.jsp" method="POST">
                <table border="0">
                <tbody>
                    <tr>
                        <td>Sort 1: </td>
                        <td>
                            <select class="sort" id="s1" name="sortOptions1">
                                <option value="">None</option>
                                <option value="trade_date">Trade Date</option>
                                <option value="channel_partner_id">Channel Partner ID</option>
                                <option value="transaction_type">Transaction Type</option>
                                <option value="order_id">Order #</option>
                                <option value="underlying_commodity_name">Underlying Commodity Name</option>
                                <option value="underlying_futures_code">Underlying Futures Code</option>
                                <option value="instrument">Instrument</option>
                                <option value="expiration_date">Expiration Date</option>
                                <option value="broker_name">Broker Name</option>
                                <option value="farmer_id">Farm ID</option>
                                <option value="status">Status</option>
                            </select>
                        </td>
                        <td></td>
                        <td>Sort 2: </td>
                        <td>
                            <select class="sort" id="s2" name="sortOptions2" disabled="true">
                                <option value="">None</option>
                                <option value=",trade_date">Trade Date</option>
                                <option value=",channel_partner_id">Channel Partner ID</option>
                                <option value=",transaction_type">Transaction Type</option>
                                <option value=",order_id">Order #</option>
                                <option value=",underlying_commodity_name">Underlying Commodity Name</option>
                                <option value=",underlying_futures_code">Underlying Futures Code</option>
                                <option value=",instrument">Instrument</option>
                                <option value=",expiration_date">Expiration Date</option>
                                <option value=",broker_name">Broker Name</option>
                                <option value=",farmer_id">Farm ID</option>
                                <option value=",status">Status</option>
                            </select>
                        </td>
                        <td></td>
                        <td>Sort 3: </td>
                        <td>
                            <select class="sort" id="s3" name="sortOptions3" disabled="true">
                                <option value="">None</option>
                                <option value=",trade_date">Trade Date</option>
                                <option value=",channel_partner_id">Channel Partner ID</option>
                                <option value=",transaction_type">Transaction Type</option>
                                <option value=",order_id">Order #</option>
                                <option value=",underlying_commodity_name">Underlying Commodity Name</option>
                                <option value=",underlying_futures_code">Underlying Futures Code</option>
                                <option value=",instrument">Instrument</option>
                                <option value=",expiration_date">Expiration Date</option>
                                <option value=",broker_name">Broker Name</option>
                                <option value=",farmer_id">Farm ID</option>
                                <option value=",status">Status</option>
                            </select>
                        </td>
                        <td></td>
                        <td>Sort 4: </td>
                        <td>
                            <select class="sort" id="s4" name="sortOptions4" disabled="true">
                                <option value="">None</option>
                                <option value=",trade_date">Trade Date</option>
                                <option value=",channel_partner_id">Channel Partner ID</option>
                                <option value=",transaction_type">Transaction Type</option>
                                <option value=",order_id">Order #</option>
                                <option value=",underlying_commodity_name">Underlying Commodity Name</option>
                                <option value=",underlying_futures_code">Underlying Futures Code</option>
                                <option value=",instrument">Instrument</option>
                                <option value=",expiration_date">Expiration Date</option>
                                <option value=",broker_name">Broker Name</option>
                                <option value=",farmer_id">Farm ID</option>
                                <option value=",status">Status</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
                </table>
            <input disabled type="submit" value="Sort!"  id="sortButton" name="sort" />
            </form> 
                <script>
                    
                    /**
                     * JQUERY FUNCTION: SORTS
                     * Validity check for sorts.
                     * Runs every time that a sort sector is changed.
                     * Allows the enabling of each successive selector once the previous one is enabled.
                     */
                    $(document).ready(function() {
                        
                        //Function runs when a sort selector is changed.
                        $('.sort').change(function() {
                            
                            //Retrieve each of the sort selector elements.
                            var s1 = document.getElementById('s1');
                            var s2 = document.getElementById('s2');
                            var s3 = document.getElementById('s3');
                            var s4 = document.getElementById('s4');
                            
                            //Figure out if selectors 2-4 are disabled or not
                            var two = $('#s2').prop("disabled");
                            var three = $('#s3').prop("disabled");
                            var four = $('#s4').prop("disabled");
                            
                            //Runs this code if the second selector is disabled.
                            if(two === true){
                                //If the first selector has changed, enable the second one and the button.
                                if($('#s1').val() !== ""){
                                    $('#s2').prop("disabled",false);
                                    $('#sortButton').prop("disabled",false);
                                    //Disable the choice selected in the first selector.
                                    for(var i = 0 ; i < s2.options.length ; i++){
                                        if(s2.options[i].value === (',' + s1.value)){
                                            s2.options[i].disabled = true;
                                        }
                                    }
                                }
                            }
                            
                            //Runs this code if the third selector is disabled.
                            if(three === true){
                                //If the first selector and second selector have changed, enable the third one.
                                if($('#s1').val() !== "" && $('#s2').val() !== ""){
                                    $('#s3').prop("disabled",false);
                                    //Disable the choice selected in the first and second selectors.
                                    for(var i = 0 ; i < s3.options.length ; i++){
                                        if(s3.options[i].value === (',' + s1.value) || s3.options[i].value === s2.value){
                                            s3.options[i].disabled = true;
                                        }
                                    }
                                }
                            }
                            
                            //Runs this code if the fourth selector is disabled.
                            if(four === true){
                                //If the first selector, second selector, and third selector have changed, enable the fourth one.
                                if($('#s1').val() !== "" && $('#s2').val() !== "" && $('#s3').val() !== ""){
                                    //Disable the choice selected in the first,second, and third selectors.
                                    $('#s4').prop("disabled",false);
                                    for(var i = 0 ; i < s4.options.length ; i++){
                                        if(s4.options[i].value === (',' + s1.value) || s4.options[i].value === s2.value || s4.options[i].value === s3.value){
                                            s4.options[i].disabled = true;
                                        }
                                    }
                                }
                            }
                        });
                    });
                </script>
        </div>
        <div class="fixedUpperRight" style="width: 10%" >
            <table border="0">
                <tbody>
                    <tr>
                        <form name="searchForm" action="AdminIndividualOrderSummary.jsp">
                            <td>
                                <input type="text" name="searchInput" id="searchInput" value="" placeholder="Enter order number" size="20" autocomplete="off" onkeyup="checkNumNoDecimals(this)"/>
                            </td>
                            <td>
                                <input type="submit" value="Search" name="searchButton" method="POST" />
                            </td>
                        </form>
                    </tr>
                </tbody>
            </table>

        </div>
            </div>
            
    </div>

            <br><br>
            <%
                /**
                 * JAVA SCRIPLET USED TO ACCESS ORDERS DB FOR DISPLAY OF FULL BOOK
                 */
                //Access the Orders and ChannelPartner Databases.
                Orders orders = new Orders();
                ChannelPartner partner = new ChannelPartner();
                //Create a ResultSet Object containing all of the orders in the database.
                ResultSet orderData = orders.getOrders();
                //Create a nukmber formatter to format the decimals to have only two places.
                NumberFormat nf = NumberFormat.getInstance();
                nf.setMaximumFractionDigits(2);
                nf.setMinimumFractionDigits(2);
                
                //This code is run if the sort button is pressed. It retrives a sorted resultset object based upon the sorting parameters.
                if(request.getParameter("sort")!=null){
                    orderData = orders.getSortedOrders(request.getParameter("sortOptions1"),
                                                 request.getParameter("sortOptions2"),
                                                 request.getParameter("sortOptions3"),
                                                 request.getParameter("sortOptions4"));
                }
            %> 
        <div id="fullBookCentral" style="margin-bottom: 5%;overflow-x:scroll;table-layout: fixed!important;margin-top:335px;width:100%;border-right: 1px solid black" align="left">
            <table class="highlightRows fullBookTable table-fixed" border="1" id="tbl" style="text-align:center" name="fullBookTable">
                    <tbody>
                        <%
                            /** RUN WHILE LOOP TO GENERATE TABLE **/
                            boolean active = false;
                             while(orderData.next()){
                                 active = orderData.getString("status").equals("Active");
                        %>
                        <tr onclick="toggleClass(this,'selected');" >
                            <td class="table-col-fixed-width"><%= partner.getNameByID(orderData.getString("channel_partner_id"))%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("farmer_id")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("order_id")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("status")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getDate("trade_date")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("transaction_type")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("underlying_commodity_name")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("underlying_futures_code")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("client_month")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getDate("expiration_date")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("action")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("instrument")%></td>
                            <td class="table-col-fixed-width">European</td>
                            <td class="table-col-fixed-width"><%=orderData.getString("frequency")%></td>
                            <td class="table-col-fixed-width"><%=(int)orderData.getDouble("order_volume_quantity")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("unit_type")%></td>
                            <td class="table-col-fixed-width"><%=nf.format(orderData.getDouble("order_volume_contracts"))%></td>
                            <td class="table-col-fixed-width"><%=nf.format(orderData.getDouble("executed_strike"))%></td>
                            <td class="table-col-fixed-width"><%=nf.format(orderData.getDouble("product_current_price_per_unit"))%></td>
                            <td class="table-col-fixed-width"><%=nf.format((int)(orderData.getDouble("execution_product_total_cost")))%></td>
                            <td class="table-col-fixed-width"><%=nf.format(orderData.getDouble("current_underlying_futures_price"))%></td>
                            <% if(active){ %>
                            <td class="table-col-fixed-width"><%=nf.format(orderData.getDouble("daily_trader_volatility"))%></td>
                            <% } else{ %>
                            <td class="table-col-fixed-width">-999</td>
                            <% } %>
                            <td class="table-col-fixed-width"><%=nf.format(orderData.getDouble("delta"))%></td>
                            <td class="table-col-fixed-width"><%=nf.format(orderData.getDouble("gamma"))%></td>
                            <td class="table-col-fixed-width"><%=nf.format(orderData.getDouble("vega"))%></td>
                            <td class="table-col-fixed-width"><%=nf.format(orderData.getDouble("theta"))%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("broker_name")%></td>
                            <td class="table-col-fixed-width"><%=orderData.getString("farmer_physical_contract_id")%></td>
                        </tr> 
                        <%
                            }
                        %> 
                    </tbody>
            </table> 
                    <table class="table-fixed" style="width:200%;text-align: center;color:#ddd;background-color:#333;position:fixed;left:auto;top:350px;height:50px;border:1px solid black;border-collapse:collapse" id="header">
                    <tr>
                        <td class="table-col-fixed-width">Channel Partner</td>
                        <td class="table-col-fixed-width">Farm ID</td>
                        <td class=" table-col-fixed-width">Order #</td>
                        <td class=" table-col-fixed-width">Status</td>
                        <td class=" table-col-fixed-width">Trade Date</td>
                        <td class=" table-col-fixed-width">Transaction Type</td>
                        <td class=" table-col-fixed-width">Underlying Commodity Name</td>
                        <td class=" table-col-fixed-width">Underlying Futures Code</td>
                        <td class=" table-col-fixed-width">Client Month</td>
                        <td class=" table-col-fixed-width">Expiration Date</td>
                        <td class=" table-col-fixed-width">Action</td>
                        <td class=" table-col-fixed-width">Instrument</td>
                        <td class=" table-col-fixed-width">Product</td>
                        <td class=" table-col-fixed-width">Frequency</td>
                        <td class=" table-col-fixed-width">Order Volume Units</td>
                        <td class=" table-col-fixed-width">Unit Type</td>
                        <td class=" table-col-fixed-width">Order Volume Contracts</td>
                        <td class=" table-col-fixed-width">Executed Strike</td>
                        <td class=" table-col-fixed-width">Executed Product Price / Unit</td>
                        <td class=" table-col-fixed-width">Executed Product Total Cost</td>
                        <td class=" table-col-fixed-width">Current Underlying Futures Price</td>
                        <td class=" table-col-fixed-width">Daily Trader Volatility</td>
                        <td class=" table-col-fixed-width">Delta (Contracts)</td>
                        <td class=" table-col-fixed-width">Gamma (Contracts)</td>
                        <td class=" table-col-fixed-width">Vega ($)</td>
                        <td class=" table-col-fixed-width">Theta ($)</td>
                        <td class=" table-col-fixed-width">Broker</td>
                        <td class=" table-col-fixed-width">Farm Physical Contract ID</td>
                        <td style="width:29px"> </td>
                    </tr>
            </table>
                    <%  //Close connections with the database.
                        orders.closeConnection(); 
                        partner.closeConnection(); 
                    %>
        </div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <script>
            /**
             * This function is run when the check all button is pressed.
             * It sets the "checked" attribute of all of the checkboxes to true.
             */
            function checkAll(){
                var checkboxes = document.getElementsByClassName("checkbox");
                for(var i = 0; i<checkboxes.length; i++){
                    checkboxes[i].checked = true;
                }
            }
            
            /**
             * This function is run when the uncheck all button is pressed.
             * It sets the "checked" attribute of all of the checkboxes to false.
             */
            function uncheckAll(){
                var checkboxes = document.getElementsByClassName("checkbox");
                for(var i = 0; i<checkboxes.length; i++){
                    checkboxes[i].checked = false;
                }
            }
            
            /**
             * This function is run whenever a checkbox is checked or uncheked.
             * It sets the display of the respective column in the table to "none" if unchecked.
             * It displays the respective column in the table if checked.
             */
            function changeColumns(){
                //Retrieve the required elements.
                var table = document.getElementById("tbl");
                var header = document.getElementById("header");
                var checkboxes = document.getElementsByClassName("checkbox");
                
                //Loop through each of the checkboxes.
                for(var i = 0; i<checkboxes.length; i++){
                    //Set all checkboxes that are unchecked columns to not display
                    if(checkboxes[i].checked === false){
                        for (var j = 0; j < tbl.rows.length; j++) {
                           table.rows[j].cells[i].style.display = "none";
                           header.rows[0].cells[i].style.display = "none";
                        }
                    //Set all checkboxes that are checked columns to display
                    }else if(checkboxes[i].checked === true){

                        for (var j = 0; j < tbl.rows.length; j++) {
                           table.rows[j].cells[i].style.display = "";
                           header.rows[0].cells[i].style.display = "";
                        }
                    }
                }  
            }
            
            /**
            * This function checks to see if any values are invalid.
            * It converts any such values to dashes.
            */
            function overwriteValues(){
                var table = document.getElementById("tbl");
                for (var i = 0; i<table.rows.length; i++)
                    for(var j = 0; j<table.rows[i].cells.length; j++){
                        if(table.rows[i].cells[j].innerHTML === "-999.0" 
                                || table.rows[i].cells[j].innerHTML === "-999.00" 
                                || table.rows[i].cells[j].innerHTML === "-999" 
                                || table.rows[i].cells[j].innerHTML === "null" 
                                || table.rows[i].cells[j].innerHTML === "N/A")
                            table.rows[i].cells[j].innerHTML = "-";
                    }
            }
            
            /**
             * This function is used to toggle the class of a row. If it is one class then it is 
             * highlighted, if it blank then it is ordinary
             * @param el the element that has been clicked
             * @param className the name of the class that is being toggled on and off
             */
            function toggleClass(el, className) {
                /*this if-statement checks if the element that was clicked already has the same classname
                as the one that was passed in */
                if (el.className.indexOf(className) >= 0) {
                    //if it does then the classname is set to blank, allowing it to go back to the original formatting
                  el.className = el.className.replace(className,"");
                }
                else {
                    //class name of the element is set to the passed in name
                  el.className  += className;
                }
            }
            /**
             * Floating Header Code
             * Comments to be written by Dino Martinez.
             */
            $(document).ready(function(){
                $("#fullBookCentral").scroll(function(){
                   $("#header").css("margin-left", "-"+($('#fullBookCentral').scrollLeft())+"px"); 
                });
                $(document).scroll(function(){
                    if($(document).scrollTop()>=350){
                        $("#header").css("position", "fixed");
                        $("#header").css("top","0");
                        $("#header").css("margin-left", "-" + ($("#fullBookCentral").scrollLeft() ) + "px");
                    } else {
                        $("#header").css("position", "absolute");
                        $("#header").css("top","350px");
                    }
                });
            });
        </script>
    </body>
</html>