<%-- 
    Document   : PartnerOrderHistory.jsp
    Created on : Aug 7, 2016, 4:15:42 PM
    Author     : Peter.Colaizzo
--%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page import="java.text.NumberFormat" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <script src="http://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
    <head>
        <style>
        tr:nth-child(even){
            background-color: rgba(50,200,200,.5);
        }
        </style>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
        <title>Commodities Trading | Order History</title>
    </head>
    <body onload="overwriteValues()">
        <%
            NumberFormat formatter = NumberFormat.getInstance();
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);
            Orders orders = new Orders();
            Farmers farmer = new Farmers();
            ChannelPartner cp = new ChannelPartner();
            ResultSet orderData = orders.getOrders();
            String username = "",  pID = "";
            try{
                username = session.getAttribute("username").toString();
                pID = cp.getIDByUsername(username);
            }catch(java.lang.NullPointerException e){
                //e.printStackTrace();
                response.sendRedirect("login.jsp");
            }
        %>
        <div id="header" align="center" style="padding-bottom: 20px">
            <h1>Order History</h1>
            <form name="DateRangeChooser" action="PartnerOrderHistory.jsp" method="POST">
                <table>
                    <tr>
                        <td>
                            <select id="farmerSelector" name="farmerSelector">
                                <option value="default">Select a Farm</option>
                                <% 
                                    ResultSet farmData = farmer.getFarmers();
                                    //add all the farms that are associated with the user to the menu
                                    while(farmData.next()){
                                        if(farmData.getString("channel_partner_id").equals(pID)){
                                %>
                                            <option value="<%= farmData.getString("name")%>"><%= farmData.getString("name")%></option>
                                <%
                                        }
                                    }
                                %>
                            </select>
                        </td>
                        <td>
                            <select id="commoditySelector" name="commoditySelector">
                                <option value="default">Select a Commodity</option>
                                <option value="Euronext Wheat">Euronext Wheat</option>
                                <option value="Euronext Rapeseed">Euronext Rapeseed</option>
                                <option value="Soybean Meal">Soybean Meal</option>
                            </select>
                        </td>
                        <td>
                            <select id="sortChooser1" name="sortChooser1">
                                <option value="">Select a Sort Option</option>
                                <option value="order_id">Order Number</option>
                                <option value="farmer_physical_contract_id">Farm Physical Contract ID</option>
                                <option value="trade_date">Trade Date</option>
                                <option value="farmer_id">Farm ID</option>
                                <option value="underlying_commodity_name">Underlying Commodity Name</option>
                                <option value="status">Status</option>
                            </select>
                        </td>
                        <td>
                            <select id="sortChooser2" name="sortChooser2">
                                <option value="">Select a Sort Option</option>
                                <option value="order_id">Order Number</option>
                                <option value="farmer_physical_contract_id">Farm Physical Contract ID</option>
                                <option value="trade_date">Trade Date</option>
                                <option value="farmer_id">Farm ID</option>
                                <option value="underlying_commodity_name">Underlying Commodity Name</option>
                                <option value="status">Status</option>
                            </select>
                        </td>
                        <td>
                            <input type="submit" value="Sort" name="Sort"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        
        <%
            /*checks if the sort button was pressed and 
            if it was it calls the method to get the sorted data*/
            if(request.getParameter("Sort")!= null){
                orderData = orders.getSortedOrders(request.getParameter("sortChooser1"), request.getParameter("sortChooser2"), "", "");
            }
        %> 
        <form action="PartnerOrderPrintout.jsp" name="printForm" method="POST">
        <div>
            <table id="central" class="highlightRows, oddRowColor" style="margin-bottom: 10%">
                <thead>
                    <tr>
                        <th>Farm Name</th>
                        <th>Farm ID</th>
                        <th>Order #</th>
                        <th>Trade Date</th>
                        <th>Underlying Commodity Name</th>
                        <th>Calendar Month</th>
                        <th>Underlying Futures Code</th>
                        <th>Order Volume Units</th>
                        <th>Unit Type</th>
                        <th>Order Volume Contracts</th>
                        <th>Executed Strike</th>
                        <th>Product Executed Price / Unit</th>
                        <th>Execution Product Total Cost</th>
                        <th>Interest Rate</th>
                        <th>Status</th>
                        <th>Farm Physical Contract ID</th>
                        <th>Print</th>
                       
                    </tr>
                </thead>
                <tbody>
                    <%  int count = 0;
                        while(orderData.next()){
                            try{
                                //checks to only use orders that are for this partner and are not of the status 'pending'
                            if(orderData.getString("farmer_id").indexOf(pID) >= 0 && !orderData.getString("status").equals("pending")){
                                count++;
                    %>
                                <tr class="tableRow" id="<%= "tableRow" + count%>">
                                    <td><input type="hidden" id="<%= ("farmer_name" + count)%>" name="<%= ("farmer_name" + count)%>" value="<%= farmer.getNameById(orderData.getString("farmer_id"))%>" /> <%= farmer.getNameById(orderData.getString("farmer_id"))%></td>
                                    <td><input type="hidden" name="<%= ("farmer_id" + count)%>" value="<%= orderData.getString("farmer_id")%>" /> <%= orderData.getString("farmer_id") %></td>
                                    <td><input type="hidden" name="<%= ("order_id" + count)%>" value="<%= orderData.getString("order_id")%>" /> <%= orderData.getString("order_id")%></td>
                                    <td><input type="hidden" name="<%= ("trade_date" + count)%>" value="<%= orderData.getDate("trade_date")%>" /> <%= orderData.getDate("trade_date") %></td>
                                    <td><input type="hidden" id="<%= ("commodity_name" + count)%>" name="<%= ("underlying_commodity_name" + count)%>" value="<%= orderData.getString("underlying_commodity_name")%>" /> <%= orderData.getString("underlying_commodity_name")%></td>
                                    <td><input type="hidden" name="<%= ("client_month" + count)%>" value="<%= orderData.getString("client_month")%>" /> <%= orderData.getString("client_month")%></td>
                                    <td><input type="hidden" name="<%= ("underlying_futures_code" + count)%>" value="<%= orderData.getString("underlying_futures_code")%>" /> <%= orderData.getString("underlying_futures_code")%></td>
                                    <td><input type="hidden" name="<%= ("order_volume_quantity" + count)%>" value="<%= orderData.getDouble("order_volume_quantity")%>" /> <%= orderData.getDouble("order_volume_quantity")%></td>
                                    <td><input type="hidden" name="<%= ("unit_type" + count)%>" value="<%= orderData.getString("unit_type")%>" /> <%= orderData.getString("unit_type")%></td>
                                    <td><input type="hidden" name="<%= ("order_volume_contracts" + count)%>" value="<%= orderData.getDouble("order_volume_contracts")%>" /> <%= orderData.getDouble("order_volume_contracts")%></td>
                                    <td><input type="hidden" name="<%= ("executed_strike" + count)%>" value="<%= orderData.getDouble("executed_strike")%>" /> <%= orderData.getDouble("executed_strike")%></td>
                                    <td><input type="hidden" name="<%= ("product_current_price_per_unit" + count)%>" value="<%= formatter.format(orderData.getDouble("product_current_price_per_unit"))%>" /> <%= formatter.format(orderData.getDouble("product_current_price_per_unit"))%></td>
                                    <td><input type="hidden" name="<%= ("execution_product_total_cost" + count)%>" value="<%= formatter.format(orderData.getDouble("execution_product_total_cost"))%>" /> <%= formatter.format(orderData.getDouble("execution_product_total_cost"))%></td>
                                    <td><input type="hidden" name="<%= ("interestRate" + count)%>" value="<%= orderData.getDouble("current_interest_rate")%>" /> <%= orderData.getDouble("current_interest_rate")%></td>
                                    <td><input type="hidden" name="<%= ("status" + count)%>" value="<%= orderData.getString("status")%>" /> <%= orderData.getString("status")%></td>
                                    <td><input type="hidden" name="<%= ("farm_physical_contract_id" + count)%>" value="<%=orderData.getString("farmer_physical_contract_id")%>"/> <%=orderData.getString("farmer_physical_contract_id")%></td>
                                    <td><input type="submit" name="<%= ("print" + count)%>" value="Print" /></td>
                                </tr>
                                
                    <%        }
                            }
                            catch(NullPointerException e){
                                orderData.next();
                                //e.printStackTrace();
                            }
                        }
                    %>
                <input type="hidden" name="count" value="<%= count%>"/>
                </tbody>
            </table>
        </div>
</form>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="PartnerHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="PartnerReports.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  
        orders.closeConnection();
        farmer.closeConnection();
        cp.closeConnection(); 
    %>
    <script>
        function overwriteValues(){
                var table = document.getElementById("central");
                for (var i = 0; i<table.rows.length; i++)
                    for(var j = 0; j<table.rows[i].cells.length; j++){
                        var innerText = table.rows[i].cells[j].innerHTML;
                        var visibleText = innerText.substring(innerText.indexOf(">") + 2);
                        if( visibleText === "-999.0" 
                                || visibleText === "-999.00" 
                                || visibleText === "-999" 
                                || visibleText === "null" 
                                || visibleText === "N/A")
                            table.rows[i].cells[j].innerHTML = "-";
                    }
            }
        //hides orders that are not associated with the farm that is selected
        $(document).ready(function() {
            $('#farmerSelector').change(function() {
                var selectedFarmer = $(this).val();
                var tableRows = $('.tableRow');
                if(selectedFarmer === 'default'){
                   for(var i = 0; i < tableRows.length; i++){
                       tableRows[i].style.display = 'table-row';
                   } 
                }
                else{
                   for(var i = 0; i < tableRows.length; i++){
                        if($('#farmer_name' + (i+1)).val() === selectedFarmer){
                            tableRows[i].style.display = 'table-row';
                        }else{
                            tableRows[i].style.display = 'none'; 
                        }  
                   } 
                }
            });
        });
        //hides orders that are not of the selected commodity
        $(document).ready(function() {
            $('#commoditySelector').change(function() {
                var selectedCommodity = $(this).val();
                var tableRows = $('.tableRow');
                if(selectedCommodity === 'default'){
                   for(var i = 0; i < tableRows.length; i++){
                       tableRows[i].style.display = 'inline';
                   } 
                }
                else{
                   for(var i = 0; i < tableRows.length; i++){
                        if($('#commodity_name' + (i+1)).val() === selectedCommodity){
                            tableRows[i].style.display = 'table-row';
                        }else{
                           tableRows[i].style.display = 'none'; 
                        }  
                   } 
                }
            });
        });
    </script>
</html>