<%-- 
    Document   : AdminViewMarketData
    Created on : Sep 24, 2016, 5:43:55 PM
    Author     : LucasCarey
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <script src="http://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Update Market Data</title>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h1>Update Market Data</h1>
        
            <form name="updateVolatility" action="AdminViewMarketData.jsp" method="POST">
        <div>
              <table id="central" class="highlightRows,oddRowColor" name="marketDataTable">
                    <thead>
                        <tr>
                            <th>Data Pull Timestamp</th>
                            <th>Underlying Futures Code</th>
                            <th>Current Underlying Futures Price</th>
                            <th>Closing Implied Volatility</th>
                            <th>Daily Trader Volatility</th>
                            <th>30 Day Interest Rate (Dollar)</th>
                            <th>90 Day Interest Rate (Dollar)</th>
                            <th>180 Day Interest Rate (Dollar)</th>
                            <th>360 Day Interest Rate (Dollar)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%  //Declare an instance of NumberFormat to truncate decimals to two places, and Dateformat to format dates..
                            NumberFormat formatter = NumberFormat.getInstance();
                            formatter.setMaximumFractionDigits(2);
                            formatter.setMinimumFractionDigits(2);
                            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");
                            //Create PulledData object to access Market Data database and create a ResultSet containing data for each commodity.
                            PulledData data = new PulledData();
                            ResultSet pulledDataRapeseed = data.getTodaysData("Euronext Rapeseed");
                            ResultSet pulledDataWheat = data.getTodaysData("Euronext Wheat");
                            ResultSet pulledDataSoybean = data.getTodaysData("Soybean Meal");
                            
                            int count = 0;
                            pulledDataRapeseed.beforeFirst();
                            /**DISPLAY ALL MARKET DATA FOR RAPESEED**/
                            while(pulledDataRapeseed.next()){
                                count++;
                        %>
                        <tr>
                            <td><%= dateFormat.format(pulledDataRapeseed.getTimestamp("timestamp")) %></td>
                            <td><%= pulledDataRapeseed.getString("underlying_futures_code") %></td>
                            <td><%= formatter.format(pulledDataRapeseed.getDouble("current_underlying_futures_price")) %></td>
                            <td><%= formatter.format(pulledDataRapeseed.getDouble("current_implied_volatility")) %></td>
                            <td><input class="updateVol" id="<%= pulledDataRapeseed.getString("underlying_futures_code")%>" type="text" autocomplete="off" name="<%= "dailyTraderVolatility" + count%>" size="8" value="<%= formatter.format(pulledDataRapeseed.getDouble("daily_trader_volatility"))%>" onkeyup="checkNum(this)"/></td>
                            <td><%= formatter.format(pulledDataRapeseed.getDouble("interest_30_day")) %></td>
                            <td><%= formatter.format(pulledDataRapeseed.getDouble("interest_90_day")) %></td>
                            <td><%= formatter.format(pulledDataRapeseed.getDouble("interest_180_day")) %></td>
                            <td><%= formatter.format(pulledDataRapeseed.getDouble("interest_360_day")) %></td>
                        </tr>
                        <% } %>
                        <%
                            pulledDataWheat.beforeFirst();
                            /**DISPLAY ALL MARKET DATA FOR WHEAT**/
                            while(pulledDataWheat.next()){
                                count++;
                        %>
                        <tr>
                            <td><%= dateFormat.format(pulledDataWheat.getTimestamp("timestamp")) %></td>
                            <td><%= pulledDataWheat.getString("underlying_futures_code") %></td>
                            <td><%= formatter.format(pulledDataWheat.getDouble("current_underlying_futures_price")) %></td>
                            <td><%= formatter.format(pulledDataWheat.getDouble("current_implied_volatility")) %></td>
                            <td><input class="updateVol" id="<%= pulledDataWheat.getString("underlying_futures_code")%>" type="text" autocomplete="off" name="<%= "dailyTraderVolatility" + count%>" size="8" value="<%= formatter.format(pulledDataWheat.getDouble("daily_trader_volatility"))%>" onkeyup="checkNum(this)"/></td>
                            <td><%= formatter.format(pulledDataWheat.getDouble("interest_30_day")) %></td>
                            <td><%= formatter.format(pulledDataWheat.getDouble("interest_90_day")) %></td>
                            <td><%= formatter.format(pulledDataWheat.getDouble("interest_180_day")) %></td>
                            <td><%= formatter.format(pulledDataWheat.getDouble("interest_360_day")) %></td>
                        </tr>
                        <% } %>
                        <%
                            pulledDataSoybean.beforeFirst();
                            /**DISPLAY ALL MARKET DATA FOR SOYBEAN MEAL**/
                            while(pulledDataSoybean.next()){
                                count++;
                        %>
                        <tr>
                            <td><%= dateFormat.format(pulledDataSoybean.getTimestamp("timestamp")) %></td>
                            <td><%= pulledDataSoybean.getString("underlying_futures_code") %></td>
                            <td><%= formatter.format(pulledDataSoybean.getDouble("current_underlying_futures_price")) %></td>
                            <td><%= formatter.format(pulledDataSoybean.getDouble("current_implied_volatility")) %></td>
                            <td><input class="updateVol" id="<%= pulledDataSoybean.getString("underlying_futures_code")%>" type="text" autocomplete="off" name="<%= "dailyTraderVolatility" + count%>" size="8" value="<%= formatter.format(pulledDataSoybean.getDouble("daily_trader_volatility"))%>" onkeyup="checkNum(this)"/></td>
                            <td><%= formatter.format(pulledDataSoybean.getDouble("interest_30_day")) %></td>
                            <td><%= formatter.format(pulledDataSoybean.getDouble("interest_90_day")) %></td>
                            <td><%= formatter.format(pulledDataSoybean.getDouble("interest_180_day")) %></td>
                            <td><%= formatter.format(pulledDataSoybean.getDouble("interest_360_day")) %></td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
        </div>
    </form> 
                    <script>
                        /**
                         * VALIDITY CHECK FUNCTION
                         * Checks to make sure that the text inputted into the update volatility textboxes are ONLY numbers.
                         * Run everytime keyup occurs in one of the textboxes.
                         * If a non number character is entered, it is deleted.
                         * @param {element} e the element that is being checked.
                         */
                        function checkNum(e){
                            var val = e.value;
                            switch(val.substring(val.length - 1, val.length)){
                                case '1':
                                case '2':
                                case '3':
                                case '4':
                                case '5':
                                case '6':
                                case '7':
                                case '8':
                                case '9':
                                case '0':
                                    break; //Do nothing if a number is entered.
                                case '.':
                                    if(val.substring(0, val.length - 1).includes('.')){
                                        e.value = val.substring(0, val.length - 1); //Truncate if a '.' is entered when there already is one.
                                    }
                                    break;
                                default:
                                    e.value = val.substring(0, val.length - 1); //Take off extraneous character.
                                    break;
                                    
                            }
                        }
                    </script>
        <div class="fixed">
             <table border="0">
                 <tbody>
                     <tr>
                         <td>
                             <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                 <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                             </form>
                         </td>
                         <td>
                             <form name="backForm" action="AdminHomePage.jsp" method="POST">
                                 <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                             </form>
                         </td>
                     </tr>
                 </tbody>
             </table>
         </div>    
    </body>
    <%  //Close Database Connection.
        data.closeConnection();
    %>
    <script>
        /**
         * AJAX FUNCTION
         * Updates Database everytime a keystroke is released with updated volatility.
         * Sends futures code and updated volatility data to the Update Volatility servlet.
         */
        $(document).ready(function() {
            $('.updateVol').keyup(function() {
                $.ajax({
                    url : 'UpdateVolatilityServlet',
                    data : {
                        code : $(this).attr('id'),
                        volatility : $(this).val()
                    },
                    sucess : function(responseText){
                        alert('Sucessfuly updated volatility to ' + responseText);
                    }
                });
            });
        });
    </script>
</html>
