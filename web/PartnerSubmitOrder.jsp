<%-- 
    Document   : PartnerSubmitOrder
    Created on : Sep 18, 2016, 10:27:34 AM
    Author     : Joey
--%>
<%@page import="org.jquantlib.time.calendars.UnitedKingdom"%>
<%@page import="org.jquantlib.time.calendars.UnitedStates"%>
<%@page import="org.jquantlib.time.Calendar"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Calendar.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>"
<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Submit Order</title>
    </head>
    <body>
        <%
            //Create instance of Orders, Pulled Data and Channel partner to access database.
            Orders orders = new Orders();
            ChannelPartner channelPartner = new ChannelPartner();
            PulledData pd = new PulledData();
            ResultSet todaysData = pd.getTodaysData(request.getParameter("commodityName")); //Create a result set with today's data for the requested commodity.
            
            //Declaration of necessary objects and variables
            Timestamp currentTimestamp = new Timestamp(java.util.Calendar.getInstance().getTimeInMillis());
            String product = "European";
            String commodity = request.getParameter("commodityName");
            int count = Integer.parseInt(request.getParameter("count")); //Total number of rows in the pricing page.
            int orderVolumeUnits= 0;
            String instrument = "";
            double conversionFactor = 0;
            Date today = new java.util.Date(); // The current time
            
            //Assignment of instrument and conversion factor based upon commodity.
            if (commodity.equals("Euronext Rapeseed") || commodity.equals("Euronext Wheat")){
                instrument = "Call";
                conversionFactor = 50;
            }else if(commodity.equals("Soybean Meal")){
                instrument = "Put";
                conversionFactor = 100;
            }
            
            //Assignment of trading calendar region based upon type of commodity being traded.
            Calendar tradingCal;
            if(commodity.equals("Soybean Meal")){
                tradingCal = new UnitedStates();
            }    
            else{ 
                tradingCal = new UnitedKingdom();
            }
            
            //Use session attribute to get username, and method to convert to Channel Partner ID.
            String username = "";
            String channelPartnerID = "";
            try{
                username = session.getAttribute("username").toString();
                channelPartnerID = channelPartner.getIDByUsername(username);
            }catch(java.lang.NullPointerException e){
                response.sendRedirect("login.jsp");
            }
            
            /**
             * MAIN LOOP: Goes through each row in the pricing table to see if an order was requested.
             */
            for(int i = 1; i <= count; i++){
                
                //Assignment of Order Volume Quantity of current row.
                try{
                    orderVolumeUnits= Integer.parseInt(request.getParameter("orderVolumeQuantity" + i));  
                }
                catch(java.lang.NumberFormatException e){
                  e.printStackTrace();
                }
                
                /**
                 * IF there are more than 0 order volume units, an order was placed for this row, and the code will proceed to submit the order.
                 */
                if(orderVolumeUnits > 0){

                    //Find days until expiration.
                    java.sql.Date expirationDate = java.sql.Date.valueOf(request.getParameter("expiration" + i)); //Expiration Date object
                    int difference = tradingCal.businessDaysBetween(new org.jquantlib.time.Date(today), new org.jquantlib.time.Date(expirationDate), true, true); //Number of business days between today and the expiration date.
                    int remainingDays = (tradingCal.businessDaysBetween(new org.jquantlib.time.Date(new java.util.Date()), new org.jquantlib.time.Date(java.sql.Date.valueOf(request.getParameter("expiration" + i))), true, true));

                    todaysData.first(); //Move iterator to first row of result set.
                    
                    //Declaration of local black scholes variables.
                    double delta = 0, gamma = 0, vega = 0, theta = 0, market_price_per_unit = 0;
                    
                    //Find the interpolated volatility using this row's futures code and executed strike.
                    Interpolation interpolation = new Interpolation(request.getParameter("futuresCode" + i), Double.parseDouble(request.getParameter("strike" + i)));
                    double interpolated_volatility = interpolation.getRequestedVolatilityStrikeValue();
                    
                    /**
                     * BULLET CALCULATIONS
                     * Perform standard black scholes calculations if the order is Bullet (FREEDOM REPRICE TABLE).
                     */
                    if(request.getParameter("frequency" + i).toLowerCase().equals("bullet")){
                        
                        //Create an instance of the black scholes class using the parameters from this given row/order.
                        BlackScholes bs = new BlackScholes(instrument.toLowerCase(), Double.parseDouble(request.getParameter("futures" + i)),  Double.parseDouble(request.getParameter("strike" + i)),
                        BlackScholes.calcCurrentInterestRate(
                                todaysData.getDouble("interest_30_day"),
                                todaysData.getDouble("interest_90_day"),
                                todaysData.getDouble("interest_180_day"), 
                                todaysData.getDouble("interest_360_day"),
                                remainingDays),
                                0, interpolated_volatility, remainingDays);
                        
                        //Use BS to find values of delta, gamma, vega, theta, and market price per unit.
                        delta = bs.getDelta() * Double.parseDouble(request.getParameter("orderVolumeQuantity" + i)) / conversionFactor;
                        gamma = bs.getGamma() * Double.parseDouble(request.getParameter("orderVolumeQuantity" + i)) / conversionFactor;
                        vega = bs.getVega() * Double.parseDouble(request.getParameter("orderVolumeQuantity" + i));
                        theta = bs.getTheta() * Double.parseDouble(request.getParameter("orderVolumeQuantity" + i));
                        market_price_per_unit = bs.getMarketPricePerUnit();
                        
                        //Perform sign conversions based upon instrument.
                        if(instrument.equals("Put")){
                            delta = Math.abs(delta);
                            gamma = -1 * Math.abs(gamma);
                            vega = -1 * Math.abs(vega);
                            theta = Math.abs(theta);
                        }
                        if(instrument.equals("Call")){
                            delta = -1 * Math.abs(delta);
                            gamma = -1 * Math.abs(gamma);
                            vega = -1 * Math.abs(vega);
                            theta = Math.abs(theta);
                        } 
                    }
                    
                    /**
                     * DAILY CALCULATIONS
                     * Perform black scholes accumulation calculations if the order is Daily (ENHANCED MARKET AVERAGE TABLE).
                     */
                    else if(request.getParameter("frequency" + i).toLowerCase().equals("daily")){
                        
                        //Create an instance of the Black Scholes Daily Accumulator class using the parameters from this given row/order.
                        BlackScholesDailyAccumulator bsda = new BlackScholesDailyAccumulator(instrument, Double.parseDouble(request.getParameter("futures" + i)), 
                        Double.parseDouble(request.getParameter("strike" + i)), request.getParameter("futuresCode" + i), 0, 
                        interpolated_volatility, new java.sql.Date(new java.util.Date().getTime()).toString(),
                        request.getParameter("expiration" + i), "sell", Double.parseDouble(request.getParameter("orderVolumeQuantity" + i)), (int) difference);
                        
                        //Use BSDA to find values of delta, gamma, vega, theta, and market price per unit
                        delta = bsda.getDeltaTotal() / conversionFactor;
                        gamma = bsda.getGammaTotal() / conversionFactor;
                        vega = bsda.getVegaTotal();
                        theta = bsda.getThetaTotal();
                        market_price_per_unit = bsda.getFairValueAvg();
                    }
                    
                    /**
                     * USE THE INSERT ORDER METHOD TO INSERT ORDER WITH ALL PARAMETERS FOR THIS ROW.
                     */
                    orders.insertOrder( channelPartnerID, //Partner ID
                                        request.getParameter("farmerID"), //Farm ID
                                        orders.generateOrderID(), //Order ID
                                        "Pending", //Status
                                        new java.sql.Date(new java.util.Date().getTime()), //Trade date
                                        "Trade", //Transaction Type
                                        commodity, //Underlying Commodity Name
                                        request.getParameter("futuresCode" + i), //Underlying futures code 
                                        request.getParameter("clientMonth" + i), //Client Month
                                        instrument, //Instrument
                                        product, //Product
                                        request.getParameter("frequency" + i), //Frequency
                                        0, //Executed Strike (calculated based on executed hedge)
                                        expirationDate, //expiration_date
                                        Double.parseDouble(request.getParameter("product_current_price_per_unit" + i)), //Product Current Price / Unit 
                                        Double.parseDouble(request.getParameter("finalprice" + i)),//execution_product_total_cost, 
                                        0,//barrier_level, 
                                        0,//digital_payout_per_unit, 
                                        Double.parseDouble(request.getParameter("futures" + i)), 
                                        interpolated_volatility,//current_implied_volatility, 
                                        market_price_per_unit,//market_price_per_unit, 
                                        delta,//delta,
                                        gamma,//gamma,
                                        vega,//vega,
                                        theta,//theta, 
                                        0,//mark_to_market,
                                        0,//accrued_value, 
                                        0,//accrued_contracts, 
                                        0,//unwind_accrued_value_paid, 
                                        0,//override_option_value_paid, 
                                        0,//unwind_total_volume_paid, 
                                        "N/A",//broker_name, 
                                        request.getParameter("contractid" + i),//farmer_physical_contract_id,
                                        0,//executed_hedge
                                        0,//digital_total_payout, 
                                        0,//unwind_option_value_paid, 
                                        0,//unwind_value_paid_per_unit,
                                        interpolated_volatility,//daily_trader_volatility
                                        Double.parseDouble(request.getParameter("orderVolumeQuantity" + i)),
                                        request.getParameter("unit_type"),//unit_type
                                        Double.parseDouble(request.getParameter("contracts" + i)),//order_volume_contracts
                                        (int) difference,//original_number_of_days
                                        0,//days accrued
                                        (int) difference,//remaining days
                                        0,//accrued_volume_units
                                        0,//unwind_vega
                                        0,//unwind_volume_units
                                        currentTimestamp,
                                        Integer.parseInt(request.getParameter("strike_shift"+ i )),
                                        "Sell"); 
                }   
            }
            
            //Redirect back to Partner Home page.
            response.sendRedirect("PartnerHomePage.jsp"); 
        %>
    </body>
    <%
        //Close connections to the database.
        orders.closeConnection();
        channelPartner.closeConnection();
    %>
</html>
