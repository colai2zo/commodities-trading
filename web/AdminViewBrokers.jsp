<%-- 
    Document   : AdminViewBrokers
    Created on : Aug 10, 2016, 3:43:14 PM
    Author     : Dino
--%>
<%@page import="java.sql.*"%>
<%@page import="com.dutchessdevelopers.commoditieswebsite.Brokers" %>
<%@page import="com.dutchessdevelopers.commoditieswebsite.Employee" %>
<%Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading|View, Edit, or Delete a Broker</title>
         <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <%
            //Get the password of the current employee to validate deletions.
            Employee employee = new Employee(); 
            ResultSet employeeData = employee.getEmployee();
            //Get Username from Session Attribute
            String username = "";
            try{
                username = session.getAttribute("username").toString();
            }catch(java.lang.NullPointerException e){
                response.sendRedirect("login.jsp");
            }
            //Get password that corresponds with username.
            String password = "";
            while(employeeData.next()){
                if(employeeData.getString("username").equals(username)){
                    password = employeeData.getString("password");
                }
            }
        %>
	<div id="header">
	    <h1>View, Edit, or Delete a Broker</h1>
	</div>
	<div id="central" align="center">
        <% //Create a Brokers object and a ResultSet object containing each of the brokers.
           Brokers brokers = new Brokers();
           ResultSet brokerData = brokers.getBroker(); 
           int brokerCount = 0; 
        %>
        <form name="submitForm" action="AdminViewBrokers.jsp" method="POST">
            <input type="hidden" id="password" value="<%= password%>" />
	<table id="brokerTable" border="1" cellpadding="15%">
	    <thead>
		<tr>
                    <td>Channel Partner</td>
		    <td>Company Name</td>
		    <td>Contact Name</td>
                    <td>Submit</td>
		    <td>Delete</td>
		</tr>
	    </thead>
	    <tbody>
                <%  //Create a row in the table for each of the brokers currently in the database.
                    while(brokerData.next()) {
                        brokerCount = brokerData.getRow(); //Update count (used to identify which row the elements are from)
                %>
                <tr>
                    <td><%= brokerData.getString("channel_partner") %></td>
                    <td><input type="text" name="<%= ("companyName" + brokerCount)%>" id="<%= ("companyName" + brokerCount)%>" value="<%= brokerData.getString("company_name")%>" size="20px" /></td>
                    <td><input type="text" name="<%= ("contactName" + brokerCount)%>" value="<%= brokerData.getString("contact_name")%>" size="20px" /></td>
                    <td>     
                            <input type="submit" value="Update" name="<%= ("submitButton" + brokerCount)%>" />
                    </td>
                    <td>
                            <input type="submit" value="Delete" name="<%= ("deleteButton" + brokerCount)%>" id="<%= ("deleteButton" + brokerCount)%>" onclick="enterPassword(this)"/>
                            <input type="submit" value="Delete" name="<%= ("trueDeleteButton" + brokerCount)%>" id="<%= ("trueDeleteButton" + brokerCount)%>" style="display:none"/>
                    </td>
                    
                </tr>
                <%
                    }           
                %>
	    </tbody>
	</table>
        </form>
        <script>
            /**
             * This method is for the purpose of validating the password when a user attempts to delete a broker.
             * It uses prompts to ask a user to enter their password and then confirm that they would like to delete the broker. 
             * @param {element} e the element which triggers the call of the method.
             */
            function enterPassword(e){
                //USE PROMPT to allow user to enter password.
                var password = prompt("Please enter your administrator password: ");
                
                /**PASSWORD CORRECT**/
                if(password === document.getElementById("password").value){
                    
                    //Final Confirmation
                    var i = e.id.substring(e.id.length - 1, e.id.length); //The row number of the element.
                    var deletes = prompt("Are you sure you would like to delete " + document.getElementById('companyName' + i).value +  " from the database? Please type 'YES' to confirm.");
                    if(deletes === 'YES'){                       
                        document.getElementById("deleteButton" + i).style.display = "none";
                        document.getElementById("trueDeleteButton" + i).type = "hidden";
                    }
                }
                
                /**PASSWORD INCORRECT**/
                else{
                    alert('Password Incorrect.');
                }
            }
        </script>
        <%
            /**
             * UPDATE ANY BROKER in Database for which the update button was clicked.
             */
            int count=0;
            brokerData.beforeFirst();
            while (brokerData.next()){
                count++;
                if(request.getParameter("submitButton" + count) != null){
                    brokers.updateBroker(brokerData.getString("company_name"), brokerData.getString("contact_name"),request.getParameter("companyName" + count), request.getParameter("contactName" + count), brokerData.getString("channel_partner"));
                    response.setHeader("Refresh", "0; URL=AdminViewBrokers.jsp");
                }
            }
            
            /**
             * DELETE ANY BROKER in Database for which the delete button was clicked and deletion confirmed.
             */
            count = 0;
            brokerData.beforeFirst();
            while (brokerData.next()){
                count++;
                if (request.getParameter("trueDeleteButton" + count) != null){
                    brokers.deleteBroker(request.getParameter("companyName" + count), request.getParameter("contactName" + count));
                    response.setHeader("Refresh", "0; URL=AdminViewBrokers.jsp");
                }
            }
        %>
	</div>
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminBrokerOptions.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    <%  /** Close Database Connections.  **/
        employee.closeConnection();
        brokers.closeConnection();
    %>
</html>