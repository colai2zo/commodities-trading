<%-- 
    Document   : reporting
    Created on : Sep 5, 2016, 10:38:52 AM
    Author     : LucasCarey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commodities Trading | Report Views</title>
    </head>
    <body>
        <h1>Report Views</h1>
        
        <div id="central" align="center">
            <form name="CPPosition" action="AdminPositionReport.jsp" method="POST">
                <input id="button" type="submit" value="Channel Partner Positions" name="positionReport" />
            </form>

            <form name="manageExpirationsForm" action="AdminManageExpirations.jsp" method="POST">
                <input id="button" type="submit" value="Manage Expirations" name="manageExpirations" />
            </form>

            <form name="adminAccountsDue" action="AdminAccountsDue.jsp" method="POST">
                <input id="button" type="submit" value="Accounts Due" name="accountsDue" />
            </form>         
        </div>
        
        <div class="fixed">
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            <form name="homeForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Return to Home Screen" name="homeButton" />
                            </form>
                        </td>
                        <td>
                            <form name="backForm" action="AdminHomePage.jsp" method="POST">
                                <input id="backAndHome" type="submit" value="Go Back" name="backButton" />
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
