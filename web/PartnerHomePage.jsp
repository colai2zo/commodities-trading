<%-- 
    Document   : PartnerHomePage
    Created on : Aug 7, 2016, 9:47:03 AM
    Author     : Dino Martinez
--%>

<%@page import="com.dutchessdevelopers.commoditieswebsite.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%Class.forName("com.mysql.jdbc.Driver");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" media="screen" rel="stylesheet" type="text/css" />
        <title>Commodities Trading | channel partner home page</title>
    </head>
    
    <body>
        <div style="float:right">
            <form name="logoutForm" action="PartnerHomePage.jsp" method="POST">
                <input style="background-color:transparent" type="submit" value="Logout" name="logout" id="logout" onclick="confirmLogout()" />
                <input style="display:none" type="submit" value="Logout" name="trueLogout" id="trueLogout" />
            </form>
        </div>
        <div id="header" align="center" >
        <%  //Retrieve username of channel partner that is currently logged in from session attribute.
            String username = "";
            try{
                username = session.getAttribute("username").toString();
            }catch(java.lang.NullPointerException e){
                response.sendRedirect("login.jsp");
            } 
            ChannelPartner partner = new ChannelPartner();
        %>
        <h1 text-align="center">Channel Partner Home Page For <%= partner.getNameByUsername(username) %></h1>
        </div>
        
        <div id="central" align="center">
        
            <h2>Please select from the following:</h2>
        
        <form name="CheckSpecsForm" action="PartnerPricing.jsp" method="POST">
            <input id="button" type="submit" value="View Today's Pricing" name="pricing" />
        </form>
        
        <form name="PendingOrdersForm" action="PartnerCurrentOrders.jsp" method="POST">
            <input id="button" type="submit" value="Current Orders" name="currentOrders" />
        </form>
       
        <form name="reportingForm" action="PartnerReports.jsp" method="POST">
            <input id='button' type="submit" value="Reports" name="reports" />
        </form>
            
        <form name="farmerInfoForm" action="PartnerFarmInfo.jsp" method="POST">
            <input id="button" type="submit" value="Farm Info" name="farmInfo" />
        </form>

        </div>
        <script>
            /**
             * This function is used to confirm, via a javascript confirm, that the user would like to logout.
             */
            function confirmLogout(){
                var c = confirm("Are you sure you want to logout?");
                if(c === true){
                    document.getElementById("trueLogout").type = "hidden";
                }
            }
        </script>
        <%  //Clear username session attribute and redirect to login screen if logout button is clicked.
            if(request.getParameter("trueLogout") != null){
                session.setAttribute("username", "");
                response.sendRedirect("login.jsp");
            }
        %>
    </body>
    <% 
        //Close connection to database.
        partner.closeConnection(); 
    %>
</html>