/**
 * The black scholes servlet serves the purpose of running black scholes calculations through HTTP GET requests.
 * Requires several parameters that are passed into the com.dutchessdevelopers.commoditieiswebsite.BlackScholes.java class.
 * @version June 2017
 * @author Dutchess Developers
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dutchessdevelopers.commoditieswebsite.BlackScholes;
import com.dutchessdevelopers.commoditieswebsite.BlackScholesDailyAccumulator;
import com.dutchessdevelopers.commoditieswebsite.Interpolation;
import org.jquantlib.time.calendars.UnitedKingdom;
import org.jquantlib.time.calendars.UnitedStates;
import org.jquantlib.time.TimeUnit;
import java.text.NumberFormat;

@WebServlet("/BlackScholesServlet")

public class BlackScholesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        /**
         * The doGet method of the BlackScholesServlet receives parameters from an HttpServletRequest and returns a double for the value of the requested Black Scholes item. <br>
         * Requested Parameters: <br>
         * option - The instrument used to place the order (call or put) <br>
         * expiry - the expiration date of the order, given in the form yyyy-mm-dd <br>
         * frequency - the frequency of the order (daily or bullet) <br>
         * code - the underlying futures code of the commodity being traded in the order (e.g. CAH7)<br>
         * item - the black scholes return item that is being requested.<br>
         * futures_price - the current underlying futures price of the commodity being traded in the order.<br>
         * executed_strike - the executed strike of the order<br>
         * interest_rate - the interest rate assigned to the order<br>
         * dividend_yield - the dividend yield assigned to the order<br>
         * daily_trader_volatility - the daily trader volatility assigned to the order.<br>
         * action - the action performed by the transaction (buy or sell).<br>
         * order_volume_quantity -  number of order volume units contained within the order
         * The method takes each of these values and plugs them into the black scholes class (bullet orders) or black scholes daily accumulator class (daily orders).
         * The method is called for the requested parameter and the response writer writes the method's response.
         * @param request, the HttpServletRequest that calls parameters passed in
         * @param response, the HttpServletResponse that sends back the Black Scholes Calculated value.
         */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                NumberFormat f = NumberFormat.getInstance(); //Create instance of number formatter
                f.setMaximumFractionDigits(6); //Must have 6 decimal places.
                f.setMinimumFractionDigits(6);
                f.setGroupingUsed(false); //No Commas on numbers
                double responseValue = 0.0; //Value that gets returned to user in HttpServletResponse.
                
                String option = request.getParameter("option"); //call or put
                String escapeDate = request.getParameter("expiry").replaceAll("/", ""); //'Replaace All' call is safeguard against improperly passed in dates.
                
                /* Instantiate Jquantlib Calendar, and Determine correct calendar to use
                 * Calls use UK trading calendar, Puts use US trading calendar.
                 */
                org.jquantlib.time.Calendar tradingCal = null; 
                if(option.toLowerCase().equals("call")){
                    tradingCal = new org.jquantlib.time.calendars.UnitedKingdom();
                }if(option.toLowerCase().equals("put")){
                    tradingCal = new org.jquantlib.time.calendars.UnitedStates();
                }
                
                //REQUEST and CALCULATE Black Scholes Parameter Values
                int days = tradingCal.businessDaysBetween(new org.jquantlib.time.Date(new java.util.Date()), new org.jquantlib.time.Date(java.sql.Date.valueOf(escapeDate)), true, true); //Calculates business days between today and expiration date, including both end dates.
                String frequency = request.getParameter("frequency").toLowerCase(); //bullet or daily
                String code = request.getParameter("code"); //underlying futures code
                String requestedItem = request.getParameter("item"); //black scholes item to be returned
                double price = Double.parseDouble(request.getParameter("futures_price")); //current underlying futures price
                double strike = Double.parseDouble(request.getParameter("executed_strike")); //executed strike
                double interest_rate = Double.parseDouble(request.getParameter("interest_rate")); //interest rate
                double dividend_yield = Double.parseDouble(request.getParameter("dividend_yield")); //dividend yield
                
                /*Create an instance of interpolation in order to calculate interpolated volattility.*/
                Interpolation i = new Interpolation(code, strike);
                double interpolated_volatility = i.getRequestedVolatilityStrikeValue();
                try{
                    interpolated_volatility = Double.parseDouble(request.getParameter("daily_trader_volatility"));
                }catch(java.lang.NullPointerException e){
                    e.printStackTrace();
                }
                
                /* BULLET CALCULATION
                   * Performs the black scholes calculation for bullet using parameter values. 
                   * Standard black scholes class calculation.
                 ***/
                if(frequency.equals("bullet")){
                    BlackScholes bs = new BlackScholes(option.toLowerCase(), price, strike, interest_rate, dividend_yield, interpolated_volatility, days); //Instantiate Black Scholes with parameter values
                    double fairValue = bs.getMarketPricePerUnit();
                    double delta = bs.getDelta();
                    double gamma = bs.getGamma();
                    double theta = bs.getTheta();
                    double vega = bs.getVega();

                    if(requestedItem.equals("delta")){responseValue = delta;}
                    if(requestedItem.equals("gamma")){responseValue = gamma;}
                    if(requestedItem.equals("vega")){responseValue = vega;}
                    if(requestedItem.equals("theta")){responseValue = theta;}
                    if(requestedItem.equals("fairValue")){responseValue = fairValue;}
                    if(requestedItem.equals("volatility")){responseValue = interpolated_volatility;}
                }
                
                /* DAILY CALCULATION 
                 * Performs the black scholes calculation for daily using parameter values. 
                 * Standard black scholes daily accumulator class calculation.
                */
                
                else if(frequency.equals("daily")){
                    String action = request.getParameter("action"); //action
                    double quantity = Double.parseDouble(request.getParameter("order_volume_quantity"));
                    
                    BlackScholesDailyAccumulator bsda = new BlackScholesDailyAccumulator( //Instantiate Black Scholes Daily Accumulator with parameter values
                    option, price, strike, code, dividend_yield, interpolated_volatility, new java.sql.Date(new java.util.Date().getTime()).toString(), escapeDate, action, quantity, 1); 
                    double deltaSum = bsda.getDeltaTotal();
                    double gammaSum = bsda.getGammaTotal();
                    double vegaSum = bsda.getVegaTotal();
                    double thetaSum = bsda.getThetaTotal();
                    double fairValue = bsda.getFairValueAvg();
                    if(requestedItem.equals("delta")){responseValue = deltaSum;}
                    if(requestedItem.equals("gamma")){responseValue = gammaSum;}
                    if(requestedItem.equals("vega")){responseValue = vegaSum;}
                    if(requestedItem.equals("theta")){responseValue = thetaSum;}
                    if(requestedItem.equals("fairValue")){responseValue = fairValue;}
                    if(requestedItem.equals("volatility")){responseValue = interpolated_volatility;}
                }

		response.setContentType("text/plain");
		response.getWriter().write(f.format(responseValue));
	}
}
