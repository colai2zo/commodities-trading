/**
 * The Futures Price Servlet serves the purpose of retrieving the current underlying futures price for a given underlying futures code.
 * Uses futures code parameter to retrieve futures price value from Bloomberg pulled data.
 * @version June 2017
 * @author Dutchess Developers
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dutchessdevelopers.commoditieswebsite.BlackScholes;
import com.dutchessdevelopers.commoditieswebsite.PulledData;
import java.sql.ResultSet;
import org.jquantlib.time.calendars.UnitedKingdom;
import org.jquantlib.time.calendars.UnitedKingdom;
import java.text.NumberFormat;

@WebServlet("/FuturesPriceServlet")
public class FuturesPriceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
        /**
         * The purpose of the doGet method in this servlet is to take in the underlying futures code and return the underlying futures price. <br>
         * Parameters: <br>
         * futuresCode - the month in which the order will expire (based on underlying futures code) <br>
         * Once recieved, the method uses these parameters to come up with the expiration date. 
         * The expiration date will be the last business day of the month always for Euronext Wheat and Euronext Rapeseed, and some Soybean futures. 
         * The expiration date will be the 3rd Friday of the months for the other Soybean futures.
         * @param request The HttpServletRequest
         * @param response The HttpServletResponse
         */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                //Create an instance of the number formatter, to limit decimal places to 2.
                NumberFormat formatter = NumberFormat.getInstance();
                formatter.setMaximumFractionDigits(2);
                formatter.setMinimumFractionDigits(2);
                String code = request.getParameter("futuresCode"); //Retrieves futures code from HttpRequest
                //Get Current Bloomberg Data
                PulledData pd = new PulledData(); 
                ResultSet data = pd.getTodaysDataByCode(code);
                double price = 0; //Value to be returned in HttpResponse
                try{
                    data.first();
                    price = data.getDouble("current_underlying_futures_price");
                }catch(java.sql.SQLException e){
                    e.printStackTrace();
                }
		response.setContentType("text/plain");
		response.getWriter().write(new Double(formatter.format(price)).toString());
	}
}