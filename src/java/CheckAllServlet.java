/**
 * The Check All Servlet serves the purpose of using HTTP GET requests to check all boxes in the AdminManageBook.jsp page.
 * The boxes are checked if their attribute is set to true, and unchecked if their attribute is set to false.
 * This operation is necessary through the servlet to prevent boxes from being checked automatically upon loading the page.
 * @version June 2017
 * @author Dutchess Developers
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * @author Joey
 */
@WebServlet("/CheckAllServlet")
public class CheckAllServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
        /**
         * When the doGet method of the CheckAllServlet is called, it sets the attributes of ALL check boxes to true. <br>
         * There are no additionally requested parameters required when calling this method.
         * @param request The HttpServletRequest
         * @param response The HttpServletResponse
         */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            HttpSession session = request.getSession();
            session.setAttribute("channelPartner", "true");
            session.setAttribute("farmID", "true");
            session.setAttribute("orderNumber", "true");
            session.setAttribute("status", "true");
            session.setAttribute("tradeDate", "true");
            session.setAttribute("transactionType", "true");
            session.setAttribute("commodityName", "true");
            session.setAttribute("futuresCode", "true");
            session.setAttribute("clientMonth", "true");
            session.setAttribute("expirationDate", "true");
            session.setAttribute("instrument", "true");
            session.setAttribute("product", "true");
            session.setAttribute("frequency", "true");
            session.setAttribute("orderVolumeUnits", "true");
            session.setAttribute("unitType", "true");
            session.setAttribute("orderVolumeContracts", "true");
            session.setAttribute("executedStrike", "true");
            session.setAttribute("executedPricePerUnit", "true");
            session.setAttribute("executedTotalCost", "true");
            session.setAttribute("futuresPrice", "true");
            session.setAttribute("volatility", "true");
            session.setAttribute("delta", "true");
            session.setAttribute("gamma", "true");
            session.setAttribute("vega", "true");
            session.setAttribute("theta", "true");
            session.setAttribute("broker", "true");
            session.setAttribute("contractID", "true");
            response.setContentType("text/plain");
            response.getWriter().write("Completed Check.");
        }
}