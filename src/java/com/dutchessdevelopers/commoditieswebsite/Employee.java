/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dutchessdevelopers.commoditieswebsite;
import java.sql.*;
import java.lang.Math;

/**
 * 
 * @author Joey
 */
public class Employee {
    //These Keys will allow me to gain access to the database.
    String URL = "jdbc:mysql://localhost:3306/commodities_trading?autoReconnect=true&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "1234";
    
    /** Database Level Variables **/
    Connection connection = null;
    PreparedStatement ps = null;
    PreparedStatement getEmployee = null;
    PreparedStatement fireEmployee = null;
    PreparedStatement insertEmployee = null;
    PreparedStatement updateEmployee = null;
    ResultSet resultSet = null;
    
    /**
     * DEFAULT CONSTRUCTOR
     * <br>
     * Initializes all database level variables.
     */
    public Employee(){
        try{
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            //getEmployee = connection.prepareStatement("SELECT * FROM employees;");

            fireEmployee = connection.prepareStatement("DELETE FROM employees"
                    + " WHERE username = ?;");
            insertEmployee = connection.prepareStatement("INSERT INTO employees"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");
            updateEmployee = connection.prepareStatement("UPDATE employees"
                    + " SET first_name=?, last_name=?, username=?, password=?, admin=?, book_management=?, reporting=?, pricing=?, timestamp=?"
                    + " WHERE username=?;"); 
                    
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Used to get all information about each employee from the database
     * @return a ResultSet containing the information about each employee
     */
    public ResultSet getEmployee(){
        try{
            getEmployee = connection.prepareStatement("SELECT * FROM employees;");
            resultSet = getEmployee.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Used to update the information of an employee, username can not be changed
     * @param firstName first name of employee
     * @param lastName last name of employee
     * @param username username of employee
     * @param password password for the employee
     * @param adminRights boolean dictating if the employee has administrator rights
     * @param bookManagementRights boolean dictating if the employee has book management rights
     * @param reportingRights boolean dictating if the employee has reporting rights
     * @param pricingRights boolean dictating if the employee has pricing rights
     * @param timestamp time that the update occurred
     * @return 1 if success, 0 if fail.
     */
    public int updateEmployee(String firstName, String lastName, String username, String password, boolean adminRights, boolean bookManagementRights, boolean reportingRights, boolean pricingRights, Timestamp timestamp){
         int result=0;
        try{
            //Replace question mark with SQL string that will place the variables.
            updateEmployee.setString(1, firstName);
            updateEmployee.setString(2,lastName);
            updateEmployee.setString(3,username);
            updateEmployee.setString(4,password);
            updateEmployee.setBoolean(5,adminRights);
            updateEmployee.setBoolean(6, bookManagementRights);
            updateEmployee.setBoolean(7, reportingRights);
            updateEmployee.setBoolean(8, pricingRights);
            updateEmployee.setTimestamp(9,timestamp);
            updateEmployee.setString(10,username);
            result = updateEmployee.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to add an employee to the database
     * @param firstName first name of employee
     * @param lastName last name of employee
     * @param username username of employee
     * @param password password for the employee
     * @param adminRights boolean dictating if the employee has administrator rights
     * @param bookManagementRights boolean dictating if the employee has book management rights
     * @param reportingRights boolean dictating if the employee has reporting rights
     * @param pricingRights boolean dictating if the employee has pricing rights
     * @param timestamp time that the update occurred
     * @return 1 if success, 0 if fail.
     */
    public int insertEmployee(String firstName, String lastName, String username, String password, boolean adminRights, boolean bookManagementRights, boolean reportingRights, boolean pricingRights, Timestamp timestamp){
        int result=0;
        try{
            //Replace question mark with SQL string that will place the variables.
            insertEmployee.setString(1, firstName);
            insertEmployee.setString(2,lastName);
            insertEmployee.setString(3,username);
            insertEmployee.setString(4,password);
            insertEmployee.setBoolean(5,adminRights);
            insertEmployee.setBoolean(6, bookManagementRights);
            insertEmployee.setBoolean(7, reportingRights);
            insertEmployee.setBoolean(8, pricingRights);
            insertEmployee.setTimestamp(9,timestamp);
            result = insertEmployee.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to delete an employee from the database
     * @param username username of the employee to be deleted
     * @return 1 if success, 0 if fail.
     */
    public int fireEmployee(String username){
        int result = 0;
        try{
           fireEmployee.setString(1, username);
           result = fireEmployee.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to get the full name of an employee based on their username
     * @param theUsername the username of the employee for whom we are finding the name
     * @return the name that is associated with the given username.
     */
    public String getNameFromUsername(String theUsername){
        ResultSet currentEmployees = getEmployee();
        String name = "";
        try{
            while(currentEmployees.next()){
                if(currentEmployees.getString("username").equals(theUsername)){
                    name = currentEmployees.getString("first_name") + " " + currentEmployees.getString("last_name");        
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return name;
    }
    
    /**
     * Used to create a secure 8-character password for each new channel partner and employee
     * @return a secure password containing numbers, uppercase letters and lowercase letters
     */
    public String createPassword(){
        String password = "";
        String character = "";
        int numOrLet;
        int letterChoice;
        int numberChoice;
        String[] alphabet = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
        String[] caps = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        while(password.length() < 7)
        {
            numOrLet = (int)(Math.random()*3) + 1;
            
            //lowercase letter
            if(numOrLet == 1)
            {
                letterChoice = (int)(Math.random()*26);
                character = alphabet[letterChoice];
            }
            //number
            else if (numOrLet == 2)
            {
                numberChoice = (int)(Math.random()*10);
                character = Integer.toString(numberChoice);
            }
            //capital letter
            if(numOrLet == 3)
            {
                letterChoice = (int)(Math.random()*26);
                character = caps[letterChoice];
            } 
            password += character;
        }
        letterChoice = (int)(Math.random()*26);
        password += caps[letterChoice];
        return password;
    }
    
    /**
     * Used to close the connection to the database after a page is done using it
     */
    public void closeConnection(){
        try{
            connection.close();
        }catch(Exception e){
            //nothing
        }
    }
    
}