/**
 * The purpose of the Black Scholes Daily Accumulator is to calculate the accrued Black Scholes values for daily orders.
 * It will use the code similar to the "Daily Tracking of Accrued Cash" table in AdminIndividualOrderSummary.jsp. 
 * @author Dutchess Developers
 * @version June 2017
 */
package com.dutchessdevelopers.commoditieswebsite;
import org.jquantlib.time.Date;
import org.jquantlib.time.calendars.UnitedKingdom;
import org.jquantlib.time.calendars.UnitedStates;
import org.jquantlib.time.Calendar;
import org.jquantlib.time.TimeUnit;
import java.util.Scanner;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BlackScholesDailyAccumulator {
    
    /**
     * INSTANCE VARIABLES
     */
    public double delta;
    public double gamma;
    public double theta;
    public double vega;
    public double fairValue;
    public double optionValueTotal;
    public double accruedValueTotal;
    
    /**
     * CONSTRUCTOR: Initialized BlackScholesDailyAccumulator object with given parameters
     * Within the constructor, the Black Scholes accrued totals are also calculated.
     * @param instrument The instrument used to place the order (call or put)
     * @param current_underlying_futures_price the current underlying futures price of the commodity being traded in the order.
     * @param executed_strike the executed strike of the order
     * @param underlying_futures_code the underlying futures code of the commodity being traded in the order (e.g. CAH7)
     * @param dividend_yield the dividend yield assigned to the order
     * @param daily_trader_volatility the daily trader volatility assigned to the order.
     * @param tradeDate the date on which the order was traded, given in the form yyyy-mm-dd
     * @param expirationDate the expiration date of the order, given in the form yyyy-mm-dd
     * @param action the action being performed on the order (buy or sell)
     * @param order_volume_quantity number of order volume units contained within the order
     * @param original_number_of_days the number of whole business days between the trade date and the expiration date
     */
    public BlackScholesDailyAccumulator(String instrument,
                                        double current_underlying_futures_price,
                                        double executed_strike,
                                        String underlying_futures_code,
                                        double dividend_yield,
                                        double daily_trader_volatility,
                                        String tradeDate,
                                        String expirationDate,
                                        String action,
                                        double order_volume_quantity,
                                        int original_number_of_days){
        double deltaUnits = 0, gammaUnits = 0, thetaUnits = 0, vegaUnits = 0, fairValueUnits = 0, optionValue = 0, accruedValue = 0;
        double euroPayout = 0; //THIS VALUE IS NOT BEING USED IN THE CURRENT VERSION OF THIS WEBSITE, IF IT IS IMPLEMENTED PASS IN THE VALUE TO THIS METHOD
        //Instantiate proper instance of trading calendar.
        org.jquantlib.time.Calendar tradingCal = null;
        if(instrument.toLowerCase().equals("put")){
            tradingCal = new UnitedStates();
        } 
        else if(instrument.toLowerCase().equals("call")){
            tradingCal = new UnitedKingdom();
        }
        org.jquantlib.time.Date today = new org.jquantlib.time.Date(new java.util.Date());
        
        //Instantiate Pulled Data.
        ResultSet todaysData = null;
        try{
            PulledData pd = new PulledData();
            todaysData = pd.getTodaysDataByCode(underlying_futures_code);
            todaysData.first();
        } catch(SQLException e){
            e.printStackTrace();
        }
        org.jquantlib.time.Date trade_date = new org.jquantlib.time.Date(java.sql.Date.valueOf(tradeDate));
        org.jquantlib.time.Date expiration_date = new org.jquantlib.time.Date(java.sql.Date.valueOf(expirationDate));
        int original_days = tradingCal.businessDaysBetween(trade_date, expiration_date, false, true) + 1;

        /** RUN THE ACCRUAL TABLE
            USE WHILE LOOP TO RUN THROUGH EACH DATE IN ACCRUAL TABLE
            CONTINUES UNTIL TRADE DATE REACHES EXPIRATION DATE
            INCREMENT THE TABLE DATE EACH TIME THROUGH  **/
        while (trade_date.shortDate().before(expiration_date.shortDate()) || trade_date.equals(expiration_date)){
            
            //Calculate current days between today and the incrementing trade date.
            int remaining_days = tradingCal.businessDaysBetween(today, trade_date, false, true) + 1; 
            
            //Calculate the current interest rate based upon the current remaining days.
            double interest_rate = 0;
            try{
                interest_rate = BlackScholes.calcCurrentInterestRate(
                                        todaysData.getDouble("interest_30_day"),
                                        todaysData.getDouble("interest_90_day"),
                                        todaysData.getDouble("interest_180_day"), 
                                        todaysData.getDouble("interest_360_day"),
                                        remaining_days);
            } catch(SQLException e){
                e.printStackTrace();
            }
            
            //Instantiate new blackscholes instance with current remaining days
            BlackScholes bs = new BlackScholes(instrument, current_underlying_futures_price, executed_strike,
            interest_rate, dividend_yield, daily_trader_volatility, remaining_days);
            
            //Multiplier to cnvert values to units.
            double daily_volume_expiring = order_volume_quantity / (double) original_days;
            
            //Call black scholes methods to calculate current vals for delta gamma vega and theta
            deltaUnits = bs.getDelta() * daily_volume_expiring;
            gammaUnits = bs.getGamma() * daily_volume_expiring;
            thetaUnits = bs.getTheta() * daily_volume_expiring;
            vegaUnits = bs.getVega()   * daily_volume_expiring;
            fairValueUnits = bs.getMarketPricePerUnit();
            optionValue = fairValueUnits * daily_volume_expiring;
            accruedValue = euroPayout * daily_volume_expiring;
            //System.out.println("DELTA IN INDIVIDUAL ROW: " + bs.getDelta());
            
            //Check parameters and convert to negative or positive if necessary.
            if(action.toLowerCase().equals("buy")){
                vegaUnits = Math.abs(vegaUnits);
                thetaUnits = -1 * Math.abs(thetaUnits);
                gammaUnits = Math.abs(gammaUnits);
                if(instrument.toLowerCase().equals("call")){
                    deltaUnits = Math.abs(deltaUnits);
                }
                else if(instrument.toLowerCase().equals("put")){
                    deltaUnits = -1 * Math.abs(deltaUnits);
                }
            }
            else if(action.toLowerCase().equals("sell")){
                vegaUnits = -1 * Math.abs(vegaUnits);
                thetaUnits = Math.abs(thetaUnits);
                gammaUnits = -1 * Math.abs(gammaUnits);
                if(instrument.toLowerCase().equals("call")){
                    deltaUnits = -1 * Math.abs(deltaUnits);
                }
                else if(instrument.toLowerCase().equals("put")){
                    deltaUnits = Math.abs(deltaUnits);
                }
            }
            
            //Incrementations
            if(today.compareTo(trade_date) <= 0){
                delta += deltaUnits;
                gamma += gammaUnits;
                vega += vegaUnits;
                theta += thetaUnits;
                fairValue += fairValueUnits;
                optionValueTotal += optionValue;
                accruedValueTotal += accruedValue;
            }
            
            //Increment the trade date.
            trade_date = tradingCal.advance(trade_date, 1, TimeUnit.Days);
        }
        fairValue /= original_days;
    }
    
    /**ACCESSORS--RETURN TOTAL VALUES**/
    
    /**
     * 
     * @return the delta total
     */
    public double getDeltaTotal(){
        return delta;
    }
    /**
     * 
     * @return the gamma total
     */
    public double getGammaTotal(){
        return gamma;
    }
    /**
     * 
     * @return the vega total
     */
    public double getVegaTotal(){
        return vega;
    }
    
    /**
     * 
     * @return the theta total
     */
    public double getThetaTotal(){
        return theta;
    }
    
    /**
     * 
     * @return the fair value accrued average
     */
    public double getFairValueAvg(){
        return fairValue;
    }
    
    /**
     * 
     * @return the option value total
     */
    public double getOptionValue(){
        return optionValueTotal;
    }
    
    /**
     * 
     * @return the accrued value total
     */
    public double getAccuredValue(){
        return accruedValueTotal;
    }
}