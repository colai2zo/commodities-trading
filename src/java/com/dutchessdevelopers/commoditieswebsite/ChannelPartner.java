/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dutchessdevelopers.commoditieswebsite;
import java.sql.*;
import java.lang.Math;

/**
 * 
 * @author Joey
 */
public class ChannelPartner {
    //These Keys will allow me to gain access to the database.
    String URL = "jdbc:mysql://localhost:3306/commodities_trading?autoReconnect=true&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "1234";
    
    /** Database Level Variables **/
    Connection connection = null;
    PreparedStatement getPartners = null;
    PreparedStatement deletePartners = null;
    PreparedStatement insertPartners = null;
    PreparedStatement updateChannelPartners = null;
    ResultSet resultSet = null;
    
    /**
     * DEFAULT CONSTRUCTOR
     * <br>
     * Initializes all database level variables.
     */
    public ChannelPartner(){
        try{
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            getPartners = connection.prepareStatement("SELECT * FROM channel_partners;");

            deletePartners = connection.prepareStatement("DELETE FROM channel_partners"
                    + " WHERE channel_partner_id = ?;");
            insertPartners = connection.prepareStatement("INSERT INTO channel_partners"
                    + " (channel_partner_id,name,username,password,timestamp)"
                    + " VALUES (?, ?, ?, ?, ?);");
            updateChannelPartners = connection.prepareStatement("UPDATE channel_partners"
                    + " SET channel_partner_id=?, name=?, username=?, password=?, timestamp=?"
                    + " WHERE channel_partner_id=?;"); 
                    
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Used to get all information about each channel partner from the database
     * @return a ResultSet containing the information about each channel partner
     */
    public ResultSet getChannelPartners(){
        try{
            resultSet = getPartners.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Used to update the information on a channel partner, ID can not be changed
     * @param id channel partner ID, used as the identifier
     * @param name new name of the channel partner
     * @param username new username for the channel partner
     * @param password new password for the channel partner
     * @param timestamp new timestamp for this update
     * @return 1 if success, 0 if fail.
     */
    public int updateChannelPartners(String id, String name, String username, String password, Timestamp timestamp){
         int result=0;
        try{
            //Replace question mark with SQL string that will place the variables.
            updateChannelPartners.setString(1, id);
            updateChannelPartners.setString(2, name);
            updateChannelPartners.setString(3, username);
            updateChannelPartners.setString(4, password);
            updateChannelPartners.setTimestamp(5, timestamp);
            updateChannelPartners.setString(6, id);
            result = updateChannelPartners.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to add a new channel partner to the database
     * @param id ID code for the new channel partner, should be unique and about 3 letters long
     * @param name name of the company
     * @param username username for the new channel partner
     * @param password password for the new channel partner
     * @param timestamp time that the channel partner was created
     * @return 1 if success, 0 if fail.
     */
    public int insertChannelPartners(String id, String name, String username, String password, Timestamp timestamp){
        int result=0;
        try{
            //Replace question mark with SQL string that will place the variables.
            insertPartners.setString(1, id);
            insertPartners.setString(2, name);
            insertPartners.setString(3, username);
            insertPartners.setString(4, password);
            insertPartners.setTimestamp(5, timestamp);
            result = insertPartners.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * @param id the channel partner ID of the channel partner to be deleted.
     * @return 1 if success, 0 if fail.
     */
    public int deleteChannelPartner(String id){
        int result = 0;
        try{
           deletePartners.setString(1, id);
           result = deletePartners.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to create a secure 8-character password for each new channel partner and employee
     * @return a secure password containing numbers, uppercase letters and lowercase letters
     */
    public String createPassword(){
        String password = "";
        String character = "";
        int numOrLet;
        int letterChoice;
        int numberChoice;
        String[] alphabet = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
        String[] caps = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        while(password.length() < 7)
        {
            numOrLet = (int)(Math.random()*3) + 1;
            
            //lowercase letter
            if(numOrLet == 1)
            {
                letterChoice = (int)(Math.random()*26);
                character = alphabet[letterChoice];
            }
            //number
            else if (numOrLet == 2)
            {
                numberChoice = (int)(Math.random()*10);
                character = Integer.toString(numberChoice);
            }
            //capital letter
            if(numOrLet == 3)
            {
                letterChoice = (int)(Math.random()*26);
                character = caps[letterChoice];
            } 
            password += character;
        }
        letterChoice = (int)(Math.random()*26);
        password += caps[letterChoice];
        return password;
    }
    
    /**
     * Used to get the ID of a channel partner based on their username
     * @param username the username of the partner for whom we are finding the ID
     * @return the ID of that partner
     */
    public String getIDByUsername(String username){
        ResultSet currentPartners = getChannelPartners();
        String id = "";
        try{
            while(currentPartners.next()){
                if(currentPartners.getString("username").equals(username)){
                    id = currentPartners.getString("channel_partner_id");        
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return id;
    }

    /**
     * Used to get the name of a channel partner based on their ID
     * @param theID the ID of the partner for whom we are finding the name
     * @return the name of that partner
     */
    public String getNameByID(String theID){
        ResultSet currentPartners = getChannelPartners();
        String name = "";
        try{
            while(currentPartners.next()){
                if(currentPartners.getString("channel_partner_id").equals(theID)){
                    name = currentPartners.getString("name");        
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return name;
    }
    
    /**
     * Used to get the name of a channel partner based on their username
     * @param theUsername the username of the partner for whom we are finding the name
     * @return the name of that partner
     */
    public String getNameByUsername(String theUsername){
        ResultSet currentPartners = getChannelPartners();
        String name = "";
        try{
            while(currentPartners.next()){
                if(currentPartners.getString("username").equals(theUsername)){
                    name = currentPartners.getString("name");        
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return name;
    }
    
    /**
     * Used to get the ID of a channel partner based on their name
     * @param theName the name of the partner for whom we are finding the ID
     * @return the ID of that partner
     */
    public String getIdByName(String theName){
        ResultSet currentPartners = getChannelPartners();
        String id = "";
        try{
            while(currentPartners.next()){
                if(currentPartners.getString("name").equals(theName)){
                    id = currentPartners.getString("channel_partner_id");        
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return id;
    }
    
    /**
     * Used to close the connection to the database after a page is done using it
     */
    public void closeConnection(){
        try{
            connection.close();
        }catch(Exception e){
            //nothing
        }
    }
}