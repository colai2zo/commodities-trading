/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dutchessdevelopers.commoditieswebsite;
import java.sql.*;

/**
 *
 * @author Lucas
 */
public class Brokers {
    //These keys will allow me to gain access to the database.
    String URL = "jdbc:mysql://localhost:3306/commodities_trading?autoReconnect=true&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "1234";
    
    /** Database Level Variables **/
    Connection connection = null;
    PreparedStatement getBroker = null;
    PreparedStatement deleteBroker = null;
    PreparedStatement insertBroker = null;
    PreparedStatement updateBroker = null;
    PreparedStatement getBrokersForCP = null;
    ResultSet resultSet = null;
    
    /**
     * DEFAULT CONSTRUCTOR
     * <br>
     * Initializes all database level variables.
     */
    public Brokers(){
        try{
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            getBroker = connection.prepareStatement("SELECT *"
                    + " FROM brokers ORDER BY channel_partner;");
            getBrokersForCP = connection.prepareStatement("SELECT * FROM brokers WHERE channel_partner=?;");
            deleteBroker = connection.prepareStatement("DELETE FROM brokers"
                    + " WHERE (company_name=? and contact_name=?);");
            insertBroker = connection.prepareStatement("INSERT INTO brokers (company_name,contact_name,channel_partner)"
                    + " VALUES(?,?,?);");
            updateBroker = connection.prepareStatement("UPDATE brokers"
                    + " SET company_name=?,contact_name=?,channel_partner=?"
                    + " WHERE (company_name=? and contact_name=?);");   
                    
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Used to get all information about each broker from the database
     * @return a ResultSet containing the information about each broker sorted by channel partner
     */
    public ResultSet getBroker(){
        try{
            resultSet = getBroker.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Used to update the information on a broker
     * @param oldCompanyName current name of the company
     * @param oldContactName current name of the contact
     * @param companyName new name of the company
     * @param contactName new name of the contact
     * @param channelPartner name of the channel partner associated with the broker
     * @return 1 if success, 0 if fail.
     */
    public int updateBroker(String oldCompanyName, String oldContactName, String companyName, String contactName, String channelPartner){
        int result=0;
        try{
            //Replace question mark with SQL string that will place the variables.
            updateBroker.setString(1, companyName);
            updateBroker.setString(2, contactName);
            updateBroker.setString(3, channelPartner);
            updateBroker.setString(4, oldCompanyName);
            updateBroker.setString(5, oldContactName);
            result = updateBroker.executeUpdate(); 
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * used to add a new broker to the database
     * @param companyName name of the broker company
     * @param contactName name of the contact at the company
     * @param channelPartner name of the channel partner associated with the broker
     * @return 1 if success, 0 if fail.
     */
    public int insertBroker(String companyName, String contactName, String channelPartner){
        int result=0;
        
        try{
            //Replace question mark with SQL string that will place the variables.
            insertBroker.setString(1, companyName);
            insertBroker.setString(2, contactName);
            insertBroker.setString(3, channelPartner);
            result = insertBroker.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to remove a broker from the database
     * @param companyName name of the broker company
     * @param contactName name of the broker contact
     * @return 1 if success, 0 if fail.
     */
    public int deleteBroker(String companyName, String contactName){
        System.out.println("Contact Name: " + contactName + "\nCompany Name: " + companyName);
        int result = 0;
        try{
           deleteBroker.setString(1, companyName);
           deleteBroker.setString(2, contactName);
           result = deleteBroker.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result; 
    }
    
    /**
     * Used to get the information about brokers associated with a certain channel partner
     * @param theCP the channel partner that you want the brokers for
     * @return a ResultSet that contains all the stored information about the brokers associated with a channel partner
     */
    public ResultSet getBrokersForCP(String theCP){
        try{
            getBrokersForCP.setString(1, theCP);
            resultSet = getBrokersForCP.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
        
    }
    
    /**
     * Used to close the connection to the database after a page is done using it
     */
    public void closeConnection(){
        try{
            connection.close();
        }catch(Exception e){
            //nothing
        }
    }
}
