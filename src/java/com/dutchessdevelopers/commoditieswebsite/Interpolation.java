package com.dutchessdevelopers.commoditieswebsite;

/*
 * The purpose of the interpolation class is to estimate a value in between two other values based on
 * a linear formula derived from the data we gather from bloomberg
 * @version June 2017
 * @author Dutchess Developers
 */

import java.util.*;
import java.sql.*;
import com.dutchessdevelopers.commoditieswebsite.*;
public class Interpolation {
    private double requested_volatility_strike_val;
    private double lower_volatility_strike;
    private double lower_volatility_strike_val;
    private double higher_volatility_strike;
    private double higher_volatility_strike_val;
    private ArrayList<Double> volatility_strikes;
    private ArrayList<Double> volatility_strike_vals;
    private double range;
    
    /**
     * The purpose of the Interpolation constructor it to take in the underlying futures code
     * and the requested volatility strike price, and to interpolate the volatility for that
     * strike price based on the two nearest strikes we have stored in our database. <br>
     * @param underlying_futures_code the exact and specific underlying futures code for which a volatility 
     * is to be interpolated
     * @param requested_volatility_strike the strike price which is being requested (namely, a strike which is 
     * in between two of the stored prices)
     */
    public Interpolation(String underlying_futures_code,double requested_volatility_strike){
        PulledData pd = new PulledData();
        volatility_strikes = new ArrayList<Double>();
        volatility_strike_vals = new ArrayList<Double>();
        ResultSet data = pd.getTodaysDataWithSimilarCode(underlying_futures_code);
        try{
        
        data.first();
        /*Populate ArrayLists based on option volatility shifts for the given commodity*/
        while(data.next()){
            
            if(data.getString("underlying_futures_code").length() > 4){
                if(requested_volatility_strike == data.getDouble("current_implied_volatility")){
                    requested_volatility_strike_val = data.getDouble("current_implied_volatility");
                    return;
                }
                volatility_strikes.add(Double.parseDouble(data.getString("underlying_futures_code").substring(6)));
                volatility_strike_vals.add(data.getDouble("current_implied_volatility"));
            }
        }
        } catch (SQLException e){
            e.printStackTrace();
        }
        
        /*Determine higher and lower volatility strikes, and their values*/
        
        //First entry in list is higher than requested value
        boolean request_is_too_low = false;
        if(requested_volatility_strike < volatility_strikes.get(0)){
            requested_volatility_strike_val = volatility_strike_vals.get(0);
            request_is_too_low = true;
        }
        
        //Last entry in list is lower than requested value
        boolean request_is_too_high = false;
        if(requested_volatility_strike > volatility_strikes.get(volatility_strikes.size() - 1)){
            requested_volatility_strike_val = volatility_strike_vals.get(volatility_strike_vals.size() - 1);
            request_is_too_high = true;
        }
        
        for(int i = 0; i < volatility_strikes.size() && !request_is_too_low && !request_is_too_high; i++){
            if(requested_volatility_strike < volatility_strikes.get(i)){
                higher_volatility_strike = volatility_strikes.get(i);
                lower_volatility_strike = volatility_strikes.get(i - 1);
                higher_volatility_strike_val = volatility_strike_vals.get(i);
                lower_volatility_strike_val = volatility_strike_vals.get(i - 1);
                i = volatility_strikes.size() + 1;
            }
        }
        
        /*Calculate requested volatility strike value*/
        if(!request_is_too_low && !request_is_too_high){
        double range = higher_volatility_strike - lower_volatility_strike;
        double higher_proportion = requested_volatility_strike - lower_volatility_strike;
        double lower_proportion = higher_volatility_strike - requested_volatility_strike;
        double numerator = (higher_proportion * higher_volatility_strike_val) + (lower_proportion * lower_volatility_strike_val);
        requested_volatility_strike_val = numerator / range;
        }
        pd.closeConnection();
    }
    
    /**
     * The purpose of getRequestedVolatilityStrikeValue is simply to return the value requested upon instantiation
     * @return requested volatility
     */
    public double getRequestedVolatilityStrikeValue(){
        return requested_volatility_strike_val;
    }
}
