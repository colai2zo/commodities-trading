/*
 * The purpose of the PulledData class is to provide a single location for all SQL
 * commands neccessary to retrieve data from our mySql database, or to update data within
 * the database.
 * @version June 2017
 * @author Dutchess Developers
 */

//UPDATE TO ACCOUNT FOR RE-INSTANTIATION
package com.dutchessdevelopers.commoditieswebsite;
import java.util.*;
import com.dutchessdevelopers.commoditieswebsite.*;
import java.sql.*;
import java.util.Calendar;

public class PulledData {
    /* These three arraylists are up to date lists of the desired futures codes. */
    ArrayList<String> codes_wheat;
    ArrayList<String> codes_rapeseed;
    ArrayList<String> codes_soy;
    
    /*SQL variables */
    Connection connection = null;
    String URL = "jdbc:mysql://localhost:3306/commodities_trading?autoReconnect=true&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "1234";
    
    /**
     * Constructs a new instance of the PulledData class by connecting to the SQL database.
     */
    public PulledData(){
        try{
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
        } catch(SQLException e){
            e.printStackTrace();
        }
        
        codes_wheat = new ArrayList();
        for(int i = 0; i < 12;i++){
            codes_wheat.add("CA");
        }
        codes_rapeseed = new ArrayList();
        for(int i = 0; i < 9;i++){
            codes_rapeseed.add("IJ");
        }
        codes_soy = new ArrayList();
        for(int i = 0;i<17;i++){
            codes_soy.add("SM");
        }
       
        //Create an object representing today, and parse it
        String extensions_wheat[] = {"H","K","U","Z"};
        String extensions_rapeseed[] = {"G","K","Q","X"};
        String extensions_soy[] = {"F","H","K","N","Q","U","V","Z"};
        Calendar today = Calendar.getInstance();
        
        //Get today's month and year and store them
        int currentMonth = today.get(Calendar.MONTH) + 1;
        int currentYear = today.get(Calendar.YEAR);
        
        //Based on month and year, generate appropriate list
        int INITIAL_J_WHEAT = 0;
        int INITIAL_J_RAPESEED = 0;
        int INITIAL_J_SOY = 0;
        switch(currentMonth){
            case 1:
                INITIAL_J_SOY=0;
                INITIAL_J_WHEAT = 0;
                INITIAL_J_RAPESEED=0;
                break;
            case 2:
                INITIAL_J_WHEAT = 0;
                INITIAL_J_SOY=1;
                INITIAL_J_RAPESEED=0;
                break;
            case 3:
                INITIAL_J_WHEAT = 0;
                INITIAL_J_SOY=1;
                INITIAL_J_RAPESEED=1;
                break;
            case 4:
                INITIAL_J_WHEAT = 1;
                INITIAL_J_SOY=2;
                INITIAL_J_RAPESEED=1;
                break;
            case 5:
                INITIAL_J_WHEAT = 1;
                INITIAL_J_SOY=2;
                INITIAL_J_RAPESEED=1;
                break;
            case 6:
                INITIAL_J_WHEAT = 2;
                INITIAL_J_SOY=3;
                INITIAL_J_RAPESEED=2;
                break;
            case 7:
                INITIAL_J_WHEAT = 2;
                INITIAL_J_SOY=3;
                INITIAL_J_RAPESEED=2;
                break;
            case 8:
                INITIAL_J_WHEAT = 2;
                INITIAL_J_SOY=4;
                INITIAL_J_RAPESEED=2;
                break;
            case 9:
                INITIAL_J_WHEAT = 2;
                INITIAL_J_SOY=5;
                INITIAL_J_RAPESEED=3;
                break;
            case 10:
                INITIAL_J_SOY=6;
                INITIAL_J_WHEAT = 3;
                INITIAL_J_RAPESEED=3;
                break;
            case 11:
                INITIAL_J_WHEAT = 3;
                INITIAL_J_SOY=7;
                INITIAL_J_RAPESEED=3;
                break;
            case 12:
                INITIAL_J_WHEAT = 3;
                INITIAL_J_SOY=7;
                INITIAL_J_RAPESEED=0;
                break;
        }
        int j = INITIAL_J_WHEAT;
        boolean first_time = true;
        for(int i = 0; i < 12; i++){
            if (j%4 == 0 && !first_time){
                currentYear++;
            }
            first_time=false;
            codes_wheat.set(i, (codes_wheat.get(i) + extensions_wheat[j%4] + String.valueOf(currentYear).substring(3)));
            j++;
        }
        currentYear = today.get(Calendar.YEAR);
        j=INITIAL_J_RAPESEED;
        first_time=true;
        for(int i = 0;i<9;i++){
            if(j%4 == 0 && !first_time){
                currentYear++;
            }
            first_time=false;
            codes_rapeseed.set(i, (codes_rapeseed.get(i) + extensions_rapeseed[j%4] + String.valueOf(currentYear).substring(3)));
            j++;
        }
        currentYear = today.get(Calendar.YEAR);
        j=INITIAL_J_SOY;
        first_time=true;
        for(int i = 0;i<17;i++){
            if(j%8 == 0 && !first_time){
                currentYear++;
            }
            first_time=false;
            codes_soy.set(i, (codes_soy.get(i) + extensions_soy[j%8] + String.valueOf(currentYear).substring(3)));
            j++;
        }
    }
    
    
    
    /**
     * The purpose of the getTodaysData method with no parameters is to return a
     * result set containing all data for all futures codes for the day.
     * @return a ResultSet object containing all of today's data.
     */
    public ResultSet getTodaysData(){
        String limit = "";

        ResultSet data = null;
        try{
            connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            PreparedStatement getData = connection.prepareStatement("select * from pulled_data" 
                    + " order by timestamp desc"
                    + ";");
            data = getData.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return data;
    }
    
    /**
     * The purpose of getTodaysData is to take in the underlying commodity name,
     * and return the result set containing all data for that commodity 
     * @param underlying_commodity_name the name of the commodity for which data will be returned (e.g Euronext Rapeseed)
     * @return a ResultSet object containing all of today's data for the given commodity name
     */
    public ResultSet getTodaysData(String underlying_commodity_name){
        
        String limit = "";
        if(underlying_commodity_name.equals("Euronext Rapeseed")){limit = "9";}
        if(underlying_commodity_name.equals("Euronext Wheat")){limit = "12";}
        if(underlying_commodity_name.equals("Soybean Meal")){limit = "17";}
        ResultSet data = null;
        try{
            connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            PreparedStatement getData = connection.prepareStatement("select * from pulled_data"
                    + " where underlying_commodity_name like '%" + underlying_commodity_name + "%'" 
                    + " order by timestamp desc;");
            data = getData.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return data;
    }
    
    /**
     * The purpose of getTodaysDataByCode is to take in the underlying futures code, and return the result <br>
     * containing all data for that single futures code 
     * @param underlying_futures_code the specific code for which data is to be returned
     * @return a ResultSet object containing all of today's data for the given futures code.
     */
    public ResultSet getTodaysDataByCode(String underlying_futures_code){

        String limit = "";
        ResultSet data = null;
        try{
            connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            PreparedStatement getData = connection.prepareStatement("select * from pulled_data"
                    + " where underlying_futures_code='" + underlying_futures_code + "'" 
                    + " order by timestamp desc"
                    + " limit 1;");
            data = getData.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return data;
    }
    
    /**
     * The purpose of getTodaysDataWithSimilarCode is to take in an underlying futures code, 
     * and return a result set containing all data for all codes containing that string 
     * *NOTE* if your parameter is 'SMV7' this will return data for 'SMV7','SMV7 200', 'SMV7 225', etc.
     * but not data for 'SMV8','SMN7',etc. 
     * @param underlying_futures_code the underlying futures which is similar to others about which you would like to retrieve data.
     * @return a ResultSet object containing all of today's data for all similar underlying futures codes.
     */
    public ResultSet getTodaysDataWithSimilarCode(String underlying_futures_code){
        String limit = "";
        ResultSet data = null;
        try{
            connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            PreparedStatement getData = connection.prepareStatement("select * from pulled_data"
                    + " where underlying_futures_code like '%" + underlying_futures_code + "%'" 
                    + " order by timestamp desc;");
            data = getData.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return data;
    }
    
    /**
     * The purpose of getMarketClose data is to take in the desired close date, and the desired futures code, 
     * and to return all data for that specific futures code on the close date <br>
     * @param closeDate the desired date at which market close data is to be returned
     * @param futuresCode the specific code which is to be returned (this will return only one code, and only if it matches the parameter exactly)
     * @return a ResultSet object containing all of the market data for the given futures code and date.
     */
    public ResultSet getMarketCloseData(java.sql.Date closeDate, String futuresCode){        
        ResultSet data = null;
        try{
            PreparedStatement getData = connection.prepareStatement("select * from market_close_data where close_date='" + closeDate + "' AND underlying_futures_code='" + futuresCode + "';");
            data = getData.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return data;
    }
    
    /**
     * The purpose of updateTraderVolatility is to take in the new volatility and the futures code,
     * and replace the old volatility with the new for that code <br>
     * @param newVolatility the new volatility value to be written over the old
     * @param futuresCode the specific and exact code for which the volatility will be changed.
     * @return 1 if successfully updated, 0 if failed to update.
     */
    public int updateDailyTraderVolatility(double newVolatility, String futuresCode){
        int result = 0;
        try{
            double volatility = (newVolatility);
            PreparedStatement updateDailyTraderVolatility = connection.prepareStatement("UPDATE pulled_data"
                    + " SET daily_trader_volatility=" + volatility + " WHERE underlying_futures_code='" + futuresCode + "';");
            result = updateDailyTraderVolatility.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * The purpose of the updateAllData method is to take in a sql string (where each command is separated by an !),
     * parse it, and execute each statement individually. <br>
     * @param sql a string containing each sql command to be executed, separated by !'s to be parsed
     */
    public void updateAllData(String sql){
        while(sql.length() > 0){
            String currentSql = sql.substring(0,sql.indexOf("!"));
            sql = sql.substring(0,sql.indexOf("!") + 1);
            
            try{
                PreparedStatement update = connection.prepareStatement(currentSql);
                update.executeUpdate();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
    }
    
    
    /**
     * The purpose of this method is to compare a futures code to a timestamp and return an integer 
     * representing their difference
     * @param code complete futures code (i.e. 'SMV7', or 'SMV7 200')
     * @param current a timestamp representing the present moment (although this can work for any timestamp)
     * @return 1 if current is after the expiry of the entered code or -1 if current is before the expiry of the entered code
     */
    public int compareCodeToTimestamp(String code, Timestamp current){
        
        String returnString = "20";
        int codeInt = Integer.parseInt(code.substring(3,4));
        int yearInt = Integer.parseInt(current.toString().substring(3,4));
        Integer totalYear = Integer.parseInt(current.toString().substring(0,4));
        if(codeInt >= yearInt){
            totalYear += (codeInt - yearInt);
        } else {
            totalYear += ((codeInt + 10) - yearInt);
        }
        
        returnString += totalYear.toString() + "-";
        
        if(code.substring(2,3).equals("Z")){
            returnString += "12";
        } else if(code.substring(2,3).equals("F")){
            returnString += "01";
        } else if (code.substring(2,3).equals("G")){
            returnString += "02";
        } else if(code.substring(2,3).equals("H")){
            returnString += "03";
        } else if(code.substring(2,3).equals("K")){
            returnString += "05";
        } else if(code.substring(2,3).equals("N")){
            returnString += "07";
        } else if(code.substring(2,3).equals("Q")){
            returnString += "08";
        } else if(code.substring(2,3).equals("U")){
            returnString += "09";
        } else if(code.substring(2,3).equals("V")){
            returnString += "10";
        }
        
        returnString += code.substring(3,4);
        
        int thisYear=totalYear;
        int thisMonth=Integer.parseInt(current.toString().substring(5,7));
        int codeYear=Integer.parseInt(returnString.substring(0,5));
        int codeMonth=Integer.parseInt(returnString.substring(6,8));
        int val = 0;
        if(thisYear > codeYear){
            //Today is after code expiry
            return 1;
        } else if (thisYear < codeYear){
            //Today is before code expiry
            return -1;
        } else {
            if(thisMonth > codeMonth){
                //Today is after code expiry
                return 1;
            } else if (thisMonth <= codeMonth){
                //Today is before code expiry
                return -1;
            }
        }
        return val;
    }
    
    /**
     * The purpose of getFuturesPrice is to take in a specific futures code,
     * and return the current underlying futures price and the daily trader volatility
     * for that futures code <br>
     * @param futuresCode the exact and specific futures code for which the price and 
     * volatility are to be retrieved
     * @return a ResultSet object containing the futures price and daily trader volatility for the specific futures code requested.
     */
    public ResultSet getFuturesPrice(String futuresCode){
        ResultSet data = null;
        try{
            PreparedStatement getFuturesPrice = connection.prepareStatement("select current_underlying_futures_price, daily_trader_volatility from pulled_data"
                    + " where underlying_futures_code = '" + futuresCode + "'" 
                    + " order by timestamp desc"
                    + " limit 1;");
            data = getFuturesPrice.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return data;
    }
    
    /**
     * Used to close the connection to the database after a page is done using it.
     */
    public void closeConnection(){
        try{
            connection.close();
        }catch(Exception e){
            //nothing
        }
    }
}