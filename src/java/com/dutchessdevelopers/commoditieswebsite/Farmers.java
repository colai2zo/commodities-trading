/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dutchessdevelopers.commoditieswebsite;
import java.sql.*;

/**
 *
 * @author Development
 */
public class Farmers {
    //These Keys will allow me to gain access to the database.
    
    String URL = "jdbc:mysql://localhost:3306/commodities_trading?autoReconnect=true&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "1234";
    
    /** Database Level Variables **/
    Connection connection = null;
    PreparedStatement getFarmers = null;
    PreparedStatement deleteFarmers = null;
    PreparedStatement insertFarmers = null;
    PreparedStatement updateFarmers = null;
    ResultSet resultSet = null;
    
    /**
     * DEFAULT CONSTRUCTOR
     * <br>
     * Initializes all database level variables.
     */
    public Farmers(){
        try{
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            getFarmers = connection.prepareStatement("SELECT * FROM farmers;");

            deleteFarmers = connection.prepareStatement("DELETE FROM farmers"
                    + " WHERE farmer_id = ?;");
            insertFarmers = connection.prepareStatement("INSERT INTO farmers"
                    + " (name,farmer_id,channel_partner_id,timestamp)"
                    + " VALUES (?, ?, ?, ?);");
            updateFarmers = connection.prepareStatement("UPDATE farmers"
                    + " SET name=?, farmer_id=?, channel_partner_id=?, timestamp=?"
                    + " WHERE farmer_id=?;"); 
                    
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Used to get all information about each farm from the database
     * @return a ResultSet containing the information about each farm
     */
    public ResultSet getFarmers(){
        try{
            resultSet = getFarmers.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Used to get all information about all the farms for a specified channel partner
     * @param partnerId ID of the channel partner whose farmers you want
     * @return a ResultSet containing all the information about all the farms for a specified channel partner
     */
    public ResultSet getFarmersForPartner(String partnerId){
        try{
            PreparedStatement getFarmersForPartner = connection.prepareStatement("SELECT * FROM farmers"
                    + " WHERE channel_partner_id = '" + partnerId + "';");
            resultSet = getFarmersForPartner.executeQuery();
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
        
    /**
     * Used to update the information about a certain farm
     * @param name name of the farm
     * @param farmer_id ID of the farm
     * @param channel_partner_id ID of the channel partner that is associated with the farm
     * @param timestamp time at which the update was made
     * @return 1 if success, 0 if fail.
     */
        public int updateFarmers(String name, String farmer_id, String channel_partner_id, Timestamp timestamp){
        int result=0;
        try{
            //Replace question mark with SQL string that will place the variables.
            updateFarmers.setString(1, name);
            updateFarmers.setString(2, farmer_id);
            updateFarmers.setString(3, channel_partner_id);
            updateFarmers.setTimestamp(4, timestamp);
            updateFarmers.setString(5, farmer_id);
            result = updateFarmers.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to add a farmer to the database
     * @param name name of the farm
     * @param farmer_id new ID code for the farm
     * @param channel_partner_id ID code for the channel partner that is adding the farm
     * @param timestamp time that the farm was added to the database
     * @return 1 if success, 0 if fail.
     */
    public int insertFarmers(String name, String farmer_id, String channel_partner_id, Timestamp timestamp){
        int result=0;
        try{
            //Replace question mark with SQL string that will place the variables.
            insertFarmers.setString(1, name);
            insertFarmers.setString(2, farmer_id);
            insertFarmers.setString(3, channel_partner_id);
            insertFarmers.setTimestamp(4, timestamp);
            result = insertFarmers.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to remove a farm from the database
     * @param fID farm ID code
     * @return 1 if success, 0 if fail.
     */
    public int deleteFarmers(String fID){
        int result = 0;
        try{
           deleteFarmers.setString(1, fID);
           result = deleteFarmers.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to generate the next farm ID in the sequence for a certain channel partner
     * @param cpID the id of the channel partner who created the farmer
     * @return the ID for a new farmer for that channel partner
     */
    public String generateFarmerID(String cpID){
            int count = 1000;
            ResultSet r = getFarmers();
                try{
                r.beforeFirst();
                while(r.next()){
                    if(
                       r.getString("channel_partner_id").equals(cpID)){
                        count++;
                    }
                }
                }catch(SQLException e){
                    e.printStackTrace();
                }
                return cpID + Integer.toString(count);
    }
    
    /**
     * Used to return the name of a farm based on their ID code
     * @param theID the ID code for the farm
     * @return the name of the farm
     */
    public String getNameById(String theID){
        ResultSet currentFarmers = getFarmers();
        String name = "";
        try{
            while(currentFarmers.next()){
                if(currentFarmers.getString("farmer_id").equals(theID)){
                    name += currentFarmers.getString("name");        
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return name;
    }
    
    /**
     * Used to close the connection to the database after a page is done using it
     */
    public void closeConnection(){
        try{
            connection.close();
        }catch(Exception e){
            //nothing
        }
    }
}