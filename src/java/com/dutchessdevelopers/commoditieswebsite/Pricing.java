/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dutchessdevelopers.commoditieswebsite;
import java.sql.*;
import java.util.Calendar;

/**
 * 
 * @author Joey
 */
public class Pricing{
    //These Keys will allow me to gain access to the database.
    String URL = "jdbc:mysql://localhost:3306/commodities_trading?autoReconnect=true&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "1234";
    
    /** Database Level Variables **/
    Connection connection = null;
    PreparedStatement getPricing = null;
    PreparedStatement deletePricing = null;
    PreparedStatement insertPricing = null;
    PreparedStatement updatePricing = null;
    ResultSet resultSet = null;
    
    /**
     * DEFAULT CONSTRUCTOR
     * <br>
     * Initializes all database level variables.
     */
    public Pricing(){
        try{
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            getPricing = connection.prepareStatement("SELECT * FROM pricing;");

            deletePricing = connection.prepareStatement("DELETE FROM pricing"
                    + " WHERE timestamp = ?;");
            insertPricing = connection.prepareStatement("INSERT INTO pricing"
                    + " (client_month, underlying_futures_code, expiration_date, current_underlying_futures_price, indicative_product_strike_price,"
                    + " current_implied_volatility, daily_trader_volatility, volatility_spread, product_fair_value_per_unit, product_markup,"
                    + " product_current_price_per_unit, product, frequency, underlying_commodity_name, strike_shift, timestamp)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            updatePricing = connection.prepareStatement("UPDATE pricing"
                    + " SET client_month=?, underlying_futures_code=?, expiration_date=?, current_underlying_futures_price=?, indicative_product_strike_price=?,"
                    + " current_implied_volatility=?, daily_trader_volatility=?, volatility_spread=?, product_fair_value_per_unit=?, product_markup=?,"
                    + " product_current_price_per_unit=?, product=?, frequency=?, underlying_commodity_name=?, strike_shift=?, timestamp=?"
                    + " WHERE timestamp=?;"); 
                    
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Used to get all of the pricing information that is stored in the database
     * @return a ResultSet object containing the requested information.
     */
    public ResultSet getPricing(){
        try{
            resultSet = getPricing.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Used to update the existing pricing information
     * @param client_month month the order was placed (e.g. January)
     * @param underlying_futures_code futures code associated with the commodity (e.g. CAH7)
     * @param expiration_date the date that the order expires
     * @param current_underlying_futures_price the current market price of the commodity
     * @param indicative_product_strike_price the price that the administrator sets for this commodity
     * @param current_implied_volatility volatility pulled directly from Bloomberg
     * @param volatility_override overridden volatility
     * @param volatility_spread difference between current implied volatility and volatility override
     * @param product_fair_value_per_unit the price calculated using black scholes model
     * @param product_markup change in the product fair value per unit
     * @param product_current_price_per_unit product fair value per unit plus product markup
     * @param product region of origin (European)
     * @param frequency frequency of the order (daily or bullet)
     * @param commodity name of the commodity
     * @param strike_shift the shift of the strike set by the administrator
     * @param timestamp the time when the pricing was set
     * @return 1 if success, 0 if fail.
     */
    public int updatePricing(String client_month, String underlying_futures_code, Date expiration_date, double current_underlying_futures_price, double indicative_product_strike_price,
                    double current_implied_volatility, double volatility_override, double volatility_spread, double product_fair_value_per_unit, double product_markup,
                    double product_current_price_per_unit, String product, String frequency, String commodity, int strike_shift, Timestamp timestamp){
        int result=0;
        
        try{

                //Replace question mark with SQL string that will place the variables.
                updatePricing.setString(1, client_month);
                updatePricing.setString(2, underlying_futures_code);
                updatePricing.setDate(3,expiration_date);
                updatePricing.setDouble(4,current_underlying_futures_price);
                updatePricing.setDouble(5,indicative_product_strike_price);
                updatePricing.setDouble(6,current_implied_volatility);
                updatePricing.setDouble(7, volatility_override);
                updatePricing.setDouble(8, volatility_spread);
                updatePricing.setDouble(9, product_fair_value_per_unit);
                updatePricing.setDouble(10,product_markup);
                updatePricing.setDouble(11, product_current_price_per_unit);
                updatePricing.setString(12,product);
                updatePricing.setString(13,frequency);
                updatePricing.setString(14, commodity);
                updatePricing.setInt(15, strike_shift);
                updatePricing.setTimestamp(16,timestamp);
                updatePricing.setTimestamp(17,timestamp);
                result = updatePricing.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to add pricing data to the database
     * @param client_month month the order was placed (e.g. January)
     * @param underlying_futures_code futures code associated with the commodity (e.g. CAH7)
     * @param expiration_date the date that the order expires
     * @param current_underlying_futures_price the current market price of the commodity
     * @param indicative_product_strike_price the price that the administrator sets for this commodity
     * @param current_implied_volatility volatility pulled directly from Bloomberg
     * @param volatility_override overridden volatility
     * @param volatility_spread difference between current implied volatility and volatility override
     * @param product_fair_value_per_unit the price calculated using black scholes model
     * @param product_markup change in the product fair value per unit
     * @param product_current_price_per_unit product fair value per unit plus product markup
     * @param product region of origin (European)
     * @param frequency frequency of the order (daily or bullet)
     * @param commodity name of the commodity
     * @param strike_shift the shift of the strike set by the administrator
     * @param timestamp the time when the pricing was set
     * @return 1 if success, 0 if fail.
     */
    public int insertPricing(String client_month, String underlying_futures_code, Date expiration_date, double current_underlying_futures_price, double indicative_product_strike_price,
                    double current_implied_volatility, double volatility_override, double volatility_spread, double product_fair_value_per_unit, double product_markup,
                    double product_current_price_per_unit, String product, String frequency, String commodity, int strike_shift, Timestamp timestamp){
        int result=0;
        try{
            //Replace question mark with SQL string that will place the variables.
            insertPricing.setString(1, client_month);
            insertPricing.setString(2, underlying_futures_code);
            insertPricing.setDate(3,expiration_date);
            insertPricing.setDouble(4,current_underlying_futures_price);
            insertPricing.setDouble(5,indicative_product_strike_price);
            insertPricing.setDouble(6,current_implied_volatility);
            insertPricing.setDouble(7, volatility_override);
            insertPricing.setDouble(8, volatility_spread);
            insertPricing.setDouble(9, product_fair_value_per_unit);
            insertPricing.setDouble(10,product_markup);
            insertPricing.setDouble(11, product_current_price_per_unit);
            insertPricing.setString(12,product);
            insertPricing.setString(13,frequency);
            insertPricing.setString(14, commodity);
            insertPricing.setInt(15, strike_shift);
            insertPricing.setTimestamp(16,timestamp);
            result = insertPricing.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to remove pricing information from the database
     * @param time timestamp of the pricing that you wish to delete
     * @return 1 if success, 0 if fail.
     */
    public int deletePricing(Timestamp time){
        int result = 0;
        try{
           deletePricing.setTimestamp(1, time);
           result = deletePricing.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to get the latest entries into the pricing database in order to 
     * display that data for the channel partner
     * @param commodity the name of the commodity
     * @param frequency frequency of the order (daily or bullet)
     * @return the ResultSet of the latest entries for that commodity
     */
    public ResultSet getCurrentPricing(String commodity, String frequency){
        int i = 0;
        ResultSet results = null;
        java.sql.Timestamp now = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        try{
            PreparedStatement getCurrentPricing = connection.prepareStatement("select * from pricing"
                    + " where timestamp like '%" + now.toString().substring(0, 10) + "%' and underlying_commodity_name = '" + commodity + "' and frequency = '" + frequency + "'" 
                    + " order by client_month asc"
                    + " limit 24;");
            results = getCurrentPricing.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return results;
    }
    
    /**
     * Used to get the most recent pricing information for a specific futures code
     * @param underlying_futures_code futures code associated with the commodity (e.g. CAH7)
     * @param frequency frequency of the order (daily or bullet)
     * @param month month the pricing is valid for as an integer from 1 to 12
     * @return a ResultSet containing the requested information
     */
    public ResultSet getCurrentPricingByCode(String underlying_futures_code, String frequency, int month){
        ResultSet data = null;
        String monthString;
        switch (month) {
            case 1:  monthString = "January";       break;
            case 2:  monthString = "February";      break;
            case 3:  monthString = "March";         break;
            case 4:  monthString = "April";         break;
            case 5:  monthString = "May";           break;
            case 6:  monthString = "June";          break;
            case 7:  monthString = "July";          break;
            case 8:  monthString = "August";        break;
            case 9:  monthString = "September";     break;
            case 10: monthString = "October";       break;
            case 11: monthString = "November";      break;
            case 12: monthString = "December";      break;
            default: monthString = "Invalid month"; break;
        }
        try{
            connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            PreparedStatement getData = connection.prepareStatement("select * from pricing"
                    + " where underlying_futures_code='" + underlying_futures_code + "'" 
                    + " and frequency='" + frequency + "'"
                    + " and client_month='" + monthString + "'"
                    + " order by timestamp desc"
                    + " limit 3;");
            data = getData.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return data;
    }
    
    /**
     * Used to close the connection to the database after a page is done using it
     */
    public void closeConnection(){
        try{
            connection.close();
        }catch(Exception e){
            //nothing
        }
    }
}