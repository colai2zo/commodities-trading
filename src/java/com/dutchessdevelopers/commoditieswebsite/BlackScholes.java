/**
 * The purpose of this class is to utilize all Black Scholes model formulas for given input of parameter values.
 * A BlackScholes object contains many parameter values that are used to calculate values for 
 * Delta 
 * Vega
 * Gamma
 * Theta
 * For each of Puts and Calls
 * TO USE: Instantiate a Black Scholes object with all parameters
 * Then, call the method(s) of the Black Scholes values that you would like to evaluate.
 * Utilizes the net.sf.javaml.utils.Statistics library for the normal probability distribution function
 */
package com.dutchessdevelopers.commoditieswebsite;

/** 
 * 
 * @author Joey Colaizzo
 */
import java.lang.Math;
import java.sql.Date;
import net.sf.javaml.utils.Statistics;
public class BlackScholes {
    
    //MASTER VARIABLES
    private String option; // "call" or "put"
    //Call - Used for rapeseed and wheat
    private double callDelta;
    private double callGamma;
    private double callVega;
    private double callTheta;
    private double call_market_price_per_unit;
    //Put - Used for Soybean Meal Only
    private double putDelta;
    private double putGamma;
    private double putVega;
    private double putTheta;
    private double put_market_price_per_unit;
    
    //SUB VARIABLES
    private double ln_so_x;
    private double t_r_q_s_2_2;
    private double s_sqrt_t;
    private double d1;
    private double d2;
    private double N_d1;
    private double N_neg_d1;
    private double N_d2;
    private double N_neg_d2;
    private double e_rt;
    private double X_e_rt;
    private double e_qt;
    private double SO_e_qt;
    private double pct_yr_to_exp;
    private double remaining_trade_days; 
    
    /**
     * 
     * @param theOption The instrument used to place the order (call or put) 
     * @param current_underlying_futures_price the current underlying futures price of the commodity being traded in the order.
     * @param executed_strike the executed strike of the order
     * @param interest_rate the interest rate assigned to the order
     * @param dividend_yield the dividend yield assigned to the order
     * @param daily_trader_volatility the daily trader volatility assigned to the commodity involved in the order.
     * @param remaining_trading_days the number of whole business days between today and the date of expiration 
     */
    public BlackScholes(String theOption, // "call" or "put"
            double current_underlying_futures_price,
            double executed_strike,
            double interest_rate,
            double dividend_yield,
            double daily_trader_volatility,
            int remaining_trading_days
            ){
        option = theOption;
        //CONVERT PERCENTAGE TO DECIMAL
        interest_rate /= 100;
        daily_trader_volatility /= 100;
        //FORMULA DEBUG PRINT STATEMENT
//        System.out.println("**BLACK SCHOLES PARAMETER VALUES**"
//                + "\nInstrument: " + theOption
//                + "\nCurrent Underlying Futures Price: " + current_underlying_futures_price
//                + "\nExecuted Strike: " + executed_strike
//                + "\nInterest Rate / 100: " + interest_rate
//                + "\nDividend Yield: " + dividend_yield
//                + "\nDaily Trader Volatility / 100: " + daily_trader_volatility
//                + "\nRemaining Trading Days: " + remaining_trading_days + "\n");
        dividend_yield = 0.0;
        
        /** Sub Calculations **/
        remaining_trade_days = (double) remaining_trading_days;
        if(remaining_trade_days < 0){
            remaining_trade_days = 0; //To avoid dividing errors.
        }
        pct_yr_to_exp = remaining_trade_days /  252;
        ln_so_x = Math.log(current_underlying_futures_price/executed_strike);
        t_r_q_s_2_2 = (interest_rate - dividend_yield + Math.pow(daily_trader_volatility, 2) / 2) * pct_yr_to_exp;
        s_sqrt_t = Math.max(0.000000000001, (daily_trader_volatility * Math.sqrt(pct_yr_to_exp)));
        d1 = (ln_so_x + t_r_q_s_2_2) / s_sqrt_t;
        d2 = d1 - s_sqrt_t;
        N_d1 = Statistics.normalProbability(d1);
        N_neg_d1 = 1 - N_d1;
        N_d2 = Statistics.normalProbability(d2);
        N_neg_d2 = 1 - N_d2;
        e_rt = Math.exp(-1 * interest_rate * pct_yr_to_exp);
        X_e_rt = executed_strike * e_rt;
        e_qt = Math.exp(-1 * dividend_yield * pct_yr_to_exp);
        SO_e_qt = current_underlying_futures_price * e_qt;
        
        //CONTINUATION OF DEBUGGER
//        System.out.println("**BLACK SCHOLES SUB-CALCULATIONS**"
//                + "\nPercent of Year to Expire: " + pct_yr_to_exp
//                + "\nln_so_x: " + ln_so_x
//                + "\nt_r_q_s_2_2: " + t_r_q_s_2_2
//                + "\ns_sqrt_t: " + s_sqrt_t
//                + "\nd1: " + d1
//                + "\nd2: " + d2
//                + "\nN(d1): " + N_d1
//                + "\nN(-d1)" + N_neg_d1
//                + "\nN(d2): " + N_d2
//                + "\nN(-d2): " + N_neg_d2
//                + "\ne_rt: " + e_rt
//                + "\nX_e_rt: " + X_e_rt
//                + "\ne_qt: " + e_qt
//                + "\nSO_e_qt: " + SO_e_qt + "\n");
        
        /** Call Calculations **/
        call_market_price_per_unit = SO_e_qt * N_d1 - X_e_rt * N_d2;
        callDelta = N_d1 * e_qt;
        callGamma = Math.exp(-1 * Math.pow(d1, 2) / 2) / Math.sqrt(2*Math.PI) *  e_qt / (current_underlying_futures_price * s_sqrt_t);
        callTheta = ( -(current_underlying_futures_price * Math.exp(-1 * Math.pow(d1, 2) / 2) / Math.sqrt(2 * Math.PI) * daily_trader_volatility *  e_qt / (2 * Math.sqrt(pct_yr_to_exp))) - (interest_rate * X_e_rt * N_d2) + (dividend_yield * current_underlying_futures_price * N_d1 * e_qt)) / 252;
        callVega = Math.exp(-1 * Math.pow(d1, 2) / 2) / Math.sqrt(2*Math.PI) * e_qt * current_underlying_futures_price * Math.sqrt(pct_yr_to_exp) / 100;
        
        /** Put Calculations **/
        put_market_price_per_unit = X_e_rt * N_neg_d2 - SO_e_qt * N_neg_d1;
        putGamma = callGamma;
        putVega = callVega;
        putDelta = e_qt * (N_d1 - 1);
        putTheta = ( -(current_underlying_futures_price * Math.exp(-1 * Math.pow(d1, 2) / 2) / Math.sqrt(2 * Math.PI) * daily_trader_volatility *  e_qt / (2 * Math.sqrt(pct_yr_to_exp))) + (interest_rate * X_e_rt * N_neg_d2) - (dividend_yield * current_underlying_futures_price * N_d1 * e_qt)) / 252;
        
        //CONTINUATION OF DEBUG STATEMENT
//        System.out.println("**BLACK SCHOLES CALCULATED VALUES**"
//                + "\nCall Market Price/Unit: " + call_market_price_per_unit
//                + "\nPut Market Price/Unit: " + put_market_price_per_unit
//                + "\nCall Delta: " + callDelta
//                + "\nPut Delta: " + putDelta
//                + "\nCall Vega: " + callVega
//                + "\nPut Vega: " + putVega
//                + "\nCall Theta: " + callTheta
//                + "\nPut Theta: " + putTheta
//                + "\nCall Gamma: " + callGamma
//                + "\nPut Gamma: " + putGamma
//                + "\n ******* " + new java.util.Date() + " *******");
    }
    
    /**
     * 
     * @return the delta value, or -999 if the instrument is unspecified.
     */
    public double getDelta(){
        if(option.toLowerCase().equals("call")){
            return callDelta;
        } else if(option.toLowerCase().equals("put")){
            return putDelta; 
        } else{
            return -999;
        }
    }
    
    /**
     * 
     * @return the gamma value, or -999 if the instrument is unspecified.
     */
    public double getGamma(){
        if(option.toLowerCase().equals("call")){
            return callGamma;
        } else if(option.toLowerCase().equals("put")){
            return putGamma; 
        } else{
            return -999;
        }
    }
    
    /**
     * 
     * @return the vega value, or -999 if the instrument is unspecified.
     */
    public double getVega(){
        if(option.toLowerCase().equals("call")){
            return callVega;
        } else if(option.toLowerCase().equals("put")){
            return putVega; 
        } else{
            return -999;
        }
    }
    
    /**
     * 
     * @return the theta value, or -999 if the instrument is unspecified.
     */
    public double getTheta(){
        if(option.toLowerCase().equals("call")){
            return callTheta;
        } else if(option.toLowerCase().equals("put")){
            return putTheta; 
        } else{
            return -999;
        }
    }
    
    /**
     * 
     * @return the market price per unit value, or -999 if the instrument is unspecified.
     */
    public double getMarketPricePerUnit(){
        if(option.toLowerCase().equals("call")){
            return call_market_price_per_unit;
        } else if(option.toLowerCase().equals("put")){
            return put_market_price_per_unit; 
        } else{
            return -999;
        }
    }
    
    /**
     * Calculates the number of days between two java.sql.Dates
     * @param date1 the starting date.
     * @param date2, which is at least one day after the starting date.
     * @return the number of days from date 1 to date 2.
     */
    public static int daysBetween(Date date1, Date date2){
        long time1 = date1.getTime(); // Milliseconds since 1970.
        long time2 = date2.getTime(); // ^^
        long diff = Math.abs(time1 - time2); //Time between the dates
        long daysBetween = diff / 1000 / 60 / 60 / 24; //Convert to days.
        return (int) daysBetween;
    }
    
       /**
     * Calculates an interpolated interest rate based upon the 30, 90, 180, and 360 day rates.
     * @param thirty_day 30 day interest rate
     * @param ninety_day 90 day interest rate
     * @param one_eighty_day 180 day interest rate
     * @param three_sixty_day 360 day interest rate
     * @param remaining_days number of business days between now and the expiration date of the order
     * @return the current interest rate
     */
    public static double calcCurrentInterestRate(double thirty_day, double ninety_day, double one_eighty_day, double three_sixty_day, int remaining_days){
        double int_rate = ninety_day;
        if (remaining_days <= 30){
            int_rate = thirty_day;
        } else if (remaining_days <=90){
            int_rate = ((remaining_days - 30) / (90 - 30) * (ninety_day - thirty_day)) + thirty_day;
        } else if (remaining_days <=180){
            int_rate = ((remaining_days - 90) / (180 - 90) * (one_eighty_day - ninety_day)) + ninety_day;
        } else if (remaining_days <=360){
            int_rate = ((remaining_days - 180) / (360 - 180) * (three_sixty_day - one_eighty_day)) + one_eighty_day;
        } else{
            int_rate = three_sixty_day;
        }
        return int_rate;
    }
}
