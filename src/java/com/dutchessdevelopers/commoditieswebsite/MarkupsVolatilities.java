/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dutchessdevelopers.commoditieswebsite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 *
 * @author Joey
 */
public class MarkupsVolatilities {
     //These Keys will allow me to gain access to the database.
    
    String URL = "jdbc:mysql://localhost:3306/commodities_trading?autoReconnect=true&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "1234";
    
    /** Database Level Variables **/
    Connection connection = null;
    PreparedStatement getMarkupsAndVolatilities = null;
    PreparedStatement deleteMarkupsAndVolatilities = null;
    PreparedStatement insertMarkupsAndVolatilities = null;
    ResultSet resultSet = null;
    
    /**
     * DEFAULT CONSTRUCTOR
     * <br>
     * Initializes all database level variables.
     */
    public MarkupsVolatilities(){
        try{
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            getMarkupsAndVolatilities = connection.prepareStatement("SELECT * FROM markups_volatilities"
                    + " WHERE product = ? AND underlying_commodity_name = ? AND row = ?;");

            deleteMarkupsAndVolatilities = connection.prepareStatement("truncate table markups_volatilities");
            insertMarkupsAndVolatilities = connection.prepareStatement("INSERT INTO markups_volatilities"
                    + " (product,underlying_commodity_name,markup,volatility,row)"
                    + " VALUES (?, ?, ?, ?, ?);");         
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Used to get all information about markups and volatilities from the database
     * @return a ResultSet containing the information about each markup and volatility
     */
    public ResultSet getMarkupsAndVolatilites(String product, String underlying_commodity_name, int row){
        try{
            getMarkupsAndVolatilities.setString(1, product);
            getMarkupsAndVolatilities.setString(2, underlying_commodity_name);
            getMarkupsAndVolatilities.setInt(3, row);
            resultSet = getMarkupsAndVolatilities.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
        
    /**
     * Used to add a farmer to the database
     * @param product the product for which the volatility spread and markup are being entered
     * @param underlying_commodity_name the commodity for which the volatility spread and markup are being entered
     * @param markup the product markup being entered
     * @param volatility_spread the volatility spread being entered
     * @param row the row in the database at which it is being entered.
     * @return 1 if success, 0 if fail.
     */
    public int insertMarkupsAndVolatilities(String product, String underlying_commodity_name, double markup, double volatility_spread, int row){
        int result=0;
        try{
            //Replace question mark with SQL string that will place the variables.
            insertMarkupsAndVolatilities.setString(1, product);
            insertMarkupsAndVolatilities.setString(2, underlying_commodity_name);
            insertMarkupsAndVolatilities.setDouble(3, markup);
            insertMarkupsAndVolatilities.setDouble(4, volatility_spread);
            insertMarkupsAndVolatilities.setInt(5, row);
            result = insertMarkupsAndVolatilities.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to remove all markups and volatilitie spreads from the database
     * @return 1 if success, 0 if fail.
     */
    public int deleteMarkupsAndVolatilities(){
        int result = 0;
        try{
           result = deleteMarkupsAndVolatilities.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
        
    /**
     * Used to close the connection to the database after a page is done using it
     */
    public void closeConnection(){
        try{
            connection.close();
        }catch(Exception e){
            //nothing
        }
    }
}
