/**
 * This class contains methods that interact with the database for the purpose of
 * adding, deleting and modifying orders places through the saranacmanagemnt website.
 */
package com.dutchessdevelopers.commoditieswebsite;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.jquantlib.time.Month;
import org.jquantlib.time.TimeUnit;
import org.jquantlib.time.calendars.*;
import org.jquantlib.time.calendars.UnitedStates;
import org.jquantlib.time.calendars.UnitedKingdom;
import java.util.Date;
/**
 *
 * @author Dutchess Developers
 */
public class Orders {
    //These Keys will allow me to gain access to the database.
    String URL = "jdbc:mysql://localhost:3306/commodities_trading?autoReconnect=true&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "1234";
    
    /** Database Level Variables **/
    Connection connection = null;
    PreparedStatement getOrders = null;
    PreparedStatement deleteOrders = null;
    PreparedStatement insertOrders = null;
    ResultSet resultSet = null;
    
    /**
     * DEFAULT CONSTRUCTOR
     * <br>
     * Initializes all database level variables.
     */
    public Orders(){
        try{
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            getOrders = connection.prepareStatement("SELECT * FROM orders ORDER BY timestamp ASC;");
            insertOrders = connection.prepareStatement("INSERT INTO orders"
                    + " (channel_partner_id, farmer_id, order_id, status, trade_date, transaction_type, underlying_commodity_name, underlying_futures_code,"
                    + " client_month, instrument, product, frequency, executed_strike, expiration_date, product_current_price_per_unit,"
                    + " execution_product_total_cost, barrier_level, digital_payout_per_unit, current_underlying_futures_price, current_implied_volatility,"
                    + " market_price_per_unit, delta, gamma, vega, theta, mark_to_market, accrued_value,"
                    + " accrued_contracts, unwind_accrued_value_paid, override_option_value_paid, unwind_total_value_paid, broker_name, farmer_physical_contract_id,"
                    + " executed_hedge, digital_total_payout, unwind_option_value_paid, unwind_value_paid_per_unit,"
                    + " daily_trader_volatility, order_volume_quantity, unit_type, order_volume_contracts,"
                    + " original_number_of_days, days_accrued, remaining_days, accrued_volume_units,"
                    + " unwind_vega, unwind_volume_units, timestamp, strike_shift, action)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "  // Ten ?'s per row.
                    +          "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                    +          "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                    +          "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                    +          "?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
                    
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Allows for user to get data for a specific order ID
     * @param order_id order ID for the order that you want to return
     * @return the ResultSet containing the requested order.
     */
    public ResultSet getOrder(String order_id){
        try{
            PreparedStatement getOrder = connection.prepareStatement("SELECT * FROM orders where order_id='" + order_id + "';");
            resultSet = getOrder.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Allows user to get data for all orders that have the same base number. ex. 1001A, 1001O1, 1001U1
     * @param orderNum the number that is common to all the desired orders
     * @return a ResultSet containing all the data for the requested orders
     */
    public ResultSet getAllRelatedOrders(String orderNum){
        try{
            PreparedStatement getRelatedOrders = connection.prepareStatement("SELECT * FROM orders where order_id like '%" + orderNum + "%';");
            resultSet = getRelatedOrders.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }
        return resultSet;
    }
    
    /**
     * Used to arrange the orders in a way requested by the user through the interface. 
     * All sort options after the first one should be passed in beginning with a 
     * comma and a space before the next column header. This method will work even if
     * not all of the options are filled out
     * @param sort1 the primary sort option(column name)
     * @param sort2 the secondary sort option(, column name)
     * @param sort3 the third sort option(, column name)
     * @param sort4 the fourth sort option(, column name)
     * @return a ResultSet of all the order currently in the database sorted in the requested manor
     */
    public ResultSet getSortedOrders(String sort1, String sort2, String sort3, String sort4){
        try{
            //if there is nothing in the sort option when it is passed in it is set to nothing
            if(sort1 == null)
                sort1 = "";
            if(sort2 == null)
                sort2 = "";
            if(sort3 == null)
                sort3 = "";
            if(sort4 == null)
                sort4 = "";
            PreparedStatement getSortedOrders = connection.prepareStatement(
                    "SELECT * FROM orders ORDER BY "
                     + sort1 + sort2 + sort3 + sort4
                            + ";");
            resultSet = getSortedOrders.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Gets a ResultSet of all the orders currently in the database
     * @return a ResultSet of all orders currently in the database sorted by timestamp
     */
    public ResultSet getOrders(){
        try{
            resultSet = getOrders.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Get orders that were placed today
     * @return a ResultSet of orders that were placed today
     */
    public ResultSet getTodaysOrders(){
        try{
            Timestamp currentDay = new Timestamp(new java.util.Date().getTime());
            String currentDayString = currentDay.toString().substring(0,10);
            PreparedStatement getTodaysOrders = connection.prepareStatement(
                    "SELECT * FROM orders"
                            + " WHERE timestamp like '%" + currentDayString + "%';");
            resultSet = getTodaysOrders.executeQuery();
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
     
    /**
     * Used in the AdminAccountsDue.jsp page to allow the administrator to view which orders are due for each channel partner individually
     * @param channelPartnerId the ID of the channel partner that the page belongs to
     * @param statusNeeded that status that the requested orders must have to meet the criteria for the AdminAccountsDue page
     * @return a ResultSet containing the orders that are due, sorted by farm ID and contract ID to allow for efficient grouping
     */
    public ResultSet getOrdersDue(String channelPartnerId, String statusNeeded){
        try{
            ResultSet finalizedOrders;
            ArrayList orderNumbers = new ArrayList();
            String statement = "SELECT * from orders "
                    + "WHERE isReconciled = false "
                    + "AND transaction_type != 'Hedge' "
                    + "AND (status != 'Pending' AND status != 'Active' AND status != 'Decline') "
                    + "AND (";
            PreparedStatement getFinalizedOrders = connection.prepareStatement(
                    "SELECT order_id FROM orders"
                            + " WHERE status = '" + statusNeeded + "'"
                            + " AND isReconciled = false"
                            + " AND channel_partner_id = '" + channelPartnerId + "';");
            finalizedOrders = getFinalizedOrders.executeQuery();
            //Adds all finalized order IDs to an ArrayList
            while(finalizedOrders.next()){
                orderNumbers.add(finalizedOrders.getString("order_id").substring(0, finalizedOrders.getString("order_id").length() - 2));
            }
            if(orderNumbers.isEmpty())
                return null;
            //Adds the order ID of each finalized order to the prepared statement that will return the final ResultSet
            for(int i = orderNumbers.size() - 1; i >= 0; i--){
                if(i == orderNumbers.size() - 1)
                    statement += " order_id like '%" + orderNumbers.get(i).toString() + "%'";
                else
                    statement += " or order_id like '%" + orderNumbers.get(i).toString() + "%'";
            }
            statement += ") order by farmer_id asc, farmer_physical_contract_id asc, timestamp asc;";
            PreparedStatement getOrdersDue = connection.prepareStatement(statement);
            resultSet = getOrdersDue.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Used on the PartnerAccountsDue.jsp page to allow the channel partner to view what orders are due for each farmer individually
     * @param channelPartnerId the ID of the channel partner using the page
     * @param farmerId the ID of the farm that the channel partner wants to view
     * @return a ResultSet containing the orders that are due for a specific farmer sorted by contract ID
     */
    public ResultSet getOrdersDuePartner(String channelPartnerId, String farmerId){
        try{
            ResultSet finalizedOrders;
            ArrayList orderNumbers = new ArrayList();
            String statement = "SELECT * from orders "
                    + "WHERE isReconciled = false "
                    + "AND transaction_type != 'Hedge' "
                    + "AND (status != 'Pending' AND status != 'Active' AND status != 'Decline') "
                    + "AND farmer_id = '" + farmerId + "' "
                    + "AND channel_partner_id = '" + channelPartnerId + "' AND (";
            PreparedStatement getFinalizedOrders = connection.prepareStatement(
                    "SELECT order_id FROM orders "
                            + "WHERE channel_partner_id = '" + channelPartnerId + "' "
                            + "AND farmer_id = '" + farmerId + "' "
                            + "AND isReconciled = false "
                            + "AND status = 'Original';");
            finalizedOrders = getFinalizedOrders.executeQuery();
            //Adds all finalized order IDs to an ArrayList
            while(finalizedOrders.next()){
                orderNumbers.add(finalizedOrders.getString("order_id").substring(0, finalizedOrders.getString("order_id").length() - 2));
            }
            if(orderNumbers.isEmpty())
                return null;
            //Adds the order ID of each finalized order to the prepared statement that will return the final ResultSet
            for(int i = orderNumbers.size() - 1; i >= 0; i--){
                if(i == orderNumbers.size() - 1)
                    statement += "order_id like '%" + orderNumbers.get(i).toString() + "%'";
                else
                    statement += " or order_id like '%" + orderNumbers.get(i).toString() + "%'";
            }
            statement += ") order by farmer_id asc, farmer_physical_contract_id asc, timestamp asc;";
            PreparedStatement getOrdersDue = connection.prepareStatement(statement);
            resultSet = getOrdersDue.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    
    /**
     * Used on the PartnerAccountsDue.jsp page to allow the channel partner to view all the orders that are due for their farms
     * @param channelPartnerId the ID of the channel partner using the page
     * @return a ResultSet containing the orders that are due under the selected channel partner, sorted by farm ID and contract ID to allow for efficient grouping
     */
    public ResultSet getOrdersDuePartner(String channelPartnerId){
        try{
            ResultSet finalizedOrders;
            ArrayList orderNumbers = new ArrayList();
            String statement = "SELECT * from orders "
                    + "WHERE isReconciled = false "
                    + "AND transaction_type != 'Hedge' "
                    + "AND (status != 'Pending' AND status != 'Active' AND status != 'Decline') "
                    + "AND channel_partner_id = '" + channelPartnerId + "' AND (";
            PreparedStatement getFinalizedOrders = connection.prepareStatement(
                    "SELECT order_id FROM orders "
                            + "WHERE channel_partner_id = '" + channelPartnerId + "' "
                            + "AND isReconciled = false "
                            + "AND status = 'Original';");
            finalizedOrders = getFinalizedOrders.executeQuery();
            //Adds all finalized order IDs to an ArrayList
            while(finalizedOrders.next()){
                orderNumbers.add(finalizedOrders.getString("order_id").substring(0, finalizedOrders.getString("order_id").length() - 2));
            }
            if(orderNumbers.isEmpty())
                return null;
            //Adds the order ID of each finalized order to the prepared statement that will return the final ResultSet
            for(int i = orderNumbers.size() - 1; i >= 0; i--){
                if(i == orderNumbers.size() - 1)
                    statement += "order_id like '%" + orderNumbers.get(i).toString() + "%'";
                else
                    statement += " or order_id like '%" + orderNumbers.get(i).toString() + "%'";
            }
            statement += ") order by farmer_id asc, farmer_physical_contract_id asc, timestamp asc;";
            PreparedStatement getOrdersDue = connection.prepareStatement(statement);
            resultSet = getOrdersDue.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Used to get a ResultSet of all orders placed today by a specific channel partner
     * @param partnerID the ID of the specified channel partner
     * @return a ResultSet of all orders placed today by a specific channel partner
     */
    public ResultSet getTodaysOrdersForPartner(String partnerID){
        try{
        Timestamp currentDay = new Timestamp(new java.util.Date().getTime());
        String currentDayString = currentDay.toString().substring(0,10);
            PreparedStatement getTodaysOrders = connection.prepareStatement(
                    "SELECT * FROM orders"
                            + " WHERE channel_partner_id ='" + partnerID + "'"
                            + " and timestamp like '%" + currentDayString + "%';");
            resultSet = getTodaysOrders.executeQuery();
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Get all orders with a status of pending
     * @return a ResultSet of all orders with the status 'pending'
     */
    public ResultSet getPendingOrders(){
        try{
            PreparedStatement getPendingOrders = connection.prepareStatement(
                    "SELECT * FROM orders WHERE status= 'pending'"
                            + ";");
            resultSet = getPendingOrders.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Get all orders with a status of finalized
     * @return a ResultSet of all orders with the status 'finalized'
     */
    public ResultSet getFinalizedOrders(){
        try{
            PreparedStatement getFinalizedOrders = connection.prepareStatement(
                    "SELECT * FROM orders WHERE status= 'Finalized'"
                            + ";");
            resultSet = getFinalizedOrders.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Get all orders with a status of active
     * @return a ResultSet of all orders with the status 'active'
     */
    public ResultSet getActiveOrders(){
        try{
            PreparedStatement getActiveOrders = connection.prepareStatement(
                    "SELECT * FROM orders WHERE status= 'active'"
                            + ";");
            resultSet = getActiveOrders.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Get all orders with a status of pending for a given channel partner
     * @param partnerID the ID of the channel partner whose pending orders are being requested
     * @return a ResultSet of all orders with the status 'pending' for a given channel partner
     */
    public ResultSet getPendingOrdersForPartner(String partnerID){
        try{
            PreparedStatement getPendingOrders = connection.prepareStatement(
                    "SELECT * FROM orders WHERE status= 'pending' "
                            + "and channel_partner_id = '" + partnerID + "'"
                            + ";");
            resultSet = getPendingOrders.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Inserts an order into the database
     * @param channel_partner_id ID for the channel partner
     * @param farmer_id ID for the farm (e.g. JOE1000)
     * @param order_id ID for the order (e.g. 1000A)
     * @param status status of the order (active, finalized, pending, declined, etc.)
     * @param trade_date date the order was placed in format yyyy-mm-dd
     * @param transaction_type the type of transaction (hedge or trade)
     * @param underlying_commodity_name name of the commodity
     * @param underlying_futures_code futures code associated with the commodity (e.g. CAH7)
     * @param client_month month the order was placed (e.g. January)
     * @param instrument the instrument used to place the order (call or put)
     * @param product region of origin (European)
     * @param frequency frequency of the order (daily or bullet)
     * @param executed_strike the strike at which the order was executed
     * @param expiration_date the date that the order expires
     * @param product_current_price_per_unit the price calculated using black scholes model
     * @param execution_product_total_cost total cost of the order being placed
     * @param barrier_level barrier level
     * @param digital_payout_per_unit digital payout per unit
     * @param current_underlying_futures_price the current market price of the commodity
     * @param current_implied_volatility volatility pulled directly from Bloomberg
     * @param market_price_per_unit price calculated using the black scholes model
     * @param delta delta
     * @param gamma gamma
     * @param vega vega
     * @param theta theta
     * @param mark_to_market accrued value total plus option value total
     * @param position_profit_loss position profit loss
     * @param accrued_contracts accrued contracts
     * @param unwind_accrued_value_paid unwind accrued value paid
     * @param override_option_value_paid override option value paid
     * @param unwind_total_volume_paid unwind total volume paid
     * @param broker_name name of the broker who facilitated the hedge
     * @param farmer_physical_contract_id order identification used by channel partner to communicate with partner
     * @param executed_hedge executed strike plus strike shift
     * @param digital_total_payout digital total payout
     * @param unwind_option_value_paid unwind option value paid
     * @param unwind_value_paid_per_unit unwind value paid per unit
     * @param daily_trader_volatility overridden volatility value
     * @param order_volume_quantity the number of units in an order
     * @param unit_type the type of unit used in the order volume quantity (metric tons or short tons)
     * @param order_volume_contracts number of contracts in the order (order volume units / 50 or 100, depending on the commodity)
     * @param original_number_of_days number of business days between the trade date and the expiration date
     * @param days_accrued number of business days since the order has been placed
     * @param remaining_days business days left until expiration
     * @param accrued_volume_units volume of the units that has accrued
     * @param unwind_vega unwind vega
     * @param unwind_volume_units volume of the units being unwound
     * @param timestamp the time when the order was placed
     * @param strike_shift the shift of the strike set by the administrator
     * @param action the action of the order (buy or sell)
     * @return 1 if success, 0 if fail.
     */
    public int insertOrder(
                    String channel_partner_id, String farmer_id, String order_id, String status, java.sql.Date trade_date, 
                    String transaction_type, String underlying_commodity_name, String underlying_futures_code,
                    String client_month, String instrument, String product, String frequency, double executed_strike, 
                    java.sql.Date expiration_date, double product_current_price_per_unit,
                    double execution_product_total_cost, double barrier_level, double digital_payout_per_unit, 
                    double current_underlying_futures_price, double current_implied_volatility,
                    double market_price_per_unit, double delta, double gamma, double vega, double theta, double mark_to_market,
                    double position_profit_loss, 
                    double accrued_contracts, double unwind_accrued_value_paid, double override_option_value_paid, 
                    double unwind_total_volume_paid, String broker_name, String farmer_physical_contract_id, 
                    double executed_hedge, double digital_total_payout, double unwind_option_value_paid,
                    double unwind_value_paid_per_unit, double daily_trader_volatility, double order_volume_quantity, 
                    String unit_type, double order_volume_contracts, int original_number_of_days,
                    int days_accrued, int remaining_days, double accrued_volume_units, 
                    double unwind_vega, double unwind_volume_units, Timestamp timestamp,
                    int strike_shift, String action){
        int result=0;
        try{
            //Replaces question marks in the SQL string that will insert into the database.
            insertOrders.setString(1, channel_partner_id);
            insertOrders.setString(2, farmer_id);
            insertOrders.setString(3, order_id);
            insertOrders.setString(4, status);
            insertOrders.setDate(5, trade_date);
            insertOrders.setString(6, transaction_type);
            insertOrders.setString(7, underlying_commodity_name);
            insertOrders.setString(8, underlying_futures_code);
            insertOrders.setString(9, client_month);
            insertOrders.setString(10, instrument);
            insertOrders.setString(11, product);
            insertOrders.setString(12, frequency);
            insertOrders.setDouble(13, executed_strike);
            insertOrders.setDate(14, expiration_date);
            insertOrders.setDouble(15, product_current_price_per_unit);
            insertOrders.setDouble(16, execution_product_total_cost);
            insertOrders.setDouble(17, barrier_level);
            insertOrders.setDouble(18, digital_payout_per_unit);
            insertOrders.setDouble(19, current_underlying_futures_price);
            insertOrders.setDouble(20, current_implied_volatility);
            insertOrders.setDouble(21, market_price_per_unit);
            insertOrders.setDouble(22, delta);
            insertOrders.setDouble(23, gamma);
            insertOrders.setDouble(24, vega);
            insertOrders.setDouble(25, theta);
            insertOrders.setDouble(26, mark_to_market);
            insertOrders.setDouble(27, position_profit_loss);
            insertOrders.setDouble(28, accrued_contracts);
            insertOrders.setDouble(29, unwind_accrued_value_paid);
            insertOrders.setDouble(30, override_option_value_paid);
            insertOrders.setDouble(31, unwind_total_volume_paid);
            insertOrders.setString(32, broker_name);
            insertOrders.setString(33, farmer_physical_contract_id);
            insertOrders.setDouble(34, executed_hedge);
            insertOrders.setDouble(35, digital_total_payout);
            insertOrders.setDouble(36, unwind_option_value_paid);
            insertOrders.setDouble(37, unwind_value_paid_per_unit);
            insertOrders.setDouble(38, daily_trader_volatility);
            insertOrders.setDouble(39, order_volume_quantity);
            insertOrders.setString(40, unit_type);
            insertOrders.setDouble(41, order_volume_contracts);
            insertOrders.setInt(42, original_number_of_days);
            insertOrders.setInt(43, days_accrued);
            insertOrders.setInt(44, remaining_days);
            insertOrders.setDouble(45, accrued_volume_units);
            insertOrders.setDouble(46, unwind_vega);
            insertOrders.setDouble(47, unwind_volume_units);
            insertOrders.setTimestamp(48, timestamp);
            insertOrders.setInt(49,strike_shift);
            insertOrders.setString(50,action);
            result = insertOrders.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to insert a trade into the database
     * @param transaction_type the type of transaction (hedge or trade)
     * @param channel_partner_id ID for the channel partner
     * @param farmer_id ID for the farm (e.g. JOE1000)
     * @param farmer_physical_contract_id order identification used by channel partner to communicate with partner
     * @param action  the action of the order (buy or sell)
     * @param underlying_futures_code futures code associated with the commodity (e.g. CAH7)
     * @param underlying_commodity_name name of the commodity
     * @param client_month month the order was placed (e.g. January)
     * @param expiration_date the date that the order expires
     * @param order_volume_quantity the number of units in the order
     * @param unit_type type of unit used for the order volume quantity (metric tons or short tons)
     * @param order_volume_contracts number of contracts in the order (order volume units / 50 or 100, depending on the commodity)
     * @param instrument the instrument used to place the order (call or put)
     * @param product region of origin (European)
     * @param frequency frequency of the order (daily or bullet)
     * @param executed_strike the strike at which the order was executed
     * @param product_current_price_per_unit the price calculated using black scholes model
     * @param execution_product_total_cost total cost of the order being placed
     * @param barrier_level barrier level
     * @param digital_payout_per_unit digital payout per unit
     * @param digital_total_payout digital total payout
     * @param order_id ID for the order (e.g. 1000A)
     * @param broker_name name of the broker who facilitated the hedge
     * @param today java.sql.Date with a value corresponding to today's date
     * @param current_underlying_futures_price the current market price of the commodity
     * @param daily_trader_volatility overridden volatility value
     * @param delta delta
     * @param gamma gamma
     * @param vega vega
     * @param theta theta
     * @param timestamp the time when the trade was placed
     * @return 1 if success, 0 if fail.
     */
    public int insertTrade(String transaction_type, String channel_partner_id, String farmer_id, String farmer_physical_contract_id,
                            String action, String underlying_futures_code, String underlying_commodity_name, String client_month,
                            java.sql.Date expiration_date, Double order_volume_quantity, String unit_type, Double order_volume_contracts,
                            String instrument, String product, String frequency, Double executed_strike, Double product_current_price_per_unit,
                            Double execution_product_total_cost, Double barrier_level, Double digital_payout_per_unit,
                            Double digital_total_payout, String order_id, String broker_name, java.sql.Date today, Double current_underlying_futures_price,
                            Double daily_trader_volatility, Double delta, Double gamma, Double vega, Double theta, Timestamp timestamp){
        int result = 0;
        try{
            org.jquantlib.time.Calendar tradingCal;
            //determine which trading calendar to use to count trade days
            if(underlying_commodity_name.equals("Soybean Meal")){
                tradingCal = new UnitedStates();
            }
            else{ 
                tradingCal = new UnitedKingdom();
            }
            int difference = tradingCal.businessDaysBetween(new org.jquantlib.time.Date(today), new org.jquantlib.time.Date(expiration_date), true, true);
            //Replaces question marks in the SQL string that will insert into the database.
            insertOrders.setString(1, channel_partner_id);
            insertOrders.setString(2, farmer_id);
            insertOrders.setString(3, order_id + "O1");
            insertOrders.setString(4, "Original");
            insertOrders.setDate(5, today);
            insertOrders.setString(6, transaction_type);
            insertOrders.setString(7, underlying_commodity_name);
            insertOrders.setString(8, underlying_futures_code);
            insertOrders.setString(9, client_month);
            insertOrders.setString(10, instrument);
            insertOrders.setString(11, product);
            insertOrders.setString(12, frequency);
            insertOrders.setDouble(13, executed_strike);
            insertOrders.setDate(14, expiration_date);
            insertOrders.setDouble(15, product_current_price_per_unit);
            insertOrders.setDouble(16, execution_product_total_cost);
            insertOrders.setDouble(17, barrier_level);
            insertOrders.setDouble(18, digital_payout_per_unit);
            insertOrders.setDouble(19, current_underlying_futures_price);
            insertOrders.setDouble(20, -999);
            insertOrders.setDouble(21, -999);
            insertOrders.setDouble(22, delta);
            insertOrders.setDouble(23, gamma);
            insertOrders.setDouble(24, vega);
            insertOrders.setDouble(25, theta);
            insertOrders.setDouble(26, -999);
            insertOrders.setDouble(27, -999);
            insertOrders.setDouble(28, -999);
            insertOrders.setDouble(29, -999);
            insertOrders.setDouble(30, -999);
            insertOrders.setDouble(31, -999);
            insertOrders.setString(32, broker_name);
            insertOrders.setString(33, farmer_physical_contract_id);
            insertOrders.setDouble(34, -999);
            insertOrders.setDouble(35, digital_total_payout);
            insertOrders.setDouble(36, -999);
            insertOrders.setDouble(37, -999);
            insertOrders.setDouble(38, daily_trader_volatility);
            insertOrders.setDouble(39, order_volume_quantity);
            insertOrders.setString(40, unit_type);
            insertOrders.setDouble(41, order_volume_contracts);
            insertOrders.setInt(42, difference);
            insertOrders.setInt(43, -999);
            insertOrders.setInt(44, -999);
            insertOrders.setDouble(45, -999);
            insertOrders.setDouble(46, -999);
            insertOrders.setDouble(47, -999);
            insertOrders.setTimestamp(48, timestamp);
            insertOrders.setDouble(49, -999);
            insertOrders.setString(50, action);
            result = insertOrders.executeUpdate();
            insertOrders.setString(3, order_id + "A");
            insertOrders.setString(4, "Active");
            result = insertOrders.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to remove an order from the database
     * @param order_number the ID number of the order to be deleted.
     * @return 1 if success, 0 if fail.
     */
    public int deleteOrders(String order_number){
        int result = 0;
        try{
            deleteOrders = connection.prepareStatement("DELETE FROM orders WHERE order_id='" + order_number + "';");
           result = deleteOrders.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to change the ID number of an order
     * @param oldID the current order ID
     * @param newID the new order ID
     * @return 1 if success, 0 if fail.
     */
    public int updateID(String oldID, String newID){
        int result = 0;
        try{
            PreparedStatement updateID = connection.prepareStatement("UPDATE orders"
                + " SET order_id='" + newID + "' WHERE order_id='" + oldID + "';");
            result = updateID.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to update the order ID to match the status of the order
     * @param id the current order ID for the order
     * @param orderStatus the current status of the order
     * @return the new, updated ID for the order passed in
     */
    public String newOrderId(String id, String orderStatus){
        String newId = id.substring(0, id.length()-1);
        
        switch(orderStatus){
            case "Active": 
                newId += "A";
                break;
            case "Original":
                newId += "O1";
                break;
            case "Decline":
                newId += "D";
                break;
            case "Finalized":
                newId += "UF";
                break;
            case "Unwind":
                newId += "U";
                break;
            case "Pending":
                newId += "P";
                break;
            case "Expired":
                newId += "E";
        }
        return newId;
    }
    
    /**
     * This method copies the data from the original order into a new active order
     * @param ID the order ID of the original order
     * @param currentTimestamp the time at which this copy is happening
     * @return 1 if success, 0 if fail.
     */
    public int copyToActive(String ID, Timestamp currentTimestamp){
        int result = 0;
        try{
            ResultSet orderData = connection.prepareStatement("SELECT * FROM orders ORDER BY timestamp ASC;").executeQuery();
            while(orderData.next()){
                String old_order_id = orderData.getString("order_id");
                String new_order_id = old_order_id.substring(0,old_order_id.length()-1) + "A";
                if(orderData.getString("order_id").equals(ID)){
                    //Replace question mark with SQL string that will place the variables.
                    insertOrders.setString(1, orderData.getString("channel_partner_id"));
                    insertOrders.setString(2, orderData.getString("farmer_id"));
                    insertOrders.setString(3, new_order_id);
                    insertOrders.setString(4, "Active");
                    insertOrders.setDate(5, new java.sql.Date(new java.util.Date().getTime()));
                    insertOrders.setString(6, orderData.getString("transaction_type"));
                    insertOrders.setString(7, orderData.getString("underlying_commodity_name"));
                    insertOrders.setString(8, orderData.getString("underlying_futures_code"));
                    insertOrders.setString(9, orderData.getString("client_month"));
                    insertOrders.setString(10, orderData.getString("instrument"));
                    insertOrders.setString(11, orderData.getString("product"));
                    insertOrders.setString(12, orderData.getString("frequency"));
                    insertOrders.setDouble(13, orderData.getDouble("executed_strike"));
                    insertOrders.setDate(14, orderData.getDate("expiration_date"));
                    insertOrders.setDouble(15, orderData.getDouble("product_current_price_per_unit"));
                    insertOrders.setDouble(16, orderData.getDouble("execution_product_total_cost"));
                    insertOrders.setDouble(17, orderData.getDouble("barrier_level"));
                    insertOrders.setDouble(18, orderData.getDouble("digital_payout_per_unit"));
                    insertOrders.setDouble(19, orderData.getDouble("current_underlying_futures_price"));
                    insertOrders.setDouble(20, orderData.getDouble("current_implied_volatility"));
                    insertOrders.setDouble(21, orderData.getDouble("market_price_per_unit"));
                    insertOrders.setDouble(22, orderData.getDouble("delta"));
                    insertOrders.setDouble(23, orderData.getDouble("gamma"));
                    insertOrders.setDouble(24, orderData.getDouble("vega"));
                    insertOrders.setDouble(25, orderData.getDouble("theta"));
                    insertOrders.setDouble(26, orderData.getDouble("mark_to_market"));
                    insertOrders.setDouble(27, 0);
                    insertOrders.setDouble(28, orderData.getDouble("accrued_contracts"));
                    insertOrders.setDouble(29, orderData.getDouble("unwind_accrued_value_paid"));
                    insertOrders.setDouble(30, orderData.getDouble("override_option_value_paid"));
                    insertOrders.setDouble(31, orderData.getDouble("unwind_total_value_paid"));
                    insertOrders.setString(32, orderData.getString("broker_name"));
                    insertOrders.setString(33, orderData.getString("farmer_physical_contract_id"));
                    insertOrders.setDouble(34, orderData.getDouble("executed_hedge"));
                    insertOrders.setDouble(35, orderData.getDouble("digital_total_payout"));
                    insertOrders.setDouble(36, orderData.getDouble("unwind_option_value_paid"));
                    insertOrders.setDouble(37, orderData.getDouble("unwind_value_paid_per_unit"));
                    insertOrders.setDouble(38, orderData.getDouble("daily_trader_volatility"));
                    insertOrders.setDouble(39, orderData.getDouble("order_volume_quantity"));
                    insertOrders.setString(40, orderData.getString("unit_type"));
                    insertOrders.setDouble(41, orderData.getDouble("order_volume_contracts"));
                    insertOrders.setInt(42, orderData.getInt("original_number_of_days"));
                    insertOrders.setInt(43, orderData.getInt("days_accrued"));
                    insertOrders.setInt(44, orderData.getInt("remaining_days"));
                    insertOrders.setDouble(45, orderData.getDouble("accrued_volume_units"));
                    insertOrders.setDouble(46, orderData.getDouble("unwind_vega"));
                    insertOrders.setDouble(47, orderData.getDouble("unwind_volume_units"));
                    insertOrders.setTimestamp(48, currentTimestamp);
                    insertOrders.setInt(49, orderData.getInt("strike_shift"));
                    insertOrders.setString(50, orderData.getString("action"));
                    result = insertOrders.executeUpdate();
                }
            }
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to update the status of an order
     * @param ID the order ID of the order that you wish to update
     * @param newStatus the new status of the order
     * @return 1 if success, 0 if fail.
     */
    public int updateStatus(String ID, String newStatus){
        int result = 0;
        try{
            PreparedStatement updateID = connection.prepareStatement("UPDATE orders"
                + " SET status='" + newStatus + "' WHERE order_id='" + ID + "';");
            result = updateID.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to update the trade date to today's date
     * @param ID order ID of the order that you wish to update
     * @return 1 if success, 0 if fail.
     */
    public int updateTradeDate(String ID){
        int result = 0;
        try{
            PreparedStatement updateID = connection.prepareStatement("UPDATE orders"
                + " SET trade_date='" + new java.sql.Date(new java.util.Date().getTime()).toString() + "' WHERE order_id='" + ID + "';");
            result = updateID.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * To be used with the new accrued values for delta vega gamma and theta calculated daily or in the accrual table.
     * @param ID order ID of the order that you wish to update
     * @param delta new value for delta
     * @param gamma new value for gamma
     * @param theta new value for theta
     * @param vega new value for vega
     * @return 1 if success, 0 if fail.
     */
    public int updateBlackScholes(String ID, double delta, double gamma, double theta, double vega){
        int result = 0;
        try{
            PreparedStatement updateID = connection.prepareStatement("UPDATE orders"
                + " SET delta='" + delta + "', gamma='" + gamma + "', vega='" + vega + "', theta='" + theta + "' WHERE order_id='" + ID + "';");
            result = updateID.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to unwind an order and put in a replacement order
     * @param ID order ID of the order you wish to unwind
     * @param unwind_volume_units total volume of the order
     * @param unwind_product_volatility volatiliy of the order to be unwound
     * @param unwind_current_price_per_unit the black scholes calculation of the current price per unit for the unwind
     * @param unwind_accrued_value_paid unwind accrued value paid
     * @param unwind_option_value_paid unwind option value paid
     * @param override_option_value_paid overridden option value paid
     * @param unwind_total_value_paid unwind total value paid
     * @param order_volume_units number of units going on the replacement order, can not exceed the unwind volume units
     * @param order_volume_contracts number of contracts going on the replacement order
     * @param product_volatility interpolated volatility plus volatility spread
     * @param product_fair_value_per_unit black scholes caluclated price per unit
     * @param current_price_per_unit product fair value per unit plus the markup
     * @param current_underlying_futures_price the current market price of the commodity
     * @param execution_product_total_cost cost of the order being placed
     * @param daily_trader_volatility cost of the order being placed
     * @param replacementDelta delta for the replacement order
     * @param replacementGamma gamma for the replacement order
     * @param replacementVega vega for the replacement order
     * @param replacementTheta theta for the replacement order
     * @param currentTimestamp time that the unwind occurred
     * @return 1 if success, 0 if fail.
     */
    public int unwind(
            //The first values are from the unwind table with the exception of underlying futures price
            String ID, 
            double unwind_volume_units,
            double unwind_product_volatility,
            double unwind_current_price_per_unit,
            double unwind_accrued_value_paid,
            double unwind_option_value_paid,
            double override_option_value_paid,
            double unwind_total_value_paid, 
            //These values are from the replacement table
            double order_volume_units,
            double order_volume_contracts, 
            double product_volatility,
            double product_fair_value_per_unit, 
            double current_price_per_unit,
            double current_underlying_futures_price,
            double execution_product_total_cost,
            double daily_trader_volatility,
            double replacementDelta,
            double replacementGamma, 
            double replacementVega, 
            double replacementTheta,
            Timestamp currentTimestamp){
        int result = 0;
        try{
            ResultSet orders = getOrders();
            int count = 1;
            int currentRow = 0;
            //This determines what number to put after the 'U' in the order ID
            while(orders.next()){
                String truncatedID = ID.substring(0,ID.length()-1);
                if(orders.getString("order_id").contains(truncatedID + "U")){
                    count++;
                }
            } 
            orders.beforeFirst();
            String status = "Unwind";
            while(orders.next()){
                if(orders.getString("order_id").equals(ID)){
                    currentRow = orders.getRow();
                    String old_order_id = orders.getString("order_id");
                    String new_order_id = "Error";
                    if(order_volume_units <= 0){
                        new_order_id = old_order_id.substring(0,old_order_id.length()-1) + "UF";
                        status = "Finalized";
                    } else{
                        new_order_id = old_order_id.substring(0,old_order_id.length()-1) + "U" + count;
                    }
                    Double unwindContracts = 0.0;
                    if(orders.getString("unit_type").equals("Short Tons"))
                        unwindContracts = unwind_volume_units / 100;
                    else if(orders.getString("unit_type").equals("Metric Tons"))
                        unwindContracts = unwind_volume_units / 50;
                    //*****INSERT NEW UNWIND ORDER*****//
                    insertOrders.setString(1, orders.getString("channel_partner_id"));
                    insertOrders.setString(2, orders.getString("farmer_id"));
                    insertOrders.setString(3, new_order_id);
                    insertOrders.setString(4, status);
                    insertOrders.setDate(5, orders.getDate("trade_date"));
                    insertOrders.setString(6, orders.getString("transaction_type"));
                    insertOrders.setString(7, orders.getString("underlying_commodity_name"));
                    insertOrders.setString(8, orders.getString("underlying_futures_code"));
                    insertOrders.setString(9, orders.getString("client_month"));
                    insertOrders.setString(10, orders.getString("instrument"));
                    insertOrders.setString(11, orders.getString("product"));
                    insertOrders.setString(12, orders.getString("frequency"));
                    insertOrders.setDouble(13, orders.getDouble("executed_strike"));
                    insertOrders.setDate(14, orders.getDate("expiration_date"));
                    insertOrders.setDouble(15, unwind_current_price_per_unit);
                    insertOrders.setDouble(16, unwind_total_value_paid);
                    insertOrders.setDouble(17, orders.getDouble("barrier_level"));
                    insertOrders.setDouble(18, orders.getDouble("digital_payout_per_unit"));
                    insertOrders.setDouble(19, orders.getDouble("current_underlying_futures_price"));
                    insertOrders.setDouble(20, orders.getDouble("current_implied_volatility"));
                    insertOrders.setDouble(21, orders.getDouble("market_price_per_unit"));
                    insertOrders.setDouble(22, orders.getDouble("delta"));
                    insertOrders.setDouble(23, orders.getDouble("gamma"));
                    insertOrders.setDouble(24, orders.getDouble("vega"));
                    insertOrders.setDouble(25, orders.getDouble("theta"));
                    insertOrders.setDouble(26, orders.getDouble("mark_to_market"));
                    insertOrders.setDouble(27, 0);
                    insertOrders.setDouble(28, orders.getDouble("accrued_contracts"));
                    insertOrders.setDouble(29, unwind_accrued_value_paid);
                    insertOrders.setDouble(30, override_option_value_paid);
                    insertOrders.setDouble(31, unwind_total_value_paid);
                    insertOrders.setString(32, orders.getString("broker_name"));
                    insertOrders.setString(33, orders.getString("farmer_physical_contract_id"));
                    insertOrders.setDouble(34, orders.getDouble("executed_hedge"));
                    insertOrders.setDouble(35, orders.getDouble("digital_total_payout"));
                    insertOrders.setDouble(36, unwind_option_value_paid);
                    insertOrders.setDouble(37, unwind_current_price_per_unit);
                    insertOrders.setDouble(38, unwind_product_volatility);
                    insertOrders.setDouble(39, unwind_volume_units);
                    insertOrders.setString(40, orders.getString("unit_type"));
                    insertOrders.setDouble(41, unwindContracts);
                    insertOrders.setInt(42, orders.getInt("original_number_of_days"));
                    insertOrders.setInt(43, orders.getInt("days_accrued"));
                    insertOrders.setInt(44, orders.getInt("remaining_days"));
                    insertOrders.setDouble(45, orders.getDouble("accrued_volume_units"));
                    insertOrders.setDouble(46, orders.getDouble("unwind_vega"));
                    insertOrders.setDouble(47, unwind_volume_units);
                    insertOrders.setTimestamp(48, currentTimestamp);
                    insertOrders.setInt(49, orders.getInt("strike_shift"));
                    insertOrders.setString(50, orders.getString("action"));
                    insertOrders.executeUpdate();
                }
            } //*****UPDATE ACTIVE ORDER*****//
            if(order_volume_units > 0){ 
                PreparedStatement unwind = connection.prepareStatement("UPDATE orders"
                        + " SET unwind_accrued_value_paid='" + unwind_accrued_value_paid + "',"
                        + " unwind_total_value_paid='" + unwind_total_value_paid + "',"
                        + " order_volume_quantity='" + order_volume_units + "',"
                        + " order_volume_contracts='" + order_volume_contracts +"',"
                        + " current_implied_volatility='" + product_volatility + "',"
                        + " daily_trader_volatility='" + daily_trader_volatility + "',"
                        + " product_current_price_per_unit='" + current_price_per_unit + "',"
                        + " execution_product_total_cost='" + execution_product_total_cost + "',"
                        + " delta='" + replacementDelta + "',"
                        + " gamma='" + replacementGamma + "',"
                        + " vega='" + replacementVega + "',"
                        + " theta='" + replacementTheta + "'"
                        + " WHERE order_id='" + ID + "';" );
                result = unwind.executeUpdate();
                //move to proper row in database
                orders.absolute(currentRow);
                //******INSERT NEW ORIGINAL******//
                insertOrders.setString(1, orders.getString("channel_partner_id"));
                insertOrders.setString(2, orders.getString("farmer_id"));
                insertOrders.setString(3, ID.substring(0,ID.length() -1) + "O" + (count + 1));
                insertOrders.setString(4, "Original");
                insertOrders.setDate(5, orders.getDate("trade_date"));
                insertOrders.setString(6, orders.getString("transaction_type"));
                insertOrders.setString(7, orders.getString("underlying_commodity_name"));
                insertOrders.setString(8, orders.getString("underlying_futures_code"));
                insertOrders.setString(9, orders.getString("client_month"));
                insertOrders.setString(10, orders.getString("instrument"));
                insertOrders.setString(11, orders.getString("product"));
                insertOrders.setString(12, orders.getString("frequency"));
                insertOrders.setDouble(13, orders.getDouble("executed_strike"));
                insertOrders.setDate(14, orders.getDate("expiration_date"));
                insertOrders.setDouble(15, current_price_per_unit);
                insertOrders.setDouble(16, execution_product_total_cost);
                insertOrders.setDouble(17, orders.getDouble("barrier_level"));
                insertOrders.setDouble(18, orders.getDouble("digital_payout_per_unit"));
                insertOrders.setDouble(19, orders.getDouble("current_underlying_futures_price"));
                insertOrders.setDouble(20, product_volatility);
                insertOrders.setDouble(21, orders.getDouble("market_price_per_unit"));
                insertOrders.setDouble(22, replacementDelta);
                insertOrders.setDouble(23, replacementGamma);
                insertOrders.setDouble(24, replacementVega);
                insertOrders.setDouble(25, replacementTheta);
                insertOrders.setDouble(26, orders.getDouble("mark_to_market"));
                insertOrders.setDouble(27, 0);
                insertOrders.setDouble(28, orders.getDouble("accrued_contracts"));
                insertOrders.setDouble(29, unwind_accrued_value_paid);
                insertOrders.setDouble(30, orders.getDouble("override_option_value_paid"));
                insertOrders.setDouble(31, unwind_total_value_paid);
                insertOrders.setString(32, orders.getString("broker_name"));
                insertOrders.setString(33, orders.getString("farmer_physical_contract_id"));
                insertOrders.setDouble(34, orders.getDouble("executed_hedge"));
                insertOrders.setDouble(35, orders.getDouble("digital_total_payout"));
                insertOrders.setDouble(36, orders.getDouble("unwind_option_value_paid"));
                insertOrders.setDouble(37, orders.getDouble("unwind_value_paid_per_unit"));
                insertOrders.setDouble(38, daily_trader_volatility);
                insertOrders.setDouble(39, order_volume_units);
                insertOrders.setString(40, orders.getString("unit_type"));
                insertOrders.setDouble(41, order_volume_contracts);
                insertOrders.setInt(42, orders.getInt("original_number_of_days"));
                insertOrders.setInt(43, orders.getInt("days_accrued"));
                insertOrders.setInt(44, orders.getInt("remaining_days"));
                insertOrders.setDouble(45, orders.getDouble("accrued_volume_units"));
                insertOrders.setDouble(46, orders.getDouble("unwind_vega"));
                insertOrders.setDouble(47, orders.getDouble("unwind_volume_units"));
                insertOrders.setTimestamp(48, currentTimestamp);
                insertOrders.setInt(49, orders.getInt("strike_shift"));
                insertOrders.setString(50, orders.getString("action"));
                insertOrders.executeUpdate();
            }else{
                //this deletes the active if nothing was put onto the replacement order
                deleteOrders(ID);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Used to get a ReseultSet of orders that are expiring today
     * @return ResultSet of orders expiring today
     */
    public ResultSet getExpiredOrders(){
        ResultSet resultSet = null;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        org.jquantlib.time.Date today = new org.jquantlib.time.Date(new java.util.Date()).add(1);
        try{
            PreparedStatement getExpiredOrders = connection.prepareStatement(
                    "SELECT * FROM orders WHERE expiration_date = '" + df.format(today.shortDate()) + "' "
                            + "AND status = 'active';");
            resultSet = getExpiredOrders.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Used to get the orders that are expiring in the next five business days according to the United Kingdom business calendar
     * @return a ResultSet of orders expiring within the next five business days
     */
    public ResultSet getOrdersExpiringSoon(){
        ResultSet resultSet = null;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        org.jquantlib.time.Calendar cal = new UnitedKingdom();
        org.jquantlib.time.Date today = new org.jquantlib.time.Date(new java.util.Date()).add(1);
        org.jquantlib.time.Date soon = new org.jquantlib.time.Date();
        soon = cal.advance(today, 5, TimeUnit.Days).dec();
        try{
            PreparedStatement getOrdersExpiringSoon = connection.prepareStatement(
                    "SELECT * FROM orders WHERE expiration_date > '" + df.format(today.shortDate()) + "'"
                            + " and expiration_date <= '" + df.format(soon.shortDate()) + "'"
                            + " and status = 'active'"
                            + ";");
            resultSet = getOrdersExpiringSoon.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultSet;
    }
    
    /**
     * Used to set the executed hedge and strike after the order has been accepted
     * @param ID the order ID of the order that you wish to update
     * @param executedHedge executed strike plus strike shift
     * @param executedStrike the strike at which the order was executed
     * @return 1 if success, 0 if fail.
     */
    public int setExecutedHedgeAndStrike(String ID, double executedHedge, double executedStrike){
        int result = 0;
        try{
            PreparedStatement setHedge = connection.prepareStatement("UPDATE orders"
            + " SET executed_hedge='" + executedHedge + "', executed_strike='" + executedStrike + "' WHERE order_id='" + ID + "';");
            result = setHedge.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * Generates the next order ID based on how many orders are already in the database. Used for new orders.
     * @return a string of the next order ID in the sequence
     */
    public String generateOrderID(){
        int id = 1000;
        try{
            resultSet = getOrders.executeQuery();
            while(resultSet.next()){
                    id++;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        String ID = new Integer(id).toString() + "P";
        return ID;   
    }
    
    /**
     * Generates the next order ID based on how many orders are already in the database. Used for new trades and hedges.
     * @return a string of the next order ID in the sequence
     */
    public String generateTradeID(){
        int id = 1000;
        try{
            resultSet = getOrders.executeQuery();
            while(resultSet.next()){
                    id++;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        String ID = Integer.toString(id);
        return ID;   
    }
    
    /**
     * Used to generate the correct year for a particular month. e.g. If the current month is
     * March and the month of February is passed in, the method will return next year.
     * @param theMonth integer representing the month of the year that is being evaluated (1-12)
     * @return the year that should go with the month that was passed in
     */
    public static int getProperYear(int theMonth){
        Calendar today = Calendar.getInstance();
        int year = today.get(Calendar.YEAR);
        int currentMonth = today.get(Calendar.MONTH);
        if(currentMonth >= theMonth)
            return (year + 1);
        else
            return year;
    }
    
    /**
     * Used to generate the futures code for a commodity
     * @param theMonth integer representing a month of the year
     * @param underlying_commodity_name name of the commodity
     * @return the underlying futures code that is associated with that commodity in that month
     */
    public static String getFuturesCode(int theMonth, String underlying_commodity_name){
	String returnString = "";
        switch (underlying_commodity_name) {
            case "Soybean Meal":
                returnString += "SM";
                if(theMonth == 1 || theMonth == 2){
                    returnString += "H";
                }else if(theMonth == 3 || theMonth == 4){
                    returnString += "K";
                }else if(theMonth == 5 || theMonth == 6){
                    returnString += "N";
                }else if(theMonth == 7){
                    returnString += "Q";
                }else if(theMonth == 8){
                    returnString += "U";
                }else if(theMonth == 9){
                    returnString += "V";
                }else if(theMonth == 10 || theMonth == 11){
                    returnString += "Z";
                }else if(theMonth == 12){
                    returnString += "F";
                }
                int year = getProperYear(theMonth);
                String yearString = Integer.toString(year);
                returnString += yearString.substring(yearString.length()-1);
                break;
            case "Euronext Rapeseed":
                returnString += "IJ";
                if(theMonth == 1 || theMonth == 12){
                    returnString += "H";
                }else if(theMonth == 3 || theMonth == 4 || theMonth == 2){
                    returnString += "M";
                }else if(theMonth == 5 || theMonth == 6){
                    returnString += "Q";
                }else if(theMonth == 9 || theMonth == 8 || theMonth == 7){
                    returnString += "X";
                }else if(theMonth == 10 || theMonth == 11){
                    returnString += "F";
                }
                year = getProperYear(theMonth);
                yearString = Integer.toString(year);
                returnString += yearString.substring(yearString.length()-1);
                break;
            case "Euronext Wheat":
                returnString += "CA";
                if(theMonth == 1 || theMonth == 12 || theMonth == 11){
                    returnString += "H";
                }else if(theMonth == 3 || theMonth == 2){
                    returnString += "K";
                }else if(theMonth == 5 || theMonth == 6 || theMonth == 4 || theMonth == 7){
                    returnString += "U";
                }else if(theMonth == 9 || theMonth == 8 || theMonth == 10){
                    returnString += "Z";
                }
                year = getProperYear(theMonth);
                yearString = Integer.toString(year);
                returnString += yearString.substring(yearString.length()-1);
                break;
            default:
                break;
        }
    return returnString;
    }
    
    /**
     * Used to modify the database from the interface of the website. Can not be used to request data
     * @param mySQLCode code written in SQL to modify the database
     */
    public void modifyDatabase(String mySQLCode){
        try{
            PreparedStatement modifyDatabase = connection.prepareStatement(mySQLCode);
            modifyDatabase.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Used to close the connection to the database after a page is done using it
     */
    public void closeConnection(){
        try{
            connection.close();
        }catch(Exception e){
            //nothing
        }
    }
}