/**
 * The expiration date servlet serves the purpose of generating an expiration date for an order based upon the underlying commodity and client month.
 * Requires multiple parameters to be passed in through HttpServletRequest.
 * @version June 2017
 * @author Dutchess Developers
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dutchessdevelopers.commoditieswebsite.Orders;
import com.dutchessdevelopers.commoditieswebsite.PulledData;
import java.sql.ResultSet;
import org.jquantlib.time.calendars.UnitedKingdom;
import org.jquantlib.time.calendars.UnitedStates;
import org.jquantlib.time.Date;
import org.jquantlib.time.Period;

@WebServlet("/ExpirationDateServlet")
public class ExpirationDateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
        /**
         * The purpose of the doGet method in this servlet is to take in the client month and commodity and return the expiration date.
         * Parameters: <br>
         * clientMonth - the month in which the order will expire (based on underlying futures code) <br>
         * underlyingCommodity - the name of the underlying commodity (e.g. Euronext Wheat) <br>
         * Once recieved, the method uses these parameters to come up with the expiration date. 
         * The expiration date will be the last business day of the month always for Euronext Wheat and Euronext Rapeseed, and some Soybean futures. 
         * The expiration date will be the 3rd Friday of the months for the other Soybean futures.
         * @param request The HttpServletRequest
         * @param response The HttpServletResponse
         */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                /*Retrieve parameter values from HttpServletRequest*/
                int month = Integer.parseInt(request.getParameter("clientMonth"));
                String commodity = request.getParameter("underlyingCommodity");
                String responseDate = ""; //Variable to be returned in HttpServletResponse
                
                //Determine date for Wheat and Rapeseed (Last business day)
                if(commodity.equals("EuronextWheat") || commodity.equals("EuronextRapeseed")){
                    int year = Orders.getProperYear(month);
                    UnitedKingdom cal = new UnitedKingdom();
                    Date date = new Date(25,month,year);
                    while(!cal.isEndOfMonth(date)){ //While loop -- Goes to end of month.
                        date = cal.advance(date, Period.ONE_DAY_FORWARD);
                    }
                    responseDate = new java.sql.Date(date.shortDate().getTime() + 24*60*60*1000).toString();
                }
                
                //Determine date for Soybean Meal
                else if(commodity.equals("SoybeanMeal")){
                    //Last business day for months 1,3,5,10
                    if(month == 1 || month == 3 || month == 5 || month == 10){
                        int year = Orders.getProperYear(month);
                        UnitedStates cal = new UnitedStates();
                        Date date = new Date(25,month,year);
                        while(!cal.isEndOfMonth(date)){
                            date = cal.advance(date, Period.ONE_DAY_FORWARD);
                        }
                        responseDate = new java.sql.Date(date.shortDate().getTime() + 24*60*60*1000).toString();
                    }
                    //3rd Friday for all other months
                    else{
                        int year = Orders.getProperYear(month);
                        UnitedStates cal = new UnitedStates();
                        Date date = new Date(1,month,year);
                        int friCount = 0;
                        while(friCount < 3){
                            if(date.weekday().toString().toLowerCase().equals("friday")){
                                friCount++;
                            }
                            date = cal.advance(date, Period.ONE_DAY_FORWARD);
                        }
                        date = cal.advance(date, Period.ONE_DAY_BACKWARD);
                        responseDate = new java.sql.Date(date.shortDate().getTime() + 24*60*60*1000).toString();
                    }
                }
                response.setContentType("text/plain");
		response.getWriter().write(responseDate);
        }
}