/**
 * The Check Box Servlet serves the purpose of using HTTP GET requests to check or uncheck a single check box in the AdminManageBook.jsp page.
 * The box is checked if its attribute is set to true, and unchecked if its attribute is set to false.
 * This operation is necessary through the servlet to prevent boxes from being checked automatically upon loading the page.
 * @version June 2017
 * @author Dutchess Developers
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/CheckBoxServlet")
public class CheckBoxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
        /**
         * When the doGet method of the CheckAllServlet is called, it sets the attribute of the item called to the value retrieved. <br>
         * Requested Parameters: <br>
         * item - the name of the column heading which the check box represents
         * value - true (check check box) or false (uncheck check box)
         * @param request The HttpServletRequest
         * @param response The HttpServletResponse
         */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            //Retrieve parapeters from HttpServletRequest
            String id = request.getParameter("item");
            String value = request.getParameter("value");
            request.getSession().setAttribute(id,value); // Set attribute to check or uncheck box
            response.setContentType("text/plain");
            response.getWriter().write(request.getSession().getAttribute(id).toString());
        }
}