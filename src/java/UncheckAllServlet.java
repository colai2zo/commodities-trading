/**
 * The Uncheck All Servlet serves the purpose of using HTTP GET requests to uncheck all boxes in the AdminManageBook.jsp page.
 * The boxes are checked if their attribute is set to true, and unchecked if their attribute is set to false.
 * This operation is necessary through the servlet to prevent boxes from being checked automatically upon loading the page.
 * @version June 2017
 * @author Dutchess Developers
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UncheckAllServlet")
public class UncheckAllServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        /**
         * When the doGet method of the UncheckAllServlet is called, it sets the attributes of ALL check boxes to false. <br>
         * There are no additionally requested parameters required when calling this method.
         * @param request The HttpServletRequest
         * @param response The HttpServletResponse
         */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            HttpSession session = request.getSession();
            session.setAttribute("channelPartner", "false");
            session.setAttribute("farmID", "false");
            session.setAttribute("orderNumber", "false");
            session.setAttribute("status", "false");
            session.setAttribute("tradeDate", "false");
            session.setAttribute("transactionType", "false");
            session.setAttribute("commodityName", "false");
            session.setAttribute("futuresCode", "false");
            session.setAttribute("clientMonth", "false");
            session.setAttribute("expirationDate", "false");
            session.setAttribute("instrument", "false");
            session.setAttribute("product", "false");
            session.setAttribute("frequency", "false");
            session.setAttribute("orderVolumeUnits", "false");
            session.setAttribute("unitType", "false");
            session.setAttribute("orderVolumeContracts", "false");
            session.setAttribute("executedStrike", "false");
            session.setAttribute("executedPricePerUnit", "false");
            session.setAttribute("executedTotalCost", "false");
            session.setAttribute("futuresPrice", "false");
            session.setAttribute("volatility", "false");
            session.setAttribute("delta", "false");
            session.setAttribute("gamma", "false");
            session.setAttribute("vega", "false");
            session.setAttribute("theta", "false");
            session.setAttribute("broker", "false");
            session.setAttribute("contractID", "false");
            response.setContentType("text/plain");
            response.getWriter().write("Completed Uncheck.");
        }
}