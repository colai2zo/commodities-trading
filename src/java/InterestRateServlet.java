/**
 * The Interest Rate Servlet serves the purpose of generating an interest rate for an order based upon the underlying futures code and the expiration date.
 * Requires multiple parameters to be passed in through HttpServletRequest.
 * @version June 2017
 * @author Dutchess Developers
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dutchessdevelopers.commoditieswebsite.BlackScholes;
import com.dutchessdevelopers.commoditieswebsite.PulledData;
import java.sql.ResultSet;
import org.jquantlib.time.calendars.UnitedKingdom;
import org.jquantlib.time.calendars.UnitedKingdom;

@WebServlet("/InterestRateServlet")
public class InterestRateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
        /**
         * The main purpose of the doGet method is to utilize a GET request to return the interest rate for a futures code in a given time frame. <br>
         * Parameters: <br>
         * futuresCode - the underlying futures code of the commodity being traded in the order (e.g. CAH7) <br>
         * option - The instrument used to place the order (call or put) <br>
         * expiry - the expiration date of the order, given in the form yyyy-mm-dd <br>
         * The method takes in the parameters using an HttpServletRequest, then uses them to find out values for 30, 90, 180, and 360 day rates.
         * It then returns an interpolated interest rate based on the days until expiration.
         * @param request, the HttpServletRequest
         * @param response, the HttpServletResponse
         */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                //Retrieve parameter values.
                String code = request.getParameter("futuresCode");
                String option = request.getParameter("option").toLowerCase();
                String escapeDate = request.getParameter("expiry");
                
                //Instantiate new trading calendar and choose correct calendar based upon instrument.
                org.jquantlib.time.Calendar tradingCal = null;
                if(option.toLowerCase().equals("call")){
                    tradingCal = new org.jquantlib.time.calendars.UnitedKingdom();
                }if(option.toLowerCase().equals("put")){
                    tradingCal = new org.jquantlib.time.calendars.UnitedStates();
                }
                
                //Use trading calendar to determine days between now and expiration date.
                int days = tradingCal.businessDaysBetween(new org.jquantlib.time.Date(new java.util.Date()), new org.jquantlib.time.Date(java.sql.Date.valueOf(escapeDate)), true, true);
                //Retrieve Bloomberg interest rate data.
                PulledData data = new PulledData();
                ResultSet int_rate_data = data.getTodaysDataByCode(code);
                double thirty=0, ninety=0, one_eighty=0, three_sixty=0;
                try{
                    int_rate_data.first();
                    thirty = int_rate_data.getDouble("interest_30_day");
                    ninety = int_rate_data.getDouble("interest_90_day");
                    one_eighty = int_rate_data.getDouble("interest_180_day");
                    three_sixty = int_rate_data.getDouble("interest_360_day");
                }catch(java.sql.SQLException e){
                    e.printStackTrace();
                }
                
                double intRate = BlackScholes.calcCurrentInterestRate(thirty, ninety, one_eighty, three_sixty, days); // Use Black Scholes method to determine interpolated interest rate.
                
		response.setContentType("text/plain");
		response.getWriter().write(new Double(intRate).toString());
	}
}