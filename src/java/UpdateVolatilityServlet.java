/**
 * The Update Volatility Servlet serves the purpose of updating the Daily Trader Volatility for a particular futures code.
 * The servlet requires the passing in of the underlying futures code and updated volatility. 
 * It will then update the database accordingly.
 * @version June 2017
 * @author Dutchess Developers
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dutchessdevelopers.commoditieswebsite.BlackScholes;
import com.dutchessdevelopers.commoditieswebsite.PulledData;
import java.sql.ResultSet;
import org.jquantlib.time.calendars.UnitedKingdom;
import org.jquantlib.time.calendars.UnitedKingdom;

@WebServlet("/UpdateVolatilityServlet")
public class UpdateVolatilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
        /**
         * The function of the doGet method is the use an HttpRequest to retrieve the code and volatility, and update the database accordingly. <br>
         * Parameters: <br>
         * code - the underlying futures code of the commodity being traded in the order (e.g. CAH7) <br>
         * volatility - the daily trader volatility assigned to the order. <br>
         * The method then utilizes the updateDailyTraderVolatility() method of the PulledData class to update the volatility value.
         */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                //Retrieve parameters using HttpServletRequest
                String code = request.getParameter("code");
                double volatility = Double.parseDouble(request.getParameter("volatility"));
		
                //Instantiate PulledData and use it to update volatility in database given parameters.
                PulledData data = new PulledData();
                data.updateDailyTraderVolatility(volatility, code);
                
                response.setContentType("text/plain");
		response.getWriter().write(new Double(volatility).toString());
	}
}