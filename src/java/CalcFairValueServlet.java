/**
 * The Calc Fair Value Servlet serves the purpose of running  a black scholes calculation for Market Price / Unit through HTTP GET requests.
 * Requires several parameters that are passed into the com.dutchessdevelopers.commoditieiswebsite.BlackScholes.java class.
 * @version June 2017
 * @author Dutchess Developers
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dutchessdevelopers.commoditieswebsite.BlackScholes;
import com.dutchessdevelopers.commoditieswebsite.BlackScholesDailyAccumulator;
import org.jquantlib.time.Date;
import org.jquantlib.time.Date;
import org.jquantlib.time.Date;
import org.jquantlib.time.Date;
import java.text.NumberFormat;

@WebServlet("/CalcFairValueServlet")
public class CalcFairValueServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
        /**
         * The doGet method of the CalcFairValueServlet receives parameters from an HttpServletRequest and returns a double for the value of the product current price per unit.
         * Requested Parameters: <br>
         * option - The instrument used to place the order (call or put) <br>
         * trade_date - The date on which the order was placed, given in the form yyyy-mm-dd <br>
         * expiration_date - the expiration date of the order, given in the form yyyy-mm-dd <br>
         * frequency - the frequency of the order (daily or bullet) <br>
         * underlying_futures_code - the underlying futures code of the commodity being traded in the order (e.g. CAH7)<br>
         * futures_price - the current underlying futures price of the commodity being traded in the order.<br>
         * executed_strike - the executed strike of the order<br>
         * interest_rate - the interest rate assigned to the order<br>
         * dividend_yield - the dividend yield assigned to the order<br>
         * daily_trader_volatility - the daily trader volatility assigned to the order.<br>
         * action - the action performed by the transaction (buy or sell).<br>
         * order_volume_quantity -  number of order volume units contained within the order
         * days - number of business days between now and the expiration date
         * The method takes each of these values and plugs them into the black scholes class (bullet orders) or black scholes daily accumulator class (daily orders).
         * The method is called for the requested parameter and the response writer writes the method's response.
         * @param request, the HttpServletRequest that calls parameters passed in
         * @param response, the HttpServletResponse that sends back the Black Scholes Calculated value.
         */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                NumberFormat formatter = NumberFormat.getInstance(); //Create instance of number formatter
                formatter.setMaximumFractionDigits(2); //Must have 2 decimal places.
                formatter.setMinimumFractionDigits(2);
                
                /* Retreive Parameters from Http Request using request.getParameter() method */
		String option = request.getParameter("option"); 
                double price = Double.parseDouble(request.getParameter("futures_price"));
                double strike = Double.parseDouble(request.getParameter("executed_strike"));
                double interest_rate = Double.parseDouble(request.getParameter("interest_rate"));
                double dividend_yield = Double.parseDouble(request.getParameter("dividend_yield"));
                double volatility = Double.parseDouble(request.getParameter("daily_trader_volatility"));
                int days = Integer.parseInt(request.getParameter("days"));  
                String frequency = request.getParameter("frequency"); // daily or bullet
                double fairValue = 0; //Value that gets returned to user in HttpServletResponse.
                
                /* BULLET CALCULATION
                   * Performs the black scholes calculation for bullet using parameter values. 
                   * Standard black scholes class calculation.
                 */
                if(frequency.equals("bullet")){
                    BlackScholes bs = new BlackScholes(option, price, strike, interest_rate, dividend_yield, volatility, days); //Instantiate Black Scholes with parameter values
                    fairValue = bs.getMarketPricePerUnit();
                }
                else if(frequency.equals("daily")){
                    //Retrieve additional parameters required specifically for daily operations
                    String code = request.getParameter("underlying_futures_code");
                    String tradeDate = request.getParameter("trade_date");
                    String expiration = request.getParameter("expiration_date");
                    String action = request.getParameter("action");
                    double quantity = Double.parseDouble(request.getParameter("order_volume_quantity"));
                    BlackScholesDailyAccumulator bsda = new BlackScholesDailyAccumulator(option, price, strike, code, dividend_yield, volatility, tradeDate, expiration, action, quantity , days ); //Instantiate Black Scholes Daily Accumulator with parameter values.
                    fairValue = bsda.getFairValueAvg();
                }
		response.setContentType("text/plain");
		response.getWriter().write(formatter.format(fairValue));
	}
}
