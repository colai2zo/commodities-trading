/**Class to allow access to the bloomberg database
 * IMPLEMENTATION: Every time a new set of data is needed, call this class's default constructor. 
 *		Any time a specific piece of data is needed, call the accessor for that piece of data.
 */
package bloombergpull;
import com.bloomberglp.blpapi.*; 
import java.sql.*;
import java.util.*;
 
public class RequestResponse {
	private String URL;
	private String USERNAME;
	private String PASSWORD;	
        public static HashMap<String, ArrayList> dataMap;
	private static String underlyingFuturesCode;
        private static Connection connection;
//CONSTRUCTORS
public RequestResponse(){
    //System.out.println("Creating request/response... ");
    URL = "jdbc:mysql://localhost:3306/commodities_trading";
    USERNAME = "root";
    PASSWORD = "1234";
    
    dataMap = new HashMap();
    
    try{
        connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
    } catch (SQLException E){
        E.printStackTrace();
    }
}


//ACCESSORS
public ArrayList getData(String currentFuturesCode){
    //System.out.println("Retrieving data for " + currentFuturesCode);
    try{
        run(currentFuturesCode);
    } catch (Exception E){
        E.printStackTrace();
    }
    return dataMap.get(currentFuturesCode);
}

//BLOOMBERG CODE
public static void run(String currentFuturesCode) throws Exception { 
	underlyingFuturesCode = currentFuturesCode;
        SessionOptions sessionOptions = new SessionOptions(); 
	sessionOptions.setServerHost("localhost"); 
	sessionOptions.setServerPort(8194);
	Session session = new Session(sessionOptions);
	
	if (!session.start()) {
		System.out.println("Could not start session."); 
                System.exit(1);
	}
	if (!session.openService("//blp/refdata")/**Reference Data or Field data???*/) {
		System.out.println("Could not open service " +
		"//blp/refdata");
                System.exit(1);
	}
        
	CorrelationID requestID = new CorrelationID(10);
	Service refDataSvc = session.getService("//blp/refdata");
	Request request =
		refDataSvc.createRequest("ReferenceDataRequest");
	
        request.append("securities", "US0001M Index");
        request.append("securities", "US0003M Index");
        request.append("securities", "US0006M Index");
        request.append("securities", "US0012M Index");
	request.append("securities", (underlyingFuturesCode + "Comdty")); 
        
	request.append("fields", "PX_LAST"); 
        request.append("fields", "PX_SETTLE");
        request.append("fields", "VOLATILITY_30D");
        
	
	session.sendRequest(request, requestID);
	//System.out.println("CORRELATION ID:  " + requestID);
	boolean continueToLoop = true; 
	while (continueToLoop) {
		Event event = session.nextEvent(); 
		switch (event.eventType().intValue()) {
			case Event.EventType.Constants.RESPONSE: // final event 
                                handleResponseEvent(event);
				//System.out.println("REQUEST ENDED");
                                continueToLoop = false; 	 // fall through
                                break;
			case Event.EventType.Constants.PARTIAL_RESPONSE: 
				handleResponseEvent(event);
				break; 
			default:
				handleOtherEvent(event); 
                                break;
		}
	}
}


private static void handleResponseEvent(Event event) throws Exception { 
    MessageIterator iter = event.messageIterator();
    //System.out.println("Response event being handled...");
    double thirtyDayIntD = 0;
    double ninetyDayIntD = 0;
    double oneEightyDayIntD = 0;
    double threeSixtyDayIntD = 0;
    double last_trade = 0;  
    double volatility = 0;
    double traderVolatility = 0;
    double yest_settle = 0;
    String underlyingFuturesName = "";
    
	while (iter.hasNext()) {
		Message message = iter.next();
		Element ReferenceDataResponse = message.asElement();
		if (ReferenceDataResponse.hasElement("responseError")) {
			//handle error
                        System.out.println("Response Error");
                        System.exit(0);
		}
		Element securityDataArray =
			ReferenceDataResponse.getElement("securityData"); 
		int numItems = securityDataArray.numValues();
		for (int i = 0; i < numItems; ++i) {
				Element securityData = securityDataArray.getValueAsElement(i); 
				String security = securityData.getElementAsString("security"); 
				int sequenceNumber =
					securityData.getElementAsInt32("sequenceNumber");
                                ArrayList<Object> data_list = new ArrayList();
                                java.sql.Timestamp now = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
                                data_list.add(now);
                                data_list.add(underlyingFuturesCode);
			if (securityData.hasElement("securityError")) {
				Element securityError =
				securityData.getElement("securityError");
				//handle error 
				return;
			} else{
                                if(!security.equals(underlyingFuturesCode + "Comdty")){
                                     Element fieldData = securityData.getElement("fieldData");
                                    //System.out.println("\nSECURITY NAME " + security + "\n");
                            
                                
                                switch(security){
                                    case "US0001M Index":
                                        thirtyDayIntD  = fieldData.getElementAsFloat64("PX_LAST");
                                        //System.out.println("30 day interest rate: " + thirtyDayIntD);
                                        data_list.add(thirtyDayIntD);
                                        break;
                                    case "US0003M Index":
                                        ninetyDayIntD  = fieldData.getElementAsFloat64("PX_LAST");
                                        data_list.add(ninetyDayIntD);
                                        break;
                                    case "US0006M Index":
                                        oneEightyDayIntD  = fieldData.getElementAsFloat64("PX_LAST");
                                        data_list.add(oneEightyDayIntD);
                                        break;
                                    case "US0012M Index":
                                        threeSixtyDayIntD  = fieldData.getElementAsFloat64("PX_LAST");
                                        data_list.add(threeSixtyDayIntD);
                                        break;
                                }
				
                                } else {
                                   Element fieldData = securityData.getElement("fieldData"); 
				last_trade = fieldData.getElementAsFloat64("PX_LAST"); 	
                                volatility = fieldData.getElementAsFloat64("VOLATILITY_30D");
                                traderVolatility = fieldData.getElementAsFloat64("VOLATILITY_30D");
                                yest_settle = fieldData.getElementAsFloat64("PX_SETTLE");
                                
				
                                data_list.add(last_trade);
                                data_list.add(traderVolatility);
                                data_list.add(volatility);
                                
                                
                                //ADD INFO TO IN-CLASS VARIABLES
                                
                                switch (underlyingFuturesCode.substring(0,2)){
                                    case "SM":
                                        underlyingFuturesName += "Soybean Meal";
                                        break;
                                    case "IJ":
                                        underlyingFuturesName += "Euronext Rapeseed";
                                        break;
                                    case "CA":
                                        underlyingFuturesName += "Euronext Wheat";
                                        break;
                                }
                                data_list.add(underlyingFuturesName);
                                 
                                data_list.add(thirtyDayIntD);
                                data_list.add(ninetyDayIntD);
                                data_list.add(oneEightyDayIntD);
                                data_list.add(threeSixtyDayIntD);
                                data_list.add(yest_settle);
                                data_list.add(new java.sql.Date(new java.util.Date().getTime() - (24*60*60*1000)));
                                data_list.add(underlyingFuturesCode);
                                dataMap.put(underlyingFuturesCode, data_list);
                                }
                               
			}
		}
	}
        //ADD INFO TO DATABASE
        /**java.sql.Timestamp now = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        PreparedStatement update = connection.prepareStatement("INSERT INTO pulled_data (timestamp,underlying_commodity_name,"
        + "underlying_futures_code,current_underlying_futures_price,daily_trader_volatility,current_implied_volatility,"
        + "interest_30_day,interest_90_day,interest_180_day,interest_360_day)"
        + " VALUES ('" + now + "','" + underlyingFuturesName + "','" + underlyingFuturesCode + "','" + last_trade + "','" + traderVolatility 
        + "','" + volatility + "','" + thirtyDayIntD + "','" + ninetyDayIntD + "','" + oneEightyDayIntD + "','" + threeSixtyDayIntD + "');");	
        update.executeUpdate();*/

}


private static void handleOtherEvent(Event event) throws Exception
{
	//System.out.println("EventType=" + event.eventType()); 
	MessageIterator iter = event.messageIterator();
	while (iter.hasNext()) {
		Message message = iter.next(); 
		//System.out.println("correlationID=" +
		//	message.correlationID()); 
		//System.out.println("messageType=" + message.messageType()); 
			//message.print(System.out);
		if (Event.EventType.Constants.SESSION_STATUS == event.eventType().intValue()
			&& "SessionTerminated".equals(message.messageType().toString())){
		//		System.out.println("Terminating: " +
		//			message.messageType());
			System.exit(1);
			}
		}
	}
}