/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bloombergpull;

import com.bloomberglp.blpapi.*; 
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.sql.Timestamp;


public class RequestView{
    static ArrayList<String> codes_wheat;
    static ArrayList<String> codes_rapeseed;
    static ArrayList<String> codes_soy;
    static RequestResponse req;
    public static void main(String[] args){
       //Create GUI display for text
        JFrame frame = new JFrame();
        JTextField text = new JTextField();
        
        //Initialize frame
        frame.setSize(new Dimension(300,200));
        frame.setTitle("Bloomberg Data Pull");
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Initialize text field
        text.setSize(new Dimension(200,100));
        text.setLocation(50,50);
        
	codes_wheat = new ArrayList();
        codes_wheat.add("CAZ6");
        codes_wheat.add("CAH7");
        for(int i = 1; i <= 25; i++){
            codes_wheat.add("CAH7C " + (130 + 5*i));
        }
        codes_wheat.add("CAK7");
        for(int i = 1; i <= 25; i++){
            codes_wheat.add("CAK7C " + (130 + 5*i));
        }
        codes_wheat.add("CAU7");
        for(int i = 1; i <= 25; i++){
            codes_wheat.add("CAU7C " + (130 + 5*i));
        }
        codes_wheat.add("CAZ7");
        for(int i = 1; i <= 25; i++){
            codes_wheat.add("CAZ7C " + (130 + 5*i));
        }
        codes_wheat.add("CAH8");
        for(int i = 1; i <= 25; i++){
            codes_wheat.add("CAH8C " + (130 + 5*i));
        }
        codes_wheat.add("CAK8");
        for(int i = 1; i <= 25; i++){
            codes_wheat.add("CAK8C " + (130 + 5*i));
        }
        codes_wheat.add("CAU8");
        codes_wheat.add("CAZ8");
        //codes_wheat.add("CAH9");
        //codes_wheat.add("CAK9");
        //codes_wheat.add("CAU9");
        
        codes_rapeseed = new ArrayList();
        codes_rapeseed.add("IJG7");
        codes_rapeseed.add("IJK7");
        for(int i = 1; i <= 30 ; i++){
            codes_rapeseed.add("IJK7C " + (250 + 10*i));
        }
        codes_rapeseed.add("IJQ7");
        for(int i = 1; i <= 30 ; i++){
            codes_rapeseed.add("IJQ7C " + (250 + 10*i));
        }
        codes_rapeseed.add("IJX7");
        for(int i = 1; i <= 30 ; i++){
            codes_rapeseed.add("IJX7C " + (250 + 10*i));
        }
        codes_rapeseed.add("IJG8");
        codes_rapeseed.add("IJK8");
        codes_rapeseed.add("IJQ8");
        codes_rapeseed.add("IJX8");
        codes_rapeseed.add("IJG9");
        
        
        codes_soy = new ArrayList();
        codes_soy.add("SMZ6");
        codes_soy.add("SMF7");
        codes_soy.add("SMH7");
        for(int i = 1; i <= 30; i++){
            codes_soy.add("SMH7C " + (200 + 10*i));
        }
        codes_soy.add("SMK7");
        for(int i = 1; i <= 30; i++){
            codes_soy.add("SMK7C " + (200 + 10*i));
        }
        codes_soy.add("SMN7");
        for(int i = 1; i <= 30; i++){
            codes_soy.add("SMN7C " + (200 + 10*i));
        }
        codes_soy.add("SMQ7");
        for(int i = 1; i <= 30; i++){
            codes_soy.add("SMQ7C " + (200 + 10*i));
        }
        codes_soy.add("SMU7");
        for(int i = 1; i <= 30; i++){
            codes_soy.add("SMU7C " + (200 + 10*i));
        }
        codes_soy.add("SMV7");
        for(int i = 1; i <= 30; i++){
            codes_soy.add("SMV7C " + (200 + 10*i));
        }
        codes_soy.add("SMZ7");
        for(int i = 1; i <= 30; i++){
            codes_soy.add("SMZ7C " + (200 + 10*i));
        }
        codes_soy.add("SMF8");
        for(int i = 1; i <= 30; i++){
            codes_soy.add("SMF8C " + (200 + 10*i));
        }
        
        req = new RequestResponse();
        String s = updateAllData();
        System.out.println(s);
        //String s = "";
        for(String str : codes_wheat){
            s += str + "\n";
        }
        for(String str : codes_soy){
            s += str + "\n";
        }
        for(String str : codes_rapeseed){
            s += str + "\n";
        }
        text.setText(s);
        frame.add(text);
	}
	
	public static String updateAllData(){
        //System.out.println("Updating data... ");
        java.sql.Timestamp now = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        System.out.println(now);
        //System.out.println("Generating futures codes... ");
        for(String s : codes_wheat){
            int compare = compareCodeToTimestamp(s, now);
            if(compare == 1){
                //Adjust array list
                for(int i = 0; i < 25;i++)
                    codes_wheat.remove(1);
                codes_wheat.remove(0);
                String lastCode = codes_wheat.get(codes_wheat.size() - 1);
                String addedCode = "CA";
                String currentYear = lastCode.substring(3,4);
                switch(lastCode.substring(2,3)){
                    case "Z":
                        addedCode = "H";
                        Integer tmp = Integer.parseInt(currentYear) + 1;
                        addedCode += tmp.toString();
                        break;
                    case "H":
                        addedCode = "K" + currentYear;
                        break;
                    case "K":
                        addedCode = "U" + currentYear;
                        break;
                    case "U":
                        addedCode = "Z" + currentYear;
                        break;
                }
                codes_wheat.add(addedCode);
                int last_of_strike_shifts = codes_wheat.size() - 2;
                String last_strike_string = codes_wheat.get(last_of_strike_shifts);
                for(int i = last_of_strike_shifts; i < (last_of_strike_shifts + 25); i++){
                   codes_wheat.add(i,(last_strike_string + "C " + (130 + 5*i)));
                }
                
            }
        }
        
        for(String s : codes_rapeseed){
            int compare = compareCodeToTimestamp(s, now);
            if(compare == 1){
                //Adjust array list
                for(int i = 0; i < 30;i++)
                    codes_rapeseed.remove(1);
                codes_rapeseed.remove(0);
                String lastCode = codes_rapeseed.get(codes_rapeseed.size() - 1);
                String addedCode = "IJ";
                String currentYear = lastCode.substring(3,4);
                switch(lastCode.substring(2,3)){
                    case "X":
                        addedCode = "G";
                        Integer tmp = Integer.parseInt(currentYear) + 1;
                        addedCode += tmp.toString();
                        break;
                    case "G":
                        addedCode = "K" + currentYear;
                        break;
                    case "K":
                        addedCode = "Q" + currentYear;
                        break;
                    case "Q":
                        addedCode = "X" + currentYear;
                        break;
                }
                codes_rapeseed.add(addedCode);
                int last_of_strike_shifts = codes_rapeseed.size() - 5;
                String last_strike_string = codes_rapeseed.get(last_of_strike_shifts);
                for(int i = last_of_strike_shifts; i < (last_of_strike_shifts + 30); i++){
                   codes_rapeseed.add(i,(last_strike_string + "C " + (250 + 10*i)));
                }
            }
        }
        
        for(String s : codes_soy){
            int compare = compareCodeToTimestamp(s, now);
            if(compare == 1){
                //Adjust array list
                for(int i = 0; i < 30;i++)
                    codes_wheat.remove(2);
                codes_soy.remove(0);
                String lastCode = codes_soy.get(codes_soy.size() - 1);
                String addedCode = "SM";
                String currentYear = lastCode.substring(3,4);
                switch(lastCode.substring(2,3)){
                    case "Z":
                        addedCode = "F";
                        Integer tmp = Integer.parseInt(currentYear) + 1;
                        addedCode += tmp.toString();
                        break;
                    case "F":
                        addedCode = "H" + currentYear;
                        break;
                    case "H":
                        addedCode = "K" + currentYear;
                        break;
                    case "K":
                        addedCode = "N" + currentYear;
                        break;
                    case "N":
                        addedCode = "Q" + currentYear;
                        break;
                    case "Q":
                        addedCode = "U" + currentYear;
                        break;
                    case "U":
                        addedCode = "V" + currentYear;
                        break;
                    case "V":
                        addedCode = "Z" + currentYear;
                        break;
                }
                codes_soy.add(addedCode);
                int last_of_strike_shifts = codes_rapeseed.size() - 1;
                String last_strike_string = addedCode;
                for(int i = last_of_strike_shifts; i < (last_of_strike_shifts + 30); i++){
                   codes_rapeseed.add(i,(last_strike_string + "C " + (200 + 10*i)));
                }
            }
        }
        
        //System.out.println("Finished generating futures codes");
	String returnString = "";
        
        //System.out.println("Adding sql codes... ");
        for(String s : codes_wheat){
            returnString += "INSERT INTO pulled_data (timestamp,underlying_futures_code,"
                    + "current_underlying_futures_price,daily_trader_volatility,current_implied_volatility,"
                    + "underlying_commodity_name,interest_30_day,interest_90_day,interest_180_day,interest_360_day) VALUES (";
            String s2 = "";
            String s3 = "";
            ArrayList<Object> reqList = req.getData(s);
            int count = 1;
            for(Object d : reqList){
                if(count <= 10)
                    s2+="'"+d +"',";
                else
                    s3 += "'" + d + "',";
                count++;
            }
            s2=s2.substring(0,s2.length()-1);
            s3=s3.substring(0,s3.length()-1);
            returnString += s2 + ");INSERT INTO market_close_data(settlement_price,close_date,underlying_futures_code) VALUES (" + s3 + ");";
        }
        for(String s : codes_rapeseed){
            returnString += "INSERT INTO pulled_data (timestamp,underlying_futures_code,"
                    + "current_underlying_futures_price,daily_trader_volatility,current_implied_volatility,"
                    + "underlying_commodity_name,interest_30_day,interest_90_day,interest_180_day,interest_360_day) VALUES (";
            String s2 = "";
            String s3 = "";
            ArrayList<Object> reqList = req.getData(s);
            int count = 1;
            for(Object d : reqList){
                if(count <= 10)
                    s2+="'"+d +"',";
                else
                    s3 += "'" + d + "',";
                count++;
            }
            s2=s2.substring(0,s2.length()-1);
            s3=s3.substring(0,s3.length()-1);
            
            returnString += s2 + ");INSERT INTO market_close_data(settlement_price,close_date,underlying_futures_code) VALUES (" + s3 + ");";
        }
        
        for(String s : codes_soy){
            returnString += "INSERT INTO pulled_data (timestamp,underlying_futures_code,"
                    + "current_underlying_futures_price,daily_trader_volatility,current_implied_volatility,"
                    + "underlying_commodity_name,interest_30_day,interest_90_day,interest_180_day,interest_360_day) VALUES (";
            String s2 = "";
            String s3 = "";
            ArrayList<Object> reqList = req.getData(s);
            int count = 1;
            for(Object d : reqList){
                if(count <= 10)
                    s2+="'"+d +"',";
                else
                    s3 += "'" + d + "',";
                count++;
            }
            s2=s2.substring(0,s2.length()-1);
            s3=s3.substring(0,s3.length()-1);
            returnString += s2 + ");INSERT INTO market_close_data(settlement_price,close_date,underlying_futures_code) VALUES (" + s3 + ");";
        }
		
	return returnString;
    }
        
        public static int compareCodeToTimestamp(String code, Timestamp current){
       
        String returnString = "20";
        int codeInt = Integer.parseInt(code.substring(3,4));
        int yearInt = Integer.parseInt(current.toString().substring(3,4));
        Integer totalYear = Integer.parseInt(current.toString().substring(0,4));
        if(codeInt >= yearInt){
            totalYear += (codeInt - yearInt);
        } else {
            totalYear += ((codeInt + 10) - yearInt);
        }
        
        returnString += totalYear.toString() + "-";
         if(code.substring(0,2) == "CA"){
            if(code.substring(2,3).equals("Z")){
                returnString += "10";
            } else if(code.substring(2,3).equals("H")){
                returnString += "01";
            } else if(code.substring(2,3).equals("K")){
                returnString += "03";
            } else if(code.substring(2,3).equals("U")){
                returnString += "07";
            }
         } else if (code.substring(0,2) == "IJ"){
             if(code.substring(2,3).equals("G")){
                returnString += "12";
            } else if(code.substring(2,3).equals("K")){
                returnString += "03";
            } else if(code.substring(2,3).equals("Q")){
                returnString += "06";
            } else if(code.substring(2,3).equals("X")){
                returnString += "09";
            }
         } else if (code.substring(0,2) == "SM"){
             //Be sure to account for possibility of using the third friday of the month(meaning months will be -1 and days will be accounted for
             if(code.substring(2,3).equals("Z")){
                returnString += "11";
            } else if(code.substring(2,3).equals("F")){
                returnString += "12";
            } else if(code.substring(2,3).equals("H")){
                returnString += "02";
            } else if(code.substring(2,3).equals("K")){
                returnString += "04";
            } else if(code.substring(2,3).equals("N")){
                returnString += "06";
            } else if(code.substring(2,3).equals("Q")){
                returnString += "07";
            } else if(code.substring(2,3).equals("U")){
                returnString += "08";
            } else if(code.substring(2,3).equals("V")){
                returnString += "09";
            }
         }
        
        returnString += code.substring(3,4);
        
        int thisYear=totalYear;
        int thisMonth=Integer.parseInt(current.toString().substring(5,7));
        int codeYear=Integer.parseInt(returnString.substring(0,5));
        int codeMonth=Integer.parseInt(returnString.substring(6,8));
        int val = 0;
        if(thisYear > codeYear){
            //Today is after code expiry
            return 1;
        } else if (thisYear < codeYear){
            //Today is before code expiry
            return -1;
        } else {
            if(thisMonth > codeMonth){
                //Today is after code expiry
                return 1;
            } else if (thisMonth <= codeMonth){
                //Today is before code expiry
                return -1;
            }
        }
        return val;
    }
}



